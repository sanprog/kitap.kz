@php

    $commentVisit=[];
    $n = 1;
@endphp
<div class="comments">
    <div class="comment-title">
    <span class="comment-title__text">
      Пікірлер
      <span class="comment-title-count">{{ count($comments) }}</span>
    </span>
    </div>
    <form  action="{{ route('comment.book') }}" method="POST" class="comment-add" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="parent" value="0">
        <input type="hidden" name="id" value="{{ $config['id'] }}">
        <input type="hidden" name="type" value="{{ $config['type'] }}">
        <div class="comment-add__avatar">
            <div class="comment-add__avatar-img" style="background-image: url({{ $user['avatar'] ? $user['avatar']: '../images/setting.png' }} "></div>
            <span class="comment-add__avatar-name">{{ $user['name']}}</span>
        </div>
        <div class="comment-add__field">
            <textarea name="comment" id="" class="comment-add__field-textarea" placeholder="Добавьте комментарий..."></textarea>
            <div class="comment-add__btn">
                <button type="submit" class="comment-btn__send __single">Отправить</button>
            </div>
        </div>
    </form>


@php
    foreach ($comments as $comment) {
        if(!isset($commentVisit[$comment->id])){
            $stack = [$comment];

            while(count($stack)>0){
                $v = $stack[count($stack) - 1];
                array_pop($stack);

                $commentVisit[$v->id] = 1;

                $n++;
@endphp
                @include('comment._form',[
                    'comment' => $v,
                    'n' => $n
                ])
@php
                if(isset($children[$v->id])){
                    foreach ($children[$v->id] as $child) {
                         if(!isset($commentVisit[$child->id])){
                                $stack[] = $child;
                         }
                    }

                }
                if($v->parent_id>0){
@endphp
</div>
</div>
</div>
@php
    }

}
                        @endphp
                        </div>
</div>
@php
        }

    }
@endphp
{{--<span class="comment-more">Загрузить еще</span>--}}
</div>
<!--//Content-->

