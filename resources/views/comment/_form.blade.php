



@if($comment->parent_id > 0)
<div class="comment-inner" id="comment-inner-{{ $n }}">
@endif
    <div class="comment-item" id="comment-item-{{ $n }}">
        <div class="coment-avatar">
            <div class="coment-avatar__img" style="background-image: url({{ $comment->avatar ? $comment->avatar: '../images/setting.png' }})"></div>
        </div>
        <div class="comment-field">
            <div class="comment-top">
                <span class="coment-name">{{ $comment->name }}</span>
                <span class="comment-date">{{ date("H:i d.m.y",strtotime($comment->updated_at)) }}</span>
            </div>
            <div class="comment-text">
                {{ $comment->comment }}
            </div>
            <div class="comment-options">
                <div class="comment-options__actions">
                    <div class="comment-actions__item">
                        <span class="comment-actions__item-ico fa fa-thumbs-up" hidden></span>
                        <span class="comment-actions__item-text" data-toggle="comment-like-{{ $comment->id }}">Нравится(<span id="like_count-{{ $comment->id }}">{{ $comment->likes_count }}</span>)</span>
                    </div>
                    <div class="comment-actions__item">
                        <span class="comment-actions__item-ico fa fa-mail-reply-all"></span>
                        <span class="comment-actions__item-text" data-toggle="comment-add-{{ $n }}">Ответить</span>
                    </div>
                </div>
            </div>
            <div class="comment-inner" id="comment-add-{{ $n }}" style="display: none">
                <form  action="{{ route('comment.book') }}" method="POST" class="comment-add __inner">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="parent" value="{{ $comment->id }}">
                    <input type="hidden" name="id" value="{{ $config['id'] }}">
                    <input type="hidden" name="type" value="{{ $config['type'] }}">
                    <div class="comment-add__avatar">
                        <div class="comment-add__avatar-img" style="background-image: url({{ $user['avatar'] ? $user['avatar']: '../images/setting.png' }} "></div>
                        <span class="comment-add__avatar-name">{{ $user['name']}}</span>
                    </div>
                    <div class="comment-add__field">
                        <textarea name="comment" id="" class="comment-add__field-textarea" placeholder="Добавьте комментарий..."></textarea>
                        <div class="comment-add__btn">
                            <button type="submit" class="comment-btn__send __single">Отправить</button>
                        </div>
                    </div>
                </form>
            </div><!-- inner -->

<!-- comment -->
