@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('book.audiobook') }}</title>
@endsection

@section('content')

    <section class="filter">
        @widget('genreWidget')
        <div class="filter-bg">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('book.audiobook') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('audiobooks') !!}
    <div class="catalogmobile">
        <div class="catalogmobile-top">
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-genres">{{ trans('book.genre') }}</span>
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-catalog">{{ trans('book.category') }}</span>
        </div>
        @widget('genreWidget',['type' => 2])

        @widget('categoryWidget',['type' => 2])
    </div>
    <div class="main-row">
        <div class="main-col-left">
            <!-- studies -->
            <section class="studies">
                <h3 class="studies-title">
                    <span class="studies-title__ico fa fa-check"></span>{{ trans('book.read_book') }}
                </h3>
                <div class="studies-row">
                    @foreach($popularAudiobooks as $item)
                    <div class="studies-item __hover-item">
                      <div class="studies-item__img __relative" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')">
                        <div class="hover-img__text">
                          <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                          </a>
                        </div>  
                      </div>
                        <span class="studies-item__title">{{ $item->book->name }}</span>
                        <span class="studies-item__text">
                        @foreach($item->book->authors as $author)
                            {{ $author->name }}
                        @endforeach
                        </span>
                    </div>
                    @endforeach
                </div>
                <div class="studies-row __mobile js-new-slider owl-carousel owl-theme">
                    @foreach($popularAudiobooks as $item)
                    <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="item">
                        <div class="new-item">
                            <div class="studies-item__img" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')"></div>
                            <span class="new-item__title">{{ $item->book->name }}</span>
                            <span class="new-item__text"></span>
                        </div>
                    </a>
                    @endforeach
                </div>
                {{--banners--}}

                @if($banner)
                <a class="studie-book _liter" href="{{ $banner->uri }}" style="background-image: url('/storage/{{ $banner->img_path }}')">
                    <span class="actual-item__desc">{{ $banner->title }}</span>
                </a>
                @endif
            </section>
            <!-- endstudies -->

            @widget('libraryWidget')

            @widget('offersWidget')


            <section class="new">
                <h3 class="new-title">
                    <span class="new-title__ico fa fa-thumb-tack"></span>{{ trans('book.new_book') }}
                </h3>
                <div class="new-row">
                    @foreach($newestAudiobooks as $item)
                        <div class="studies-item __hover-item">
                            <div class="studies-item__img __relative" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')">
                                <div class="hover-img__text">
                                    <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                    </a>
                                </div>
                            </div>
                            <span class="studies-item__title">{{ $item->book->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->book->authors as $author)
                                        {{ $author->name }}
                                @endforeach
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="new-row __mobile js-new-slider owl-carousel owl-theme">
                    @foreach($newestAudiobooks as $item)
                    <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="item">
                        <div class="new-item">
                            <div class="studies-item__img" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')"></div>
                            <span class="new-item__title">{{ $item->book->name }}</span>
                            <span class="new-item__text"></span>
                        </div>
                    </a>
                    @endforeach
                </div>
            </section>

            <section class="speaker">
                <h3 class="speaker-title">
                    <span class="speaker-title__ico fa fa-volume-down"></span>{{ trans('book.dictor_tandau') }}
                </h3>
                <div class="speaker-block">
                    <div class="speaker-slider">
                        <div class="speaker-carousel owl-carousel owl-theme">
                            @foreach($dictors as $item)
                            <div class="item">
                                <div class="speaker-inner">
                                    <div class="speaker-top">
                                        <div class="speaker-user">
                                            <img src="{{ $item->img_src or '../images/speaker.png' }}" alt="" class="speaker-user__img" style="width:auto;">
                                            <div class="speaker-user__text">
                                                <span class="speaker-user__text-name">{{ $item->dictor_name }}</span>
                                            </div>
                                        </div>
                                        <div class="speaker-action">
                                            <!-- <a href="" class="speaker-top__btn">
                                              <span class="speaker-top__btn-text">Таңдау</span>
                                              <span class="speaker-top__btn-ico">
                                                <span class="speaker-top__ico-img fa fa-check"></span>
                                              </span>
                                            </a> -->
                                            <a href="{{ route('dictor.show', ['dictor' => $item->id]) }}" class="speaker-top__btn __blue">
                                                <span class="speaker-top__btn-text">{{ trans('book.dictor_about') }}</span>
                                                <span class="speaker-top__btn-ico">
                                                    <span class="speaker-top__ico-img fa fa-address-book-o"></span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="speaker-row">
                                        @for ($i = 0; $i < count($item->audiobooks); $i++)
                                        @break($i > 1)
                                        <div class="speaker-item">
                                            <div class="speaker-book">
                                                <img src="{{ $item->audiobooks[$i]->book->img_path or '../images/book.png' }}" alt="" class="speaker-book__img">
                                                <div class="speaker-book__text">
                                                    <a href="{{ route('audiobooks.show', ['slug' => $item->audiobooks[$i]->book->slug]) }}" class="speaker-book__text-link"></a>
                                                    <span class="speaker-book__text-btn">{{ trans('button.otu') }}</span>
                                                </div>
                                            </div>
                                            <div class="speaker-info">
                                                    @foreach($item->audiobooks[$i]->book->authors as $author)
                                                        <a href="{{ route('author.show', ['author' => $author->id]) }}">
                                                            <span class="speaker-info__author">
                                                                {{ $author->name }}
                                                            </span>
                                                        </a>
                                                    @endforeach
                                                <span class="speaker-info__name">
                                                    {{ $item->audiobooks[$i]->book->name }}
                                                </span>
                                                <div class="speaker-info__rating">
                                                    <span class="speaker-ingo__rating-text">{{ trans('book.how_much') }}</span>
                                                    <div class="speaker-rating">
                                                        <span class="speaker-rating__text">67%</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="main-col-right">

            @widget('categoryWidget',['type' => 1])

            {{--@widget('reviewWidget')--}}

            @widget('statisticWidget')

            @widget('choiceWidget')

        </div>
    </div>
@endsection
