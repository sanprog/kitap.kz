@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('book.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('audiobooks.show', $model) !!}

<section class="bookitem">
    <div class="audioitem-top">
        {{--<div class="bookitem-social">
            <a href="#" class="ico icons-vkgray bookitem-social__ico"></a>
            <a href="#" class="ico icons-fbgray bookitem-social__ico"></a>
            <a href="#" class="ico icons-twgray bookitem-social__ico"></a>
            <a href="#" class="ico icons-gplusgray bookitem-social__ico"></a>
        </div>--}}
        <div class="bookitem-boook">
            <img src="{{ $model->img_path or '/images/book.png' }}" alt="" class="bookitem-book__img">
            <span class="booitem-book__rating-text">{{ trans('book.rating') }} {{ round($model->audiobook->averageRating, 1, PHP_ROUND_HALF_UP) }}/5</span>
            <div class="bookitem-book__rating">
                @for ($i = 5; $i >= 1; $i--)
                    <span data-rank="{{ $i }}" class="setRank bookitem-book__rating-ico fa fa-star {{ ($model->audiobook->averageRating >= $i) ? '_active' : '' }}" aria-hidden="true"></span>
                @endfor
            </div>
            <div id="altRank" style="font-size: 10px; text-align: center; height: 10px;"></div>
            <form id="formRank" action="{{ route('audiobook.set.rank', ['slug' => $model->slug]) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" id="inputRate" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="" data-size="xs">
                <input type="hidden" name="id" required="" value="{{ $model->audiobook->id }}">
            </form>
        </div>
        <div class="bookitem-info">
            @if(!$model->authors->isEmpty())
                <div class="bookitem-info-header">
                    <span class="bookitem-info-header__desc"><span class="fa fa-pencil"></span> {{ trans('book.author') }}</span>
                    @foreach($model->authors as $author)
                    <a href="{{ route('author.show', ['author' => $author->id]) }}" class="bookitem-info__author">{{ $author->name }}</a>
                    @endforeach
                </div>
            @endif
            @if($model->audiobook->dictor)
            <div class="bookitem-info-header">
                <span class="bookitem-info-header__desc"><span class="fa fa-microphone"></span>{{ trans('book.dictor') }}</span>
                <a href="{{ route('dictor.show', ['dictor' => $model->audiobook->dictor->id]) }}" class="bookitem-info__author">{{ $model->audiobook->dictor->dictor_name }}</a>
            </div>
            @endif
            <h3 class="bookitem-info__name">{{ $model->name }}</h3>
            <div class="bookitem-info__desc">{!! $model->description !!}</div>
            <div class="bookitem-item__options">
                {{--<button class="bookitem-options__btn __pink">
                    <span class="bookitem-options__btn-text">Сатып алу</span>
                    <span class="bookitem-options__btn-ico">
                        <span class="bookitem-options-ico__img fa fa-shopping-cart" aria-hidden="true"></span>
                    </span>
                </button>--}}
            </div>
            <div class="bookitem-item__options">
                {{--<button class="bookitem-options__btn">
                    <span class="bookitem-options__btn-text">Аудио тыңдау</span>
                    <span class="bookitem-options__btn-ico">
                        <span class="bookitem-options-ico__img fa fa-headphones" aria-hidden="true"></span>
                    </span>
                </button>--}}
                @if (!Auth::guest())
                    @if(!$library_books)
                    <a href="{{ route('book.savetolib', ['id' => $model->audiobook->id, 'type' => \App\Model\Library::TYPE_AUDIOBOOK]) }}" class="bookitem-options__btn __blue">
                        <span class="bookitem-options__btn-text">{{ trans('button.add') }}</span>
                        <span class="bookitem-options__btn-ico">
                            <span class="bookitem-options-ico__img fa fa-plus"></span>
                        </span>
                    </a>
                    @else
                        <a href="{{ route('profile.deleteitem', ['type' => $library_books->resource_type, 'id' => $library_books->resource_id]) }}" class="bookitem-options__btn __blue">
                            <span class="bookitem-options__btn-text">{{ trans('button.sub') }}</span>
                            <span class="bookitem-options__btn-ico">
                                <span class="bookitem-options-ico__img fa fa-minus"></span>
                            </span>
                        </a>
                    @endif
                @endif
                <a href="{{ route('books.show', ['slug' => $model->slug ]) }}" class="bookitem-options__btn __blue">
                    <span class="bookitem-options__btn-text">{{ trans('button.read') }}</span>
                    <span class="bookitem-options__btn-ico">
                        <span class="bookitem-options-ico__img fa fa-book" aria-hidden="true"></span>
                    </span>
                </a>
            </div>
            <div class="socila-info">
                <div class="social-info__item">
                    <span class="social-info__item-ico fa fa-heart" aria-hidden="true"></span>
                    <span class="social-info__item-text">20</span>
                </div>
                <div class="social-info__item">
                    <span class="social-info__item-ico fa fa-plus-square" aria-hidden="true"></span>
                    <span class="social-info__item-text">20</span>
                </div>
                <div class="social-info__item">
                    <span class="social-info__item-ico fa fa-commenting" aria-hidden="true"></span>
                    <span class="social-info__item-text">20</span>
                </div>
            </div>

        </div>
    </div>
    <div class="bookitem-tags">
        <div class="bookitem-tags-container">
            @foreach($model->genre as $genre)
            <a  href="#" class="bookitem-tags__item">
                {{ $genre->name }}
            </a>
            @endforeach
        </div>
    </div>
    <div class="audiobook _inner">
        <div class="poet-audio">
            <audio id="Poet"></audio>
            <div class="Player audio-player" data-player-name="Poet">
                <div class="player-block">
                    <div class="player-block__control-wrapper">
                        <div class="player__buttons-panel player__buttons-control-panel">
                            <div class="player-block__prev" data-player-button="prev"></div>
                            <div class="player-block__play" data-player-button="play"></div>
                            <div class="player-block__next" data-player-button="next"></div>
                        </div>
                    </div>
                    <div class="player-block__time-wrapper">
                        <div class="player-block__time-current" data-player-time="current">00:00</div>
                        <div class="player-block__tracker" data-player-scale="time">
                            <div class="time-scale__buffered" data-player-scale="buffered"></div>
                            <div class="time-scale__progress" data-player-scale="progress"></div>
                        </div>
                        <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                    </div>
                    <div class="player__volume-panel">
                        <div class="player__volume" data-player-button="volume"></div>
                        <div class="player__volume-scale-wrapper">
                            <div class="player__volume-scale" data-player-scale="volume">
                                <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="player-block__playlist" data-player-playlist="Poet">
                    @foreach($model->audiobook->audiofile as $key => $item)
                        <li data-player-src="/storage/{{ $item->file_path }}">
                            <div class="player-block__playlist__text">
                                <span class="player-block__playlist__number">{{ ++$key }}.</span>
                                <span class="player-block__playlist__author">{{--Б. Соқпақбаев--}}</span>
                                <span class="player-block__playlist__name">{{ $item->name }}</span>
                            </div>
                            <span class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</section>
    @widget('commentWidget',['id' => $model->id, 'type' => 'book'])
    <script>
		var playlist = {
            @foreach($model->audiobook->audiofile as $key => $item)
            {{ $key++ }}: {
				name: '{{ $item->name }}',
				band: '',
				cover: '/images/poet1.png',
				src: '/storage/{{ $item->file_path }}'
			},
            @endforeach
		};

    </script>
@endsection

@push('scripts')
<script>
	$(function() {
		$('.bookitem-book__rating').on('click', '.setRank', function() {
			$('#inputRate').val($(this).data('rank'));
			$('#formRank').submit();
		});
		/*$( '.setRank' )
			.mouseenter( function() {
				$('#altRank').html('Проголосовать ' + $(this).data('rank'));

			} )
			.mouseleave( function() {
				$('#altRank').html('');
			} );*/
	});
</script>
@endpush
