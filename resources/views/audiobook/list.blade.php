@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('book.audiobook') }}</title>
@endsection

@section('content')

    <section class="filter">
        @widget('genreWidget')
        <div class="filter-bg">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('book.audiobook') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('audiobooks') !!}
    <div class="catalogmobile">
        <div class="catalogmobile-top">
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-genres">{{ trans('book.genre') }}</span>
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-catalog">{{ trans('book.category') }}</span>
        </div>
        @widget('genreWidget',['type' => 2])

        @widget('categoryWidget',['type' => 2])
    </div>
    <div class="main-row">
        <div class="main-col-left">
            <!-- studies -->
            <section class="studies">
                <h3 class="studies-title">
                    <span class="studies-title__ico fa fa-check" aria-hidden="true"></span>{{ trans('book.audiobook') }}
                </h3>
                <div class="studies-row">
                    @foreach($model as $item)
                    <div class="studies-item __hover-item">
                      <div class="studies-item__img __relative" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')">
                        <div class="hover-img__text">
                          <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                          </a>
                        </div>  
                      </div>
                        <span class="studies-item__title">{{ $item->book->name }}</span>
                        <span class="studies-item__text">{{ $item->dictor->dictor_name }}</span>
                    </div>
                    @endforeach
                </div>
                <div class="studies-row __mobile">
                    @foreach($model as $item)
                    <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="item">
                        <div class="new-item" style="margin-top:1rem">
                            <div class="studies-item__img" style="background-image:url('{{ $item->book->img_path or '/images/book.png' }}')"></div>
                            <span class="new-item__title">{{ $item->book->name }}</span>
                            <span class="new-item__text"></span>
                        </div>
                    </a>
                    @endforeach
                </div>
                {{ $model->links('pagination.default') }}
            </section>
            <!-- endstudies -->
        </div>
        <div class="main-col-right">

            @widget('categoryWidget')

            {{--@widget('reviewWidget')--}}

            @widget('statisticWidget')

            @widget('choiceWidget')

        </div>
    </div>
@endsection
