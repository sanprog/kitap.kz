@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('video') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('search.video') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('video.show', $model) !!}

    <section class="ted">
        <div class="ted-container">
            {{--<a href="#" class="ted-logo">
                <img src="/images/ted.png" alt="" class="ted-logo__img">
            </a>--}}
            <h3 class="ted-title">{{ $model->name }}</h3>
            <div class="ted-video">
                <figure id="videoContainer" class="ted-video__inner">
                    <video id="video" controls preload="metadata" poster="images/video3.png" class="ted-video__item">
                        <source src="/storage/{{$model->file_path}}" type="video/mp4">
                        <source src="video/video.webm" type="video/webm">
                        <source src="video/video.ogg" type="video/ogg">
                    </video>
                </figure>
            </div>
            <div class="ted-controls">
                <div class="ted-controls__col">
                    @if (!Auth::guest())
                        @if(!$library_videos)
                            <a href="{{ route('book.savetolib', ['id' => $model->id, 'type' => \App\Model\Library::TYPE_VIDEO]) }}" class="bookitem-options__btn __blue">
                                <span class="bookitem-options__btn-text">{{ trans('button.add') }}</span>
                                <span class="bookitem-options__btn-ico">
                                <span class="bookitem-options-ico__img fa fa-plus"></span>
                            </span>
                            </a>
                        @else
                            <a href="{{ route('profile.deleteitem', ['type' => $library_videos->resource_type, 'id' => $library_videos->resource_id]) }}" class="bookitem-options__btn __blue">
                                <span class="bookitem-options__btn-text">{{ trans('button.sub') }}</span>
                                <span class="bookitem-options__btn-ico">
                                    <span class="bookitem-options-ico__img fa fa-minus"></span>
                                </span>
                            </a>
                        @endif
                    @endif
                    {{--<div class="ted-social">
                        <a href="#" class="ted-ico"><span class="ted-ico__img fa fa-vk"></span></a>
                        <a href="#" class="ted-ico"><span class="ted-ico__img fa fa-facebook"></span></a>
                        <a href="#" class="ted-ico"><span class="ted-ico__img fa fa-google-plus _small"></span></a>
                        <a href="#" class="ted-ico"><span class="ted-ico__img fa fa-twitter"></span></a>
                    </div>--}}
                </div>
                {{--<div class="ted-controls__col">
                    <div class="ted-actions">
                      <span class="ted-actions__item">
                        <span class="fa fa-heart"></span>
                        20
                      </span>
                        <span class="ted-actions__item">
                            <span class="fa fa-plus-square"></span>
                            20
                          </span>
                        <span class="ted-actions__item">
                            <span class="fa fa-comment"></span>
                            20
                          </span>
                    </div>
                </div>--}}
            </div>

            <div class="ted-desc _active">
                {!! $model->description !!}
            </div>
            <span class="ted-desc__btn js-ted-desc" style="display: none;">
                <span class="fa fa-angle-double-down ted-desc__btn-ico" aria-hidden="true"></span>
            </span>
        </div>
    </section>

@endsection
