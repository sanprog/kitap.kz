@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('search.video') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('search.video') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('video') !!}
    @foreach($categories as $category)
        @if($category->videos->isEmpty()) @continue @endif
        <section class="video">
            <div class="container">
                <h3 class="video-title">
                    <div class="video-title__name">
                        <span class="video-title__ico fa fa-film"></span>{{ $category->name }}
                    </div>
                    <a href="{{ route('video.list.category', ['category' => $category->id]) }}" class="video-title__btn">
                        {{ trans('other.allvideo') }}
                    </a>
                </h3>
                <div class="video-row">
                    @foreach($category->videos as $item)
                        @if($loop->index >= 4 ) @continue @endif
                    <a href="{{  route('video.show', ['slug' => $item->slug]) }}" class="video-item">
                        <div class="video-item__top {{--_active--}}" style="background-image: url('/storage/{{ $item->img_path or '../images/video_bg.png' }}');">
                            <div class="video-item__top-action">
                                <span class="video-item-action__ico"></span>
                            </div>
                        </div>
                        <span class="video-item__title">{{ $item->name }}</span>
                        {{--<div class="video-item__rating">
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-heart" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-comment" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                        </div>--}}
                    </a>
                    @endforeach
                </div>
            </div>
        </section>
    @endforeach
    {{--<section class="videobanner">
        <div class="container videobanner-container">
            <div class="videobanner-info">
                <h3 class="videobanner-info__title">What is Lorem Ipsum?</h3>
                <p class="videobanner-info__text">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
        </div>
    </section>--}}
    {{--<section class="video">
        <div class="container">
            <h3 class="video-title _ted">
                {{ $categories[1]->name }}
            </h3>
            <div class="video-row">
                @foreach($categories[1]->videos as $item)
                    <a href="{{  route('video.show', ['slug' => $item->slug]) }}" class="video-item">
                        <div class="video-item__top --}}{{--_active--}}{{--" style="background-image: url('/storage/{{ $item->img_path or '../images/video_bg.png' }}');">
                            <div class="video-item__top-action">
                                <span class="video-item-action__ico"></span>
                            </div>
                        </div>
                        <span class="video-item__title">{{ $item->name }}</span>
                        --}}{{--<div class="video-item__rating">
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-heart" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-comment" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                        </div>--}}{{--
                    </a>
                @endforeach
            </div>
        </div>
    </section>--}}
    {{--<section class="video">
        <div class="container">
            <h3 class="video-title">
                <span class="video-title__ico fa fa-university" aria-hidden="true"></span>{{ $categories[2]->name }}
            </h3>
            <div class="video-row">
                @foreach($categories[2]->videos as $item)
                    <a href="{{  route('video.show', ['slug' => $item->slug]) }}" class="video-item">
                        <div class="video-item__top --}}{{--_active--}}{{--" style="background-image: url('/storage/{{ $item->img_path or '../images/video_bg.png' }}');">
                            <div class="video-item__top-action">
                                <span class="video-item-action__ico"></span>
                            </div>
                        </div>
                        <span class="video-item__title">{{ $item->name }}</span>
                        --}}{{--<div class="video-item__rating">
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-heart" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                            <div class="video-rating__block">
                                <span class="video-rating__ico fa fa-comment" aria-hidden="true"></span>
                                <span class="video-rating__text">20</span>
                            </div>
                        </div>--}}{{--
                    </a>
                @endforeach
            </div>
        </div>
    </section>--}}




@endsection
