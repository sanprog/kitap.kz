@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('search.video') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('video') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('search.video') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('video.list', $categoryName) !!}

    <section class="video">
        <div class="container">
            <h3 class="video-title">
                <div class="video-title__name">
                    <span class="video-title__ico fa fa-film"></span>{{ $categoryName }}
                </div>
            </h3>
            <div class="video-row">
                @foreach($model as $item)
                <a href="{{ route('video.show', ['slug' => $item->slug]) }}" class="video-item">
                    <div class="video-item__top" style="background-image: url('/storage/{{ $item->img_path or '../images/video_bg.png' }}');">
                        <div class="video-item__top-action"></div>
                    </div>
                    <span class="video-item__title">{{ $item->name }}</span>
                    {{--<div class="video-item__rating">
                        <div class="video-rating__block">
                            <span class="video-rating__ico fa fa-heart"></span>
                            <span class="video-rating__text">20</span>
                        </div>
                        <div class="video-rating__block">
                            <span class="video-rating__ico fa fa-plus-square"></span>
                            <span class="video-rating__text">20</span>
                        </div>
                        <div class="video-rating__block">
                            <span class="video-rating__ico fa fa-comment"></span>
                            <span class="video-rating__text">20</span>
                        </div>
                    </div>--}}
                </a>
               @endforeach
            </div>
            {{ $model->links('pagination.default') }}
        </div>
    </section>



@endsection
