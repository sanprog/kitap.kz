@extends('layouts.app')

@section('content')
<div class="container">
    <div class="new-password" style="display: block;">
        <form class="new-password-from" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <h3 class="new-password-title">{{ trans('passwords.title') }}</h3>
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="modal-row">
                <input type="password" id="register_password" class="modal-input" placeholder="Құпиясөз" name="password">
                <span class="fa fa-lock modal-ico"></span>
            </div>
            <div class="modal-row">
                <input type="password" class="modal-input" placeholder="Құпиясөзді растау" name="password_confirmation">
                <span class="fa fa-lock modal-ico"></span>
            </div>
            <button type="submit" class="moda-btn __green">{{ trans('button.forgot') }}</button>
            {{--<button class="moda-btn __pink __single">Вход с телефона</button>--}}

        </form>
    </div>
</div>
@endsection
