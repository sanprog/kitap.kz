
<!doctype html>
<html>
<head>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/reader/js/turn.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        body {
            background-color: #fffaec;
        }


        /* helpers */

        .t {
            display: table;
            width: 100%;
            height: 90%;
        }

        .tc {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }

        .rel {
            position: relative;
        }

        /* book */

        .book {
            margin: 0 auto;
            width: 90%;
            height: 90%;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .book .page {
            height: 100%;
        }

        .book .page img {
            max-width: 100%;
            height: 100%;
        }

        #controls{
            width:100%;
            text-align:center;
            margin:20px 0px;
            font:20px arial;
        }

        #controls input, #controls label{
            font:20px arial;
        }
    </style>
</head>
<body>

<div class="t">
    <div class="tc rel">
        <div class="book" id="book">
        </div>
    </div>
</div>


<div id="controls">
    <label for="page-number"></label> <input type="text" size="3" id="page-number"> / <span id="number-pages"></span>
</div>


<script type="text/javascript">

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	(function () {
		'use strict';

		var module = {
			ratio: 1.302,
			init: function (id) {
				var me = this;

				// if older browser then don't run javascript
				if (document.addEventListener) {
					this.el = document.getElementById(id);
					this.resize();
					this.plugins();

					// on window resize, update the plugin size
					window.addEventListener('resize', function (e) {
						var size = me.resize();
						$(me.el).turn('size', size.width, size.height);
					});
				}
			},
			resize: function () {
				// reset the width and height to the css defaults
				this.el.style.width = '';
				this.el.style.height = '';

				var width = this.el.clientWidth,
					height = Math.round(width / this.ratio),
					padded = Math.round(document.body.clientHeight * 0.9);

				// if the height is too big for the window, constrain it
				if (height > padded) {
					height = padded;
					width = Math.round(height * this.ratio);
				}

				// set the width and height matching the aspect ratio
				this.el.style.width = width + 'px';
				this.el.style.height = height + 'px';

				return {
					width: width,
					height: height
				};
			},
			plugins: function () {
				// run the plugin
				var me = this;
				$(this.el).turn({
					pages: {{ $countPages }},
					gradients: false,
					acceleration: true,
					when: {
						turning: function(e, page, view) {

							// Gets the range of pages that the book needs right now
							var range = $(this).turn('range', page);

							// Check if each page is within the book
							for (page = range[0]; page<=range[1]; page++)
								me.addPage(page, $(this));

						},

						turned: function(e, page) {
							$('#page-number').val(page);
						}
					}
				});
				// hide the body overflow
				document.body.className = 'hide-overflow';
			},
			// Adds the pages that the book will need
			addPage: function(page, book) {
                // 	First check if the page is already in the book
                if (!book.turn('hasPage', page)) {
                    // Create an element for this page
                    var element = $('<div />', {'class': 'page '+((page%2==0) ? 'odd' : 'even'), 'id': 'page-'+page}).html('<i class="loader"></i>');
                    // If not then add the page
                    book.turn('addPage', element, page);
                    // Let's assum that the data is comming from the server and the request takes 1s.
                    $.ajax({
                        type: "POST",
                        url: "{{ route('book.readspec.page', ['slug' => $book->slug]) }}",
                        data:{'page':page},
                        beforeSend: function(){},
                        success: function(response){
							element.html(response);
                        },
						error: function(response) {}
                    });
                }
            }
		};

		module.init('book');
	}());

	$('#number-pages').html({{ $countPages }});

	$(window).ready(function() {
		$('#page-number').keydown(function(e) {

			if (e.keyCode == 13)
				$('#book').turn('page', $('#page-number').val());

		});
	});

</script>
</body>
</html>
