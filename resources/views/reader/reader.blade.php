<!DOCTYPE html>
<html class="no-js" xmlns="w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ $book->name }}" />
    <meta property="og:description"   content="{{ $book->description }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ $book->img_path or '/images/book.png' }}" />

    <title>{{ $book->name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="/reader/css/normalize.css">
    <link rel="stylesheet" href="/reader/css/main.css">
    <link rel="stylesheet" href="/reader/css/popup.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="/reader/js/libs/zip.min.js"></script>
    <script src="/reader/js/init.js"></script>
    <script src="/reader/js/libs/screenfull.min.js"></script>
    <script src="/reader/js/epub.min.js"></script>
    <script src="/reader/js/hooks.min.js"></script>
    <script src="/reader/js/reader.js"></script>
    <script src="https://use.fontawesome.com/388ada2422.js"></script>
    <link rel="stylesheet" href="{{ asset('reader/css/readerstyle.css') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <link rel="manifest" href="/images/manifest.json">
    <link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#5bbad5">
</head>
<body>
    <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<!--
<div id="booklist"> <a class="tohome" href="/home">К списку книг</a></div>
<div id="savebook">Сохранить книгу</div>
<div id="sendchapter">Сохранить главу</div>
-->
<div id="sidebar">
    <div id="panels" class="sidebar-panels">
      <div class="sidebar-panels__inner">
        <a class="show_view icon-list active" id="show-Toc" data-view="Toc"></a>
        <a class="show_view icon-bookmark" id="show-Bookmarks" data-view="Bookmarks"></a>
        <a class="show_view icon-pencil" id="show-Notes" data-view="Notes"></a>
        <div class="search-input">
          <input id="searchBox" placeholder="іздеу" type="text">
          <a class="show_view icon-search" id="show-Search" data-view="Search"></a>
        </div>
      </div>
    </div>
    <div class="view" id="tocView">
        <h3 class="viewTitle">Бөлімдер</h3>
    </div>
    <div class="view" id="searchView">
        <ul id="searchResults"></ul>
    </div>
    <div class="view" id="bookmarksView">
        <h3 class="viewTitle">Бетбелгі</h3>
        <ul id="bookmarks"></ul>
    </div>
    <div class="view" id="notesView">
        <h3 class="viewTitle" style="margin-left:15px">Ойтүйін</h3>
        <div id="new-note">
            <textarea id="note-text"></textarea>
            <button id="note-anchor">ойтүйін</button>
        </div>
        <ol id="notes"></ol>
    </div>
</div>
<div id="main" data-book="{{ $book->file_path }}" data-book-id="{{ $book->id }}">
    <div class="loading">
        @include('layouts.reader_loader')
    </div>
    <div id="titlebar">
        <div id="opener"><a class="icon-menu" id="slider">Menu</a></div>
        <div id="metainfo">
            <span id="book-title">
            @php
                $text = ".";
                foreach ($book->authors as $author){
                    $text = $text." ".$author['name'];
                }
            @endphp
            {{ $book->name.$text }}</span><span id="title-seperator"> - </span><span id="chapter-title"></span></div>
        <div id="title-controls"><a class="icon-cog" id="settings">Settings</a>
            <div id="settings-panel">
                <div class="setting" id="fontfamily-setting">
                    <div class="setting-title"><i class="icon-font"></i><span>Қаріп</span></div>
                    <div class="setting-content"><span class="setting-content-value" id="ArialRegular">Arial</span><span class="setting-content-value" id="TimesNewRomanRegular">Times New Roman</span><span class="setting-content-value active-setting" id="HelveticaRegular">Helvetica</span></div>
                </div>
                <div class="setting" id="fontsize-setting">
                    <div class="setting-title"><i class="icon-fontsize-1"></i><span>Қаріп өлшемі</span></div>
                    <div class="setting-content"><span class="setting-content-value active-setting" id="font16">16px</span><span class="setting-content-value" id="font20">20px</span><span class="setting-content-value" id="font24">24px</span></div>
                </div>
                <div class="setting" id="interval-setting">
                    <div class="setting-title"><i class="icon-align-left"></i><span>Жол аралығы</span></div>
                    <div class="setting-content"><span class="setting-content-value active-setting" id="1-2em">ұсақ</span><span class="setting-content-value" id="1-6em">орташа</span><span class="setting-content-value" id="2em">ірі</span></div>
                </div>
                <div class="setting" id="theme-setting">
                    <div class="setting-title"><i class="icon-picture"></i><span>Фон</span></div>
                    <div class="setting-content theme"><span class="setting-content-value active-setting" id="yellow"></span><span class="setting-content-value" id="white"></span><span class="setting-content-value" id="grey"></span></div>
                </div>
                <div class="setting clearfix" id="nightwatch-setting">
                    <div class="setting-title" style="float:left"><span>Түнгі режим</span></div>
                    <div class="setting-content" style="float:right;margin-top:0"><i class="night-icon icon-moon-inv"></i></div>
                </div>
            </div><a class="icon-bookmark-empty" id="bookmark">Сделать закладку</a><a class="icon-resize-full" id="fullscreen">Полноэкранный режим</a>
        </div>
    </div>
    <div id="divider"></div>
    <div class="arrow" id="prev">‹</div>
    <div id="viewer"></div>
    <div class="arrow" id="next">›</div>
    <div id="progress">
        <div id="progress-range">
            <div id="progress-status"></div><span id="progress-value"></span>
            <div id="progress-slider"><span id="current-chapter"></span></div>
        </div>
    </div>
    <div id="loader"><img src="/reader/img/loader.gif"></div>
    {{--<div id="text-fragment-area">
        <div class="save-quote-block fragment-block">
            <button class="save-quote-button">Сохранить цитату</button>
            <div class="save-quote-message"><span class="save-message-text"> </span><a class="tomyQuotes" href="/profile/quotes">перейти к моим цитатам</a></div>
        </div>
        <div class="share-block fragment-block">
            <button class="share-quote-button">Поделиться цитатой</button>
        </div>
        <div class="mistake-block fragment-block">
            <h3 class="fragment-block-title">Сообщить об ошибке</h3>
            <textarea class="text-fragment"></textarea>
            <h3 class="fragment-block-title">Ваш комментарий</h3>
            <textarea class="comment"></textarea>
            <div class="send-mistake-block">
                <button class="send-mistake-button">Отправить</button>
            </div>
        </div>
        <div class="close-button"><i class="icon-cancel"></i></div>
        <div class="result-message">
            <div class="result-message-text"></div>
            <div class="gotoComments"><a class="tomyComments" href="/mistakes">перейти к комментариям</a></div>
        </div>
    </div>--}}

    <div id="text-fragment-area" class="modal js-modal-container js-addquote">
        <div class="modal-inner __addquote-modal js-addquote-inner">
            <form method="post" action="{{ route('profile.savequote') }}" class="addquote">
                {{ csrf_field() }}
                <textarea name="quote" id="" class="addquote-textarea text-fragment" maxlength="50">
                </textarea>
                <input type="hidden" value="{{ $book->id }}" name="book_id">
                <button class="addquote-btn">Цитатаны сақтау</button>
            </form>
            <button class="addquote-btn" id="send-mistake-button" style="width: 280px">Мәтіндегі қатені түзеу</button>
            <span class="addquote-desc">Әлеуметтік желілерге салу</span>
            <div class="addquote-social">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&quote=" class="addquote-social__item facebook-link">
                    <span class="fa fa-facebook"></span>
                </a>
                {{--
                <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="addquote-social__item __gp">
                    <span class="fa fa-google-plus _small"></span>
                </a>
                <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="addquote-social__item __vk">
                    <span class="fa fa-vk _small"></span>
                </a>--}}
            </div>
            <span class="fa fa-times addquote-close js-addquote-close"></span>
        </div>
    </div>

    <div style="display:none">
        @if(!empty($progressfind))<input type="text" class="progress_page" value="{{ $progressfind }}">@endif
    </div>

</div>
<div class="overlay"><img src="/reader/img/Spinner.gif"></div>
<script src="{{ asset('js/app.min.js') }}"></script>
</body>
</html>
