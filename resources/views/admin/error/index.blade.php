@extends('admin.layout.main')

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ошибки в тексте
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">

                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped no-margin">
                                    <colgroup>
                                                                        <col width="150px">
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>Действия</th>
                                            <th>Книга</th>
                                            <th>Текст</th>
                                            <th>Ссылка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($items as $item)
                                            <tr>
                                                <td>
                                                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                                       data-action="destroy"
                                                       data-href="{{route('admin.'.$controller_name.'.destroy',$item)}}">
                                                            <i class="fa fa-trash"></i>
                                                    </a>

                                                </td>
                                                <td>{{$item->book_id}}</td>
                                                <td>{{$item->selected_text}}</td>
                                                <td><a target="_blank" href="{{ explode("#", $item->url)[0] }}?cfi={{ explode("#", $item->url)[1] }}">{{$item->url}}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                                        <!-- /.table -->
                            </div>
                                                <!-- /.mail-box-messages -->
                        </div>
                                        <!-- /.box-body -->

                    </div>
                                <!-- /. box -->
                </div>
                        <!-- /.col -->
            </div>
        </section>
@endsection