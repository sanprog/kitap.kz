 <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="/images/favicon.ico?1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('/js/jquery-3.1.1.min.js') }}"></script>

    <!-- Category admin js css-->

    <script src="/admin/js/editable/jquery-editable-poshytip.js"></script>
    <script src="/admin/js/alertify/alertify.js"></script>
    <script src="https://unpkg.com/vue@2.5.2/dist/vue.js"></script>


    <link rel="stylesheet" href="/admin/css/jqtree/jqtree.css">
    <link rel="stylesheet" href="/admin/css/alertify/alertify.core.css">
   <link rel="stylesheet" href="/admin/css/alertify/alertify.default.css">
    <link rel="stylesheet" href="/admin/css/editable/jquery-editable.css">


    <!-- EndCategory -->



    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/admin/css/AdminLTE.min.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/admin/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    {!! Assets::css() !!}
    @stack('head_scripts')
    <script>
        window.core_project = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        @-webkit-keyframes ld {
            0%   { transform: rotate(0deg) scale(1); }
            50%  { transform: rotate(180deg) scale(1.1); }
            100% { transform: rotate(360deg) scale(1); }
        }
        @-moz-keyframes ld {
            0%   { transform: rotate(0deg) scale(1); }
            50%  { transform: rotate(180deg) scale(1.1); }
            100% { transform: rotate(360deg) scale(1); }
        }
        @-o-keyframes ld {
            0%   { transform: rotate(0deg) scale(1); }
            50%  { transform: rotate(180deg) scale(1.1); }
            100% { transform: rotate(360deg) scale(1); }
        }
        @keyframes ld {
            0%   { transform: rotate(0deg) scale(1); }
            50%  { transform: rotate(180deg) scale(1.1); }
            100% { transform: rotate(360deg) scale(1); }
        }
        .loading-progress {
            position: relative;
            opacity: .8;
            color: transparent !important;
            text-shadow: none !important;
        }

        .loading-progress:hover,
        .loading-progress:active,
        .loading-progress:focus {
            cursor: default;
            color: transparent;
            outline: none !important;
            box-shadow: none;
        }

        .loading-progress:before {
            content: '';
            display: inline-block;
            position: absolute;
            background: transparent;
            border: 1px solid #fff;
            border-top-color: transparent;
            border-bottom-color: transparent;
            border-radius: 50%;
            box-sizing: border-box;
            top: 50%;
            left: 50%;
            margin-top: -12px;
            margin-left: -12px;

            width: 24px;
            height: 24px;

            -webkit-animation: ld 1s ease-in-out infinite;
            -moz-animation:    ld 1s ease-in-out infinite;
            -o-animation:      ld 1s ease-in-out infinite;
            animation:         ld 1s ease-in-out infinite;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('main')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>KT</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Kitap.kz</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown" aria-expanded="false">
                            <span>{{  Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">

                            <li role="presentation">
                                <a href="#"><i class="dropdown-icon fa fa-globe"></i>&nbsp;&nbsp;Перейти на сайт</a>
                            </li>
                            <li class="divider" role="presentation"></li>
                            <li role="presentation">
                                <a href="#"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Выход</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                {!! \AdminMenu::generate() !!}
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
        <div class="modal modal-danger fade" role="dialog" id="modal-destroy">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Внимание</h4>
                    </div>
                    <div class="modal-body">
                        <p>Вы уверены что хотите удалить?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-action="yes_destroy">Да</button>
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Нет</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->

<script src="/admin/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/admin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<link rel="stylesheet" href="/admin/plugins/growl/jquery.growl.css" >
<script src="/admin/plugins/growl/jquery.growl.js" defer></script>
<script src="/admin/plugins/jQueryForm/jquery.form.min.js"></script>
<script src="/admin/js/form.js"></script>
<script src="/admin/js/adminlte.min.js"></script>

<!-- May have conflict with adminLte.tree -->
<script src="/admin/js/jqtree/tree.jquery.js"></script>

{!! Assets::js() !!}
<script src="/admin/js/main.js"></script>

@if (isset($footer_js))
    <script>
        $(function () {
            {!! $footer_js !!}
        });
    </script>


@endif
</body>
</html>
