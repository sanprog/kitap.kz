<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;

        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                        <label for="{{$controller_name}}-author_name">E-mail: </label>
                        <input type="email" id="{{$controller_name}}-name" class="form-control" name="email"
                               autocomplete="off"
                               value="{{ $item->email or old('email') }}"
                        >
                        @if ($errors->has('email'))
                            <span class="help-block help-block-error">{{ $errors->first('email') }}</span>
                        @endif
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
