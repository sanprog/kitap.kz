@if(!$model->poetAudios->isEmpty())
    <table class="table table-hover table-striped">
        <thead class="thead-light">
        <tr>
            <th scope="col">Удалить</th>
            <th scope="col">Файл</th>
        </tr>
        </thead>
        <tbody>
        @foreach($model->poetAudios as $item)
            <tr>
                <td><a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                       data-action="destroy"
                       data-href="{{route('admin.poet.destroytrack',['id' => $item->id])}}"
                    ><i class="fa fa-trash"></i></a></td>
                <td>{{$item->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>Не добавлено ни одного аудиофайла</p>
@endif
