<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    <div class="form-group {{$errors->has('author_id')?'has-error':''}}">
                        <label>Выберите автора : </label>
                        <select class="selectAuthor form-control" name="author_id">

                            @if ($model->exists)


                                    <option value="{{$model->author->id}}" selected>{{$model->author->name}}</option>

                            @endif
                        </select>

                        @if ($errors->has('author_id'))
                            <span class="help-block help-block-error">{{ $errors->first('author_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('desc')?'has-error':''}}">
                        <label for="{{$controller_name}}-author_text">Описание: </label>
                        <textarea id="{{$controller_name}}-author_text" class="form-control" name="desc"
                                  style="height: 300px">
                                {!! $model->desc or old('desc') !!}
                            </textarea>
                        @ckeditor('desc')
                        @if ($errors->has('desc'))
                            <span class="help-block help-block-error">{{ $errors->first('desc') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('tracks.*')?'has-error':''}}">
                        <label for="{{$controller_name}}-author_name">Аудиофайлы: </label>
                        @includeWhen($model->exists,'admin.specauthor._track_table')
                        <input id="{{$controller_name}}-name"
                               class="form-control"
                               type="file"
                               multiple="multiple"
                               name="tracks[]"
                        >
                        @if ($errors->has('tracks.*'))
                            <span class="help-block help-block-error">{{ $errors->first('tracks.*') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

	$('.selectAuthor').select2({
		placeholder : 'Поиск автора',
		ajax        : {
			url            : '/inside/selectAuthor/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});
</script>