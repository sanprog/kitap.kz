<a href="{{route('admin.audiobook.edit', ['id' => $model->audiobook->id])}}" class="btn-default btn btn-sm">Редактировать аудиокнигу</a>
<div class="form-group">
    Диктор : {{$model->audiobook->dictor->dictor_name}}
</div>
<div class="form-group">
    <label for="">Аудиофайлы : </label>
    @include('admin.audiobook._audiofile_table', ['model' => $model->audiobook])
</div>

