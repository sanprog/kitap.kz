<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{ URL::previous() }}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                        <label for="{{$controller_name}}-name">Наши предложения: </label>
                        {{ Form::checkbox('offer', $model->offer,  $model->offer) }}

                        @if ($errors->has('name'))
                            <span class="help-block help-block-error">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                        <label for="{{$controller_name}}-name">Название книги: </label>
                        <input type="text" id="{{$controller_name}}-name" class="form-control" name="name"
                               autocomplete="off"
                               value="{{ $model->name or old('name') }}"
                        >
                        @if ($errors->has('name'))
                            <span class="help-block help-block-error">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    {{--<div class="form-group {{$errors->has('slug')?'has-error':''}}">
                        <label for="{{$controller_name}}-slug">ЧПУ : </label>
                        <input type="text" id="{{$controller_name}}-slug" class="form-control" name="slug"
                               autocomplete="off"
                               value="{{ old('slug', $model->slug) }}"
                        >
                        @if ($errors->has('slug'))
                            <span class="help-block help-block-error">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>--}}

                    <div class="form-group {{$errors->has('category_id')?'has-error':''}}">
                        <label>Выберите категорию : </label>
                        <select class="selectCategory form-control" name="category_id[]" multiple="multiple">
                            @if ($model->exists || request('ci'))
                                @foreach ($model->categories as $cat)
                                    <option value="{{$cat->id}}" selected>{{$cat->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        {{$model->getAttribute('category_id')}}

                        @if ($errors->has('category_id'))
                            <span class="help-block help-block-error">{{ $errors->first('category_id') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('author_id')?'has-error':''}}">
                        <label>Выберите автора : </label>
                        <select class="selectAuthor form-control" name="author_id[]" multiple="multiple">
                            @if ($model->exists)
                                @foreach ($model->authors as $author)
                                    <option value="{{$author->id}}" selected>{{$author->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        {{$model->getAttribute('author_id')}}

                        @if ($errors->has('author_id'))
                            <span class="help-block help-block-error">{{ $errors->first('author_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('genre_id')?'has-error':''}}">
                        <label>Выберите жанр : </label>
                        <select class="selectGenre form-control" name="genre_id[]" multiple="multiple">
                            @if ($model->exists)
                                @foreach ($model->genre as $genre)
                                    <option value="{{$genre->id}}" selected>{{$genre->name}}</option>
                                @endforeach
                            @endif
                        </select>

                        {{$model->getAttribute('genre_id')}}

                        @if ($errors->has('genre_id'))
                            <span class="help-block help-block-error">{{ $errors->first('genre_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('description')?'has-error':''}}">
                        <label for="{{$controller_name}}-description">О Книге: </label>
                        <textarea id="{{$controller_name}}-description" class="form-control" name="description"
                                  style="height: 300px">
                                {{ $model->description or old('description') }}
                            </textarea>
                        @ckeditor('description')
                        @if ($errors->has('description'))
                            <span class="help-block help-block-error">{{ $errors->first('description') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('file_path')?'has-error':''}}">
                        <label for="{{$controller_name}}-file">Выберите книгу</label>
                        <input type="file" id="{{$controller_name}}-file_path" class="form-control" name="file_path"
                               autocomplete="off"
                               value="{{$model->getAttribute('file_path')}}"
                        >
                        @if ($model->exists)
                            {{$model->getAttribute('file_path')}}
                        @endif
                        @if ($errors->has('file_path'))
                            <span class="help-block help-block-error">{{ $errors->first('file_path') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('img_path')?'has-error':''}}">
                        <label for="{{$controller_name}}-img_path">Изображение</label>
                        <input type="file" id="{{$controller_name}}-img_path" class="form-control" name="img_path"
                               autocomplete="off"
                               value="{{$model->getAttribute('img_path')}}"
                        >
                        @if ($model->exists)
                            <img src="{{$model->getAttribute('img_path')}}"
                                 style="width: 100%; max-width: 500px; max-height: 500px;">
                        @endif
                        @if ($errors->has('img_path'))
                            <span class="help-block help-block-error">{{ $errors->first('img_path') }}</span>
                        @endif
                    </div>

                    @if ($model->exists)
                        <div class="form-group {{$errors->has('img_path')?'has-error':''}}">
                            <label for="{{$controller_name}}-img_path">Связанная аудиокнига</label>
                            @if($model->audiobook)
                                @include('admin.book._audiobook_group')
                            @else
                                <div class="form-group">
                                    Нет связанной аудиокниги, вы можете:
                                    <a href="{{route('admin.audiobook.create').'?b='.$model->id}}" class="btn-default btn btn-sm">Добавить аудиокнигу</a>
                                </div>
                            @endif
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$('.selectCategory').select2({
		placeholder : 'Поиск категории',
		ajax        : {
			url            : '/inside/selectCategoryBook/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

	$('.selectAuthor').select2({
		placeholder : 'Поиск автора',
		ajax        : {
			url            : '/inside/selectAuthor/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

	$('.selectGenre').select2({
		placeholder : 'Поиск жанра',
		ajax        : {
			url            : '/inside/selectGenre/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

</script>