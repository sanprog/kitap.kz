@extends('admin.layout.main')

@section('content')
    <section class="content-header">
        <h1>
            Создание текстовой страницы
        </h1>
    </section>
    <section class="content" id="content-page">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('admin.'.$controller_name.'.store')}}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    @include('admin.pages._form')
                </form>
            </div>
        </div>
    </section>
@endsection
