@extends('admin.layout.main')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Переводы
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="page-actions">
                            <div style="max-width: 250px; margin-top: 5px;">
                                <form action="">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="{{(string)$search}}" placeholder="Поиск" name="search">
                                        <span class="input-group-btn">
                                              <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped no-margin">
                                <colgroup>
                                    <col width="50px">
                                    <col width="50px">
                                    <col width="150px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>Действия</th>
                                    <th>Группа</th>
                                    <th>Переменная</th>
                                    @foreach ($locales as $locale)
                                        <th>Значение {!! strtoupper($locale->locale) !!}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td>
                                            <a class="btn btn-primary btn-sm" title="Редактировать" href="{{route('admin.'.$controller_name.'.edit',$item)}}"><i class="fa fa-pencil"></i></a>
                                            @if (false)
                                                <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                                   data-action="destroy"
                                                   data-href="{{route('admin.'.$controller_name.'.destroy',$item)}}"
                                                ><i class="fa fa-trash"></i></a>
                                            @endif
                                        </td>
                                        <td>{{$item->group}}</td>
                                        <td>{{$item->item}}</td>
                                        @foreach ($locales as $locale)
                                            <td>
                                                @if (trans()->has($item->getCodeAttribute(),$locale->locale,false))
                                                    {!! trans($item->getCodeAttribute(),[],$locale->locale) !!}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        {{ $items->links() }}
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection
