<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить</button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        @foreach ($locales as $lang)
            @php
                $prefix_field_name=$lang->locale.'_';
                if($translate=$translates->get($lang->locale)){
                  $text=$translate->text;
                }else{
                    $text='';
                }
            @endphp
                <div class="form-group {{$errors->has($prefix_field_name.'text')?'has-error':''}}">
                    <label for="{{$controller_name}}-{{$lang->locale}}-text">Текст {{strtoupper($lang->locale)}}</label>
                    <textarea class="form-control" id="{{$controller_name}}-{{$lang->locale}}-text" name="{{$controller_name}}[{{$prefix_field_name}}text]">{!! $text!!}</textarea>
                    @if ($errors->has($prefix_field_name.'text'))
                        <span class="help-block help-block-error">{{ $errors->first($prefix_field_name.'text') }}</span>
                    @endif
                </div>
        @endforeach
    </div>
</div>
