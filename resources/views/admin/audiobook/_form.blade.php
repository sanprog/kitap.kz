<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group {{$errors->has('book_id')?'has-error':''}}">
                        <label for="book_id">Выберите книгу : </label>
                        <select class="form-control" name="book_id" id="bookSelect" {{$model->exists ? 'disabled' : ''}}>
                            <option selected></option>
                            @if ($model->exists || request('b'))
                                <option value="{{$model->book->id}}" selected>{{$model->book->name}}</option>
                            @endif
                        </select>

                        @if ($errors->has('book_id'))
                            <span class="help-block help-block-error">{{ $errors->first('book_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('dictor_id')?'has-error':''}}">
                        <label for="dictor_id">Выберите диктора : </label>
                        <select class="form-control" name="dictor_id" id="dictorSelect">
                            @if ($model->exists)
                                <option value="{{$model->dictor->id}}" selected>{{$model->dictor->dictor_name}}</option>
                            @endif
                        </select>

                        @if ($errors->has('dictor_id'))
                            <span class="help-block help-block-error">{{ $errors->first('dictor_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('audiobook.*')?'has-error':''}}">
                        <label for="{{$controller_name}}-author_name">Аудиофайлы: </label>
                        @includeWhen($model->exists,'admin.audiobook._audiofile_table')
                        <input id="{{$controller_name}}-name"
                               class="form-control"
                               type="file"
                               multiple="multiple"
                               name="audiofiles[]"
                        >
                        @if ($errors->has('audiobook.*'))
                            <span class="help-block help-block-error">{{ $errors->first('audiobook.*') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$('#bookSelect').select2({
		placeholder : 'Поиск книги',
		ajax        : {
			url            : '/inside/selectBook/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

	$('#dictorSelect').select2({
		placeholder : 'Поиск диктора',
		ajax        : {
			url            : '/inside/selectDictor/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.dictor_name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

</script>