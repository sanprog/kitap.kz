<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif



                    <div class="form-group {{$errors->has('title')?'has-error':''}}">
                        <label for="{{$controller_name}}-title">Страница: </label>

                        {{ Form::select('type', [
                            'public'  => 'Художественная литература',
                            'science' => 'Научная литература',
                            'audio'   => 'Аудиокниги',
                        ], old('type', $model->type), ['class' => 'form-control']) }}

                        @if ($errors->has('type'))
                            <span class="help-block help-block-error">{{ $errors->first('type') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('title')?'has-error':''}}">
                        <label for="{{$controller_name}}-title">Тайтл: </label>
                        <input type="text" id="{{$controller_name}}-name" class="form-control" name="title"
                               autocomplete="off"
                               value="{{ old('title', $model->title) }}"
                        >
                        @if ($errors->has('title'))
                            <span class="help-block help-block-error">{{ $errors->first('title') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('uri')?'has-error':''}}">
                        <label for="{{$controller_name}}-uri">ссылка на ресурс: </label>
                        <div class="input-group">
                            <span class="input-group-addon">http://kitap.kz/</span>
                            <input type="text" id="{{$controller_name}}-uri" class="form-control" name="uri"
                                   autocomplete="off"
                                   value="{{ old('uri', $model->uri) }}">
                        </div>

                        @if ($errors->has('uri'))
                            <span class="help-block help-block-error">{{ $errors->first('uri') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('img_path')?'has-error':''}}">
                        <label for="{{$controller_name}}-img_path">Изображение</label>
                        <input type="file" id="{{$controller_name}}-img_path" class="form-control" name="img_path"
                               autocomplete="off"
                               value="{{$model->getAttribute('img_path')}}"
                        >
                        @if ($model->exists)
                            <img src="/storage/{{$model->getAttribute('img_path')}}"
                                 style="width: 100%; max-width: 500px; max-height: 500px;">
                        @endif
                        @if ($errors->has('img_path'))
                            <span class="help-block help-block-error">{{ $errors->first('img_path') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

