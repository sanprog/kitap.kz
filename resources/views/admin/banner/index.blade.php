@extends('admin.layout.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Баннеры
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="page-actions">
                            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped no-margin">
                                <colgroup>
                                    <col width="150px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Заголовок</th>
                                    <th>Ресурс</th>
                                    <th>Изображение</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dates as $data)
                                    <tr>
                                        <td>
                                            <a class="btn btn-primary btn-sm" title="Редактировать" href="{{route('admin.'.$controller_name.'.edit',$data)}}"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                               data-action="destroy"
                                               data-href="{{route('admin.'.$controller_name.'.destroy',$data)}}"
                                            ><i class="fa fa-trash"></i></a>
                                        </td>
                                        <td>{{$data->title}}</td>
                                        <td style="max-width: 850px;">
                                            http://kitap.kz/{{ $data->uri }}
                                        </td>
                                        <td><img src="/storage/{{$data->getAttribute('img_path')}}"  style="width: 100px;"></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    {{--<div class="box-footer">--}}
                        {{--{{ $items->links('admin.layouts.partials.pagination.default') }}--}}
                    {{--</div>--}}
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection
