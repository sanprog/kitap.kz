@extends('admin.layout.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Статьи
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="page-actions">
                            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="box-header with-border">
                        <form action="{{route('admin.'.$controller_name.'.index')}}" enctype="multipart/form-data" method="GET">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="{{$controller_name}}-course_id">Поиск</label>
                                    <input type="text" name="q" class="form-control" value="{{ !empty(\request('q')) ? \request('q') : ''}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn-success btn btn-sm"><i class="glyphicon glyphicon-search"></i> Поиск</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped no-margin">
                                <colgroup>
                                    <col width="150px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Название статьи</th>
                                    <th>ЧПУ</th>
                                    <th>Краткое содержание</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dates as $data)
                                    <tr>
                                        <td>
                                            <a class="btn btn-primary btn-sm" title="Редактировать" href="{{route('admin.'.$controller_name.'.edit',$data)}}"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                               data-action="destroy"
                                               data-href="{{route('admin.'.$controller_name.'.destroy',$data)}}"
                                            ><i class="fa fa-trash"></i></a>
                                        </td>
                                        <td>{{$data->name}}</td>
                                        <td style="max-width: 850px;">
                                            {!!html_entity_decode($data->slug)!!}
                                        </td>
                                        <td>{{$data->title}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                    {{--<div class="box-footer">--}}
                        {{--{{ $items->links('admin.layouts.partials.pagination.default') }}--}}
                    {{--</div>--}}
                </div>
            {{ $dates->appends(Illuminate\Support\Facades\Input::all())->links() }}
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection
