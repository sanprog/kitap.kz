@extends('admin.layout.main')

@section('content')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <section class="content-header">
        <h1>
            Создание баннера
        </h1>
    </section>
    <section class="content" id="content-page">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('admin.'.$controller_name.'.store')}}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    @include('admin.'.$controller_name.'._form')
                </form>

            </div>
        </div>
    </section>
@endsection





