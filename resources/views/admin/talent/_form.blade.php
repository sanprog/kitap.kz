<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div class="form-group {{$errors->has('name')?'has-error':''}}">
                        <label for="{{$controller_name}}-title">Имя: </label>
                        <input type="text" id="{{$controller_name}}-name" class="form-control" name="name"
                               autocomplete="off"
                               value="{{ old('name', $model->name) }}"
                        >
                        @if ($errors->has('name'))
                            <span class="help-block help-block-error">{{ $errors->first('name') }}</span>
                        @endif
                    </div>


                    <div class="form-group {{$errors->has('img_path')?'has-error':''}}">
                        <label for="{{$controller_name}}-img_path">Изображение</label>
                        <input type="file" id="{{$controller_name}}-img_path" class="form-control" name="img_path"
                               autocomplete="off"
                               value="{{$model->getAttribute('img_path')}}"
                        >
                        @if ($model->exists)
                            <img src="/storage/{{$model->getAttribute('img_path')}}"
                                 style="width: 100%; max-width: 500px; max-height: 500px;">
                        @endif
                        @if ($errors->has('img_path'))
                            <span class="help-block help-block-error">{{ $errors->first('img_path') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{$errors->has('description')?'has-error':''}}">
                        <label for="{{$controller_name}}-description">Описание: </label>
                        <textarea id="{{$controller_name}}-description" class="form-control" name="description"
                                  style="height: 300px">
                            {{ $model->description or old('description') }}
                        </textarea>
                        @ckeditor('description')
                        @if ($errors->has('description'))
                            <span class="help-block help-block-error">{{ $errors->first('description') }}</span>
                        @endif
                    </div>

                        <div class="box box-success lineBlocks">
                            <div class="box-header with-border">
                                <h3 class="box-title">Книги</h3>
                            </div>
                            @if($model->items->count() != 0)
                            <table class="table table-bordered">
                                <tbody><tr>
                                    <th style="width: 10px">#</th>
                                    <th>img_path</th>
                                    <th>book_path</th>
                                    <th>audio_path</th>
                                    <th style="width: 40px;">delete</th>
                                </tr>
                                    @foreach($model->items as $key => $item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{$item->img_path}}</td>
                                        <td>{{$item->book_path}}</td>
                                        <td>{{$item->audio_path}}</td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                               data-action="destroy"
                                               data-href="{{route('admin.'.$controller_name.'.destroyitem',['id' => $item->id])}}"
                                            ><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                            <div class="box-body repeat">
                                @for($i = 0; $i < 3-$model->items->count(); $i++)
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="{{$controller_name}}-img_path">Обложка</label>
                                        <input type="file" id="{{$controller_name}}-img_path_items" class="form-control" name="items[{{$i}}][img_path]"
                                               autocomplete="off"
                                               value="{{$model->getAttribute('img_path_items')}}"
                                        >
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="{{$controller_name}}-img_path">Файл книги</label>
                                        <input type="file" id="{{$controller_name}}-img_path_items" class="form-control" name="items[{{$i}}][book_path]"
                                               autocomplete="off"
                                               value="{{$model->getAttribute('book_path_items')}}"
                                        >
                                    </div>
                                    <label for="{{$controller_name}}-img_path">Аудиофайл</label>
                                    <div class="col-xs-5">
                                        <input type="file" id="{{$controller_name}}-img_path_items" class="form-control" name="items[{{$i}}][audio_path]"
                                               autocomplete="off"
                                               value="{{$model->getAttribute('audio_path_items')}}"
                                        >
                                    </div>
                                </div>
                                @endfor
                            </div>
                            <!-- /.box-body -->
                        </div>
                        {{--<div class="box-header with-border">
                            <button id="addLine" type="button" class="btn bg-olive pull-right"><i class="fa fa-plus"></i> Добавить еще</button>
                        </div>--}}


                </div>
            </div>
        </div>
    </div>
</div>
{{--<script>
    $('#addLine').on('click', function() {
            addBlock = $('div.repeat:first').clone();
            addBlock.find('input').val('');
            addBlock.insertAfter("div.repeat:last");
		}
    );
</script>--}}

