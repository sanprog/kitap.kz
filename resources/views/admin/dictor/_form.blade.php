


<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить</button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="form-group {{$errors->has('dictor_name')?'has-error':''}}">
                            <label for="{{$controller_name}}-dictor_name">ФИО диктора: </label>
                            <input type="text" id="{{$controller_name}}-name" class="form-control" name="dictor_name" autocomplete="off"
                                   value="{{ old('dictor_name', $model->dictor_name) }}"
                            >
                            @if ($errors->has('dictor_name'))
                                <span class="help-block help-block-error">{{ $errors->first('dictor_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('description')?'has-error':''}}">
                            <label for="{{$controller_name}}-description">О Дикторе: </label>
                            <textarea id="{{$controller_name}}-description" class="form-control" name="description" style="height: 300px">
                                    {{ old('description', $model->description) }}
                            </textarea>
                            @ckeditor('description')
                        @if ($errors->has('description'))
                                <span class="help-block help-block-error">{{ $errors->first('description') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('img_src')?'has-error':''}}">
                            <label for="{{$controller_name}}-img_src">Изображение</label>
                            <input type="file" id="{{$controller_name}}-img_src" class="form-control" name="img_src" autocomplete="off"
                                   value="{{$model->getAttribute('img_src')}}"
                            >
                            @if ($model->exists)
                                <img src="/storage{{$model->getAttribute('img_src')}}" style="width: 100%; max-width: 500px; max-height: 500px;">
                            @endif
                            @if ($errors->has('img_src'))
                                <span class="help-block help-block-error">{{ $errors->first('img_src') }}</span>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

