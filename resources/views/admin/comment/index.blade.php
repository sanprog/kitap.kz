@extends('admin.layout.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Книги
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <form action="{{route('admin.'.$controller_name.'.index')}}" enctype="multipart/form-data" method="GET">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="{{$controller_name}}-course_id">Поиск</label>
                                    <input type="text" name="q" class="form-control" value="{{ !empty(\request('q')) ? \request('q') : ''}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn-success btn btn-sm"><i class="glyphicon glyphicon-search"></i> Поиск</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped no-margin">
                                <colgroup>
                                    <col width="150px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Комментарий</th>
                                    <th>Название</th>
                                    <th>Ссылка</th>
                                    <th>Юзер</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($comments as $comment)
                                    <tr>
                                        <td>
                                            <a class="btn btn-primary btn-sm" title="Редактировать" href="{{route('admin.'.$controller_name.'.edit',$comment)}}"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                                               data-action="destroy"
                                               data-href="{{route('admin.'.$controller_name.'.destroy',$comment)}}"
                                            ><i class="fa fa-trash"></i></a>
                                        </td>

                                        <td>{{$comment->comment}}</td>
                                        <td>{{$comment->model->name}}</td>

                                        <td>
                                            @if($comment->model_type == 'App\Model\Book')
                                                {{ route('books.show', ['slug' => $comment->model->slug]) }}
                                            @endif
                                        </td>
                                        <td>{{ \App\User::getAuthor($comment->user_id)['name'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>

                    <!-- /.box-body -->
                    {{--<div class="box-footer">--}}
                    {{--{{ $items->links('admin.layouts.partials.pagination.default') }}--}}
                    {{--</div>--}}
                </div>
            {{ $comments->appends(Illuminate\Support\Facades\Input::all())->links() }}
            <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection
