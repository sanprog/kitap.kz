<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.index')}}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="form-group {{$errors->has('book_id')?'has-error':''}}">
                            <label for="book_id">Выберите книгу : </label>
                            <select class="form-control" name="book_id" id="bookSelect">
                                <option selected>...</option>
                                @if ($model->exists || request('b'))
                                    <option value="{{$model->book->id}}" selected>{{$model->book->name}}</option>
                                @endif
                            </select>

                            @if ($errors->has('book_id'))
                                <span class="help-block help-block-error">{{ $errors->first('book_id') }}</span>
                            @endif
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#bookSelect').select2({
        placeholder : 'Поиск книги',
        ajax        : {
            url            : '/inside/selectBook/',
            dataType       : 'json',
            delay          : 250,
            processResults : function(data) {
                return {
                    results : $.map(data, function(item) {
                        return {
                            text : item.name,
                            id   : item.id
                        }
                    })
                };
            },
            cache          : true
        }
    });

</script>

