<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <button type="submit" class="btn-success btn btn-sm" name="continue"><i class="fa fa-retweet"></i> Сохранить
            </button>
            &nbsp;&nbsp;
            <button name="commit" type="submit" class="btn-default btn btn-sm" onclick="$(this).val(1)">
                <i class="fa fa-check"></i>
                <span class="hidden-xs hidden-sm">Сохранить и Закрыть {{ $controller_name }}</span>
            </button>
            &nbsp;&nbsp;
            <a href="{{route('admin.'.$controller_name.'.create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Создать ещё</span></a>
            &nbsp;&nbsp;
            <a href="{{ URL::previous() }}" class="btn btn-default btn-sm">
                <i class="fa fa-arrow-left"></i>
                <span class="hidden-xs hidden-sm">Вернуться</span>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="{{$controller_name}}_main">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->unique() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="form-group {{$errors->has('book_id')?'has-error':''}}">
                            <label for="book_id">Выберите книгу : </label>
                            <select class="bookSelect form-control" name="book_id[]" multiple="multiple">
                                @if ($model->exists)
                                    @foreach ($model->books as $book)
                                        <option value="{{$book->id}}" selected>{{$book->name}}</option>
                                    @endforeach
                                @endif
                            </select>

                            @if ($errors->has('book_id'))
                                <span class="help-block help-block-error">{{ $errors->first('book_id') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('title')?'has-error':''}}">
                            <label for="{{$controller_name}}-title">Тайтл: </label>
                            <input type="text" id="{{$controller_name}}-name" class="form-control" name="title"
                                   autocomplete="off"
                                   value="{{ old('title', $model->title) }}"
                            >
                            @if ($errors->has('title'))
                                <span class="help-block help-block-error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{$errors->has('website')?'has-error':''}}">
                            <label for="{{$controller_name}}-website">URL сайта: </label>
                            <input type="text" id="{{$controller_name}}-website" class="form-control" name="website"
                                   autocomplete="off"
                                   value="{{ old('website', $model->website) }}"
                            >
                            @if ($errors->has('website'))
                                <span class="help-block help-block-error">{{ $errors->first('website') }}</span>
                            @endif
                        </div>


                        <div class="form-group {{$errors->has('img_path')?'has-error':''}}">
                            <label for="{{$controller_name}}-img_path">Изображение</label>
                            <input type="file" id="{{$controller_name}}-img_path" class="form-control" name="img_path"
                                   autocomplete="off"
                                   value="{{$model->getAttribute('img_path')}}"
                            >
                            @if ($model->exists)
                                <img src="/storage/{{$model->getAttribute('img_path')}}"
                                     style="width: 100%; max-width: 500px; max-height: 500px;">
                            @endif
                            @if ($errors->has('img_path'))
                                <span class="help-block help-block-error">{{ $errors->first('img_path') }}</span>
                            @endif
                        </div>

                </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$('.bookSelect').select2({
		placeholder : 'Поиск книги',
		ajax        : {
			url            : '/inside/selectBook/',
			dataType       : 'json',
			delay          : 250,
			processResults : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.name,
							id   : item.id
						}
					})
				};
			},
			cache          : true
		}
	});

</script>
