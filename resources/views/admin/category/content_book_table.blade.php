<div class="box">
    <div class="box-header with-border">
        <div class="page-actions">
            <a href="{{route('admin.book.create',['ci' => $categoryId])}}" class="btn btn-default">Добавить книгу</a>
        </div>
    </div>
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-hover table-striped no-margin">
                <colgroup>
                    <col width="150px">
                </colgroup>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название книги</th>
                    <th>ЧПУ</th>
                    <th>путь к ephub</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($tableData as $data)
                    <tr>
                        <td>
                            <a href="{{route('admin.book.edit',['id' => $data->id])}}"
                               class='btn btn-primary btn-sm' title="Редактировать"><i class="fa fa-pencil"></i></a>

                            <a class="btn btn-danger btn-sm" href="javascript:void(0)" title="Удалить"
                               data-action="destroy"
                               data-href="{{route('admin.book.destroy',['id' => $data->id])}}"
                            ><i class="fa fa-trash"></i></a>
                        </td>
                        <td>{{$data->name}}</td>
                        <td>{{$data->slug}}</td>
                        <td>{{$data->file_path}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
