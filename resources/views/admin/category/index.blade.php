@extends('admin.layout.main')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

        <h1>
            Категории
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <ul class="nav nav-tabs">
            @foreach($rootCategories as $node)
                <li class="{{(request()->id == $node->id) ? ' active' : ''}}">
                    <a href="{{ route('admin.category.view', ['id' => $node->id]) }}">{{$node->name}}</a>
                </li>
            @endforeach
        </ul>

        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary ">
                    <div class="box-header with-border">
                        <div class="page-actions">
                            <a href="#" class="newCategory btn btn-default">Добавить категорию</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="tree"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="box-body">

                    <div id="renderBlock">
                    </div>
                </div>
            </div>
        </div>

    </section>
    <script type="text/javascript">
		var data = {!!   $categories !!};
		var serverUrl = "{!! Request::url() !!}";

		function run() {
			var a = document.createElement("script");
			a.src = "/admin/js/site.js";
			document.body.appendChild(a)
		}

		window.addEventListener ? window.addEventListener("load", run, !1) :
			window.attachEvent ? window.attachEvent("onload", run) : window.onload = run;

		function renderBooks(e) {
			$('.itemName').val(e.value).change();

			$.get('/inside/category/' + e.value + '/getcontent',
				{
					async      : true,
					beforeSend : function() {
						$('#renderBlock').html('<img  src="/admin/img/ajax_loader/Spinner.gif">');
					}
				},
				function(response) {
				    console.log(response);
					$('#renderBlock').html(response);
				});
		}

    </script>

@endsection