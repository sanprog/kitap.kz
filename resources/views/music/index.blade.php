@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('music.title') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('music.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('music') !!}
    <div class="catalogmobile">
        <div class="catalogmobile-top">
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-catalog" style="width: 100%;cursor: pointer;">{{ trans('music.category') }}</span>
        </div>
        @widget('categoryWidget',['type' => 2])
    </div>
    <div class="main-row">
        <div class="main-col-left">
            <section class="traditional">
                <h3 class="traditional-title">
                    <span class="traditional-title__ico fa fa-music" aria-hidden="true"></span>{{ trans('music.tradition') }}
                </h3>
                <!-- row3 -->
                <div class="traditional-row">

                    @foreach($traditional as $item)
                        <div class="traditional-item __hover-item">
                            <div class="__relative">
                              <img src="/storage/{{ $item->img_path or '../images/music_bg.png' }}" alt="" class="traditional-item__img">
                              <div class="hover-img__text">
                                <a href="{{ route('music.show', ['slug' => $item->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                </a>
                              </div> 
                            </div>                            
                            <span class="traditional-item__title">{{ $item->name }}</span>
                            <span class="traditional-item__text"></span>
                            {{--<div class="traditional-item__rating">
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-heart" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                            </div>--}}
                        </div>
                    @endforeach

                </div>
                <!-- end row3 -->

                <a class="traditional-book">
    <span class="traditional-book__text">{{ trans('music.an') }}</span>
                </a>

                <h3 class="traditional-title">
                    <span class="traditional-title__ico fa fa-qrcode" aria-hidden="true"></span>{{ trans('music.many') }}
                </h3>
                <div class="traditional-row">
                    @foreach($model as $item)
                        <div class="traditional-item __hover-item">
                            <div class="__relative">
                              <img src="/storage/{{ $item->img_path or '../images/music_bg.png' }}" alt="" class="traditional-item__img">
                              <div class="hover-img__text">
                                <a href="{{ route('music.show', ['slug' => $item->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                </a>
                              </div> 
                            </div>                             
                            <span class="traditional-item__title">{{ $item->name }}</span>
                            <span class="traditional-item__text"></span>
                            {{--<div class="traditional-item__rating">
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-heart" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                            </div>--}}
                        </div>
                    @endforeach
                </div>

                <h3 class="traditional-title">
                    <span class="traditional-title__ico fa fa-asterisk" aria-hidden="true"></span>{{ trans('music.shahnama') }}
                </h3>
                <!-- row2 -->
                <div class="traditional-row">
                    @foreach($shahnama as $item)
                        <div class="traditional-item __hover-item">
                            <div class="__relative">
                              <img src="/storage/{{ $item->img_path or '../images/music_bg.png' }}" alt="" class="traditional-item__img">
                              <div class="hover-img__text">
                                <a href="{{ route('music.show', ['slug' => $item->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                </a>
                              </div> 
                            </div>                             
                            <span class="traditional-item__title">{{ $item->name }}</span>
                            <span class="traditional-item__text"></span>
                            {{--<div class="traditional-item__rating">
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-heart" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                                <div class="traditional-rating__block">
                                    <span class="traditional-rating__ico fa fa-plus-square" aria-hidden="true"></span>
                                    <span class="traditional-rating__text">20</span>
                                </div>
                            </div>--}}
                        </div>
                    @endforeach
                </div>
                <!-- end row2 -->
            </section>

        </div>
        <div class="main-col-right">
            @widget('categoryWidget')
            <aside class="plate">
                <span class="plate-title">
                    <span class="plate-title__ico fa fa-bullseye" aria-hidden="true"></span>{{ trans('music.syrly') }}
                </span>
            </aside>
            <aside class="plate __mobile">
                <span class="plate-title">
                    <span class="plate-title__ico fa fa-bullseye" aria-hidden="true"></span>{{ trans('music.syrly') }}
                </span>
            </aside>
        </div>
    </div>


@endsection
