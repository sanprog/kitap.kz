@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('music') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('music.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('music.show', $model->name) !!}

    <section class="audioinner">
        <div class="audioinner-top">
            <div class="audioinner-boook">
                <img src="/storage/{{ $model->img_path or '../images/music_bg.png' }}" alt="" class="audioinner-book__img">
            </div>
            <div class="audioinner-info">
                <div class="audioinner-info__top">
                    <h1 class="audioinner-info__author">
                        @foreach($model->authors as $author)
                            {{ $author->name }}&nbsp;
                        @endforeach
                    </h1>
                    <h3 class="audioinner-info__name">{{ $model->name }}</h3>
                </div>
                <div class="audioinner-item__options">
                    @if (!Auth::guest())
                    @if(!$library_audios)
                        <a href="/save/{{ $model->id }}/audio" class="audioinner-options__btn __blue">
                              <span class="audioinner-options__btn-text">
                                {{ trans('button.add') }}
                              </span>
                                            <span class="audioinner-options__btn-ico">
                                <span class="audioinner-options-ico__img fa fa-plus"></span>
                              </span>
                        </a>
                    @else
                        <a href="{{ route('profile.deleteitem', ['type' => $library_audios->resource_type, 'id' => $library_audios->resource_id]) }}" class="bookitem-options__btn __blue">
                            <span class="bookitem-options__btn-text">{{ trans('button.sub') }}</span>
                            <span class="bookitem-options__btn-ico">
                                <span class="bookitem-options-ico__img fa fa-minus"></span>
                            </span>
                        </a>
                    @endif
                    @endif
                    {{--<div class="socila-info">
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-heart" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-plus-square" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-commenting" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
        {{--<div class="audioinner-book__rating">
            <span class="audioinner-book__rating-ico fa fa-star _active" aria-hidden="true"></span>
            <span class="audioinner-book__rating-ico fa fa-star _active" aria-hidden="true"></span>
            <span class="audioinner-book__rating-ico fa fa-star _active" aria-hidden="true"></span>
            <span class="audioinner-book__rating-ico fa fa-star _active" aria-hidden="true"></span>
            <span class="audioinner-book__rating-ico fa fa-star" aria-hidden="true"></span>
        </div>--}}


        <div class="audiobook _inner">
            <div class="poet-audio">
                <audio id="Poet"></audio>
                <div class="Player audio-player" data-player-name="Poet">
                    <div class="player-block">
                        <div class="player-block__control-wrapper">
                            <div class="player__buttons-panel player__buttons-control-panel">
                                <div class="player-block__prev" data-player-button="prev"></div>
                                <div class="player-block__play" data-player-button="play"></div>
                                <div class="player-block__next" data-player-button="next"></div>
                            </div>
                        </div>
                        <div class="player-block__time-wrapper">
                            <div class="player-block__time-current" data-player-time="current">00:00</div>
                            <div class="player-block__tracker" data-player-scale="time">
                                <div class="time-scale__buffered" data-player-scale="buffered"></div>
                                <div class="time-scale__progress" data-player-scale="progress"></div>
                            </div>
                            <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                        </div>
                        <div class="player__volume-panel">
                            <div class="player__volume" data-player-button="volume"></div>
                            <div class="player__volume-scale-wrapper">
                                <div class="player__volume-scale" data-player-scale="volume">
                                    <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="player-block__playlist" data-player-playlist="Poet">
                        @foreach($model->track as $key => $item)
                            <li data-player-src="/storage/{{ $item->file_path }}">
                                <div class="player-block__playlist__text">
                                    <span class="player-block__playlist__number">{{ ++$key }}.</span>
                                    <span class="player-block__playlist__author">{{--Б. Соқпақбаев--}}</span>
                                    <span class="player-block__playlist__name">{{ $item->name }}</span>
                                </div>
                                <span class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>




        <div class="audioinner-text">
            {!! $model->description !!}
        </div>
    </section>

    <script>
		var playlist = {
        @foreach($model->track as $key => $item)
        {{ $key++ }}: {
			name: '{{ $item->name }}',
				band: '',
				cover: '/images/poet1.png',
				src: '/storage/{{ $item->file_path }}'
		},
        @endforeach
		};

    </script>

@endsection
