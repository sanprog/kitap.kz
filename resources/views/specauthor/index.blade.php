@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.specauthor.title') }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ trans('project.specauthor.title') }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ '/images/sp5.png' }}" />
@endsection

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _specauthor">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.specauthor.title') }}</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.specauthor') !!}

    <section class="specauthor">
        <div class="specauthor-container">
            <div class="specauthor-info">
                <div class="specbook-social">
                    <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-vk"></span></a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-facebook"></span></a>
                    <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-google-plus _small"></span></a>
                    {{--<a href="#" class="specbook-ico"><span class="specbook-ico__img fa fa-twitter"></span></a>--}}
                </div>
                {!! trans('project.specauthor.desc') !!}
            </div>
            <div class="specauthor-row">
                @foreach($items as $item)
                    <a href="{{ route('specauthor.show',['id' => $item->id]) }}" class="specauthor-item">
                        <div class="specauthor-img">
                            <img src="/storage/{{ $item->author->img_path }}" alt="" class="specauthor-item__img">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="specauthor-item__name">{{ $item->author->name }}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </section>

@endsection