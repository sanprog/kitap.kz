@extends('layouts.app')

@section('headtitle')
    <title>{{ $item->author->name }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg _specauthor">
            <div class="container filter-container">
                <a href="{{ route('specauthor') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">Ұлылардың үні</h1>
            </div>
        </div>
    </section>
    {!!  Breadcrumbs::render('special.specauthor') !!}
    <section class="authorspeech">
        <div class="authorspeech-top">
            <div class="authorspeech-image">
                <img src="/storage/{{ $item->author->img_path }}" alt="" class="authorspeech-image__item">
            </div>
            <div class="authorspeech-info">
                <h3 class="authorspeech-info__title">{{ $item->author->name }}</h3>
                <p class="authorspeech-info__text">
                    {!! $item->desc !!}
                </p>
            </div>
        </div>
        <div class="authorspeech-bottom">
            <div class="audio">
                <audio id="Test"></audio>
                <div class="Player audio-player" data-player-name="Test">
                    <div class="player-block">
                        <div class="player-block__control-wrapper">
                            <div class="player__buttons-panel player__buttons-control-panel">
                                <div class="player-block__prev" data-player-button="prev"></div>
                                <div class="player-block__play" data-player-button="play"></div>
                                <div class="player-block__next" data-player-button="next"></div>
                            </div>
                        </div>
                        <div class="player-block__time-wrapper">
                            <div class="player-block__time-current" data-player-time="current">00:00</div>
                            <div class="player-block__tracker" data-player-scale="time">
                                <div class="time-scale__buffered" data-player-scale="buffered"></div>
                                <div class="time-scale__progress" data-player-scale="progress"></div>
                            </div>
                            <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                        </div>
                        <div class="player__volume-panel">
                            <div class="player__volume" data-player-button="volume"></div>
                            <div class="player__volume-scale-wrapper">
                                <div class="player__volume-scale" data-player-scale="volume">
                                    <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="player-block__playlist" data-player-playlist="Test">
                    @php
                    $i = 1;
                    @endphp
                    @foreach($item->specauthorAudio as $audio)
                        <li data-player-src="/storage/{{ $audio->file_path }}">
                            <div class="player-block__playlist__text">
                                <span class="player-block__playlist__number">{{ $i++ }}</span>
                                <span class="player-block__playlist__name">{{ $audio->name }}</span>
                            </div>
                            <span class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    <script>
        var playlist = {
        @foreach($item->specauthorAudio as $key => $audio)
        {{ $key++ }}: {
            name: '{{ $audio->name }}',
                band: '',
                cover: '/images/poet1.png',
                src: '/storage/{{ $audio->file_path }}'
        },
        @endforeach
        };

    </script>

@endsection