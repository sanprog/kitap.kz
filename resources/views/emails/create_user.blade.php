@extends('emails.layout')

@section('content')
<p style="padding-bottom: 20px;"><font face="Arial,Helvetica,sans-serif" style="font-size:28px;"><b><?= trans('mail.create_user.title') ?></b></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><b><?= trans('mail.create_user.hello', ['login' => $name]) ?></b></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><?= trans('mail.create_user.invitation', ['role' => $roles[$role]]) ?></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><?= trans('mail.create_user.your_credentials') ?></font></p>

<p style="padding-bottom: 10px;">
	<font face="Arial,Helvetica,sans-serif" style="font-size:16px;">
		<?= trans('mail.create_user.email') ?> <?= $email ?>
	</font>
</p>
<p style="padding-bottom: 10px;">
	<font face="Arial,Helvetica,sans-serif" style="font-size:16px;">
		<?= trans('mail.create_user.password') ?> <?= $password ?>
	</font>
</p>
@endsection