@extends('emails.layout')

@section('content')


<div style="Margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
        <tr>
            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align:bottom;width:600px;">
                <![endif]-->
                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                    <tr>
                                        <td align="left" style="font-size:0px;padding:0px;padding-top:0;padding-bottom:30px;word-break:break-word;">
                                            <div style="margin:40px 0 40px 0;font-family:Roboto,Helvetica Neue;font-size:16px;font-style:normal;line-height:1;text-align:left;color:#000000;">
                                                <p style="padding-bottom: 20px;"><font face="Arial,Helvetica,sans-serif" style="font-size:28px;"><b><?=trans('mail.recovery.title_recovery')?></b></font></p>
                                                <p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><b><?=trans('mail.recovery.hello',['login'=> $login ])?></b></font></p>
                                                <p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><?=trans('mail.recovery.confirmation')?> {{ $link }}</font></p>
                                            </div>
                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--[if mso | IE]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
</div>
@endsection