@extends('emails.layout')

@section('content')
        <!--[if mso | IE]>
<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->


<div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
            style="background:#ffffff;background-color:#ffffff;width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0 0 30px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:600px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-100 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:Roboto,Helvetica Neue;font-size:20px;font-style:normal;line-height:1;text-align:center;color:#000000;"
                                            >
                                                Оқылымды кітаптар
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:150px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:100px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/book1.png"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="100"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Биография
                                            </div>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;line-height:20px;text-align:center;color:#000;"
                                            >
                                                Кайрат<br/>Исмаилов
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:100px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/book2.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="100"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Биография
                                            </div>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;line-height:20px;text-align:center;color:#000;"
                                            >
                                                Кайрат<br/>Исмаилов
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:100px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/book2.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="100"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Биография
                                            </div>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;line-height:20px;text-align:center;color:#000;"
                                            >
                                                Кайрат<br/>Исмаилов
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:4px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:100px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/book2.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="100"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:16px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Биография
                                            </div>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;line-height:20px;text-align:center;color:#000;"
                                            >
                                                Кайрат<br/>Исмаилов
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:60px 0 30px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:600px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-100 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <p
                                                    style="border-top:solid 1px #ebebeb;font-size:1;margin:0px auto;width:100%;"
                                            >
                                            </p>

                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    style="border-top:solid 1px #ebebeb;font-size:1;margin:0px auto;width:600px;"
                                                    role="presentation" width="600px"
                                            >
                                                <tr>
                                                    <td style="height:0;line-height:0;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:150px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:56px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/circle1.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="56"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Кітап
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:56px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/circle2.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="56"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Аудиокітап
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:56px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/circle3.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="56"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Музыка
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:150px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-25 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:15px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:56px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/circle4.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="56"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:5px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:roboto,helvetica;font-size:14px;font-weight:bold;line-height:1;text-align:center;color:#000;"
                                            >
                                                Видео
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:30px 0 40px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:600px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-100 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <p
                                                    style="border-top:solid 1px #ebebeb;font-size:1;margin:0px auto;width:100%;"
                                            >
                                            </p>

                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    style="border-top:solid 1px #ebebeb;font-size:1;margin:0px auto;width:600px;"
                                                    role="presentation" width="600px"
                                            >
                                                <tr>
                                                    <td style="height:0;line-height:0;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->


<div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
            style="background:#ffffff;background-color:#ffffff;width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0 0 30px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:600px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-100 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;word-break:break-word;"
                                        >

                                            <div
                                                    style="font-family:Roboto,Helvetica Neue;font-size:20px;font-style:normal;line-height:1;text-align:center;color:#000000;"
                                            >
                                                Актуальные материалы
                                            </div>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:300px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-50 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:40px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:221px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/actual1.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="221"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                <td
                        style="vertical-align:bottom;width:300px;"
                >
                <![endif]-->

                <div
                        class="mj-column-per-50 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:40px;padding-left:0px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:collapse;border-spacing:0px;"
                                            >
                                                <tbody>
                                                <tr>
                                                    <td style="width:221px;">

                                                        <a
                                                                href="https://kitap.kz/" target="_blank"
                                                        >

                                                            <img
                                                                    alt="https://kitap.kz/" height="auto"
                                                                    src="https://kitap.kz/images/email/actual2.jpg"
                                                                    style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                    width="221"
                                                            />

                                                        </a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<table
        align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
>
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->

<div style="Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
    >
        <tbody>
        <tr>
            <td
                    style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
            >
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td
                                style="vertical-align:bottom;width:600px;"
                        >
                <![endif]-->

                <div
                        class="mj-column-per-100 outlook-group-fix"
                        style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                >

                    <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                    >
                        <tbody>
                        <tr>
                            <td style="vertical-align:bottom;padding:0px;">

                                <table
                                        border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                >

                                    <tr>
                                        <td
                                                align="center"
                                                style="font-size:0px;padding:0px;padding-top:0;padding-bottom:30px;word-break:break-word;"
                                        >

                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="border-collapse:separate;line-height:100%;"
                                            >
                                                <tr>
                                                    <td
                                                            align="center" bgcolor="#17b2bf" role="presentation"
                                                            style="border:none;border-radius:5px;color:#FFFFFF;cursor:auto;padding:15px 70px;"
                                                            valign="middle"
                                                    >
                                                        <a
                                                                href="#"
                                                                style="background:#17b2bf;color:#FFFFFF;font-family:Roboto,Helvetica Neue;font-size:15px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;"
                                                                target="_blank"
                                                        >
                                                            Толығырақ
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>

                                </table>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!--[if mso | IE]>
                </td>

                </tr>

                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>

</div>


<!--[if mso | IE]>
</td>
</tr>
</table>

<![endif]-->
@endsection