@extends('emails.layout')

@section('content')
<p style="padding-bottom: 20px;"><font face="Arial,Helvetica,sans-serif" style="font-size:28px;"><b><?= trans('mail.registration.title') ?></b></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><b><?= trans('mail.registration.hello', ['login' => $email]) ?></b></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><?= trans('mail.registration.success') ?></font></p>
<p style="padding-bottom: 10px;"><font face="Arial,Helvetica,sans-serif" style="font-size:16px;"><?= trans('mail.registration.your_credentials') ?></font></p>

<p style="padding-bottom: 10px;">
    <font face="Arial,Helvetica,sans-serif" style="font-size:16px;">
        <?= trans('mail.registration.email') ?> <?= $email ?>
    </font>
</p>
<p style="padding-bottom: 10px;">
    <font face="Arial,Helvetica,sans-serif" style="font-size:16px;">
        <?= trans('mail.registration.password') ?> <?= $password ?>
    </font>
</p>
@endsection