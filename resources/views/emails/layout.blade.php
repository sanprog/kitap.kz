<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>
        Кітап
    </title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width: 480px) {
            @-ms-viewport {
                width: 320px;
            }
        @viewport  {
            width: 320px;
        }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix {
            width: 100% !important;
        }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    </style>
    <!--<![endif]-->


    <style type="text/css">
        @media only screen and (min-width: 480px) {
            .mj-column-per-65 {
                width: 65% !important;
            }

            .mj-column-per-35 {
                width: 35% !important;
            }

            .mj-column-per-100 {
                width: 100% !important;
            }

            .mj-column-per-25 {
                width: 25% !important;
            }

            .mj-column-per-50 {
                width: 50% !important;
            }
        }
    </style>


    <style type="text/css">


    </style>

</head>
<body>


<div

>


    <!--[if mso | IE]>
    <table
            align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="background:#FFFFFF;background-color:#FFFFFF;Margin:0px auto;max-width:600px;">

        <table
                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="background:#FFFFFF;background-color:#FFFFFF;width:100%;"
        >
            <tbody>
            <tr>
                <td
                        style="direction:ltr;font-size:0px;padding:30px 0 30px 20px;text-align:center;vertical-align:top;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    style="vertical-align:bottom;width:377px;"
                            >
                    <![endif]-->

                    <div
                            class="mj-column-per-65 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:bottom;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="left"
                                                    style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"
                                            >

                                                <table
                                                        align="left" border="0" cellpadding="0" cellspacing="0"
                                                        role="presentation"
                                                        style="border-collapse:collapse;border-spacing:0px;"
                                                >
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:101px;">

                                                            <a
                                                                    href="https://kitap.kz/" target="_blank"
                                                            >

                                                                <img
                                                                        alt="https://kitap.kz/" height="auto"
                                                                        src="https://kitap.kz/images/email/logo.jpg"
                                                                        style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                        width="101"
                                                                />

                                                            </a>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    <td
                            style="vertical-align:bottom;width:203px;"
                    >
                    <![endif]-->

                    <div
                            class="mj-column-per-35 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:bottom;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="left"
                                                    style="font-size:0px;padding:0px;word-break:break-word;"
                                            >

                                                <table
                                                        0="[object Object]" 1="[object Object]" 2="[object Object]"
                                                        border="0"
                                                        style="cellspacing:0;color:#000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:100%;"
                                                >
                                                    <tr style="list-style: none;line-height:1">
                                                        <td>
                                                            <a href="https://kitap.kz/">
                                                                <img width="33"
                                                                     src="https://kitap.kz/images/email/vk.jpg"/>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://medium.com/@RecastAI">
                                                                <img width="33"
                                                                     src="https://kitap.kz/images/email/fb.jpg"/>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://kitap.kz/">
                                                                <img width="33"
                                                                     src="https://kitap.kz/images/email/tw.jpg"/>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://kitap.kz/">
                                                                <img width="33"
                                                                     src="https://kitap.kz/images/email/g.jpg"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table
                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
        >
            <tbody>
            <tr>
                <td
                        style="direction:ltr;font-size:0px;padding:0 0 30px;text-align:center;vertical-align:top;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    style="vertical-align:bottom;width:600px;"
                            >
                    <![endif]-->

                    <div
                            class="mj-column-per-100 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:bottom;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="center"
                                                    style="font-size:0px;padding:0px;padding-top:0;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"
                                            >

                                                <table
                                                        align="center" border="0" cellpadding="0" cellspacing="0"
                                                        role="presentation"
                                                        style="border-collapse:collapse;border-spacing:0px;"
                                                >
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:600px;">

                                                            <a
                                                                    href="https://kitap.kz/" target="_blank"
                                                            >

                                                                <img
                                                                        alt="https://kitap.kz/" height="auto"
                                                                        src="https://kitap.kz/images/email/title.jpg"
                                                                        style="border:none;display:block;outline:none;text-decoration:none;width:100%;"
                                                                        width="600"
                                                                />

                                                            </a>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->

    <!-- Intro text -->








    @yield('content')












    <!--[if mso | IE]>
    <table
            align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="background:#17b2bf;background-color:#17b2bf;Margin:0px auto;max-width:600px;">

        <table
                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="background:#17b2bf;background-color:#17b2bf;width:100%;"
        >
            <tbody>
            <tr>
                <td
                        style="direction:ltr;font-size:0px;padding:40px 40px 0;text-align:center;vertical-align:top;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    style="vertical-align:top;width:260px;"
                            >
                    <![endif]-->

                    <div
                            class="mj-column-per-50 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="left"
                                                    style="font-size:0px;padding:0px;word-break:break-word;"
                                            >

                                                <table
                                                        0="[object Object]" 1="[object Object]" 2="[object Object]"
                                                        border="0"
                                                        style="cellspacing:0;color:#000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:100%;"
                                                >
                                                    <tr style="list-style: none;line-height:1">
                                                        <td style="padding: 0 30px 10px 0;vertical-align: top;"
                                                            align="center">
                                                            <img width="15"
                                                                 src="https://kitap.kz/images/email/flag.png"/>
                                                        </td>
                                                        <td style="padding: 0 0 10px 0;">
                                                            <span style="cursor: auto; color: #ffffff; font-family: Roboto,Helvetica Neue; text-align: left; padding: 0; font-size: 14px; line-height: 20px;">Ұлттық аударма бюросы Қазақстанның ашық кітапханасы</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="list-style: none;line-height:1">
                                                        <td style="word-wrap: break-word; font-size: 0px; padding: 0 30px 10px 0;vertical-align: top;"
                                                            align="center">
                                                            <img width="15"
                                                                 src="https://kitap.kz/images/email/road.png"/>
                                                        </td>
                                                        <td>
                <span style="cursor: auto; color: #ffffff; font-family: Roboto,Helvetica Neue; text-align: left; padding: 0; font-size: 14px; line-height: 20px;">Қазақстан Республикасы,
                Алматы қаласы
                Бұхар жырау, 33. «Жеңіс»
                бизнес орталығы,
                8-қабат</span>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    <td
                            style="vertical-align:top;width:260px;"
                    >
                    <![endif]-->

                    <div
                            class="mj-column-per-50 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="center"
                                                    style="font-size:0px;padding:0px;word-break:break-word;"
                                            >

                                                <table
                                                        0="[object Object]" 1="[object Object]" 2="[object Object]"
                                                        border="0"
                                                        style="cellspacing:0;color:#000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:100%;"
                                                >
                                                    <tr style="list-style: none;line-height:1">
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   role="presentation" width="100%">
                                                                <tr style="list-style: none;line-height:1">
                                                                    <td style="padding: 0 20px 30px 40px;vertical-align: top;"
                                                                        align="center">
                                                                        <img width="15"
                                                                             src="https://kitap.kz/images/email/email.png"/>
                                                                    </td>
                                                                    <td style="padding: 0 0 30px 0;">
                                                                        <span style="cursor: auto; color: #ffffff; font-family: Roboto,Helvetica Neue; text-align: left; padding: 0; font-size: 14px; line-height: 20px;">E-mail: <a
                                                                                    href="mailto:info@kitap.kz"
                                                                                    style="color: #ffffff;text-decoration: none;">info@kitap
                                                                                .kz</a></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="list-style: none;line-height:1">
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   role="presentation" width="100%">
                                                                <tr style="list-style: none;line-height:1">
                                                                    <td style="word-wrap: break-word; font-size: 0px; padding: 0 20px 40px 40px;vertical-align: top;"
                                                                        align="center">
                                                                        <img width="15"
                                                                             src="https://kitap.kz/images/email/phone.png"/>
                                                                    </td>
                                                                    <td style="padding: 0 0 40px 0;">
                                                                        <span style="cursor: auto; color: #ffffff; font-family: Roboto,Helvetica Neue; text-align: left; padding: 0; font-size: 14px; line-height: 20px;"><a
                                                                                    href="tel:+7 (777) 777 77 77"
                                                                                    style="color: #ffffff;text-decoration: none;">+7 (777) 777 77 77</a></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="list-style: none;line-height:1;text-align: center;">
                                                        <td style="word-wrap: break-word; font-size: 0px; padding: 0 20px 40px 40px;vertical-align: top;"
                                                            align="center">
                                                            <span style="cursor: auto; color: #ffffff; font-family: Roboto,Helvetica Neue; text-align: center; padding: 0; font-size: 14px; line-height: 20px;"><a
                                                                        href="tel:+7 (777) 777 77 77"
                                                                        style="text-decoration:underline;color: #ffffff;">Отписаться</a></span>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="background:#17b2bf;background-color:#17b2bf;Margin:0px auto;max-width:600px;">

        <table
                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="background:#17b2bf;background-color:#17b2bf;width:100%;"
        >
            <tbody>
            <tr>
                <td
                        style="direction:ltr;font-size:0px;padding:30px 0;text-align:center;vertical-align:top;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    style="vertical-align:bottom;width:600px;"
                            >
                    <![endif]-->

                    <div
                            class="mj-column-per-100 outlook-group-fix"
                            style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                    >

                        <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                        >
                            <tbody>
                            <tr>
                                <td style="vertical-align:bottom;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                    >

                                        <tr>
                                            <td
                                                    align="center"
                                                    style="font-size:0px;padding:0px;word-break:break-word;"
                                            >

                                                <div
                                                        style="font-family:Roboto,Helvetica Neue;font-size:14px;line-height:1;text-align:center;color:#ffffff;"
                                                >
                                                    © Ұлттық аударма бюросы 2017
                                                </div>

                                            </td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->


</div>

</body>
</html>
  