@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->dictor_name }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('other.dictor') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('dictor', $model->id) !!}

    <section class="speakerinner">
        <div class="speakerinner-top">
            <div class="speakerinner-img">
                <img src="{{ $model->img_src or '../images/speaker.png' }}" alt="" class="speakerinner-img__item">
            </div>
            <div class="speakerinner-info">
                <h1 class="speakerinner-info__title">
                    {{ $model->dictor_name }}:
                </h1>
                <p>
                    {!! $model->description !!}
                </p>
            </div>
        </div>
        <div class="speakerinner-bottom">
            <h3 class="speakerinner-subtitle">
                <span class="speakerinner-title__ico fa fa-microphone"></span>{{ trans('other.dictor_audio') }}
            </h3>
            <div class="speakerinner-row">
                @foreach($audiobooks as $item)
                <div class="speakerinner-item">
                    <div class="speakerinner-item__img" style="background-image:url('{{ $item->book->img_path or '../images/book.png' }}')"></div>
                    <span class="speakerinner-item__title">{{ $item->book->name }}</span>
                    <span class="speakerinner-item__text">{!! $item->book->description !!}</span>
                </div>
                @endforeach
            </div>
            {{ $audiobooks->links('pagination.default') }}
        </div>
    </section>
@endsection
