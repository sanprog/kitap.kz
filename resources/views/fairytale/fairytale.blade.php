@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.fairytail.title') }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ trans('project.fairytail.title') }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ '/images/sp4.png' }}" />
@endsection

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg __fairytale">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.fairytail.title') }}</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.fairytale') !!}

    <section class="allfairytales">
        <div class="allfairytales-inner">
            <div class="allfairytales-info">
                <div class="specbook-social">
                    <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-vk"></span></a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-facebook"></span></a>
                    <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-google-plus _small"></span></a>
                    {{--<a href="#" class="specbook-ico"><span class="specbook-ico__img fa fa-twitter"></span></a>--}}
                </div>

                {!! trans('project.fairytail.desc') !!}
            </div>
            @foreach($items as $item)
                <div class="allfairytales-item">
                    <div class="allfairytale-boook">
                        <a href="{{ route('audiobooks.show',['slug' => $item->book->slug]) }}">
                            <img src="{{ $item->book->img_path }}" alt="" class="allfairytale-book__img">
                        </a>
                    </div>
                    <div class="allfairytale-info">
                        <a href="{{ route('audiobooks.show',['slug' => $item->book->slug]) }}" class="allfairytale-info__name">{{ $item->book->name }}</a>
                        <div class="allfairytale-item__options">
                            <a href="{{ route('audiobooks.show',['slug' => $item->book->slug]) }}">
                                <button class="allfairytale-options__btn __blue">
                                    <span class="allfairytale-options__btn-text">{{ trans('button.otu') }}</span>
                                    <span class="allfairytale-options__btn-ico">
                                        <span class="allfairytale-options-ico__img fa fa-plus"></span>
                                    </span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection