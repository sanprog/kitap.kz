@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('other.article') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('article.show', $model->name) !!}

    <section class="article">
        <div class="article-container">
            <h3 class="article-title">{{ $model->name }}</h3>
            <div class="article-slider__info">
                <div class="article-slider__info-col">
                    <span class="article-slider-date"><span class="fa fa-calendar"></span>{{ $model->created_at }} </span>
                    {{--<span class="article-slider-actions">
                        <span class="article-slider-actions__item"><span class="fa fa-eye"></span> 20</span>
                        <span class="article-slider-actions__item"><span class="fa fa-commenting"></span> 20</span>
                        <a href="#" class="article-slider-actions__link">пікір</a>
                    </span>--}}
                </div>
                <div class="article-slider__info-col">
                    {{--<div class="article-slider-social">
                        <a href="#" class="article-slider-social__ico tooltip" title="Tweeter арқылы бөлісу"><span class="article-slider-social__ico__img fa fa-vk"></span></a>
                        <a href="#" class="article-slider-social__ico tooltip"><span class="article-slider-social__ico__img fa fa-facebook" title="Tweeter арқылы бөлісу"></span></a>
                        <a href="#" class="article-slider-social__ico tooltip" title="Tweeter арқылы бөлісу"><span class="article-slider-social__ico__img fa fa-google-plus _small"></span></a>
                        <a href="#" class="article-slider-social__ico tooltip" title="Tweeter арқылы бөлісу"><span class="article-slider-social__ico__img fa fa-twitter"></span></a>
                    </div>--}}
                </div>
            </div>
            <div class="article-desc">
                {!! $model->text !!}
            </div>
        </div>
    </section>

@endsection