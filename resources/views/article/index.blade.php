@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('other.article') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg _special">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('other.article') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('article') !!}
    <section class="allarticles">
        <div class="container">
            <div class="allarticles-row">
                @foreach($model as $item)
                <a href="{{ route('article.show', ['slug' => $item->slug]) }}" class="allarticles-item">
                    <div class="allarticles-item__inner">
                        <div class="allarticles-img __art1" style='background-image: url("storage/{{ $item->thumb_nail }}")'>
                            <div class="allarticles-img__text">
                                <span class="allarticles-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <div class="allarticles-info">
                            <div class="allarticles-info__top">
                                <span class="allarticles-text">{{--{!! $item->title !!}--}}</span>
                                <span class="allarticles-title">{{ $item->name }}</span>
                            </div>
                            <div class="allarticles-info__bottom">
                                <span class="allarticles-info__date"><span class="fa fa-calendar"></span>{{ $item->created_at }}</span>
                                <div class="allarticles-info__action">
                                    {{--<span class="allarticles-info__action-item"><span class="fa fa-eye"></span> 20 қаралым</span>
                                    <span class="allarticles-info__action-item"><span class="fa fa-commenting"></span> 20 пікір</span>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            {{ $model->links('pagination.default') }}
        </div>
    </section>


@endsection
