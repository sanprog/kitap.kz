@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('other.notfound') }}</title>
@endsection

@section('content')
    <section class="error">
        <img class="error-person" src="images/human404.png">
        <div class="error-info">
            <span class="error-info__title">{{ trans('other.notfound') }}</span>
            <a href="/" class="error-info__link">{{ trans('other.back') }}</a>
        </div>
        <div class="error-bg">
            <div class="error-bg__wave"></div>
            <div class="error-bg__wave"></div>
        </div>
    </section>
@endsection