@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('payment.process') }}</title>
@endsection

@section('content')

    @include('layouts.loader')

    <section class="page-outer">
        <div class="page-inner">
            <div class="content-text">
                @if ($errors->any())
                    <div class="payment-errors">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <a class="button button_theme_orange" href="{{lang_route('index')}}">{!! trans('payment.index_retry') !!}</a>
                @else
                    <form action="{{array_get($order_request,'process_link')}}" method="post" name="SendOrder" id="epay_form" class="layout __loading">
                        <input type="hidden" name="Signed_Order_B64" value="{{array_get($order_request,'order')}}" />
                        <input type="hidden" name="appendix" value="{{array_get($order_request,'appendix')}}" />
                        @if (!\Auth::guest())
                            @if (\Auth::user()->email)
                                <input type="hidden" name="email" value="{{\Auth::user()->email}}" />
                            @endif
                            <input type="hidden" name="person_id" value="{{\Auth::user()->user_id}}" />
                        @endif
                        <input type="hidden" name="Language" value="{{array_get($order_request,'lang')}}" />
                        <input type="hidden" name="BackLink" value="{{array_get($order_request,'back_link')}}" />
                        <input type="hidden" name="FailureBackLink" value="{{array_get($order_request,'failure_back_link')}}" />
                        <input type="hidden" name="PostLink" value="{{array_get($order_request,'post_link')}}" />
                        <input type="hidden" name="FailurePostLink" value="{{array_get($order_request,'failure_post_link')}}" />
                        <div class="ef_message" style="width: 100%; height: 800px;">
                            <p style="text-align:center;margin-top:2rem;font-size:1.6rem;">{{ trans('payment.process') }}</p>
                            <noscript><br />
                                <input class="button" type="submit" name="GotoPay" value="{{ trans('payment.continue') }}" />
                            </noscript>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </section>
@endsection
