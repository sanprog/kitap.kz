@extends('layouts.app')

@section('content')

    @if ($error)
        <section class="header-title-outer">
            <div class="header-title-inner">
                <h1>Оплата</h1>
            </div>
        </section>
        <section class="payment-alert-page-outer">
            <div class="payment-alert-page-inner">
                <div class="payment-alert-page">
                    <div class="content-text">
                        <p>{{$error}}</p>
                    </div>
                </div>
            </div>
        </section>
    @else
        <section class="header-title-outer">
            <div class="header-title-inner">
                <h1>Оплата</h1>
            </div>
        </section>
        <section class="payment-alert-page-outer">
            <div class="payment-alert-page-inner">
                <div class="payment-alert-page">
                    <div class="content-text">{!! trans('subscribe.subscribe_payment_success') !!}</div>
                </div>
            </div>
        </section>
    @endif
@endsection
