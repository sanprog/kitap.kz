@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('other.partner') }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg _partners">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('other.partner') }}</h1>
            </div>
        </div>
    </section>

{!!  Breadcrumbs::render('partners') !!}

    <section class="partners">
        <div class="container">
            <div class="partners-row">
                @foreach($partners as $partner)
                    <div class="partners-item">
                        @if( $partner->website )
                            <a href="http://{{ $partner->website }}" class="partners-item__top" target="_blank">
                                <img src="/storage/{{ $partner->img_path or '../images/book.png' }}" alt="" class="partners-item__img">
                            </a>
                        @else
                            <a class="partners-item__top">
                                <img src="/storage/{{ $partner->img_path or '../images/book.png' }}" alt="" class="partners-item__img">
                            </a>
                        @endif
                        <div class="partners-item__info">
                            <span class="partners-info__title">{{ $partner->title }}</span>
                            @if( !$partner->books->isEmpty() )
                                <a href="{{ route('partners.show', [$partner->id]) }}" class="partners-info__link">{{ trans('other.plink') }}</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
