@extends('layouts.app')

@section('headtitle')
    <title>{{ $partner->title }}</title>
@endsection


@section('content')
    <!-- filter -->
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('partners') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ $partner->title }}</h1>
            </div>
        </div>
    </section>
    <!-- enddfilter -->
    {!!  Breadcrumbs::render('partner', $partner) !!}

    <div class="main-row _traditional">
        <div class="main-col-left">
            <section class="partnersinner">
                <div class="partnersinner-row">
                    @foreach($books as $book)
                        <a href="/books/{{ $book->slug }}" class="partnersinner-item">
                          <img src="{{ $book->img_path or '/images/book.png' }}" class="partnersinner-item__img"/>
                          <span class="partnersinner-item__title">{{ $book->name }}</span>
                          <span class="partnersinner-item__text">
                              @foreach($book->authors as $author)
                                  {{ $author->name }}
                              @endforeach
                          </span>
                        </a>
                    @endforeach
                </div>
            </section>

            {{ $books->links('pagination.default') }}

        </div>
        <div class="main-col-right">
            <div class="partner-sidebar">
                <div class="partner-sidebar__logo">
                    <img src="/storage/{{ $partner->img_path or '../images/book.png' }}" alt="" class="partners-item__img">
                </div>
                <span class="partner-sidebar__text">{{ $partner->title }}</span>
            </div>

        </div>
    </div>
@endsection
