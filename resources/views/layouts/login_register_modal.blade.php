
@if(app('request')->input('auth') == 1)
<div class="modal js-modal-container js-reg" style="display:block;">
@else
<div class="modal js-modal-container js-reg">
@endif
    <div class="modal-inner js-reg-inner">
        <div class="reg">
            <div class="reg-inner">
                <div class="reg-top">
                    <ul class="tabs reg-top__tabs">
                        <li class="tab-link current reg-top__tabs-item" data-tab="tab-1">{{ trans('auth.auth') }}</li>
                        <li class="tab-link reg-top__tabs-item" data-tab="tab-2">{{ trans('auth.register') }}</li>
                    </ul>
                </div>
                <div id="tab-1" class="tab-content reg-content current">
                    @if(app('request')->input('auth') == 1)
                    <form class="modal-form login __hidden js-reg" method="POST" action="{{ route('login', ['b' => request('b')]) }}" style="display:block;">
                    @else
                    <form class="modal-form login __hidden js-reg" method="POST" action="{{ route('login') }}">
                    @endif
                        {{ csrf_field() }}

                        <h3 class="modal-title">{{ trans('auth.auth') }}</h3>

                        <div class="modal-row" >
                            <input type="text" name="login" id="login_email" class="modal-input" placeholder="Электронды пошта адресі">
                            <span class="fa fa-user modal-ico"></span>
                        </div>

                        <div class="modal-row" title="">
                            <input name="password" type="password" id="login_password" class="modal-input" placeholder="Құпиясөз">
                            <span class="fa fa-lock modal-ico"></span>
                        </div>
                        <div class="modal-info">
                            <div class="modal-agree">
                                <input type="checkbox" value="Есте сақтау" id="remember" name="remember"
                                       class="modal-agree__input" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember" class="modal-agree__label">{{ trans('auth.remember') }}</label>
                            </div>
                            <span data-tab="tab-3" class="modal-info__text tab-link">{{ trans('auth.forgot') }}</span>
                        </div>
                        <button data-action="auth" class="moda-btn __blue">Кіру</button>
                        {{--<button class="moda-btn __green">Тіркелу</button>--}}
                        {{--<button class="moda-btn __pink">Вход с телефона</button>--}}
                        <span class="modal-text">Әлеуметтік желілер арқылы кіру</span>
                        <div class="modal-social">
                            <a href="{{ route('login.social', ['provider' => 'facebook']) }}" class="modal-social__item ico icons-modal-fb"><span
                                        class="typcn typcn-social-facebook"></span></a>
                            <a href="{{ route('login.social', ['provider' => 'google']) }}" class="modal-social__item ico icons-modal-gplus"><span
                                        class="fa fa-google-plus"></span></a>
                            <a href="{{ route('login.social', ['provider' => 'vkontakte']) }}" class="modal-social__item ico icons-modal-vk"><span class="fa fa-vk"></span></a>
                        </div>
                    </form>

                </div>
                <div id="tab-2" class="tab-content reg-content">
                    <form class="modal-form login" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <h3 class="modal-title">{{ trans('auth.register') }}</h3>
                        <div class="modal-row">
                            <input type="email" id="register_email" name="email" class="modal-input" placeholder="Электронды пошта адресі" value="{{ old('email') }}">
                            <span class="fa fa-user modal-ico"></span>
                        </div>
                        <div class="modal-row">
                            <input type="password" id="register_password" class="modal-input" placeholder="Құпиясөз" name="password">
                            <span class="fa fa-lock modal-ico"></span>
                        </div>
                        <div class="modal-row">
                            <input type="password" class="modal-input" placeholder="Құпиясөзді растау" name="password_confirmation">
                            <span class="fa fa-lock modal-ico"></span>
                        </div>
                        <button  data-action="register" class="moda-btn __green">{{ trans('button.register') }}</button>
                        {{--<button class="moda-btn __pink __single">Вход с телефона</button>--}}
                        <span class="modal-text">Әлеуметтік желілер арқылы кіру</span>
                        <div class="modal-social">
                            <a href="{{ route('login.social', ['provider' => 'facebook']) }}" class="modal-social__item ico icons-modal-fb"><span
                                        class="typcn typcn-social-facebook"></span></a>
                            <a href="{{ route('login.social', ['provider' => 'google']) }}" class="modal-social__item ico icons-modal-gplus"><span
                                        class="fa fa-google-plus"></span></a>
                            <a href="{{ route('login.social', ['provider' => 'vkontakte']) }}" class="modal-social__item ico icons-modal-vk"><span class="fa fa-vk"></span></a>
                        </div>
                    </form>
                </div>
                <div id="tab-3" class="tab-content reg-content">
                    <form class="modal-form login js-pass" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <h3 class="modal-title">Кіру</h3>
                        <div class="modal-row">
                            <input type="email" name="email" id="reset_email" class="modal-input" placeholder="Электронды пошта адресі">
                            <span class="fa fa-user modal-ico"></span>
                        </div>
                        <button type="submit"  class="moda-btn __green">{{ trans('button.forgot') }}</button>
                        <button type="button" data-tab="tab-1" class="moda-btn __pink js-back tab-link">{{ trans('button.back') }}</button>
                        <span class="modal-text __left-text">{{ trans('auth.send_email') }}</span>
                    </form>
                </div>
                <!-- container -->
            </div>
        </div>
    </div>
</div>
