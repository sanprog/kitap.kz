<!doctype html>
<html lang="ru" class="no-js">
<head>
    <meta charset="UTF-8">
    @yield('headtitle')
    <title>Кітап</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:image" content=""/>
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="article">
    <meta property="twitter:image:src" content="../images/social-share.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('headmeta')

    <link href="{{ asset('css/styles.css') }}?v={!! file_exists(public_path('/css/style.css'))?filemtime(public_path('/css/style.css')):1 !!}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <link rel="manifest" href="/images/manifest.json">
    <link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#5bbad5">

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113626007-1"></script>
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-113626007-1');
    </script>

</head>

<body{{ empty($exception) && Route::currentRouteName() != 'terror' ? '' : ' class=__error' }}>
<!-- Block: Main Container -->
<main>
    <header id="top">
        <div class="header-top">
            <a href="{{ url('/') }}" class="header-logo">
                <img src="/images/logo.png" alt="{{ config('app.name', 'Laravel') }}" class="header-logo__img">
            </a>
            <div class="header-top__col">
                <div class="header-search __desktop">
                    <form action="{{ route('search') }}" method="get">
                        <input type="text" name="q" value="" class="header-search__input"
                               placeholder="Авторды немесе кітап атауын енгізіңіз">
                        <button type="submit" class="header-search__ico fa fa-search" aria-hidden="true"></button>
                        <input type="submit" style="display:none"/>
                    </form>
                </div>
                <a class="header-help" href="{{ route('donate') }}">
                    <span class="header-help__text">{{ trans('button.koldau') }}</span>
                    <span class="header-help__ico">
                        <span class="header-help__ico-img fa fa-skyatlas"></span>
                    </span>
                </a>
            </div>
        </div>

        <div class="header-bottom">
            <div class="container header-bottom__container">
                <a href="{{ url('/') }}" class="header-logo-mobile">
                    <img src="/images/logosmall.png" alt="" class="header-logo-mobile__img">
                </a>
                <nav class="header-nav" data-submenu="js-nav">
                    <div class="header-nav__item __hidden">
                        <form action="{{ route('search') }}" method="get" class="header-search">
                            <input type="text" name="q" value="" class="header-search__input" placeholder="Іздеу">
                            <button type="submit" class="header-search__ico">
                                <span class="fa fa-search"></span>
                            </button>
                        </form>
                    </div>
                    <div class="header-nav__item __dropdown {{ (request()->is('books/*'))? '__active' : '' }}">
                      <span class="header-nav__item-link">
                        <a href="{{ route('books.public') }}" class="header-nav__item-text ">{{ trans('search.book') }}</a>
                        <span class="header-nav__item-ico">
                          <span class="fa fa-book"></span>
                        </span>
                      </span>
                        <div class="header-nav__dropdown">
                            <a href="{{ route('books.public') }}" class="header-nav__dropdown-item">{{ trans('search.korkem') }}</a>
                            <a href="{{ route('books.science') }}" class="header-nav__dropdown-item">{{ trans('search.gylymi') }}</a>
                        </div>
                    </div>
                    <a href="{{ route('audiobooks') }}" class="header-nav__item {{ request()->is('audiobooks/*') || request()->is('audiobooks') ? '__active' : '' }}">
                        <span class="header-nav__item-text">{{ trans('search.audiobook') }}</span><span class="header-nav__item-ico">
                        <span class="fa fa-headphones"></span>
                      </span>
                    </a>
                    <a href="{{ route('special') }}" class="header-nav__item {{ request()->is('special') || request()->is('special/*') ? '__active' : '' }}">
                        <span class="header-nav__item-text">{{ trans('project.special.title') }}</span><span class="header-nav__item-ico">
                        <span class="fa fa-th-large"></span>
                      </span>
                    </a>
                    <a href="{{ route('article') }}" class="header-nav__item {{ request()->is('article') || request()->is('article/*') ? '__active' : '' }}">
                        <span class="header-nav__item-text">{{ trans('other.article') }}</span><span class="header-nav__item-ico">
                        <span class="fa fa-image"></span>
                      </span>
                    </a>
                    <a href="{{ route('video') }}" class="header-nav__item {{ request()->is('video') || request()->is('video/*') ? '__active' : '' }}">
                        <span class="header-nav__item-text">{{ trans('search.video') }}</span><span class="header-nav__item-ico">
                        <span class="fa fa-youtube-play"></span>
                      </span>
                    </a>
                    <a href="{{ route('music') }}" class="header-nav__item {{ request()->is('music') || request()->is('music/*') ? '__active' : '' }}">
                        <span class="header-nav__item-text">{{ trans('search.music') }} </span><span class="header-nav__item-ico">
                        <span class="fa fa-music"></span>
                      </span>
                    </a>
                    <a target="_blank" href="https://100kitap.kz/kz" class="header-nav__item">
                        <span class="header-nav__item-text">100 кітап</span><span class="header-nav__item-ico">
                        <span class="fa fa-book"></span>
                      </span>
                    </a>
                    {{--<a href="audiolib.html" class="header-nav__item __donate">
                        <span class="header-nav__item-text">Жобаны қолдау</span><span class="header-nav__item-ico">
                        <span class="fa fa-skyatlas"></span>
                      </span>
                    </a>--}}
                </nav>

                @if (Auth::guest())
                    <button {{--href="{{ route('login') }}"--}} class="header-enter js-enter"><span
                                class="header-enter__ico fa fa-user"></span>{{ trans('auth.auth') }}</button>
                @else
                    <div class="header-loggedin">
                        <div class="header-loggedin-top">
                            <div class="header-loggedin-top__ico"><span class="fa fa-user"></span></div>
                            <span class="header-loggedin-top__name">{{ Auth::user()->email }}</span>
                            <span class="fa fa-caret-down"></span>
                        </div>
                        <div class="header-loggedin__dropdown">
                            <a href="{{route('profile')}}" class="header-loggedin__item">
                                <span class="header-loggedin__item-ico fa fa-street-view"></span>
                                {{ trans('widget.bet') }}
                            </a>
                            <a href="{{ route('profile.mylibrary') }}" class="header-loggedin__item">
                                <span class="header-loggedin__item-ico fa fa-leanpub"></span>
                                {{ trans('widget.library') }}
                            </a>
                            <a href="{{ route('profile.myquote') }}" class="header-loggedin__item">
                                <span class="header-loggedin__item-ico fa fa-quote-right"></span>
                                {{ trans('profile.quote.title') }}
                            </a>
                            {{--<a href="settinginner.html" class="header-loggedin__item">
                                <span class="header-loggedin__item-ico fa fa-cog"></span>
                                Настройки
                            </a>--}}
                            <a class="header-loggedin__item"
                               href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <span class="header-loggedin__item-ico fa fa-share-square-o"></span>
                                {{ trans('other.exit') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>

                    <div class="header-loggedin-mobile">
                        <div class="header-loggedin-mobile-top" data-submenu-open="js-loged-nav">
                            <span class="fa fa-user header-loggedin-mobile-top__ico"></span>
                        </div>
                        <div class="header-loggedin-mobile__dropdown" data-submenu="js-loged-nav">
                            <div class="header-loggedin-mobile__item __header-loggedin-mobile-info">
                                <div class="header-loggedin-mobile-info__ico"><span class="fa fa-user-o"></span></div>
                                <div class="header-loggedin-mobile-info__desc">
                                    <span class="header-loggedin-mobile-info__name"></span>
                                    <a href="#" class="header-loggedin-mobile-info__link">{{ Auth::user()->email }}</a>
                                </div>
                            </div>
                            <a href="{{route('profile')}}" class="header-loggedin-mobile__item">
                                <span class="header-loggedin-mobile__item-text">
                                  {{ trans('widget.bet') }}
                                </span>
                                <span class="header-loggedin-mobile__item-ico">
                                  <span class="fa fa-street-view"></span>
                                </span>
                            </a>
                            <a href="{{ route('profile.mylibrary') }}" class="header-loggedin-mobile__item __active">
                                <span class="header-loggedin-mobile__item-text">
                                  {{ trans('widget.library') }}
                                </span>
                                <span class="header-loggedin-mobile__item-ico">
                                  <span class="fa fa-leanpub"></span>
                                </span>
                            </a>
                            <a href="{{ route('profile.myquote') }}" class="header-loggedin-mobile__item">
                                <span class="header-loggedin-mobile__item-text">
                                  {{ trans('profile.quote.title') }}
                                </span>
                                <span class="header-loggedin-mobile__item-ico">
                                  <span class="fa fa-quote-right"></span>
                                </span>
                            </a>
                            {{--<a href="settinginner.html" class="header-loggedin-mobile__item">
                                <span class="header-loggedin-mobile__item-text">
                                  Настройки
                                </span>
                                <span class="header-loggedin-mobile__item-ico">
                                  <span class="fa fa-cog"></span>
                                </span>
                            </a>--}}
                            <span class="header-loggedin-mobile__item">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="header-loggedin-mobile__item-text">
                                  {{ trans('other.exit') }}
                                </a>
                                <span class="header-loggedin-mobile__item-ico">
                                  <span class="fa fa-share-square-o"></span>
                                </span>
                              </span>
                        </div>
                    </div>
                @endif
                <span class="fa fa-bars header-burger js-burger"  data-submenu-open="js-nav"></span>
            </div>
        </div>
    </header>
    @yield('content')
    @if(empty($exception) && Route::currentRouteName() != 'terror')
        @include('layouts.footer')
    @endif
    @includeWhen(Auth::guest(), 'layouts.login_register_modal')
    <a href="#top" class="top-link top" id="tp"></a>
</main>
@if (session('status'))
    <div class="push">
        <div class="push-item _active">
            <span class="push-img"></span>
            <span class="push-text">
              {!!  session('status') !!}
            </span>
        </div>
        <div class="push-item">
            <span class="push-img"></span>
            <span class="push-text">
              {!!  session('status') !!}
            </span>
        </div>
    </div>
@endif
@if (session('error'))
    <div class="push">
        <div class="push-item _active _error">
            <span class="push-ico fa fa-exclamation-circle"></span>
            <span class="push-text">
            {!!  session('error') !!}
          </span>
        </div>
        <div class="push-item">
            <span class="push-ico fa fa-exclamation-circle"></span>
            <span class="push-text">
              {!!  session('error') !!}
            </span>
        </div>
    </div>
@endif

<!-- End Block: Main Container -->

<script src="{{ asset('js/vendors.js') }}"></script>
<script src="{{ asset('js/app.min.js') }}"></script>
@if (isset($footer_js))
    <script>
		$(function() {
            {!! $footer_js !!}
		});
    </script>
@endif
@stack('scripts')
</body>

</html>
