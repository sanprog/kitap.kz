
<footer>
    <div class="container footer-container">
        <div class="footer-top">
            <div class="footer-links">
                <div class="footer-links__col">
                    <h3 class="footer-links-title">{{ trans('footer.akparat') }}</h3>
                    <span class="footer-links-item"><a href="{{ route('about') }}" class="footer-links-item__inner">{{ trans('page.about.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('toauthor') }}" class="footer-links-item__inner">{{ trans('page.toauthor.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('dictionary') }}" class="footer-links-item__inner">{{ trans('page.dict.title') }}</a></span>
                </div>
                <div class="footer-links__col">
                    <h3 class="footer-links-title">{{ trans('footer.faq') }}</h3>
                    <span class="footer-links-item"><a href="{{ route('partners') }}" class="footer-links-item__inner">{{ trans('other.partner') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('faq') }}" class="footer-links-item__inner">{{ trans('page.faq.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('contacts') }}" class="footer-links-item__inner">{{ trans('page.contact.title') }}</a></span>
                </div>
                <div class="footer-links__col __mobile">
                    <span class="footer-links-item"><a href="{{ route('about') }}" class="footer-links-item__inner">{{ trans('page.about.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('toauthor') }}" class="footer-links-item__inner">{{ trans('page.toauthor.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('dictionary') }}" class="footer-links-item__inner">{{ trans('page.dict.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('partners') }}" class="footer-links-item__inner">{{ trans('other.partner') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('faq') }}" class="footer-links-item__inner">{{ trans('page.faq.title') }}</a></span>
                    <span class="footer-links-item"><a href="{{ route('contacts') }}" class="footer-links-item__inner">{{ trans('page.contact.title') }}</a></span>
                    <span class="footer-links-item">
                        <span class="footer-app-item__text">{{ trans('footer.kitap') }}</span>
                        <a target="_blank" href="https://itunes.apple.com/kz/app/audiokitap/id1051643562?mt=8" class="footer-app-item__link footer-app-apple"></a>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.ionicframework.kitapkcell" class="footer-app-item__link footer-app-google"></a>
                    </span>
                    <span class="footer-links-item">
                        <span class="footer-app-item__text">{{ trans('footer.audiokitap') }}</span>
                        <a target="_blank" href="https://itunes.apple.com/kz/app/kitap/id1046439139?mt=8" class="footer-app-item__link footer-app-apple"></a>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.bmg.audiokitap" class="footer-app-item__link footer-app-google"></a>
                    </span>
                </div>
                <div class="footer-app">
                    <div class="footer-app-item">
                        <span class="footer-app-item__text">{{ trans('footer.kitap') }}</span>
                        <a target="_blank" href="https://itunes.apple.com/kz/app/audiokitap/id1051643562?mt=8" class="footer-app-item__link footer-app-apple"></a>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.ionicframework.kitapkcell" class="footer-app-item__link footer-app-google"></a>
                    </div>
                    <div class="footer-app-item">
                        <span class="footer-app-item__text">{{ trans('footer.audiokitap') }}</span>
                        <a target="_blank" href="https://itunes.apple.com/kz/app/kitap/id1046439139?mt=8" class="footer-app-item__link footer-app-apple"></a>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.bmg.audiokitap" class="footer-app-item__link footer-app-google"></a>
                    </div>
                </div>
            </div>
            <div class="footer-subscribe">
                <div class="footer-email">
                    <form action="{{ route("email.save") }}" method="POST" >
                        {{ csrf_field() }}
                        <input type="email" class="footer-email__input" name="email" placeholder="Email">
                        <button type="submit" class="footer-email__btn">{{ trans('button.subscribe') }}</button>
                    </form>
                </div>
                <span class="footer-subscribe-text">{!! trans('footer.subscribe') !!}</span>
            </div>
        </div>
        <div class="footer-bottom">
            <span class="footer-copy">{!! trans('footer.copy') !!}</span>
            <div class="footer-social">
                <a target="_blank" href="https://vk.com/club54845108" class="footer-ico"><span class="footer-ico__img fa fa-vk"></span></a>
                <a target="_blank" href="https://www.facebook.com/kitap.kz/" class="footer-ico"><span class="footer-ico__img fa fa-facebook"></span></a>
                <a target="_blank" href="https://www.instagram.com/kitap_kz/" class="footer-ico"><span class="footer-ico__img fa fa-instagram"></span></a>
            </div>
        </div>
    </div>
</footer>
