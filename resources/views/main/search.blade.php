@extends('layouts.app')

@section('headtitle')
    <title>{{ request('q') }}</title>
@endsection

@section('searchLine')
    <section class="search">
        <div class="search-container">
            <form action="{{ route('search') }}" method="get">
                <input type="text" name="q" value="{{ request('q') }}" class="search-input" placeholder="Kitap.kz - тен терең іздеу">
                <button type="submit" class="search-ico">
                    <span class="fa fa-search search-ico__img"></span>
                </button>
                
            </form>
        </div>
    </section>
@endsection

@if($books->isEmpty() && $audiobooks->isEmpty() && $musics->isEmpty())
    @section('content')
        @yield('searchLine')
        <section class="searchresult __no-result __active">
            <div class="no-result__block">
                <img src="images/glass.png" class="no-result__img" alt="">
                <span class="no-result__text">{{ trans('search.empty') }}</span>
            </div>
        </section>
    @endsection
@endif

@section('content')

    @yield('searchLine')

<section class="searchresult">
    <div class="searchresult-top">
        <ul class="tabs searchresult-top__tabs">
            @if($books->isNotEmpty())<li class="tab-link {{ empty(request('curr'))  ? 'current': request('curr')=='book' ? 'current':'' }} searchresult__tabs-item" data-tab="tab-2">{{ trans('search.book') }}</li>@endif
            @if($audiobooks->isNotEmpty())<li class="tab-link {{ !empty(request('curr')) ? request('curr') == 'audio' ? 'current':'':'' }} searchresult__tabs-item" data-tab="tab-3">{{ trans('search.audiobook') }}</li>@endif
            @if($musics->isNotEmpty())<li class="tab-link {{ !empty(request('curr')) ? request('curr') == 'music' ? 'current' :'':'' }} searchresult__tabs-item" data-tab="tab-4">{{ trans('search.music') }}</li>@endif
            @if($videos->isNotEmpty())<li class="tab-link {{ !empty(request('curr')) ? request('curr') == 'video' ? 'current':'':'' }} searchresult__tabs-item" data-tab="tab-5">{{ trans('search.video') }}</li>@endif
            @if($authors->isNotEmpty())<li class="tab-link {{ !empty(request('curr')) ? request('curr') == 'author' ? 'current':'':'' }} searchresult__tabs-item" data-tab="tab-6">{{ trans('search.author') }}</li>@endif
            @if($dictors->isNotEmpty())<li class="tab-link {{ !empty(request('curr')) ? request('curr') == 'dict' ? 'current':'':'' }} searchresult__tabs-item" data-tab="tab-7">{{ trans('search.dictor') }}</li>@endif
            {{--<li class="tab-link searchresult__tabs-item" data-tab="tab-8">Қолданушылар</li>--}}
        </ul>
        <select name="" id="" class="searchresult-select">
          @if($books->isNotEmpty())<option value="tab-2">Кітап</option>@endif
          @if($audiobooks->isNotEmpty())<option value="tab-3">Аудиокітап</option>@endif
          @if($musics->isNotEmpty())<option value="tab-4">Музыка</option>@endif
          @if($videos->isNotEmpty())<option value="tab-5">Видео</option>@endif
          @if($authors->isNotEmpty())<option value="tab-6">Авторлар</option>@endif
          @if($dictors->isNotEmpty())<option value="tab-7">Дикторлар</option>@endif
          {{--<option value="tab-8">Қолданушылар</option>--}}
        </select>
    </div>
    <div id="tab-1" class="tab-content searchresul-content current">
        {{--<div class="searchresult-tem">
        <span class="searchresult-title">
          Қолданушылар
          <span class="searchresult-title__number">2</span>
        </span>
            <div class="searchresult-user">
                <img src="images/userfem.png" alt="" class="searchresult-user__img">
                <div class="searchresult-user__info">
                    <span class="searchresult-user__info-name">Али Алиев</span>
                    <div class="searchresult-user__action">
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-book"></span>
                            <span class="searchresult-user__action-text">90 кітап</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-microphone"></span>
                            <span class="searchresult-user__action-text">90 аудиокітап</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-music"></span>
                            <span class="searchresult-user__action-text">90 музыка</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-youtube-play"></span>
                            <span class="searchresult-user__action-text">90 видео</span>
                        </div>
                    </div>
                </div>
                <span class="searchresult-user__status">online</span>
            </div>
        </div>--}}
    </div>
    <div id="tab-2" class="tab-content searchresul-content {{ empty(request('curr'))  ? 'current': request('curr')=='book' ? 'current':'' }}">
        @if($books->isNotEmpty())
            <div class="searchresult-tem">
        <span class="searchresult-title">
          {{ trans('search.book') }}
          <span class="searchresult-title__number">{{ $books->total() }}</span>
        </span>
                @foreach($books as $book)
                    <div class="searchresult-book">
                        <img src="{{ $book->img_path or '/images/book.png'  }}" alt="" class="searchresult-book__img">
                        <div class="searchresult-book__info">
                            <div class="searchresult-book__info-top">
                                <span class="searchresult-book__info-author">@foreach ($book->authors as $autor) {{ $autor->name }} @endforeach</span>
                                <a href="{{ route('books.show', ['slug' => $book->slug]) }}"><span class="searchresult-book__info-name">{{ $book->name }}</span></a>
                            </div>
                            <!--
                            <div class="searchresult-book__action">
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-heart" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-plus-square" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-commenting" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                @endforeach
                {{ $books->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'book'])->links('pagination.default') }}
            </div><!--item-->

        @endif
    </div>
    <div id="tab-3" class="tab-content searchresul-content {{ !empty(request('curr')) ? request('curr') == 'audio' ? 'current':'':'' }}">
        @if($audiobooks->isNotEmpty())
            <div class="searchresult-tem">
        <span class="searchresult-title">
          {{ trans('search.audiobook') }}
          <span class="searchresult-title__number">{{ $audiobooks->total() }}</span>
        </span>
                @foreach($audiobooks as $audiobook)
                    <div class="searchresult-book">
                        <img src="{{ $audiobook->book->img_path or '/images/book.png'  }}" alt="" class="searchresult-book__img">

                        <div class="searchresult-book__info">
                            <div class="searchresult-book__info-top">
                                <span class="searchresult-book__info-author">@foreach ($audiobook->book->authors as $autor) {{ $autor->name }} @endforeach</span>
                                <a href="{{ route('audiobooks.show', ['slug' => $audiobook->book->slug]) }}"><span class="searchresult-book__info-name">{{ $audiobook->book->name }}</span></a>
                            </div>
                            <!--
                            <div class="searchresult-book__action">
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-heart" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-plus-square" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                                <div class="searchresult-book__action-item">
                                    <span class="searchresult-book__action-ico fa fa-commenting" aria-hidden="true"></span>
                                    <span class="searchresult-book__action-text">20</span>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
            @endforeach<!--book-->

            </div><!--item-->
            {{ $audiobooks->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'audio'])->links('pagination.default') }}
        @endif
    </div>
    <div id="tab-4" class="tab-content searchresul-content {{ !empty(request('curr')) ? request('curr') == 'music' ? 'current' :'':'' }}">
        @if($musics->isNotEmpty())
            <div class="searchresult-tem">
        <span class="searchresult-title">
          {{ trans('search.music') }}
          <span class="searchresult-title__number">{{ $musics->total() }}</span>
        </span>
                <div class="searchresult-music">
                    @foreach($musics as $music)
                        <div class="searchresult-music__item">
                            <div class="searchresult-music__img">
                                <img src="/storage/{{ $music->img_path or '../images/music_bg.png' }}" alt="" class="searchresult-music__img-item">
                                <!--<span class="searchresult-music__ico"></span>-->
                            </div>
                            <div class="searchresult-music__info">
                                <div class="searchresult-book__info-top">
                                    <span class="searchresult-book__info-author">@foreach ($music->authors as $autor) {{ $autor->name }} @endforeach</span>
                                    <a href="{{ route('music.show', ['slug' => $music->slug]) }}"><span class="searchresult-book__info-name">{{ $music->name }}</span></a>
                                </div>
                                <!--
                                <div class="searchresult-book__action">
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-heart" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-plus-square" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-commenting" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    @endforeach
                </div>
            </div><!--item-->
            {{ $musics->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'music'])->links('pagination.default') }}
        @endif
    </div>
    <div id="tab-5" class="tab-content searchresul-content" {{ !empty(request('curr')) ? request('curr') == 'video' ? 'current':'':'' }}>
        @if($videos->isNotEmpty())
            <div class="searchresult-tem">
        <span class="searchresult-title">
          {{ trans('search.video') }}
          <span class="searchresult-title__number">{{ $videos->total() }}</span>
        </span>
                <div class="searchresult-video">
                    @foreach($videos as $video)

                        <div class="searchresult-video__item">
                            <div class="searchresult-video__img" style="background-image: url(/storage/{{ $video->img_path }});background-size: cover;">
                                <span class="searchresult-video__ico"></span>
                            </div>
                            <div class="searchresult-video__info">
                                <span class="searchresult-video__info-name"></span>
                                <a href="{{ route('video.show', ['slug' => $video->slug]) }}"><span class="searchresult-video__info-name">{{ $video->name }}</span></a>
                                <!--
                                <div class="searchresult-book__action">
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-heart" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-plus-square" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                    <div class="searchresult-book__action-item">
                                        <span class="searchresult-book__action-ico fa fa-commenting" aria-hidden="true"></span>
                                        <span class="searchresult-book__action-text">20</span>
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>
                    @endforeach
                </div><!--video-item-->
            </div><!--item-->
            {{ $videos->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'video'])->links('pagination.default') }}
        @endif
    </div>
    <div id="tab-6" class="tab-content searchresul-content {{ !empty(request('curr')) ? request('curr') == 'author' ? 'current':'':'' }}">
        @if($authors->isNotEmpty())
            <div class="searchresult-tem">
                <span class="searchresult-title">
                  {{ trans('search.author') }}
                  <span class="searchresult-title__number">{{ $authors->total() }}</span>
                </span>
                <div class="searchresult-author">
                    @foreach($authors as $author)
                        <div class="searchresult-video__item">
                            <img src="/storage/{{ $author->img_path or '../images/book.png' }}" class="searchresult-autor__img" style="max-height: 150px">
                            <div class="searchresult-author__info">
                                <a href="{{ route('author.show', ['author' => $author->id]) }}" class="searchresult-autor__name">{{ $author->name }}</a>
                                <span class="searchresult-author__books"><span class="fa fa-book searchresult-author__books-ico"></span>{{ count($author->books) }} кітап</span>
                            </div>
                        </div>
                    @endforeach
                </div><!--author-item-->
            </div><!--item-->
            {{ $authors->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'author'])->links('pagination.default') }}
        @endif
    </div>
    <div id="tab-7" class="tab-content searchresul-content {{ !empty(request('curr')) ? request('curr') == 'dict' ? 'current':'':'' }}">
        @if($dictors->isNotEmpty())
            <div class="searchresult-tem">
        <span class="searchresult-title">
          {{ trans('search.dictor') }}
          <span class="searchresult-title__number">{{ $dictors->total() }}</span>
        </span>
                <div class="searchresult-author">
                    @foreach($dictors as $dictor)
                        <div class="searchresult-video__item">
                            <div class="searchresult-author__info">
                                <span class="searchresult-autor__name">{{ $dictor->dictor_name }}</span>
                               <!-- <span class="searchresult-author__books"><span class="fa fa-microphone searchresult-author__books-ico"></span>90 кітап</span>-->
                            </div>
                        </div>
                    @endforeach
                </div><!--author-item-->
            </div><!--item-->

            {{ $dictors->appends(Illuminate\Support\Facades\Input::all())->appends(['curr' => 'dict'])->links('pagination.default') }}
        @endif
    </div>
    {{--<div id="tab-8" class="tab-content searchresul-content">
        <div class="searchresult-tem">
        <span class="searchresult-title">
          Қолданушылар
          <span class="searchresult-title__number">2</span>
        </span>
            <div class="searchresult-user">
                <img src="images/userfem.png" alt="" class="searchresult-user__img">
                <div class="searchresult-user__info">
                    <span class="searchresult-user__info-name">Али Алиев</span>
                    <div class="searchresult-user__action">
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-book"></span>
                            <span class="searchresult-user__action-text">90 кітап</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-microphone"></span>
                            <span class="searchresult-user__action-text">90 аудиокітап</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-music"></span>
                            <span class="searchresult-user__action-text">90 музыка</span>
                        </div>
                        <div class="searchresult-user__action-item">
                            <span class="searchresult-user__action-ico fa fa-youtube-play"></span>
                            <span class="searchresult-user__action-text">90 видео</span>
                        </div>
                    </div>
                </div>
                <span class="searchresult-user__status">online</span>
            </div>
        </div>
    </div>--}}
    </div>
</section>
@endsection
