@extends('layouts.app')

@section('content')

    @includeWhen($slider, 'main.section.slider')

    @include('main.section.popular')

    @include('main.section.categories')

    @include('main.section.popaudio')

    @include('main.section.translate')

    @includeWhen(count($actual), 'main.section.actual')

    @includeWhen(count($quotes), 'main.section.quote')

@endsection

