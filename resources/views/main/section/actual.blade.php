<section class="actual">
    <div class="container">
        <h3 class="actual-title">
            <span class="actual-title__ico fa fa-bookmark"></span>{{ trans('main.actual') }}
        </h3>
        <div class="actual-row">
            @foreach($actual as $item)
            <div class="actual-item" style="background-image: url('/storage/{{$item->img_path}}')">
                <span class="actual-item__desc">{{ $item->title }}</span>
                <div class="actual-item__info">
                    <a href="{{$item->uri}}" class="actual-info__btn">{{ trans('button.tolgyrak') }}</a>
                    <span class="actual-info__text">{{ $item->title }}</span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>