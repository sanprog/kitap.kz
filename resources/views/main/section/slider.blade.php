

<section class="infoSlider">
    <div class="infoSliderContainer">
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image:url('/images/2.jpg');">
            </div>
        </div>
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image:url('/images/3.jpg');">
            </div>
        </div>
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image:url('/images/4.jpg');">
            </div>
        </div>
    </div>
    <div class="infoSlider-icons">
        <div class="container">
            <span class="infoSlider-icon icons-arrow-left" id="infoSlider-icon__next"></span>
            <span class="infoSlider-icon icons-arrow-right" id="infoSlider-icon__prev"></span>
        </div>
    </div>
</section>
<section class="infoSlider-mobile">
    <div class="infoSliderContainer">
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image: url('/images/1.jpg');"></div>
        </div>
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image: url('/images/2.jpg');"></div>
        </div>
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image: url('/images/3.jpg');"></div>
        </div>
        <div class="infoSlider-item">
            <div class="infoSlider-bg" style="background-image: url('/images/4.jpg');"></div>
        </div>
    </div>
</section>
