<section class="popaudio">
    <div class="container">
        <h3 class="popaudio-title"><span class="popaudio-title__ico fa fa-headphones"></span>{{ trans('main.popaudio') }}
        </h3>
        <div class="popaudio-row">
            @foreach($popularAudiobooks as $item)
                <div class="popular-item __hover-item">
                    <div class="__relative">
                      <img src="{{$item->book->img_path or '../images/book.png' }}" alt="" class="popaudio-item__img">
                      <div class="hover-img__text">
                        <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                        </a>
                      </div>           
                    </div>
                    <span class="studies-item__title">{{ $item->book->name }}</span>
                    <span class="studies-item__text">{{ $item->book->authors[0]->name or '' }}</span>
                </div>
            @endforeach
        </div>
        <div class="popaudio-row __mobile js-popaudio-slider owl-carousel owl-theme">
            @foreach($popularAudiobooks as $item)
                <div class="item" style="width:100%;">
                    <a href="{{ route('audiobooks.show', ['slug' => $item->book->slug]) }}" class="popaudio-item">
                        <img src="{{$item->book->img_path or '../images/book.png' }}" alt="" class="popaudio-item__img" style="height:15rem;">
                        <span class="popaudio-item__title">{{ $item->book->name }}</span>
                        <span class="popaudio-item__text">{{ $item->book->authors[0]->name or '' }}</span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>
