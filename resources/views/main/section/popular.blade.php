<section class="popular">
    <div class="container">
        <h3 class="popular-title"><span class="popular-title__ico typcn typcn-book"></span>{{ trans('main.popular') }}</h3>
        <div class="popular-row">
            @foreach($popularBooks as $item)
            <div class="popular-item __hover-item">
              <div class="__relative">
                <img src="{{ $item->img_path or '/images/book.png' }}" alt="" class="popular-item__img">
                <div class="hover-img__text">
                  <a href="{{ route('books.show', ['slug' => $item->slug]) }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                  </a>
                </div>  
              </div>
                <span class="studies-item__title">{{ $item->name }}</span>
                <span class="studies-item__text">
                @foreach($item->authors as $author)
                    {{ $author->name }}
                @endforeach
                </span>
            </div>
            @endforeach
        </div>
        <div class="popular-row __mobile js-popular-slider owl-carousel owl-theme">
            @foreach($popularBooks as $item)
            <div class="popular-item" style="width:100%;">
                <a href="{{ route('books.show', ['slug' => $item->slug]) }}">
                    <img src="{{$item->img_path or '../images/book.png' }}" alt="" class="popular-item__img" style="height:15rem;">
                    <span class="popular-item__title">{{ $item->name }}</span>
                    <span class="popular-item__text">{{ $item->authors[0]->name or "" }}</span>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
