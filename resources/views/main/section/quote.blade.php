<section class="quote">
    <div class="container quote-container">
        <h3 class="quote-title"><span class="fa fa-quote-left quote-title__ico"></span>{!! trans('main.quote') !!}</h3>
        <div class="quote-block">
            <div class="quote-slider">
                @foreach($quotes as $quote)
                <div class="quote-item">
                    <div class="quote-item-inner">
                        <div class="quote-avatar">
                            <img src="{{ $quote->book->img_path }}" alt="" class="quote-avatar__img">
                            <a href="{{ route('books.show', $quote->book->slug) }}" class="quote-avatar__link">
                            <span class="quote-avatar__btn-text">
                              {!! trans('button.read') !!}
                            </span>
                                <span class="quote-avatar__btn-ico">
                              <span class="quote-avatar__btn-ico__img fa fa-plus"></span>
                            </span>
                            </a>
                        </div>
                        <div class="quote-item-info">
                            <span class="fa fa-quote-left quote-avatar__ico"></span>
                            <h3 class="quote-item__name">{{ $quote->book->name }}</h3>
                            <span class="quote-item__author"></span>
                            <p class="quote-item__text">{{ $quote->fragment }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <span class="ico icons-quote__left"></span>
            <span class="ico icons-quote__right"></span>
        </div>
    </div>
</section>