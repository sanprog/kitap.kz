<section class="categories">
    <div class="container">
        <h3 class="categories-title"><span class="categories-title__ico fa fa-th-large" aria-hidden="true"></span>{{ trans('main.categories.title') }}</h3>
        <div class="categories-row">
            <a href="{{ route('video') }}" class="categories-item">
                <div class="categories-item__img">
                    <span class="categories-item__img-ico ico icons-video"></span>
                </div>
                <span class="categories-item__title">{{ trans('main.categories.videot') }}</span>
                <span class="categories-item__text">
                  {{ trans('main.categories.videod') }}
                </span>
            </a>
            <a href="{{ route('music') }}" class="categories-item">
                <div class="categories-item__img">
                    <span class="categories-item__img-ico ico icons-music"></span>
                </div>
                <span class="categories-item__title">{{ trans('main.categories.musict') }}</span>
                <span class="categories-item__text">
                  {!!  trans('main.categories.musicd')  !!}
                </span>
            </a>
            <a href="{{ route('books') }}" class="categories-item">
                <div class="categories-item__img">
                    <span class="categories-item__img-ico ico icons-kitap"></span>
                </div>
                <span class="categories-item__title">{{ trans('main.categories.bookt') }}</span>
                <span class="categories-item__text">
                  {{ trans('main.categories.bookd') }}
                </span>
            </a>
            <a href="{{ route('audiobooks') }}" class="categories-item">
                <div class="categories-item__img">
                    <span class="categories-item__img-ico ico icons-audiobook"></span>
                </div>
                <span class="categories-item__title">{{ trans('main.categories.audiobookt') }}</span>
                <span class="categories-item__text">
                          {{ trans('main.categories.audiobookd') }}
                </span>
            </a>
            {{--<a href="#" class="categories-item">
                <div class="categories-item__img">
                    <span class="categories-item__img-ico ico icons-community"></span>
                </div>
                <span class="categories-item__title">Қауымдастық</span>
                <span class="categories-item__text">
                    Қызықты қауымдастық әр адамның ой-пікірі мен жекпе-жегімен бөліседі
                    Қызықтан құр қалмаңыз!
                </span>
            </a>--}}
        </div>
        <div class="categories-slider">
            <div class="categories-slider-inner js-categories-slider owl-carousel owl-theme">
                <div class="item">
                    <a href="{{ route('video') }}" class="categories-item">
                        <div class="categories-item__img">
                            <span class="categories-item__img-ico ico icons-video"></span>
                        </div>
                        <span class="categories-item__title">{{ trans('main.categories.videot') }}</span>
                        <span class="categories-item__text">
                          {{ trans('main.categories.videod') }}
                        </span>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('music') }}" class="categories-item">
                        <div class="categories-item__img">
                            <span class="categories-item__img-ico ico icons-music"></span>
                        </div>
                        <span class="categories-item__title">{{ trans('main.categories.musict') }}</span>
                        <span class="categories-item__text">
                          {!! trans('main.categories.musicd') !!}
                        </span>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('books') }}" class="categories-item">
                        <div class="categories-item__img">
                            <span class="categories-item__img-ico ico icons-kitap"></span>
                        </div>
                        <span class="categories-item__title">{{ trans('main.categories.bookt') }}</span>
                        <span class="categories-item__text">
                          {{ trans('main.categories.bookd') }}
                        </span>
                    </a>
                </div>
                <div class="item">
                    <a href="{{ route('audiobooks') }}" class="categories-item">
                        <div class="categories-item__img">
                            <span class="categories-item__img-ico ico icons-audiobook"></span>
                        </div>
                        <span class="categories-item__title">{{ trans('main.categories.audiobookt') }}</span>
                        <span class="categories-item__text">
                                 {{ trans('main.categories.audiobookd') }}
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
