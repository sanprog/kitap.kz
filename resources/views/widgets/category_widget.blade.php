<h3 class="catalog-title">
    <span class="catalog-title__ico fa fa-align-left" aria-hidden="true"></span>{{ trans('main.categories.title') }}
</h3>
<ul class="catalog-menu-wrapper">
	<?
    $traverse = function ($categories, $prefix = '', $postfix = '', $level=1) use (&$traverse, $uriPrefix, $parentLine, $activeCategory) {

        echo $prefix;
        foreach ($categories as $category) {
			$isChildActive = false;
            $open = in_array($category->id, $parentLine) ? 'open' : '';
            if($category->children->isNotEmpty()) {
            	foreach ($category->children as $child) {
                    if($child->id == $activeCategory) {
						$isChildActive = true;
                    }
                }
				echo $isChildActive ? '<li class="catalog-menu__dropmenu is-active">' : '<li class="catalog-menu__dropmenu">';
            } else {
				echo ($category->id == $activeCategory) ? '<li class="is-active">' : '<li>';
            }
            echo ($category->id == $activeCategory) ? '<a class="is-active" href="'.$uriPrefix.'/'.$category->id.'">'.$category->name : '<a href="'.$uriPrefix.'/'.$category->id.'">'.$category->name;
            echo $category->children->isNotEmpty() ? '<span></span></a>' :'</a>';

            if($category->children->isNotEmpty()) {
            	echo $isChildActive ? '<ul class="catalog-menu__submenu" style="display:block">' : '<ul class="catalog-menu__submenu">';
            }


            $traverse($category->children, '', '', $level+1);
            echo ($category->children->isNotEmpty()) ? '</ul>' : '';
            echo '</li>';
        }
        echo $postfix;
    };

    $traverse($categoryTree);
	?>
</ul>
{{--<script>
    $('.catalog-item__icon').trigger('accordion.refresh');
</script>--}}



{{--<div class="catalog" data-accordion-group="">
    <div class="catalog-item __level1 __first accordion" data-accordion="">
        <a href="#" class="catalog-item__link" data-control="">
            Хронология бойынша таңдау</a>
        <div class="catalog-item__child" data-content=""
             style="max-height: 0px; overflow: hidden; transition: max-height 400ms ease;">
            <div class="catalog-item __level2 accordion open" data-accordion="">
                <a href="#" class="catalog-item__link" data-control="">Халық ауыз әдебиеті</a>
                <div class="catalog-item__child" data-content=""
                     style="max-height: 52px; overflow: hidden; transition: max-height 400ms ease;">
                    <div class="catalog-item __level3 accordion" data-accordion="">
                        <a href="#" class="catalog-item__link" data-control="">Ертегі</a>
                        <div class="catalog-item__child" data-content=""
                             style="max-height: 0px; overflow: hidden; transition: max-height 400ms ease;">
                            <div class="catalog-item __level4">
                                <a href="#" class="catalog-item__link">Батырлар жыры</a>
                            </div>
                            <div class="catalog-item __level4">
                                <a href="#" class="catalog-item__link">Лиро-эпостық жырлар</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="catalog-item __level2">
                <a href="#" class="catalog-item__link">Ежелгі дәуір әдебиеті</a>
            </div>
            <!-- lvl2 -->
        </div>
    </div>
    <div class="catalog-item __level1">
        <a href="#" class="catalog-item__link">
            Бабалар сөзі</a>
    </div>
    <div class="catalog-item __level1 __last">
        <a href="#" class="catalog-item__link">
            100 kitap </a>
    </div>
    <!-- lvl1 -->
</div>--}}




