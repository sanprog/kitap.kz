<div class="catalogmobile-genres" data-submenu="mobile-genres">
    <div class="catalogmobile-genres__inner">
        @foreach($genres as $ganre)
            @if(count($ganre->books) > 0)
                <a class="catalogmobile-genres-item {{ in_array($ganre->id, $g) ? '__active' : '' }}"
                    onclick="renderUrl({{ $ganre->id }})" style="cursor: pointer;"
                    data-id="{{ $ganre->id }}">{{ $ganre->name }}</a>
            @endif
        @endforeach

    </div>
</div>