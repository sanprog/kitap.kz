@if($book)
<aside class="choice">
    <h3 class="choice-title">
        <span class="choice-title__ico fa fa-fa"></span>{{ trans('widget.okyrman') }}
    </h3>
    <div class="choice-img">
        <div class="choice-img__title"><span class="choice-img__title-ico fa fa-book"></span>
            <span class="choice-img__title-text"><span class="_bold">1000 рет</span> оқылды</span>
        </div>
    </div>
    <div class="choice-item">
        <div class="choice-book">
            <img src="{{ $book->img_path }}"  alt="" class="choice-book__img">
            <div class="choice-book__rating">
                @for ($i = 1; $i <= 5; $i++)
                    <span class="choice-book__rating-ico fa fa-star {{ ($book->averageRating >= $i) ? '_active' : '' }}"></span>
                @endfor
            </div>
        </div>
        <div class="choice-desc">
            <h4 class="choice-desc__title">{{ $book->name }}</h4>
            <p class="choice-desc__text">
                {!! $book->description !!}
            </p>
        </div>
        <div class="choice-item__options">
            @if($book->aodiobook)
            <a href="{{ route('audiobooks.show', ['slug' => $book->slug]) }}" class="choice-options__btn">
                <span class="choice-options__btn-text">
                  {{ trans('button.tyndau') }}
                </span>
                <span class="choice-options__btn-ico">
                  <span class="choice-options-ico__img fa fa-headphones"></span>
                </span>
            </a>
            @endif
            <a href="{{ route('book.read', ['slug' => $book->slug]) }}" class="choice-options__btn __blue">
                <span class="choice-options__btn-text">
                  {{ trans('button.read') }}
                </span>
                <span class="choice-options__btn-ico">
          <span class="choice-options-ico__img fa fa-book"></span>
        </span>
            </a>
        </div>
    </div>
</aside>
@endif