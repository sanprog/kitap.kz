@if(count($model) > 0)
<section class="library">
    <h3 class="library-title">
        <span class="library-title__ico fa fa-user"></span>Менің кітапханам
    </h3>
    <div class="library-block">
        <div class="library-top">
            <div class="library-user">
                <img src="../images/user.png" alt="" class="library-user__img">
                <div class="library-user__text">
                    <span class="library-user__text-name">{{ $user->name }}</span>
                    <span class="library-user__text-email">{{ $user->email }}</span>
                </div>
            </div>
            <a href="{{ route('profile.mylibrary') }}" class="library-top__btn">
                <span class="library-top__btn-text">{{ trans('widget.bet') }}</span>
                <span class="library-top__btn-ico">
          <span class="library-top__ico-img fa fa-user"></span>
        </span>
            </a>
        </div>
        <div class="library-slider">
            <div class="library-carousel owl-carousel owl-theme">
                @foreach($model as $item)
                <div class="item">
                    <div class="library-item">
                        <div class="library-book">
                            <img src="{{ $item->img_path or '/images/book.png' }}" alt="" class="library-book__img">
                            <div class="library-book__text">
                                <a href="{{ route('books.show', ['slug' => $item->slug]) }}" class="library-book__text-btn">
                                    <span>{{ trans('button.otu') }}</span>
                                </a>
                            </div>
                        </div>
                        <div class="library-info">
                            <span class="library-info__author">{{ !$item->authors->isEmpty() ? implode($item->authors->pluck('name')->toArray(), ' ') : ''}}</span>
                            <span class="library-info__name">{{ $item->name }}</span>
                            {{--<div class="library-info__rating">
                                <span class="library-ingo__rating-text">Қанша оқылды?</span>
                                <div class="library-rating">
                                    <span class="library-rating__text">67%</span>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
