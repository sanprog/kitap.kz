<div class="filter-block animated js-filter-block">
    <div class="container">
        <div class="filter-block__container">
            @foreach($genres as $genre)
                @if(count($genre->books)> 0)
                     <a class="filter-item {{ in_array($genre->id, $g) ? '__active' : '' }}"
                         onclick="renderUrl({{ $genre->id }})" style="cursor: pointer;"
                         data-id="{{ $genre->id }}">{{ $genre->name }}</a>
                @endif
            @endforeach
        </div>
        <span class="filter-control js-filter">
        <span class="filter-control__ico"></span>
      </span>
    </div>
</div>

<script>
	function renderUrl(id) {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var g = url.searchParams.get("g");
		if(g === null){
			window.location.replace('?g='+id);
			return;
		}
		var genresList = g.split(',');
		var serchedId = jQuery.inArray(id.toString(), genresList);
		if(genresList[0] == ''){
			genresList[0] = id;
		}
		else if(serchedId === -1){
			genresList.push(id);
		}
		else {
			genresList.splice(serchedId, 1);
		}

		window.location.replace('?g='+genresList.join());
	}
</script>