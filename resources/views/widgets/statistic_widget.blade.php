<aside class="statistics">
    <h3 class="statistics-title">
        <span class="fa fa-pie-chart statistics-title__ico" aria-hidden="true"></span>Статистика
    </h3>
    <div class="statistics-item">
        <img src="/images/stat1.png" alt="" class="statistics-item__img">
        <div class="statistics-info">
            <span class="statistics-info__number">{{ $todayBooks }}</span>
            <span class="statistics-info__text">кітап бүгін оқылды</span>
        </div>
    </div>
    <img src="/images/line.png" alt="" class="statistics-line">
    <div class="statistics-item">
        <img src="/images/stat2.png" alt="" class="statistics-item__img">
        <div class="statistics-info">
            <span class="statistics-info__number">{{ $onlineUsers }}</span>
            <span class="statistics-info__text">онлайн</span>
        </div>
    </div>
    <img src="/images/line.png" alt="" class="statistics-line">
    <div class="statistics-item">
        <img src="/images/stat3.png" alt="" class="statistics-item__img">
        <div class="statistics-info">
            <span class="statistics-info__number">50 046</span>
            <span class="statistics-info__text">мобильді қосымша жүктелді</span>
        </div>
    </div>
</aside>
