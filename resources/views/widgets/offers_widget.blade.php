@if(count($offers) > 0)
<section class="offers">
    <h3 class="offers-title">
        <span class="offers-title__ico fa fa-thumbs-o-up"></span>{{ trans('widget.offer') }}
    </h3>
    <div class="offers-block">
        <div class="offers-slider">
            <div class="offers-carousel owl-carousel owl-theme">
                @foreach($offers as $item)

                <div class="item">
                    <div class="offers-item">
                        {{--<div class="offers-social">
                            <a href="#" class="ico icons-vkgray offers-social__ico"></a>
                            <a href="#" class="ico icons-fbgray offers-social__ico"></a>
                            <a href="#" class="ico icons-twgray offers-social__ico"></a>
                            <a href="#" class="ico icons-gplusgray offers-social__ico"></a>
                        </div>--}}
                        <div class="offers-item-top">
                            <div class="offers-book">
                                <img src="{{ $item->img_path or '../images/b3.png' }}" alt="" class="offers-book__img">
                                <div class="offers-book__rating">
                                    <span class="offers-book__rating-ico fa fa-star _active"></span>
                                    <span class="offers-book__rating-ico fa fa-star _active"></span>
                                    <span class="offers-book__rating-ico fa fa-star _active"></span>
                                    <span class="offers-book__rating-ico fa fa-star _active"></span>
                                    <span class="offers-book__rating-ico fa fa-star"></span>
                                </div>
                            </div>
                            <div class="offers-info">
                                <span class="offers-info__author">{{ !$item->authors->isEmpty() ? implode($item->authors->pluck('name')->toArray(), ' ') : ''}}</span>
                                <span class="offers-info__name">{{ $item->name }}</span>
                                <div class="offers-info__desc">{!! $item->description !!}</div>
                            </div>
                        </div>
                        <div class="offers-item__options">
                            @if($item->audiobook)
                            <a href="{{ route('audiobooks.show', ['slug' => $item->slug]) }}" class="offers-options__btn">
                                <span class="offers-options__btn-text">{{ trans('button.tyndau') }}</span>
                                <span class="offers-options__btn-ico">
                                    <span class="offers-options-ico__img fa fa-headphones"></span>
                                </span>
                            </a>
                            @endif
                            @if($item->file_path)
                                <a href="{{ route('book.read', ['slug' => $item->slug]) }}" target="_blank"
                                   class="offers-options__btn __blue">
                                    <span class="offers-options__btn-text">{{ trans('button.read') }}</span>
                                    <span class="offers-options__btn-ico">
                                      <span class="offers-options-ico__img fa fa-plus"></span>
                                    </span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
        </div>
    </div>
</section>
@endif
