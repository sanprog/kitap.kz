
<div class="catalogmobile-categories" data-submenu="mobile-catalog">
    <ul class="catalogmobile-wrapper">
    <?

    $traverse = function ($categories, $prefix = '', $postfix = '', $level=1) use (&$traverse, $uriPrefix, $parentLine) {

        echo $prefix;
        foreach ($categories as $category) {
            $open = in_array($category->id, $parentLine) ? 'open' : '';
            echo $category->children->isNotEmpty() ? '<li class="catalogmobile__dropmenu">' : '<li>';
            echo '<a href="'.$uriPrefix.'/'.$category->id.'">'.$category->name.'<span></span></a>';
            echo ($category->children->isNotEmpty()) ? '<ul class="catalogmobile__submenu">' : '';


            $traverse($category->children, '', '</li>', $level+1);
            echo ($category->children->isNotEmpty()) ? '</ul>' : '';
            echo '</li>';
        }
        echo $postfix;
    };
    $traverse($categoryTree);
    ?>
        <ul class="catalogmobile-wrapper">
</div>