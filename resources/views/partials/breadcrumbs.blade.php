@if (count($breadcrumbs))

    <div class="breadcrumbs">
        <div class="container">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <a href="{{ $breadcrumb->url }}" class="breadcrumbs-item">{{ $breadcrumb->title }}</a>
            @else
                    <a href="#" class="breadcrumbs-item _active">{{ $breadcrumb->title }}</a>
            @endif

        @endforeach
        </div>
    </div>

@endif
