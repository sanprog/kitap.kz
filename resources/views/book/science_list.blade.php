@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('book.gylymi') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('books.science') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('book.gylymi') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('books.science.list') !!}
    <div class="catalogmobile">
        <div class="catalogmobile-top">
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-catalog" style="width: 100%;cursor: pointer;">{{ trans('book.category') }}</span>
        </div>
        @widget('genreWidget',['type' => 2])

        @widget('categoryWidget',['type' => 2])
    </div>
    <div class="main-row">
        <div class="main-col-left">
            <!-- studies -->
            <section class="studies">
                <div class="studies-row">
                    @foreach($books as $item)
                        <div class="studies-item __hover-item">
                            <div class="studies-item__img __relative" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')">
                                <div class="hover-img__text">
                                    <a href="/books/{{ $item->slug }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                    </a>
                                </div>
                            </div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="studies-row __mobile">
                    @foreach($books as $item)
                    <div class="item" style="margin-top:1rem">
                        <a href="/books/{{ $item->slug }}" class="studies-item">
                            <div class="studies-item__img" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')"></div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </a>
                    </div>
                    @endforeach
                </div>
            </section>
            <!-- endstudies -->
            {{ $books->links('pagination.default') }}


        </div>
        <div class="main-col-right">

            @widget('categoryWidget')

            {{--@widget('reviewWidget')--}}

            @widget('statisticWidget')

            @widget('choiceWidget')


        </div>
    </div>
@endsection
