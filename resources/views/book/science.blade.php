@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('book.gylymi') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('book.gylymi') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('books.science') !!}
    <div class="catalogmobile">
        <div class="catalogmobile-top">
            <span class="catalogmobile-top__btn" data-submenu-open="mobile-catalog" style="width: 100%;cursor: pointer;">{{ trans('book.category') }}</span>
        </div>
        @widget('categoryWidget',['type' => 2])
    </div>
    <div class="main-row">
        <div class="main-col-left">
            <!-- studies -->
            <section class="studies">
                <h3 class="studies-title">
                    <span class="studies-title__ico fa fa-check" aria-hidden="true"></span>{{ trans('book.read_book') }}
                </h3>
                <div class="studies-row">
                    @foreach($popularBooks as $item)
                        <div class="studies-item __hover-item">
                            <div class="studies-item__img __relative" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')">
                                <div class="hover-img__text">
                                    <a href="/books/{{ $item->slug }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                    </a>
                                </div>
                            </div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="studies-row __mobile js-studies-slider owl-carousel owl-theme">
                    @foreach($popularBooks as $item)
                    <div class="item">
                        <a href="/books/{{ $item->slug }}" class="studies-item">
                            <div class="studies-item__img" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')"></div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </a>
                    </div>
                    @endforeach
                </div>
                {{--banners--}}
                @if($banner)
                    <a class="studie-book _liter" href="{{ $banner->uri }}" style="background-image: url('/storage/{{ $banner->img_path }}')">
                        <span class="actual-item__desc">{{ $banner->title }}</span>
                    </a>
                @endif
            </section>
            <!-- endstudies -->

            @widget('libraryWidget')

            @widget('offersWidget')


            <section class="new">
                <h3 class="new-title">
                    <span class="new-title__ico fa fa-check" aria-hidden="true"></span>{{ trans('book.new_book') }}
                </h3>
                <div class="studies-row">
                    @foreach($newestBooks as $item)
                        <div class="studies-item __hover-item">
                            <div class="studies-item__img __relative" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')">
                                <div class="hover-img__text">
                                    <a href="/books/{{ $item->slug }}" class="hover-img__text-btn">{{ trans('button.otu') }}
                                    </a>
                                </div>
                            </div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="studies-row __mobile js-studies-slider owl-carousel owl-theme">
                    @foreach($newestBooks as $item)
                    <div class="item">
                        <a href="/books/{{ $item->slug }}" class="studies-item">
                            <div class="studies-item__img" style="background-image:url('{{ $item->img_path or '/images/book.png' }}')"></div>
                            <span class="studies-item__title">{{ $item->name }}</span>
                            <span class="studies-item__text">
                                @foreach($item->authors as $author)
                                    {{ $author->name }}
                                @endforeach
                            </span>
                        </a>
                    </div>
                    @endforeach
                </div>
            </section>

        </div>
        <div class="main-col-right">

            @widget('categoryWidget')

            {{--@widget('reviewWidget')--}}

            @widget('statisticWidget')

            @widget('choiceWidget')


        </div>
    </div>
@endsection
