@extends('layouts.app')

@section('headtitle')
    <title>{{ $book->name }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ $book->name }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ $book->img_path or '/images/book.png' }}" />
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('books') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('book.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('books.show', $book) !!}

    <section class="bookitem">
        <div class="audioitem-top">
            {{--<div class="bookitem-social">
                <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="bookitem-social__ico">
                    <span class="bookitem-ico__img fa fa-vk"></span>
                </a>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="bookitem-social__ico">
                    <span class="bookitem-ico__img fa fa-facebook"></span>
                </a>
                <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="bookitem-social__ico">
                    <span class="bookitem-ico__img fa fa-google-plus"></span>
                </a>
                {{--<a href="#" class="bookitem-social__ico">
                    <span class="bookitem-ico__img fa fa-twitter"></span>
                </a>
            </div>--}}
            <div class="bookitem-boook">
                <img src="{{ $book->img_path or '/images/book.png' }}" alt="" class="bookitem-book__img">
                <span class="booitem-book__rating-text">{{ trans('book.rating') }} {{ round($book->averageRating, 1, PHP_ROUND_HALF_UP) }}/5</span>
                <div class="bookitem-book__rating">
                    @for ($i = 5; $i >= 1; $i--)
                        <span data-rank="{{ $i }}" class="setRank bookitem-book__rating-ico fa fa-star {{ ($book->averageRating >= $i) ? '_active' : '' }}" aria-hidden="true"></span>
                    @endfor
                </div>
                <div id="altRank" style="font-size: 10px; text-align: center; height: 10px;"></div>
                <form id="formRank" action="{{ route('book.set.rank', ['slug' => $book->slug]) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" id="inputRate" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="" data-size="xs">
                    <input type="hidden" name="id" required="" value="{{ $book->id }}">
                </form>
            </div>
            <div class="bookitem-info">
                <div class="bookitem-social">
                    <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="bookitem-social__ico">
                        <span class="bookitem-ico__img fa fa-vk"></span>
                    </a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="bookitem-social__ico">
                        <span class="bookitem-ico__img fa fa-facebook"></span>
                    </a>
                    <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="bookitem-social__ico">
                        <span class="bookitem-ico__img fa fa-google-plus"></span>
                    </a>
                </div>
                    @if(!$book->authors->isEmpty())
                        <div class="bookitem-info-header" style="width:max-content">
                            <span class="bookitem-info-header__desc"><span class="fa fa-pencil"></span> {{ trans('author.title') }}</span>
                            @foreach($book->authors as $author)
                                <a class="bookitem-info__author" href="{{ route('author.show', ['author' => $author->id]) }}" style="display:inline-block">{{ $author->name }}</a>
                            @endforeach
                        </div>
                    @endif
                    <h3 class="bookitem-info__name">{{ $book->name }}</h3>
                    <div class="bookitem-info__desc">{!! $book->description !!}</div>
                    @if($book->audiobook)
                    <div class="bookitem-item__options">
                        <a href="{{ route('audiobooks.show', ['slug' => $book->slug]) }}" class="bookitem-options__btn">
                            <span class="bookitem-options__btn-text">{{ trans('button.tyndau') }}</span>
                            <span class="bookitem-options__btn-ico">
                                <span class="bookitem-options-ico__img fa fa-headphones"></span>
                            </span>
                        </a>
                    </div>
                    @endif
                    <div class="bookitem-item__options">
                        @if (!Auth::guest())
                            @if(!$library_books)
                                <a href="{{ route('book.savetolib', ['id' => $book->id, 'type' => \App\Model\Library::TYPE_BOOK]) }}" class="bookitem-options__btn __blue">
                                    <span class="bookitem-options__btn-text">{{ trans('button.add') }}</span>
                                    <span class="bookitem-options__btn-ico">
                                        <span class="bookitem-options-ico__img fa fa-plus"></span>
                                    </span>
                                </a>
                            @else
                                <a href="{{ route('profile.deleteitem', ['type' => $library_books->resource_type, 'id' => $library_books->resource_id]) }}" class="bookitem-options__btn __blue">
                                    <span class="bookitem-options__btn-text">{{ trans('button.sub') }}</span>
                                    <span class="bookitem-options__btn-ico">
                                        <span class="bookitem-options-ico__img fa fa-minus"></span>
                                    </span>
                                </a>
                            @endif
                        @endif
                        @if($book->file_path)
                        <a target="_blank" href="{{ route('book.read', ['slug' => $book->slug]) }}" class="bookitem-options__btn __blue">
                            <span class="bookitem-options__btn-text">{{ trans('button.read') }}</span>
                            <span class="bookitem-options__btn-ico">
                                <span class="bookitem-options-ico__img fa fa-book"></span>
                            </span>
                        </a>
                        @endif
                    </div>
                    {{--<div class="socila-info">
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-heart" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-plus-square" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                        <div class="social-info__item">
                            <span class="social-info__item-ico fa fa-commenting" aria-hidden="true"></span>
                            <span class="social-info__item-text">20</span>
                        </div>
                    </div>--}}
            </div>
        </div>
        <div class="bookitem-tags">
            <div class="bookitem-tags-container">
                @foreach($book->genre as $genre)
                    <a  href="{{ route('books.public') }}?g={{ $genre->id }}" class="bookitem-tags__item">
                       {{ $genre->name }}
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    @widget('commentWidget',['id' => $book->id, 'type' => 'book'])
@endsection

@push('scripts')
<script>
	$(function() {
        $('.bookitem-book__rating').on('click', '.setRank', function() {
            $('#inputRate').val($(this).data('rank'));
            $('#formRank').submit();
		});
		/*$( '.setRank' )
            .mouseenter( function() {
            	$('#altRank').html('Проголосовать ' + $(this).data('rank'));

		} )
            .mouseleave( function() {
				$('#altRank').html('');
		} );*/
	});
</script>
@endpush