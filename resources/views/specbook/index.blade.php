@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.specbook.title') }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ trans('project.specbook.title') }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ '/images/sp3.png' }}" />
@endsection

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _sb">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.specbook.title') }}</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.specbook') !!}

    <section class="specbook">
        <div class="specbook-top">
            <div class="specbook-top__img">
                <img src="/images/specbooklogo.png" alt="" class="specbook-top__img-logo">
            </div>
            <div class="specbook-top__text">
                <div class="specbook-social">
                    <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-vk"></span></a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-facebook"></span></a>
                    <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-google-plus _small"></span></a>
                    {{--<a href="#" class="specbook-ico"><span class="specbook-ico__img fa fa-twitter"></span></a>--}}
                </div>
                {!! trans('project.specbook.desc') !!}
            </div>
        </div>
        <div class="specbook-bottom">
            @foreach($items as $item)
            <div class="specbook-item">
                <div class="specbook-item-top">
                    <div class="specbook-item-img">
                        <div class="specbook-img__logo" style="background-image: url('{{ $item->img_path }}')"></div>
                    </div>
                    <div class="specbook-item-info">
                        <div class="specbook-item-info__top">
                            <span class="specbook-item-info__author"></span>
                            <span class="specbook-item-info__name">
                              {{ $item->name }}
                            </span>
                        </div>
                        <a href="{{ route('book.readspec', ['slug' => $item->slug])}}" class="specbook-options__btn">
                            <span class="specbook-options__btn-text">
                              Оқу
                            </span>
                            <span class="specbook-options__btn-ico">
                              <span class="specbook-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
                <a href="{{ route('book.readspec', ['slug' => $item->slug])}}" class="specbook-options__btn __mobile">
                    <span class="specbook-options__btn-text">
                      Оқу
                    </span>
                    <span class="specbook-options__btn-ico">
                      <span class="specbook-options-ico__img fa fa-plus"></span>
                    </span>
                </a>
            </div>
            @endforeach
            <button data-url="{{ $items->nextPageUrl() }}" type="button" name="button" class="specbook-more">Тағы да жүктеу <span class="fa fa-angle-double-down"></span></button>
        </div>
    </section>
@endsection

@push('scripts')
<script>

	$(document).ready(function() {
		$(document).on('click', '.specbook-more', function (e) {

			$.ajax({
				url : $(this).data('url'),
				success: function(response){
					console.log(response);
					$('.specbook-more').remove();
					$('.specbook-bottom').append(response);

				},
				error: function(response) {}
			});
		});
	});

</script>
@endpush
