 @foreach($items as $item)
            <div class="specbook-item">
                <div class="specbook-item-top">
                    <div class="specbook-item-img">
                        <div class="specbook-img__logo" style="background-image: url('{{ $item->img_path }}')"></div>
                    </div>
                    <div class="specbook-item-info">
                        <div class="specbook-item-info__top">
                            <span class="specbook-item-info__author">Әлімхан Ермеков</span>
                            <span class="specbook-item-info__name">
                              Ұлы математика курсы
                              "Қазақстан" баспасы, 1935
                            </span>
                        </div>
                        <a href="{{ route('book.readspec', ['slug' => $item->slug])}}" class="specbook-options__btn">
                            <span class="specbook-options__btn-text">
                              Оқу
                            </span>
                            <span class="specbook-options__btn-ico">
                              <span class="specbook-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
                <a href="{{ route('book.readspec', ['slug' => $item->slug])}}" class="specbook-options__btn __mobile">
                    <span class="specbook-options__btn-text">
                      Оқу
                    </span>
                    <span class="specbook-options__btn-ico">
                      <span class="specbook-options-ico__img fa fa-plus"></span>
                    </span>
                </a>
            </div>
            @endforeach
 @if (!$items->onFirstPage())
            <button  data-url="{{ $items->nextPageUrl() }}" type="button" name="button" class="specbook-more">Тағы да жүктеу <span class="fa fa-angle-double-down"></span></button>
     @endif