@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('book.author') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('author.show', $model->name) !!}

    <section class="author">
        <div class="author-top">
            <div class="author-img">
                <img src="/storage/{{ $model->img_path or '../images/author_bg.png' }}" alt="" class="author-img__item">
            </div>
            <div class="autor-info">
                <h1 class="autor-info__title">
                    {{ $model->name }}
                </h1>
                {!! $model->description !!}
            </div>
        </div>
        <div class="author-bottom">
            <div class="author-tab__top">
                <ul class="tabs author-top__list">
                    @if(count($model->books) > 0)
                    <li class="tab-link author-top__item current" data-tab="author-1">
                        <span class="fa fa-book author-top__item-ico"></span> {{ trans('search.book') }} <span class="author-top__item-count">{{ count($model->books) }}</span>
                    </li>
                    @endif
                    @if(count($model->audiobooks) > 0)
                    <li class="tab-link author-top__item" data-tab="author-2">
                        <span class="fa fa-microphone author-top__item-ico"></span>  {{ trans('search.audiobook') }} <span class="author-top__item-count">3</span>
                    </li>
                    @endif
                    @if(count($model->music) > 0)
                    <li class="tab-link author-top__item" data-tab="author-3">
                        <span class="fa fa-music author-top__item-ico"></span> {{ trans('music.title') }} <span class="author-top__item-count">3</span>
                    </li>
                    @endif
                </ul>
            </div>
            <div id="author-1" class="tab-content author-content current">
                <div class="author-row">
                    <div class="author-book">
                        @foreach($books as $book)
                        <a href="{{ route('books.show', ['slug' => $book->slug]) }}" class="author-book__item">
                            <img src="{{$book->img_path}}" alt="" class="author-book__item-img">
                            <span class="author-book__name">{{ $book->name }}</span>
                        </a>
                        @endforeach
                    </div>
                    {{ $books->links('pagination.default') }}
                </div>
            </div>
            <div id="author-2" class="tab-content author-content">
                <div class="author-row">
                    <div class="author-book">
                        <a href="#" class="author-book__item">
                            <img src="/images/s1.png" alt="" class="author-book__item-img">
                            <span class="author-book__name">{{ trans('book.biography') }}</span>
                        </a>
                        <a href="#" class="author-book__item">
                            <img src="/images/s3.png" alt="" class="author-book__item-img">
                            <span class="author-book__name">{{ trans('book.biography') }}</span>
                        </a>
                        <a href="#" class="author-book__item">
                            <img src="/images/s4.png" alt="" class="author-book__item-img">
                            <span class="author-book__name">{{ trans('book.biography') }}</span>
                        </a>
                    </div>
                </div>
            </div>
            <div id="author-3" class="tab-content author-content">
                <div class="author-row">
                    <div class="author-music">
                        <a href="#" class="author-music__item">
                            <div class="author-music__img">
                                <img src="/images/search1.png" alt="" class="author-music__img-item">
                                <span class="author-music__img-ico"></span>
                            </div>
                            <span class="author-music__name">Dutch</span>
                        </a>
                        <a href="#" class="author-music__item">
                            <div class="author-music__img">
                                <img src="/images/search1.png" alt="" class="author-music__img-item">
                                <span class="author-music__img-ico"></span>
                            </div>
                            <span class="author-music__name">Dutch</span>
                        </a>
                        <a href="#" class="author-music__item">
                            <div class="author-music__img">
                                <img src="/images/search1.png" alt="" class="author-music__img-item">
                                <span class="author-music__img-ico"></span>
                            </div>
                            <span class="author-music__name">Dutch</span>
                        </a>
                        <a href="#" class="author-music__item">
                            <div class="author-music__img">
                                <img src="/images/search1.png" alt="" class="author-music__img-item">
                                <span class="author-music__img-ico"></span>
                            </div>
                            <span class="author-music__name">Dutch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection