@extends('layouts.app')

@section('headtitle')
    <title>{{$poet->name}}</title>
@endsection

@section('content')
    <!-- filter -->
    <section class="filter">
      <div class="filter-bg _poet">
        <div class="container filter-container">
          <a href="#" class="filter-link fa fa-chevron-left"></a>
          <h1 class="filter-title">Қазақтың ақындары</h1>
        </div>
      </div>
    </section>
    <!-- endfilter -->

    {!!  Breadcrumbs::render('special.specpoets', $poet->name) !!}
    <section class="poet">
        <div class="poet-info">
            <div class="poet-info__img">
              <img src="/storage/{{$poet->img_path}}" alt="" class="poet-info__img-item">
            </div>
            <div class="poet-info__text">
              <span class="poet-info__text-name">{{$poet->name}}</span>
              {!! $poet->description !!}
            </div>
        </div>

        <div class="audiobook _inner">
            <div class="poet-audio">
                <audio id="Poet"></audio>
                <div class="Player audio-player" data-player-name="Poet">
                    <div class="player-block">
                        <div class="player-block__control-wrapper">
                            <div class="player__buttons-panel player__buttons-control-panel">
                                <div class="player-block__prev" data-player-button="prev"></div>
                                <div class="player-block__play" data-player-button="play"></div>
                                <div class="player-block__next" data-player-button="next"></div>
                            </div>
                        </div>
                        <div class="player-block__time-wrapper">
                            <div class="player-block__time-current" data-player-time="current">00:00</div>
                            <div class="player-block__tracker" data-player-scale="time">
                                <div class="time-scale__buffered" data-player-scale="buffered"></div>
                                <div class="time-scale__progress" data-player-scale="progress"></div>
                            </div>
                            <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                        </div>
                        <div class="player__volume-panel">
                            <div class="player__volume" data-player-button="volume"></div>
                            <div class="player__volume-scale-wrapper">
                                <div class="player__volume-scale" data-player-scale="volume">
                                    <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="player-block__playlist" data-player-playlist="Poet">
                        @foreach($poet->poetAudios as $key => $item)
                            <li data-player-src="/storage/{{ $item->file_path }}">
                                <div class="player-block__playlist__text">
                                    <span class="player-block__playlist__number">{{ ++$key }}.</span>
                                    <span class="player-block__playlist__author">{{--Б. Соқпақбаев--}}</span>
                                    <span class="player-block__playlist__name">{{ $item->name }}</span>
                                </div>
                                <span class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </section>

    <script>
		var playlist = {
        @foreach($poet->poetAudios as $key => $item)
        {{ $key++ }}: {
			name: '{{ $item->name }}',
				band: '',
				cover: '/images/poet1.png',
				src: '/storage/{{ $item->file_path }}'
		},
        @endforeach
		};

    </script>
@endsection
