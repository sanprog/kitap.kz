@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.poet.title') }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ trans('project.poet.title') }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ '/images/sp6.png' }}" />
@endsection

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _poet">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.poet.title') }}</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.specpoets') !!}

    <section class="specpoets">
        <div class="specpoets-container">
            <div class="specpoets-info">
                <div class="specbook-social">
                    <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-vk"></span></a>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-facebook"></span></a>
                    <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="specbook-ico"><span class="specbook-ico__img fa fa-google-plus _small"></span></a>
                    {{--<a href="#" class="specbook-ico"><span class="specbook-ico__img fa fa-twitter"></span></a>--}}
                </div>

                {!! trans('project.poet.desc') !!}
            </div>
            <div class="specpoets-row">
                @foreach($poets as $poet)
                    <a href="{{ route('poets.show', ['id' => $poet->id]) }}" class="specpoets-item">
                        <div class="specpoets-img">
                            <img src="/storage/{{$poet->img_path}}" alt="" class="specpoets-item__img">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="specpoets-item__name">{{$poet->name}}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
@endsection
