@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.kus.title') }}</title>
@endsection

@section('headmeta')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ trans('project.kus.title') }}" />
    <meta property="og:image"         content="{{ URL::to('/') }}{{ '/images/sp2.png' }}" />
@endsection

@section('content')
    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _talents">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.kus.title') }}</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.talents') !!}

    <section class="talents">
        <div class="talents-info">
            <div class="talents-social">
                <a target="_blank" href="https://vk.com/share.php?url={{ Request::url() }}" class="talents-social-ico"><span class="talents-social-ico__img fa fa-vk"></span></a>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="talents-social-ico"><span class="talents-social-ico__img fa fa-facebook"></span></a>
                <a target="_blank" href="https://plus.google.com/share?url={{ Request::url() }}" class="talents-social-ico"><span class="talents-social-ico__img fa fa-google-plus _small"></span></a>
                {{--<a href="#" class="talents-social-ico"><span class="talents-social-ico__img fa fa-twitter"></span></a>--}}
            </div>
            <div class="talents-desc">
                {!! trans('project.kus.desc') !!}
                <span class="talents-desc__btn js-talents-desc__btn">
        <span class="fa fa-angle-double-down"></span>
      </span>
            </div>
        </div>
        <div class="talents-row">
            @foreach($model as $item)
            <a href="{{ route('talents.show', ['talent' => $item->id]) }}" class="talents-item">
                <div class="talents-img">
                    <img src="/storage/{{ $item->img_path }}" alt="" class="talents-item__img">
                    <div class="special-img__text">
                        <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                    </div>
                </div>
                <span class="talents-item__name">{{ $item->name }}</span>
            </a>
            @endforeach
        </div>
    </section>
@endsection
