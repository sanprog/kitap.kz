@extends('layouts.app')

@section('headtitle')
    <title>{{ $model->name }}</title>
@endsection

@section('content')
    <!-- filter -->
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('talents') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ $model->name }}</h1>
            </div>
        </div>
    </section>
    <!-- enddfilter -->
    {!!  Breadcrumbs::render('special.talents') !!}

    <section class="talentsinner">
        <div class="talentsinner-info">
            <div class="talentsinner-info__img">
                <img src="/storage/{{ $model->img_path }}" alt="" class="talentsinner-info__img-logo">
                {{--<div class="talentsinner-actions">
                    <span class="talentsinner-actions__item"> <span class="fa fa-eye"></span> 20</span>
                    <span class="talentsinner-actions__item"><span class="fa fa-thumbs-o-up"></span> 20</span>
                </div>--}}
            </div>
            <div class="talentsinner-info__text">
                <div class="talentsinner-info__top">
                    <h1 class="talentsinner-info__title">{{ $model->name }}</h1>
                    {{--<div class="talentsinner-social">
                        <a href="#" class="talentsinner-ico"><span class="talentsinner-ico__img fa fa-vk"></span></a>
                        <a href="#" class="talentsinner-ico"><span class="talentsinner-ico__img fa fa-facebook"></span></a>
                        <a href="#" class="talentsinner-ico"><span class="talentsinner-ico__img fa fa-google-plus _small"></span></a>
                        <a href="#" class="talentsinner-ico"><span class="talentsinner-ico__img fa fa-twitter"></span></a>
                    </div>--}}
                </div>
                <h3 class="talentsinner-info__subtitle">{{ trans('project.kus.subtitle') }}</h3>
                <div class="talentsinner-desc" style="overflow:hidden;margin-bottom: 1rem;">
                    {!! $model->description !!}
                </div>
                <span class="talentsinner-desc__btn js-talentsinner-desc__btn_2">
                    <span class="fa fa-angle-double-down"></span>
                </span>
            </div>
        </div>
        <div class="audio talents-audio">
            <div class="speakerinner-row __no-margin">
                @foreach($model->items as $item)
                    <a href="{{ route('talents.read',['talent' => $item->id]) }}" class="speakerinner-item __hover-item">
                        <div class="speakerinner-item__img __relative" style="background-image:url('{{ $item->img_path ? "/storage/".$item->img_path : '/images/book.png' }}')">
                            <div class="hover-img__text">
                                 <span class="hover-img__text-btn">{{ trans('button.read') }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>                

            <audio id="Poet"></audio>
            <div class="Player audio-player" data-player-name="Poet">
                <div class="player-block">
                    <div class="player-block__control-wrapper">
                        <div class="player__buttons-panel player__buttons-control-panel">
                            <div class="player-block__prev" data-player-button="prev"></div>
                            <div class="player-block__play" data-player-button="play"></div>
                            <div class="player-block__next" data-player-button="next"></div>
                        </div>
                    </div>
                    <div class="player-block__time-wrapper">
                        <div class="player-block__time-current" data-player-time="current">00:00</div>
                        <div class="player-block__tracker" data-player-scale="time">
                            <div class="time-scale__buffered" data-player-scale="buffered"></div>
                            <div class="time-scale__progress" data-player-scale="progress"></div>
                        </div>
                        <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                    </div>
                    <div class="player__volume-panel">
                        <div class="player__volume" data-player-button="volume"></div>
                        <div class="player__volume-scale-wrapper">
                            <div class="player__volume-scale" data-player-scale="volume">
                                <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="player-block__playlist" data-player-playlist="Poet">
                    @foreach($model->items as $key => $item)
                        <li data-player-src="/storage/{{ $item->audio_path }}">
                            <div class="player-block__playlist__text">
                                <span class="player-block__playlist__number">{{ ++$key }}.</span>
                                <span class="player-block__playlist__author">{{--Б. Соқпақбаев--}}</span>
                                <span class="player-block__playlist__name">{{ $model->name }}</span>
                            </div>
                            <span id="{{ $item->id }}" class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="talents-audio __mobile">
            @foreach($model->items as $key => $item)
            <div class="talents-audio-item">
                <img src="/storage/{{ $item->img_path }}" alt="" class="talents-audio-item__img">
                <audio id="player_{{ $item->id }}"></audio>
                <div class="Player audio-player" data-player-name="player_{{ $item->id }}">
                    <div class="player-block">
                        <div class="player-block__control-wrapper">
                            <div class="player__buttons-panel player__buttons-control-panel">
                                <div class="player-block__prev" data-player-button="prev"></div>
                                <div class="player-block__play" data-player-button="play"></div>
                                <div class="player-block__next" data-player-button="next"></div>
                            </div>
                        </div>
                        <div class="player-block__time-wrapper">
                            <div class="player-block__time-current" data-player-time="current">00:00</div>
                            <div class="player-block__tracker" data-player-scale="time">
                                <div class="time-scale__buffered" data-player-scale="buffered"></div>
                                <div class="time-scale__progress" data-player-scale="progress"></div>
                            </div>
                            <div class="player-block__time-duration" data-player-time="duration">00:00</div>
                        </div>
                        <div class="player__volume-panel">
                            <div class="player__volume" data-player-button="volume"></div>
                            <div class="player__volume-scale-wrapper">
                                <div class="player__volume-scale" data-player-scale="volume">
                                    <div class="volume-scale__progress" data-player-scale="volume-progress"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="player-block__playlist" data-player-playlist="player_{{ $item->id }}">
                        <li data-player-src="/storage/{{ $item->audio_path }}">
                            <div class="player-block__playlist__text">
                                <span class="player-block__playlist__number">{{ ++$key }}.</span>
                                <span class="player-block__playlist__author">{{--Б. Соқпақбаев--}}</span>
                                <span class="player-block__playlist__name">{{ $model->name }}</span>
                            </div>
                            <span class="player-block__playlist__ico fa fa-play-circle" aria-hidden="true"></span>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    </section>

    <script>
		var playlist = {
        @foreach($model->items as $key => $item)
        {{ $key++ }}: {
			name: '{{ $model->name }}',
				band: '',
				cover: '/images/poet1.png',
				src: '/storage/{{ $item->audio_path }}'
		},
        @endforeach
		};

        @foreach($model->items as $item)
        var playlist_{{ $item->id }} = {
            0: {
    			name: '{{ $model->name }}',
    				band: '',
    				cover: '/images/poet1.png',
    				src: '/storage/{{ $item->audio_path }}'
    		},
        };
        @endforeach
    </script>
@endsection
