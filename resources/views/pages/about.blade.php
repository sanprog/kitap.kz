 @extends('layouts.app')

 @section('headtitle')
     <title>{{ trans('page.about.title') }}</title>
 @endsection

@section('content')
<section class="filter">
    <div class="filter-bg _about">
        <div class="container filter-container">
            <h1 class="filter-title">{{ trans('page.about.title') }}</h1>
        </div>
    </div>
</section>

{!!  Breadcrumbs::render('about') !!}

<section class="about">
    <div class="container about-container">
        <div class="about-item">
            <div class="about-item__top">
                <span class="about-item__img"><img src="images/abook.png" alt="" class="aabout-item__img-item"></span>
            </div>
            <p class="about-item__text">{!! trans('page.about.text1') !!}</p>
        </div>
        <div class="about-item">
            <div class="about-item__top">
                <span class="about-item__img"><img src="images/atarget.png" alt="" class="aabout-item__img-item"></span>
            </div>
            <h3 class="about-title">{{ trans('page.about.title2') }}</h3>
            <p class="about-item__text">{!! trans('page.about.text2') !!}</p>
        </div>
        <div class="about-item">
            <div class="about-item__top">
                <span class="about-item__img"><img src="images/aopenbook.png" alt="" class="aabout-item__img-item"></span>
            </div>
            <p class="about-item__text">{!! trans('page.about.text3') !!}</p>
        </div>
        <div class="about-item">
            <div class="about-item__top">
                <span class="about-item__img"><img src="images/amonument.png" alt="" class="aabout-item__img-item"></span>
            </div>
            <p class="about-item__text">{!! trans('page.about.text4') !!}</p>
        </div>
    </div>
</section>
@endsection