@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('page.toauthor.title') }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('page.toauthor.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('toauthor') !!}

    <section class="toauthor">
        <div class="container">
            <div class="toauthor-info">
                <div class="toauthor-text">
                    {!! trans('page.toauthor.desc') !!}
                </div>
                <span class="toauthor-info__label">{{ trans('page.toauthor.label') }}</span>
                <a href="to_Authors.doc" class="toauthor-btn">
                    <span class="toauthor-btn__text">{{ trans('button.jukteu') }}</span>
                    <span class="toauthor-btn__ico">
          <span class="toauthor-btn__ico-img fa fa-arrow-down"></span>
        </span>
                </a>
            </div>
        </div>

@endsection
