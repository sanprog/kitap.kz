@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('page.faq.title') }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('page.faq.title') }}</h1>
            </div>
        </div>
    </section>

{!!  Breadcrumbs::render('faq') !!}

    <section class="faq">
        <h1 class="faq-title">
            <!-- <img src="images/faq.png" alt="" class="faq-title__img"> -->
            <span class="fa fa-quora faq-title__ico"></span>
        </h1>
        <div class="faq-block">
            <div class="faq-item">
                <div class="faq-item__title">{{ trans('page.faq.q1') }}<span class="faq-item__title-ico"></span></div>
                <p class="faq-item__text">{{ trans('page.faq.answer') }}</p>
            </div>
            <div class="faq-item">
                <div class="faq-item__title">{{ trans('page.faq.q2') }}<span class="faq-item__title-ico"></span></div>
                <p class="faq-item__text">{{ trans('page.faq.answer') }}</p>
            </div>
            <div class="faq-item">
                <div class="faq-item__title">{{ trans('page.faq.q3') }}<span class="faq-item__title-ico"></span></div>
                <p class="faq-item__text">{{ trans('page.faq.answer') }}</p>
            </div>
            <div class="faq-item">
                <div class="faq-item__title">{{ trans('page.faq.q4') }}<span class="faq-item__title-ico"></span></div>
                <p class="faq-item__text">{{ trans('page.faq.answer') }}</p>
            </div>
            <div class="faq-item">
                <div class="faq-item__title">{{ trans('page.faq.q5') }}<span class="faq-item__title-ico"></span></div>
                <p class="faq-item__text">{{ trans('page.faq.answer') }}</p>
            </div>
        </div>
    </section>
@endsection
