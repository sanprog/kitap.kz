@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('page.donate.head_title') }}</title>
@endsection

@section('content')
<section class="dheader">
    <div class="container dheader-container">
        <h1 class="dheader-title">{!!   trans('page.donate.title') !!}</h1>
    </div>
</section>

<section class="dinfo">
    <div class="container">
        <h3 class="dinfo-title">{!! trans('page.donate.ititle') !!}</h3>
        <div class="dinfo-row">
            <div class="dinfo-col">
                <img src="images/dcode.png" alt="" class="dinfo-img">
                <span class="dinfo-text">{!! trans('page.donate.itext1') !!}</span>
            </div>
            <div class="dinfo-col">
                <img src="images/dbook.png" alt="" class="dinfo-img">
                <span class="dinfo-text">{!! trans('page.donate.itext2') !!}</span>
            </div>
            <div class="dinfo-col">
                <img src="images/dcard.png" alt="" class="dinfo-img">
                <span class="dinfo-text">{!! trans('page.donate.itext3') !!}</span>
            </div>
        </div>
    </div>
</section>

<section class="dabout">
    <div class="container dabout-container">
        <div class="dabout-col">
            <h3 class="dabout-text">{!! trans('page.donate.dabout') !!}</h3>
        </div>
        <div class="dabout-col">
            <a href="index.html" class="dabout-link">
                <img src="images/logobig.png" alt="" class="dabout-link__img">
            </a>
        </div>
    </div>
</section>

<section class="donate">
    <form action="{{ route('send.donate') }}" method="post" class="donate-form">
        {{ csrf_field() }}
        <h3 class="donate-form__title">{!! trans('page.donate.donate') !!}</h3>
        <div class="donate-container">
            <select name="donate"  id="" class="donate-select selectpicker" onchange="other_donate(this)">
                <option value="{{ trans('page.donate.sum1') }}" data-icon="ico-tenge">{{ trans('page.donate.sum1') }}</option>
                <option value="{{ trans('page.donate.sum2') }}" data-icon="ico-tenge">{{ trans('page.donate.sum2') }}</option>
                <option value="{{ trans('page.donate.sum3') }}" data-icon="ico-tenge">{{ trans('page.donate.sum3') }}</option>
                <option value="other">{{ trans('page.donate.other') }}</option>
            </select>
            <input id="other-donate" name="other-donate" class="form-control donate-other-input" placeholder="Басқа сомма" type="number"/>
            <button class="donate-help">
                <span class="donate-help__text">{{ trans('button.koldau') }}</span>
                <span class="donate-help__ico">
          <span class="donate-help__ico-img fa fa-skyatlas"></span>
        </span>
            </button>
        </div>
    </form>
</section>
<section class="dfree">
    <div class="container dfree-container">
        <div class="dfree-col">
            <img src="images/dship.png" alt="" class="dfree-img">
        </div>
        <div class="dfree-col">
            <p class="dfree-text">{{ trans('page.donate.free') }}</p>
        </div>
    </div>
</section>
@endsection
