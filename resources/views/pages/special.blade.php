@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('project.special.title') }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg _special">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('project.special.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('special') !!}

    <section class="special">
        <div class="container">
            <div class="special-row">
                <a href="https://100kitap.kz/kz" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url(../images/sp1.png);">
                                <div class="special-img__text">
                                    <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                                </div>

                        </div>
                        <span class="special-title">{{ trans('project.special.100kitap.title') }}</span>
                        <span class="special-text">{{ trans('project.special.100kitap.desc') }}</span>
                    </div>
                </a>
                <a href="{{ route('talents') }}" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url(../images/sp2.png);">
                                <div class="special-img__text">
                                    <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                                </div>

                        </div>
                        <span class="special-title">{{ trans('project.special.kus.title') }}</span>
                        <span class="special-text">{{ trans('project.special.kus.desc') }}</span>
                    </div>
                </a>
                <a href="{{ route("specbook") }}" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url(../images/sp3.png);">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="special-title">{{ trans('project.special.specbook.title') }}</span>
                        <span class="special-text">{{ trans('project.special.specbook.desc') }}</span>
                    </div>
                </a>
                <a href="{{ route("fairytale") }}" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url('../images/sp4.png');">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="special-title">{{ trans('project.special.fairytail.title') }}</span>
                        <span class="special-text">{{ trans('project.special.fairytail.desc') }}</span>
                    </div>
                </a>
                <a href="{{ route("specauthor") }}" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url('../images/sp5.png');">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="special-title">Ұлылардың үні</span>
                        <span class="special-text">Жиырмасыншы ғасырдың қойнауынан жеткен дауыстар… Бүтін бір халықтың арманы мен мұраты… Ұлттық болмыс пен парасат биігі… Қайсар рух пен қазақы таным… Жүрекпен тыңдап, көкейге түйер асыл сөз!</span>
                    </div>
                </a>
                <a href="{{ route('poets') }}" class="special-item">
                    <div class="special-item__inner">
                        <div class="special-img" style="background-image:url('../images/sp6.png');">
                            <div class="special-img__text">
                                <span class="special-img__text-btn">{{ trans('button.otu') }}</span>
                            </div>
                        </div>
                        <span class="special-title">Қазақтың ақындары</span>
                        <span class="special-text">“Қазақтың ақындары” – Қазақстанның ашық кітапханасы қолға алған бұрын-соңды болмаған бірегей аудиожоба. Ұлт ақындары жүрегін жарып шыққан таңдаулы жырларын өздері оқып, қалың оқырманымен жаңаша сипатта қауышқалы отыр. Автордың тебіренісін тыңдап, қасиетті өлеңнің құдіретін сезінеріңіз хақ.</span>
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection