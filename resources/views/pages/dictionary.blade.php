@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('page.dict.title') }}</title>
@endsection

@section('content')
    <section class="filter">
        <div class="filter-bg">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('page.dict.title') }}</h1>
            </div>
        </div>
    </section>

    {!!  Breadcrumbs::render('dictionary') !!}

    <section class="dictionary">
        <div class="container">
            <div class="dictionaty-text">
                {!! trans('page.dict.desc') !!}
            </div>
        </div>
    </section>
@endsection