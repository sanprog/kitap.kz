@extends('layouts.app')

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _poet">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">Қазақтың ақындары</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.talents') !!}

    <section class="specpoets">
        <div class="specpoets-container">
            <div class="specpoets-info">
                <p>“Қазақтың ақындары” – Қазақстанның ашық кітапханасы қолға алған бұрын-соңды болмаған бірегей аудиожоба. Ұлт ақындары жүрегін жарып шыққан таңдаулы жырларын өздері оқып, қалың оқырманымен жаңаша сипатта қауышқалы отыр. Автордың тебіренісін тыңдап, қасиетті өлеңнің құдіретін сезінеріңіз хақ.</p>
                <p>Қазақ халқының басынан өткен қилы кезеңдер мен тағылымды тарих, сырлы сезімдер мен жан күйзелістері қашан да ақындарымыздың жырларынан танылған. Олардың мұрасы – ұлтымыздың жылнамасы іспетті. Ақындар қазақтың азат күнін жақындатты. Олардың көкейінен төгілген әр өлең тіліміз бен ділімізді түлетіп, еркіндік туралы арманымызды сөндірмеді. Сондықтан, ел болып егемендіктің қасиетін сезінгенде, ақындарымызды құрмет тұтуымыз заңды.</p>
                <p>Ұлттық дүниетанымымыздан ойып тұрып орын алған арқалы ақындарымыздың бұл аудио антологиясы шынайы тыңдаушыларын табады деп сенеміз!</p>
            </div>
            <div class="specpoets-row">
                <a href="#" class="specpoets-item">
                    <img src="/images/poet1.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Жүрсін<br>Ерман</span>
                </a>
                <a href="{{ route('main') }}/author/637" class="specpoets-item">
                    <img src="/images/poet2.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Әділғазы<br>Қайырбеков</span>
                </a>
                <a href="{{ route('main') }}/author/652" class="specpoets-item">
                    <img src="/images/poet3.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Шөмішбай<br>Сариев</span>
                </a>
                <a href="{{ route('main') }}/author/427" class="specpoets-item">
                    <img src="/images/poet4.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Биғайша<br>Медеуова</span>
                </a>
                <a href="" class="specpoets-item">
                    <img src="/images/poet5.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Оразақын<br>Асқар</span>
                </a>
                <a href="{{ route('main') }}/author/434" class="specpoets-item">
                    <img src="/images/poet6.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Өтеген <br>Күмісбаев</span>
                </a>
                <a href="{{ route('main') }}/author/325" class="specpoets-item">
                    <img src="/images/poet7.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Исраил <br>Сапарбай</span>
                </a>
                <a href="{{ route('main') }}/author/583" class="specpoets-item">
                    <img src="/images/poet8.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Ақұштап <br>Бақтыгереева</span>
                </a>
                <a href="" class="specpoets-item">
                    <img src="/images/poet9.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Ұлықбек <br>Есдәулет</span>
                </a>
                <a href="{{ route('main') }}/author/608" class="specpoets-item">
                    <img src="/images/poet10.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Жадыра <br>Дәрібаева</span>
                </a>
                <a href="{{ route('main') }}/author/346" class="specpoets-item">
                    <img src="/images/poet12.png" alt="" class="specpoets-item__img">
                    <span class="specpoets-item__name">Темірхан <br>Медетбек</span>
                </a>
            </div>
        </div>
    </section>
@endsection