@extends('layouts.app')

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _specauthor">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">Ұлылардың үні</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.specauthor') !!}

    <section class="specauthor">
        <div class="specauthor-container">
            <div class="specauthor-info">
                <p>Жиырмасыншы ғасырдың қойнауынан жеткен дауыстар… Бүтін бір халықтың арманы мен мұраты… Ұлттық болмыс пен парасат биігі… Қайсар рух пен қазақы таным… Жүрекпен тыңдап, көкейге түйер асыл сөз!</p>
                <p>Қазақстанның ашық кітапханасы халқымыздың біртуар тұлғаларының дауыстарын назарларыңызға ұсынады. Тарихыңызды танығыңыз келсе, “Ұлылардың үнін” тыңдаңыз. Бұл дауыстар сіз бен бізге жауапкершілік жүктері анық.</p>
            </div>
            <div class="specauthor-row">
                <a href="{{ route('main') }}/author/437" class="specauthor-item">
                    <img src="/images/sa1.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Мұстафа Шоқай</span>
                </a>
                <a href="{{ route('main') }}/author/308" class="specauthor-item">
                    <img src="/images/sa2.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Жұбан Молдағалиев</span>
                </a>
                <a href="{{ route('main') }}/author/358" class="specauthor-item">
                    <img src="/images/sa3.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Төлеген Айбергенов</span>
                </a>
                <a href="{{ route('main') }}/author/287" class="specauthor-item">
                    <img src="/images/sa4.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Сәбит Мұқанов</span>
                </a>
                <a href="{{ route('main') }}/author/271" class="specauthor-item">
                    <img src="/images/sa5.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Бауыржан Момышұлы</span>
                </a>
                <a href="{{ route('main') }}/author/368" class="specauthor-item">
                    <img src="/images/sa6.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Бердібек Соқпақбаев</span>
                </a>
                <a href="{{ route('main') }}/author/326" class="specauthor-item">
                    <img src="/images/sa7.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Әбділда Тәжібаев</span>
                </a>
                <a href="{{ route('main') }}/author/348" class="specauthor-item">
                    <img src="/images/sa8.jpg" alt="" class="specauthor-item__img">
                    <span class="specauthor-item__name">Сафуан Шаймерденов</span>
                </a>
            </div>
        </div>
    </section>

@endsection