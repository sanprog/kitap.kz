@extends('layouts.app')

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg __fairytale">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">Әдеби кітаптар</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.fairytale') !!}

    <section class="allfairytales">
        <div class="allfairytales-inner">
            <div class="allfairytales-info">
                <p>Ертегі – ұлттық тәрбиенің айнымас бөлшегі. Халықтың тілі мен дүниетанымы, даналығы мен тарихы жас ұрпақтың бойына ең алғаш ертегі арқылы даритыны белгілі. Ендеше, күн сайын ертегі айтып, баласының қиялына қанат бітіру – ата-ананың парызы болса керек.</p>
                <p>
                    Қазақстанның ашық кітапханасы ата-аналарға жәрдемші боларлық “Ертегі тыңдайық!” жобасын әзірледі. Жоба аясында 400-ге жуық қазақ ертегісінің аудио нұсқасы жасалды. Ертегілер тақырыбына қарай 7 бөлімге бөлініп, ұсынылып отыр. Бұл бастамамыз
                </p>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/iyal-azhayyp-ertegiler" class="allfairytale-info__name">Қиял-ғажайып ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/iyal-azhayyp-ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook2.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/zhan_zhanuarlar_turaly_ertegiler" class="allfairytale-info__name">Жан-жануарлар туралы
                        ертегілер</a>
                    <div href="{{ route('main') }}/audiobooks/zhan_zhanuarlar_turaly_ertegiler" class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/zhan_zhanuarlar_turaly_ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook3.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/batyrly-tapyrly-zhayly-ertegiler" class="allfairytale-info__name">Батырлық, тапқырлық жайлы
                        ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/batyrly-tapyrly-zhayly-ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook4.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/trmys-salt-ertegileri" class="allfairytale-info__name">Тұрмыс-салт
                        ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/trmys-salt-ertegileri" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook5.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/ozhanasyr-turaly-ertegiler" class="allfairytale-info__name">Қожанасыр туралы
                        ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/ozhanasyr-turaly-ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook8.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/ayz-ertegiler" class="allfairytale-info__name">Аңыз
                        ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/ayz-ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="allfairytales-item">
                <div class="allfairytale-boook">
                    <img src="/images/kidbook7.png" alt="" class="allfairytale-book__img">
                </div>
                <div class="allfairytale-info">
                    <a href="{{ route('main') }}/audiobooks/aldar-kse-turaly-ertegiler" class="allfairytale-info__name">Алдар көсе туралы
                        ертегілер</a>
                    <div class="allfairytale-item__options">
                        <a href="{{ route('main') }}/audiobooks/aldar-kse-turaly-ertegiler" class="allfairytale-options__btn __blue">
                            <span class="allfairytale-options__btn-text">
                                Өту
                            </span>
                            <span class="allfairytale-options__btn-ico">
                                <span class="allfairytale-options-ico__img fa fa-plus"></span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
