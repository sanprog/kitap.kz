@extends('layouts.app')

@section('content')

    <!-- filter -->
    <section class="filter">
        <div class="filter-bg _talents">
            <div class="container filter-container">
                <a href="{{ route('special') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">Құс қанаты</h1>
            </div>
        </div>
    </section>
    <!-- endfilter -->
    {!!  Breadcrumbs::render('special.talents') !!}

    <section class="talents">
        <div class="talents-info">
            <div class="talents-social">
                <a href="#" class="talents-social-ico"><span class="talents-social-ico__img fa fa-vk"></span></a>
                <a href="#" class="talents-social-ico"><span class="talents-social-ico__img fa fa-facebook"></span></a>
                <a href="#" class="talents-social-ico"><span class="talents-social-ico__img fa fa-google-plus _small"></span></a>
                <a href="#" class="talents-social-ico"><span class="talents-social-ico__img fa fa-twitter"></span></a>
            </div>
            <div class="talents-desc">
                <div class="talents-desc__item">Жас прозашыларға қатысты жоба жасаймыз дегелі көп ойландық. Біздің бір ғана мақсатымыз бар – көркем шығарманы оқырманға жеткізу. Тек жеткізу емес, оқымасына амалын қалдырмау. Жас жазушылардың бәріне хабарласып, Алматыдағы шоғырымен кездесіп, ой бөлісіп те алдық.</div>
                <div class="talents-desc__item">Жаңашыл технологияларды әдебиетке қызмет еткізу, жаухарларымызды насихаттаудың құралына айналдыру ісінде кенже қалғанымыз жасырын емес. Әлем бұл жолды бізден әлдеқашан бұрын жүріп өтті. Біз сол жасалмай қалған сан мың істің аз ғана бөлігін жанымызды сала жүзеге асырып жатырмыз. “Құс қанаты” да – соның бірі.</div>
                <div class="talents-desc__item">Жас қаламгерлеріміз бірінен соң бірі келіп, студиямызда өз шығармаларын оқып жатты. Оқырмандарымыз жазушыларымызды тек оқып қана қоймай, шығармасын оның оқуында тыңдаса деген ойымыз болды. Себебі, туынды автор оқығанда мүлдем басқа түске боялады. Спектрдің жеті түсіндей әр сезім айқын көрінеді. Жүрегінен үзіліп түскен әр сөзін аялайтындай олар. Бұл іс біздің шығармашылық топ үшін де, жас қаламгерлер үшін де жақсы тәжірибе болды. Ал оқырман үшін бұл да аздық ететіндей.   Сапалы талдау ұсынсақ, оқырманның әдеби талғамын қалыптастыруға қосқан аз ғана үлесіміз болмай ма? Таңдауымыз жас сыншы Әсия Бағдәулетқызына түсті. Ол әсілінде өте адал, әділ сыншы. Рецензиялар бірінен соң бірі поштама түсіп жатқанда, оны алғашқы болып оқып, рухани ләззатқа бөленгенде, бұл таңдауда да біз қателік жібермегенімізді түсіндім. Жобаға қатысқан жас қаламгерлер үшін де замандас сыншының сөзі маңызды болар деген ойдамын.</div>
                <div class="talents-desc__item">Сонымен, “Құс қанаты” жобасына бүгінгі күні аты аталып жүрген жиырмадан астам прозаик қатысты. Қаламгерлер өз таңдауымен үздік деген үш шығармасын назарларыңызға ұсынды. Авторлар мен шығармалар белгілі бір ретпен апта сайын жарияланып отырмақ.</div>
                <div class="talents-desc__item">Жобаның бұлай аталуы да кездейсоқтық емес. Өз басым отыз-қырық атауды тізіп, сәтті нұсқасын таба алмай әбден қиналдым. Күн күрт бұзылып, ақ қанат қар түсті көктемгі күнде. Анам болса табиғаттың бұл тосын мінезін “Құс қанаты ғой” деп бағалап жатты. Табылды! Міне! Бұл жобаның атауы – “Құс қанаты”!</div>
                <div class="talents-desc__item">Құс қанаты – жыл құстарымен бірге келетін табиғат құбылысы екенін әр қазақ білсе керек. Құс қанатының қарлы жаңбырынан, суық желінен кейін іле-шала көктем келіп, тіршілік атаулы түрленіп сала береді. Құс қанаты – жаңа өмірдің бастауы, жаңалықтың, жақсылықтың белгісі. Көктемнің келгенін, жыл құстарының туған жеріне аман жеткенін әйгілеп тұратын әдемі әпсана.   Атам қазақ осы табиғат құбылысына “құс қанаты” деген поэтикалық атау бергені де жүректің қылын қозғайды. Бұл жас жазушылар шоғыры да әдебиеттегі “құс қанаты” болғай.</div>
                <div class="talents-desc__item">Әдебиетке алды жиырма жыл, арты он жыл бұрын келіп, жанкештілікпен еңбек етіп жүрген бұл толқынға баға берер біз емес. Баға беріп, екшейтін оқырман мен уақыттың өзі. Біздікі тек – барымызды ұсыну. Оқырман мен әдебиеттің арасына дәнекер болу. Әрі әдебиеттің жұлдызды шағы туарына үміт ету!
                </div>
                <span class="talents-desc__btn js-talents-desc__btn">
        <span class="fa fa-angle-double-down"></span>
      </span>
            </div>
        </div>
        <div class="talents-row">
            <a href="{{ route('main') }}/author/641" class="talents-item">
                <img src="/images/t1.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Арман <br>Әлменбет</span>
            </a>
            <a href="{{ route('main') }}/author/632" class="talents-item">
                <img src="/images/t2.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Аягүл <br>Мантаева</span>
            </a>
            <a href="{{ route('main') }}/author/631" class="talents-item">
                <img src="/images/t3.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Лира <br>Қоныс</span>
            </a>
            <a href="{{ route('main') }}/author/625" class="talents-item">
                <img src="/images/t4.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Алмас<br>Нүсіп</span>
            </a>
            <a href="{{ route('main') }}/author/626" class="talents-item">
                <img src="/images/t5.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Әлібек<br>Байбол</span>
            </a>
            <a href="{{ route('main') }}/author/624" class="talents-item">
                <img src="/images/t6.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Алмаз<br>Мырзахмет</span>
            </a>
            <a href="{{ route('main') }}/author/623" class="talents-item">
                <img src="/images/t7.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Мұрат<br>Алмасбекұлы</span>
            </a>
            <a href="{{ route('main') }}/author/622" class="talents-item">
                <img src="/images/t8.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Қанат<br>Әбілқайыр</span>
            </a>
            <a href="{{ route('main') }}/author/329" class="talents-item">
                <img src="/images/t9.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Мирас<br>Мұқаш</span>
            </a>
            <a href="{{ route('main') }}/author/621" class="talents-item">
                <img src="/images/t10.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Дархан<br>Бейсенбек</span>
            </a>
            <a href="{{ route('main') }}/author/620" class="talents-item">
                <img src="/images/t11.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Нұрлан <br>Қабдайұлы</span>
            </a>
            <a href="{{ route('main') }}/author/289" class="talents-item">
                <img src="/images/t13.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Бейбіт<br>Сарыбай</span>
            </a>
            <a href="{{ route('main') }}/author/617" class="talents-item">
                <img src="/images/t14.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Сержан<br>Закерұлы</span>
            </a>
            <a href="{{ route('main') }}/author/297" class="talents-item">
                <img src="/images/t15.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Өміржан <br>Әбдіхалық</span>
            </a>
            <a href="{{ route('main') }}/author/436" class="talents-item">
                <img src="/images/t16.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Ерболат <br>Әбікенұлы</span>
            </a>
            <a href="{{ route('main') }}/author/435" class="talents-item">
                <img src="/images/t17.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Серік <br>Сағынтай</span>
            </a>
            <a href="{{ route('main') }}/author/613" class="talents-item">
                <img src="/images/t18.png" alt="" class="talents-item__img">
                <span class="talents-item__name">Қанағат <br>Әбілқайыр</span>
            </a>
        </div>
    </section>
@endsection