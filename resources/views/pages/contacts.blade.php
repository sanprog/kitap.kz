@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('page.contact.title') }}</title>
@endsection

@section('content')
<section class="filter">
    <div class="filter-bg _contacts">
        <div class="container filter-container">
            <h1 class="filter-title">{!! trans('page.contact.title') !!}</h1>
        </div>
    </div>
</section>

{!!  Breadcrumbs::render('contacts') !!}

<section class="contacts">
    <div class="container contacts-container">
        <div class="contacts-row">
            <div class="contacts-col">
                <ul class="contacts-list">
                    <li class="contacts-list__item">
                        <span class="fa fa-flag contacts-list__item-ico" aria-hidden="true"></span>
                        {!! trans('page.contact.flag') !!}
                    </li>
                    <li class="contacts-list__item">
                        <span class="fa fa-road contacts-list__item-ico" aria-hidden="true"></span>
                        {!! trans('page.contact.road') !!}
                    </li>
                    {{--<li class="contacts-list__item">
                        <span class="fa fa-phone contacts-list__item-ico" aria-hidden="true"></span>"Ұлттық аударма бюросы" қоғамдық қоры
                        Қазақстанның ашық кітапханасы
                    </li>--}}
                    <li class="contacts-list__item">
                        <span class="fa fa-keyboard-o contacts-list__item-ico" aria-hidden="true"></span>
                        {!! trans('page.contact.contact') !!}
                    </li>
                </ul>
            </div>
            <div class="contacts-col">
                <form action="{{route('contacts.send')}}" method="post" class="contacts-form">
                    {{ csrf_field() }}
                    <div class="form-group {{$errors->has('email')?'has-error':''}}">
                        <input name="email" type="text" class="contacts-input" placeholder="Email" value="{{ $model->email or old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block help-block-error">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('text')?'has-error':''}}">
                        <textarea name="text" id="" cols="30" rows="10" class="contacts-textarea" placeholder="Хат жазыңыз">{{ $model->text or old('text') }}</textarea>
                        @if ($errors->has('text'))
                            <span class="help-block help-block-error">{{ $errors->first('text') }}</span>
                        @endif
                    </div>
                    {{--<input name="email" type="text" class="contacts-input" placeholder="Email">--}}
                    {{--<textarea name="text" id="" cols="30" rows="10" class="contacts-textarea" placeholder="Хат жазыңыз"></textarea>--}}
                    <button type="submit" class="contacts-btn">{{ trans('button.jiberu') }}</button>
                </form>
            </div>
        </div>
        <img src="images/phone.png" alt="" class="contacts-img">
    </div>
</section>
@endsection
