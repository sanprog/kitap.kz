<aside class="profilenav">
    <a href="{{ route('profile') }}" class="profilenav-link {{ Request::is('profile') ? '_active' : '' }}">
        <span class="profilenav-link__ico fa fa-street-view" aria-hidden="true"></span>{{ trans('profile.show.title') }}
    </a>
    <a href="{{ route('profile.mylibrary') }}" class="profilenav-link {{ Request::is('profile/mylibrary') ? '_active' : '' }}">
        <span class="profilenav-link__ico fa fa-leanpub" aria-hidden="true"></span>{{ trans('widget.library') }}
    </a>
    <a href="{{ route('profile.myquote') }}" class="profilenav-link {{ Request::is('profile/myquote') ? '_active' : '' }}">
        <span class="profilenav-link__ico fa fa-quote-right" aria-hidden="true"></span>{{ trans('profile.quote.title') }}
    </a>
    {{--<a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-certificate"></span>Шараларым
    </a>
    <a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-sitemap"></span>Менің топтарым
    </a>
    <a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-puzzle-piece"></span>Жекпе-жек<span
                class="profilenav-link__message">12</span>
    </a>
    <a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-users"></span>Достарым
    </a>
    <a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-comments"></span>Рецензиялар
    </a>
    <a href="#" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-bar-chart"></span>Статистика
    </a>
    <a href="settinginner.html" class="profilenav-link">
        <span class="profilenav-link__ico fa fa-cog" aria-hidden="true"></span>Настройки
    </a>--}}
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="profilenav-link _green">
        <span class="profilenav-link__ico fa fa-share-square-o" aria-hidden="true"></span>{{ trans('other.exit') }}
    </a>
</aside>