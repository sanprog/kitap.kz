@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('main.quote') }}</title>
@endsection

@section('content')
    <section class="filter __not-active">
        <div class="filter-bg _profile">
            <div class="container filter-container">
                <a href="{{ route('main') }}" class="filter-link fa fa-chevron-left"></a>
                <h1 class="filter-title">{{ trans('main.quote') }}</h1>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="profile-row">
            <div class="profile-col _left __border">
                @include('profile.nav_menu')
            </div>
            <div class="profile-col _right">
                <section class="myquote">
                    <div class="myquote-row">
                        @foreach($quotes as $item)
                        <div class="myquote-item" id="quote-{{ $item->id }}">
                            <div class="myquote-item__top">
                                <img src="{{ $item->book->img_path }}" alt="" class="myquote-item__img">
                                <div class="myquote-info">
                                    <div class="myquote-info-top">
                                        <span class="myquote-info__author">{{ !$item->book->authors->isEmpty() ? implode($item->book->authors->pluck('name')->toArray(), ' ') : ''}}</span>
                                        <span class="myquote-info__name">
                                            {{ $item->book->name }}
                                        </span>
                                    </div>
                                    {{--<div class="myquote-share">
                                        <span class="myquote-share__item"><span
                                                    class="fa fa-thumbs-up myquote-share__item-ico"></span>2  Нравится</span>
                                        <span class="myquote-share__item"><span
                                                    class="fa fa-retweet myquote-share__item-ico"></span>Бөлісу</span>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="myquote-item__text">
                                <span class="fa fa-quote-left myquote-item__img-ico"></span>
                                {{ $item->fragment }}
                            </div>
                            <div class="myquote-item__close">
                                <span class="fa fa-times js-myquote-close"></span>
                                <div class="myquote-alert js-myquote-alert">
                                    <span class="myquote-alert-text">
                                        {{ trans('profile.quote.alert') }}
                                    </span>
                                    <div class="myquote-alert-options">
                                        <button type="button"
                                                name="button"
                                                class="myquote-alert-options__btn __active"
                                                onclick='myQuoteDelete({{ $item->id }},"quote-{{ $item->id }}")'
                                        >
                                            {{ trans('button.ya') }}
                                        </button>
                                        <button type="button" name="button"
                                                class="myquote-alert-options__btn js-myquote-alert-close">{{ trans('button.jok') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- quote -->
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection