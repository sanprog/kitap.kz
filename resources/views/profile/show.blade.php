@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('profile.show.title') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg _profile">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('profile.show.title') }}</h1>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="profile-row">
            <div class="profile-col _left __border">
                @include('profile.nav_menu')
            </div>
            <div class="profile-col _right">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->unique() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="setting" enctype="multipart/form-data" method="post" action="{{ route('profile.update') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="setting-top">
                        <div class="setting-photo">
                            <div class="setting-photo__info">
                                <span class="setting-photo__info-title">{!!   trans('profile.show.photo') !!}</span>
                                <span class="setting-photo__info-text">{!!   trans('profile.show.photo_desc') !!}</span>
                            </div>
                            <label for="settingPhotoImgInput" class="setting-photo__block">
                                <div class="setting-photo__img" style="background-size: cover; background-image: url('{{ $user->avatar_url or '../images/setting.png' }}')">
                                    <input type="file" name="avatar" id="settingPhotoImgInput" class="setting-photo__input" />
                                    <img id="settingPhotoImg" src="#" alt="" />
                                </div>
                                <span class="setting-photo__btn">
                                <span class="fa fa-plus setting-photo__ico"></span>
                              </span>
                            </label>

                        </div>
                        {{--<a href="settinginner.html" class="setting-top__btn"><span
                                    class="typcn typcn-cog-outline"></span>Настройки</a>--}}
                    </div>
                    <span class="setting-desc">{!! trans('profile.show.loginpass') !!}</span>
                    <div class="setting-row _profile">
                        <label for="login" class="setting-label">{!! trans('profile.show.login') !!}</label>
                        <input type="text" class="setting-input" id="login" name="login" required=""
                               value="{{ old('login', $user->login) }}"
                               placeholder="">
                    </div>
                    <div class="setting-row _profile">
                        <label for="email" class="setting-label">{!! trans('profile.show.email') !!}</label>
                        <input type="text" class="setting-input" id="email" name="email" required=""
                               value="{{ old('email', $user->email) }}"
                               placeholder="anarahamza@gmail.com">
                    </div>
                    @if(!$is_social)
                    <div class="setting-row _profile">
                        <label for="oldpass" class="setting-label">{!! trans('profile.show.now') !!}</label>
                        <input type="password" class="setting-input" id="oldpass" name="cur_password">
                    </div>
                    @else
                    <div class="setting-row _profile" style="display:none;">
                        <input type="password" class="setting-input" id="oldpass" name="cur_password" value="none">
                    </div>
                    @endif
                    <div class="setting-row _profile">
                        <label for="newpass" class="setting-label">{!! trans('profile.show.new') !!}</label>
                        <input type="password" class="setting-input" id="newpass" name="new_password">
                    </div>
                    <div class="setting-row _profile">
                        <label for="repeatpass" class="setting-label">{!! trans('profile.show.repeat') !!}</label>
                        <input type="password" class="setting-input" id="repeatpass" name="new_password_confirmation">
                    </div>
                    <div class="setting-row _profile _btn">
                        <button class="setting-btn">{{ trans('button.saktau') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
