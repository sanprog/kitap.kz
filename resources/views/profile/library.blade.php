@extends('layouts.app')

@section('headtitle')
    <title>{{ trans('widget.library') }}</title>
@endsection

@section('content')

    <section class="filter">
        <div class="filter-bg _profile">
            <div class="container filter-container">
                <h1 class="filter-title">{{ trans('widget.library') }}</h1>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="profile-row">
            <div class="profile-col _left __border">
                @include('profile.nav_menu')
            </div>
            <div class="profile-col _right">
                <section class="mylibrary">
                    <div class="mylibrary-top">
                        <ul class="tabs mylibrary-top__list">
                            <li class="tab-link mylibrary-top__item" data-tab="lib-1">
                                <span class="mylibrary-top__item-info"><span
                                            class="fa fa-headphones mylibrary-top__info-ico" aria-hidden="true"></span><span id='audiobook'>{{ count($audiobooks) }}</span></span>
                                <span class="mylibrary-top__item-desc">{{ trans('search.audiobook') }}</span>
                            </li>
                            <li class="tab-link mylibrary-top__item current" data-tab="lib-2">
                                <span class="mylibrary-top__item-info"><span class="fa fa-book mylibrary-top__info-ico"
                                                                             aria-hidden="true"></span><span id='book'>{{ count($books) }}</span></span>
                                <span class="mylibrary-top__item-desc">{{ trans('search.book') }}</span>
                            </li>
                            <li class="tab-link mylibrary-top__item" data-tab="lib-3">
                                <span class="mylibrary-top__item-info"><span class="fa fa-music mylibrary-top__info-ico"
                                                                             aria-hidden="true"></span><span id='audio'>{{ count($music) }}</span></span>
                                <span class="mylibrary-top__item-desc">{{ trans('search.music') }}</span>
                            </li>
                            <li class="tab-link mylibrary-top__item" data-tab="lib-4">
                                <span class="mylibrary-top__item-info"><span
                                            class="fa fa-youtube-play mylibrary-top__info-ico"
                                            aria-hidden="true"></span><span id='video'>{{ count($video) }}</span></span>
                                <span class="mylibrary-top__item-desc">{{ trans('search.video') }}</span>
                            </li>
                        </ul>
                    </div>
                    <div id="lib-1" class="tab-content mylibrary-content">
                        @if(count($audiobooks) == 0)
                            <div class="mylibrary-row __no-item __active">
                                <img src="/images/box.png" class="mylibrary-no-item__img">
                                <span class="mylibrary-no-item__text">Кітапханаңыз бос</span>
                                <a href="{{ route('audiobooks') }}" class="mylibraty-no-tem__link">Кітапхана  каталогы</a>
                            </div>
                        @endif
                        @foreach($audiobooks as $book)
                            <div class="mylibrary-row __audiobook" id="row-audiobook-{{ $book->id }}">
                                <div class="mylibrary-item__close">
                                    <span class="fa fa-times js-mylibrary-close"></span>
                                    <div class="mylibrary-alert js-mylibrary-alert">
                                        <span class="mylibrary-alert-text">
                                            {{ trans('profile.quote.alert') }}
                                        </span>
                                        <div class="mylibrary-alert-options">
                                            <button type="button" onclick='mylibdelete({{ $book->id }},"audiobook","row-audiobook-{{ $book->id }}")' name="button" class="mylibrary-alert-options__btn __active">{{ trans('button.ya') }}</button>
                                            <button type="button" name="button" class="mylibrary-alert-options__btn js-mylibrary-alert-close">{{ trans('button.jok') }}</button>
                                        </div>
                                    </div>
                                </div>
                                    <div class="mybook">
                                        <div class="mybook-col__left">
                                            <div class="mybook-item" style="max-height: 200px">
                                                <img src="{{ $book->img_path or '/images/book.png' }}" alt="" class="mybook-item__img __audiobook">
                                            </div>
                                            <div class="mybook-info">
                                                <div class="mybook-info__top">
                                                    <span class="mybook-info__author">{{ $book->book->name }}</span>
                                                    <span class="mybook-info__name"><a href="{{ route('audiobooks.show', ['slug' => $book->book->slug]) }}">{{ $book->book->name }}</a></span>
                                                </div>
                                                <div class="mybook-info__rating">
                                                  <span class="mybook-info__rating-text">{{ trans('other.which') }}</span>
                                                  <div class="mybook-rating">
                                                    <span class="mybook-rating__text">67%</span>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mybook-col__right">
                                            <!--
                                            <div class="mybook-actions">
                                                <div class="mybook-actions__item">
                                                    <span class="mybook-actions__item-ico fa fa-heart" aria-hidden="true"></span>
                                                    <span class="mybook-actions__item-text">20</span>
                                                </div>
                                                <div class="mybook-actions__item">
                                                    <span class="mybook-actions__item-ico fa fa-plus-square" aria-hidden="true"></span>
                                                    <span class="mybook-actions__item-text">20</span>
                                                </div>
                                                <div class="mybook-actions__item">
                                                    <span class="mybook-actions__item-ico fa fa-commenting" aria-hidden="true"></span>
                                                    <span class="mybook-actions__item-text">20</span>
                                                </div>
                                            </div>
                                            <span class="mybook-warning">
                                              До окончания срока чтения осталось:<span class="mybook-warning__date">100 дней</span>
                                            </span>-->
                                        </div>
                                    </div>
                                    <div class="mybook-records">
                                        <div class="mybook-records__item">
                                            <div class="mybook-records__text">
                                                <span class="mybook-records__number">1.</span><span
                                                        class="mybook-records__author">Б. Соқпақбаев</span>
                                                <span class="mybook-records__name">Менің атым қожа</span>
                                            </div>
                                            <span class="mybook-records__ico fa fa-play-circle" aria-hidden="true"></span>
                                        </div>
                                        <div class="mybook-records__item __active">
                                            <div class="mybook-records__text">
                                                <span class="mybook-records__number">1.</span><span
                                                        class="mybook-records__author">Б. Соқпақбаев</span>
                                                <span class="mybook-records__name">Менің атым қожа</span>
                                            </div>
                                            <span class="mybook-records__ico fa fa-play-circle" aria-hidden="true"></span>
                                        </div>
                                    </div>
                            </div>
                        @endforeach
                        <!-- audiobook1 -->
                    </div>
                    <div id="lib-2" class="tab-content mylibrary-content current">
                        @if(count($books) == 0)
                            <div class="mylibrary-row __no-item __active">
                                <img src="/images/box.png" class="mylibrary-no-item__img">
                                <span class="mylibrary-no-item__text">Кітапханаңыз бос</span>
                                <a href="{{ route('books.public') }}" class="mylibraty-no-tem__link">Кітапхана каталогы</a>
                            </div>
                        @endif
                        @foreach($books as $book)
                            <div class="mylibrary-row __book" id = 'row-book-{{ $book->id }}'>
                                <div class="mylibrary-item__close">
                                    <span class="fa fa-times js-mylibrary-close"></span>
                                    <div class="mylibrary-alert js-mylibrary-alert">
                                        <span class="mylibrary-alert-text">
                                            {{ trans('profile.quote.alert') }}
                                        </span>
                                        <div class="mylibrary-alert-options">
                                            <button type="button" name="button" onclick='mylibdelete({{ $book->id }},"book","row-book-{{ $book->id }}")' class="mylibrary-alert-options__btn __active">{{ trans('button.ya') }}</button>
                                            <button type="button" name="button" class="mylibrary-alert-options__btn js-mylibrary-alert-close">{{ trans('button.jok') }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="mybook">
                                    <div class="mybook-col__left">
                                        <div class="mybook-item" style="max-height: 200px">
                                            <img src="{{ $book->img_path or '/images/book.png' }}" alt="" class="mybook-item__img __book">
                                        </div>
                                        <div class="mybook-info">
                                            <div class="mybook-info__top">
                                                <span class="mybook-info__author">{{ $book->author[0] }}</span>
                                                <span class="mybook-info__name"><a href="{{ route('books.show', ['slug' => $book->slug]) }}">{{ $book->name }}</a></span>
                                            </div>
                                            <div class="mybook-info__bottom">
                                                <!-- <div class="mybook-info__rating __book">
                                                  <span class="mybook-info__rating-text">Қанша оқылды?</span>
                                                  <div class="mybook-rating">
                                                    <span class="mybook-rating__text">67%</span>
                                                  </div>
                                                </div> -->
                                                <div class="mybook-actions __book">
                                                    <div class="mybook-actions__item">
                                                        <span class="mybook-actions__item-ico fa fa-heart" aria-hidden="true"></span>
                                                        <span class="mybook-actions__item-text">20</span>
                                                    </div>
                                                    <div class="mybook-actions__item">
                                                        <span class="mybook-actions__item-ico fa fa-plus-square" aria-hidden="true"></span>
                                                        <span class="mybook-actions__item-text">20</span>
                                                    </div>
                                                    <div class="mybook-actions__item">
                                                        <span class="mybook-actions__item-ico fa fa-commenting" aria-hidden="true"></span>
                                                        <span class="mybook-actions__item-text">20</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mybook-col__right __tags">
                                        <!-- <div class="mybook-tags">
                                          <span class="mybook-tags__item">сатира</span>
                                          <span class="mybook-tags__item">ғұмырнамалық</span>
                                          <span class="mybook-tags__item">әлеуметтік саяси</span>
                                        </div>
                                        <span class="mybook-warning">
                                          До окончания срока чтения осталось:<span class="mybook-warning__date">100 дней</span>
                                        </span> -->
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div id="lib-3" class="tab-content mylibrary-content">
                        @if(count($books) == 0)
                            <div class="mylibrary-row __no-item __active">
                                <img src="/images/box.png" class="mylibrary-no-item__img">
                                <span class="mylibrary-no-item__text">Кітапханаңыз бос</span>
                                <a href="{{ route('music') }}" class="mylibraty-no-tem__link">Кітапхана каталогы</a>
                            </div>
                        @endif
                        @foreach($music as $item)
                        <div class="mylibrary-row __audiobook" id="row-audio-{{ $item->id }}">
                            <div class="mylibrary-item__close">
                                <span class="fa fa-times js-mylibrary-close"></span>
                                <div class="mylibrary-alert js-mylibrary-alert">
                                        <span class="mylibrary-alert-text">
                                           {{ trans('profile.quote.alert') }}
                                        </span>
                                    <div class="mylibrary-alert-options">
                                        <button type="button" onclick='mylibdelete({{ $item->id }},"audio","row-audio-{{ $item->id }}")' name="button" class="mylibrary-alert-options__btn __active">{{ trans('button.ya') }}</button>
                                        <button type="button" name="button" class="mylibrary-alert-options__btn js-mylibrary-alert-close">{{ trans('button.jok') }}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="mybook __music">
                                <div class="mybook-col">
                                    <div class="mybook-item">
                                        <img src="/storage/{{ $item->img_path }}" alt="" class="mybook-item__img __music" >
                                    </div>
                                    <div class="mybook-info">
                                        <div class="mybook-info__top">
                                            <span class="mybook-info__author">{{ $item->author[0] }}</span>
                                            <span class="mybook-info__name">{{ $item->name }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mybook-col">
                                    <div class="mybook-actions __book">
                                        <div class="mybook-actions__item">
                                            <span class="mybook-actions__item-ico fa fa-heart"
                                                  aria-hidden="true"></span>
                                            <span class="mybook-actions__item-text">20</span>
                                        </div>
                                        <div class="mybook-actions__item">
                                            <span class="mybook-actions__item-ico fa fa-plus-square"
                                                  aria-hidden="true"></span>
                                            <span class="mybook-actions__item-text">20</span>
                                        </div>
                                        <div class="mybook-actions__item">
                                            <span class="mybook-actions__item-ico fa fa-commenting"
                                                  aria-hidden="true"></span>
                                            <span class="mybook-actions__item-text">20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    </div>
                    <div id="lib-4" class="tab-content mylibrary-content">
                        @if(count($books) == 0)
                            <div class="mylibrary-row __no-item __active">
                                <img src="/images/box.png" class="mylibrary-no-item__img">
                                <span class="mylibrary-no-item__text">Кітапханаңыз бос</span>
                                <a href="{{ route('video') }}" class="mylibraty-no-tem__link">Кітапхана каталогы</a>
                            </div>
                        @else
                        <div class="mylibrary-video">
                            <!-- row -->
                            @php
                                $i = 0;
                            @endphp
                            @foreach($video as $videoItem)
                                @php
                                    $i++;
                                @endphp
                                @if($i % 2 == 1)
                                    <div class="mylibrary-row __margin">
                                @endif
                                        <div class="mylibrary-col">
                                            <div class="mylibrary-item">
                                                <div style="background-image:url('{{ $videoItem->img_path or '/images/v1.png' }}');" class="mylibrary-item__img">
                                                    <div class="mylibrary-item__action">
                                                        <span class="mylibrary-item__action-ico"></span>
                                                        <div class="mylibrary-item__options">
                                                            <div class="mylibrary__options-item">
                                                                <span class="mylibrary__options-item__ico fa fa-heart" aria-hidden="true"></span>
                                                                <span class="mylibrary__options-item__text">20</span>
                                                            </div>
                                                            <div class="mylibrary__options-item">
                                                                <span class="mylibrary__options-item__ico fa fa-plus-square" aria-hidden="true"></span>
                                                                <span class="mylibrary__options-item__text">20</span>
                                                            </div>
                                                            <div class="mylibrary__options-item">
                                                                <span class="mylibrary__options-item__ico fa fa-comment" aria-hidden="true"></span>
                                                                <span class="mylibrary__options-item__text">20</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="mylibrary-item-info">{{ $videoItem->name }}</span>

                                                <div class="mylibrary-item__close __video">
                                                    <span class="fa fa-times js-mylibrary-close"></span>
                                                    <div class="mylibrary-alert js-mylibrary-alert">
                                                      <span class="mylibrary-alert-text">
                                                        {{ trans('profile.quote.alert') }}
                                                      </span>
                                                        <div class="mylibrary-alert-options">
                                                            <button type="button" name="button" onclick='mylibdelete({{ $videoItem->id }},"video","row-audio-{{ $videoItem->id }}")' class="mylibrary-alert-options__btn __active">{{ trans('button.ya') }}</button>
                                                            <button type="button" name="button" class="mylibrary-alert-options__btn js-mylibrary-alert-close">{{ trans('button.jok') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- item 2 -->
                                        </div>

                                @if($i % 2 == 0)
                                    </div>
                                @endif
                            @endforeach
                            <!-- row -->
                        </div>
                            @endif
                    </div>
                </section>

            </div>
        </div>
    </div>

@endsection