<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 1:54 PM
 */

return [
    'okyrman'   => 'Оқырман таңдауы',
    'library'   => 'Менің кітапханам',
    'bet'       => 'Жеке бет',
    'offer'     => 'Біздің ұсынысымыз'
];