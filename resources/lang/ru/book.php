<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 6:12 PM
 */

return  [
    'title'         =>  'Әдеби кітаптар',
    'category'      =>  'Категориялар',
    'gylymi'        =>  'Ғылыми әдебиет',
    'literatural'   =>  'Көркем әдебиет',
    'read_book'     =>  'Оқылымды кітаптар',
    'new_book'      =>  'Жаңа кітаптар',
    'genre'         =>  'Жанрлар',
    'author'        =>  'Автор',
    'biography'     =>  'Биография',
    'dictor'        =>  'Диктор',
    'rating'        =>  'Рейтинг',
    'audiobook'     =>  'Аудиокітап',
    'dictor_tandau' =>  'Диктор бойынша таңдау',
    'dictor_about'  =>  'Диктор туралы',
    'how_much'      =>  'Қанша оқылды?',
	'rating.errauth' => 'Для голосования необходимо авторизоваться',
	'rating.err'     => 'Вы уже голосовали за эту книгу',
	'rating.ok'      => 'Ваш голос учтен <br> Спасибо',
];