<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 2:37 PM
 */

return [
    'show'      =>  [
        'title'         =>  'Жеке бет',
        'photo'         =>  'Сіздің суретіңіз',
        'photo_desc'    =>  'Размер фото<br>
                                не должен быть<br>
                                меньше 300 x 300 px',
        'loginpass'     =>  'Логин және пароль:',
        'login'         =>  'Логин: <span class="required">*</span>',
        'email'         =>  'Email: <span class="required">*</span>',
        'now'           =>  'Қазыргі пароль: <span class="required">*</span>',
        'new'           =>  'Жаңа пароль: <span class="required">*</span>',
        'repeat'        =>  'Жаңа парольді қайталау:<span class="required">*</span>'
    ],
    'quote'     =>  [
        'title'     =>  'Цитаталарым',
        'alert'     =>  'Вы уверены что хотите удалить эту запись?',
    ]
];