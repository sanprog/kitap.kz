<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 5:29 PM
 */

return [
    'akparat'       =>  'Ақпарат',
    'faq'           =>  'FAQ',
    'kitap'         =>  'Kitap қосымшасын жүктеу',
    'audiokitap'    =>  'Audiokitap қосымшасын жүктеу',
    'subscribe'     =>  'kitap.kz сайтынан жаңа материалдар бойынша хат<br> алып тұрғыңыз келсе, жазылыңыз',
    'copy'          =>  '© "Ұлттық аударма бюросы" қоғамдық қоры 2018'
];