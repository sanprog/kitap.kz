<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 10:43 AM
 */
return [
    'read'          =>  'Оқу',
    'soz'           =>  'Сөзді аудару',
    'tyndau'        =>  'Тыңдау',
    'tolgyrak'      =>  'Толығырақ',
    'add'           =>  'Қосу',
    'sub'           =>  'Кетіру',
    'saktau'        =>  'Сақтау',
    'ya'            =>  'Иә',
    'jok'           =>  'Жоқ',
    'jukteu'        =>  'Жүктеу',
    'koldau'        =>  'Жобаны қолдау',
    'jiberu'        =>  'Жіберу',
    'auth'          =>  'Кіру',
    'register'      =>  'Тіркелу',
    'forgot'        =>  'Құпиясөзді қалпына келтіру',
    'back'          =>  'Артқа',
    'subscribe'     =>  'Енгізу',
    'otu'           =>  'Өту'
];