<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 12:05 PM
 */

return [
    'empty'             => 'Ештеңе табылмады',
    'book'              => 'Кітап',
    'audiobook'         => 'Аудиокітап',
    'music'             => 'Музыка',
    'video'             => 'Видео',
    'author'            => 'Авторлар',
    'dictor'            => 'Дикторлар',
    'korkem'            => 'Көркем әдебиет',
    'gylymi'            => 'Ғылыми әдебиет'
];