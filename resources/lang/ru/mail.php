<?php
return [
    'contact' => [
        'feedback_mail_body' => 'Кері байланыс формасына енгізілген мәліметтер',
        'feedback_name_title' => 'Аты',
        'feedback_email_title' => 'Email',
        'feedback_organization_title' => 'Мекеме',
        'feedback_message_title' => 'Хабарлама',
    ],
    'recovery' => [
        'title_recovery' => 'kitap.kz сайтының құпиясөзін қалпына келтіру',
        'hello' => 'Сәлеметсіз бе, :login.',
        'request' => 'Сіз <a href="http://kitap.kz" target="_blank" style="color:#3269b1;text-decoration:underline;">kitap.kz</a> порталының құпиясөзін қалпына келтіруге сұраныс жібердіңіз.',
        'confirmation' => 'Құпиясөзді қалпына келтіру үшін мына сілтемемен өтіңіз:',
        'new_password' => 'Сіздің жаңа құпиясөзіңіз: <b>:password</b>',
    ],
    'registration' => [
        'title' => 'kitap.kz сайтына тіркелу',
        'hello' => 'Сәлеметсіз бе, :login.',
        'success' => 'Сіз <a href="http://kitap.kz" target="_blank" style="color:#3269b1;text-decoration:underline;">kitap.kz</a> электронды оқыту кешеніне сәтті тіркелдіңіз.',
        'confirmation' => 'Тіркелуді растау үшін <br>мына сілтемені басыңыз:',
    ],
    'layout' => [
        'wish' => 'Сізге сәттілік тілейміз! <br>Ізгі тілекпен, <br>kitap.kz командасы'
    ],
    'ignore_message' => 'Егер сіз хатты алуға сұраныс жасамасаңыз,<br> бұл хатты елемеуіңізді сұраймыз.',
];
