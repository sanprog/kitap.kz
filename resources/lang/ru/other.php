<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 2:19 PM
 */

return [
    'allvideo'     => 'Барлық видеолар',
    'exit'         => 'Шығу',
    'which'        => 'Қанша оқылды?',
    'partner'      => 'Серіктестер',
    'plink'        => 'Ұсынған кітаптар',
    'dictor'       => 'Диктор',
    'dictor_audio' => 'Диктор оқыған кітаптар',
    'article'      => 'Мақалалар',
	'notfound'	   => 'Ештеңе табылмады',
	'back'		   => 'Артқа қайту'
];