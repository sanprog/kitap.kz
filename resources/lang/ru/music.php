<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/23/18
 * Time: 5:01 PM
 */

return [
    'title'     =>  'Музыка',
    'category'  =>  'Категориялар',
    'tradition' =>  'Дәстүрлі ән өнері',
    'an'        =>  'Әнді сүйсең, менше сүй...',
    'many'      =>  'Көп тыңдалған музыка',
    'shahnama'  =>  'Шаһнама',
    'syrly'     =>  'Сырлы пластинка'
];