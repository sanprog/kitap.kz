$(function () {
  $('body')
    .on('click', '[data-action="destroy"]', function (e) {
      e.preventDefault();
      $('.modal-dialog', '#modal-destroy').addClass('modal-sm');
      $('#modal-destroy').modal('show');
      let url = $(this).data('href');
      let relation = false;
      if ($(this).data('relation') !== undefined) {
        relation = true;
      }
      function destroy() {
        let button = $(this);
        if (check_loading_button(button)) {
          return;
        }
        $.ajax({
          url: url,
          method: 'POST',
          dataType: 'JSON',
          data: {
            _method: 'DELETE',
            _token: window.core_project.csrfToken,
            relation: relation
          },
          success: function (response) {
            if (response.reload_ajax !== undefined) {
              $('#modal-destroy').modal('hide');
              ajax_load_relation($('[data-block-relation="' + response.reload_ajax + '"]'))
            } else {
              window.location.reload();
            }
          },
          beforeSend: function () {
            loading_button(button)
          },
          complete: function () {
            loading_button(button);
          }
        });
      }

      $('#modal-destroy').off('click', '[data-action="yes_destroy"]');
      $('#modal-destroy').on('click', '[data-action="yes_destroy"]', destroy)
    })
    .on('click', '[data-action="add_relation"]', function (e) {
      e.preventDefault();
      let button = $(this);
      let url = $(this).data('href');
      let relation = $(this).data('relation');
      if (check_loading_button(button)) {
        return;
      }
      $.ajax({
        url: url,
        method: 'GET',
        dataType: 'HTML',
        data: {
          relation: $(this).data('relation')
        },
        success: function (response) {
          if ($('#modal-' + relation).length) {
            $('#modal-' + relation).remove()
          }
          $('#content-page').append(response);
          $(document).ready(function () {
            $('#modal-' + relation).modal('show');
            $('#modal-' + relation).data('relation', relation);
            update_load_modal($('#modal-' + relation));
          });
        },
        beforeSend: function () {
          loading_button(button)
        },
        complete: function () {
          loading_button(button);
        }
      });
    })
    .on('click', '[data-action="edit_relation"]', function (e) {
      e.preventDefault();
      let button = $(this);
      let url = $(this).prop('href');
      let relation = $(this).data('relation');
      if (check_loading_button(button)) {
        return;
      }
      $.ajax({
        url: url,
        method: 'GET',
        dataType: 'HTML',
        data: {
          relation: relation
        },
        success: function (response) {
          if ($('#modal-' + relation).length) {
            $('#modal-' + relation).remove()
          }
          $('#content-page').append(response);
          $(document).ready(function () {
            $('#modal-' + relation).modal('show');
            $('#modal-' + relation).data('relation', relation);
            update_load_modal($('#modal-' + relation));
          });
        },
        beforeSend: function () {
          loading_button(button)
        },
        complete: function () {
          loading_button(button);
        }
      });
    });
  $('[data-load]').each(function () {
    // ajax_load_relation($(this));
  });
  //Открывать текущий tab
  function params_unserialize(p) {
    p = p.substr(1);
    var ret = {},
      seg = p.replace(/^\?/, '').split('&'),
      len = seg.length, i = 0, s;
    for (; i < len; i++) {
      if (!seg[i]) {
        continue;
      }
      s = seg[i].split('=');
      ret[s[0]] = s[1];
    }
    return ret;
  }

  if (location.hash !== '') {
    let hash = location.hash;
    if (hash) {
      let hashs = params_unserialize(hash);
      if (hashs['tab']) {
        let obj = $('a[href="#' + hashs['tab'] + '"]');
        if ($(obj).is(':visible')) {
          $(obj).tab('show');
          load_block($('#' + hashs['tab']));
        }
      }
    }
  }
  $('a[data-toggle="tab"]', '.nav-tabs').on('shown.bs.tab', function (e) {
    load_block($($(e.target).attr('href')))
    if (!$(this).closest('.tab-pane').length) {
      if (location.hash) {
        //location.hash = 'tab=' + $(e.target).attr('href').substr(1);
        let hash = location.hash;
        let hashs = params_unserialize(hash);
        hashs['tab'] = $(e.target).attr('href').substr(1);
        location.hash = $.param(hashs);
      } else {
        location.hash = 'tab=' + $(e.target).attr('href').substr(1);
      }
    }
  });

  //Мульти модальные окна
  $(document).on('show.bs.modal', '.modal', function (event) {
    let zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
      $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
  });
});
function check_loading_button(obj) {
  return $(obj).hasClass('loading-progress')
}
function loading_button(obj) {
  if ($(obj).hasClass('loading-progress')) {
    $(obj).removeClass('loading-progress');
    return false;
  } else {
    $(obj).addClass('loading-progress');
    return true;
  }
}
function show_notice(messages, type) {
  let message_all = '';
  if (typeof messages === 'string') {
    message_all = messages;
  } else {
    $.each(messages, function (i, val) {
      message_all += val + '<br/>';
    });
  }
  if (message_all !== '') {
    let title = 'Сообщение';
    switch (type) {
      case 'error':
        title = 'Ошибка';
        break;
      case 'success':
        title = 'Отправлено на почту';
        break;

    }
    $.growl({
      title: title,
      message: message_all,
      duration: 5000,
      style: type
    });
  }
}
function update_load_modal(object) {
  update_init_plugins(object)
  $('form._ajax_form', object).activeForm({
    isModal: object
  });

}
function update_init_plugins(object) {
  if (jQuery.fn.select2 !== undefined) {
    $(".select2", object).select2();
  }
}
function load_block(block) {
  if ($('[data-load]', block).length) {
    $('[data-load]', block).each(function () {
      if ($(this).data('loaded') !== true) {
        ajax_load_relation($(this));
        $(this).data('loaded', true)
      }
    })
  }
  if ($(block).data('load') !== undefined&&$(block).data('loaded') !== true) {
    ajax_load_relation(block);
    $(block).data('loaded', true)
  }
}
function ajax_load_relation(obj) {
  $.ajax({
    url: $(obj).data('load'),
    method: 'GET',
    dataType: 'HTML',
    success: function (response) {
      $(obj).html(response);
      $('.nav-tabs>li.active').each(function () {
        $(this).removeClass('active');
        $('a', this).tab('show')
      });
      update_init_plugins(obj);
    }
  });
}
