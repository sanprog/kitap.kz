<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Support\Str;

class User extends Authenticatable
{

	public $timestamps = false;
	protected $connection = 'rms';
	protected $table = 'system_user';
	protected $primaryKey = 'user_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'login',
		'email',
		'firstname',
		'lastname',
		'born_date',
		'password',
		'status',
		'group_id',
		'salt',
		'recovery_hash',
		'info_status',
		'avatar_url'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	public function getIdAttribute() {
		return $this->user_id;
	}

    private $_is_admin = null;

    public function isAdmin()
    {
        if ($this->_is_admin === null) {
            $this->_is_admin = SAccessUser::where('user_id', $this->id)->where('group_id', 2)->exists();
        }
        return $this->_is_admin;
    }

	public function library() {
		return $this->hasMany('App\Model\Library', 'user_id');
	}

	public static function createBySocialProvider($providerUser)
	{
		return self::create([
			'email'    => $providerUser->getEmail(),
			'login'    => $providerUser->getEmail(),
			'username' => $providerUser->getNickname(),
			'name'     => $providerUser->getName(),
			'password' => BmgUserProvider::userPassword(Str::random(12), Str::random(5)),
		]);
	}

	public function save(array $options = [])
	{
		$result = parent::save($options);
		\Cache::forget('users:' . $this->user_id);
		return $result;
	}

    public static function checkUnique($login, $user_id = null)
    {
        $q = '(`email`=:email)';
        $binds = [':email' => $login];
        if ($user_id) {
            $q .= ' AND user_id!=:user_id';
            $binds[':user_id'] = $user_id;
        }
        return !self::whereRaw($q, $binds)->exists();
    }

    public static function getAuthor($id)
    {
        $user = self::find($id);
        return [
            'id'     => $user->user_id,
            'name'   => $user->firstname == null && $user->surname == null ? $user->login : $user->firstname." ".$user->surname,
            'avatar' => $user->avatar_url
        ];
    }
}
