<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SAccessUser extends Model
{
    protected $table = 'access_user';
	public $timestamps = false;
    protected $fillable = [
        'user_id',
        'group_id',
    ];
}
