<?php

namespace App\Http\Middleware;

use App\Model\Like;
use Closure;
use Auth;

class Unauthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::guest()) {
            if ($request->route()->getName() == 'like.book'){
                return response()->json([],422);
            }
            return redirect('/?auth=1&b='.\url()->previous());
        }
        return $next($request);
    }
}
