<?php

namespace App\Http\Controllers;

use App\Model\Article;
use Illuminate\Http\Request;
use App\Model\Article as Model;
use App\Model\Category;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
	/**
	 * music album list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$model = Model::where(['published' => 1])->orderBy('created_at', 'DESC')->paginate(12);

		foreach ($model as $item) {
		    if(empty($item->img_path)){
		        continue;
            }
            $s = $item->img_path;
            $peace = explode('/', $s);
            $i = stripos($peace[sizeof($peace) - 1], 'thumb_');
            if ($i === false) {
                $path = 'article/' . $item->id . '/';
                $s = explode('/', $item->img_path);

                $path = $path . 'thumb_' . $s[sizeof($s) - 1];
                /*
                    что-то с чем-то
                */
                try {
                    /*Image::make('storage/' . $item->img_path)->resize(314, 232, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save('storage/' . $path);*/
                } catch (NotReadableException $e){
                    $item->thumb_nail = $path;
                }
                $item->thumb_nail = $path;  
            }

        }

		return view('article.index', ['model' => $model]);
	}


	/**
	 * Show one music album.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{
		$model = Model::where('slug', $slug)->first();
        if(count($model)>0) {
            $model->hits += 1;
            $model->save();

            return view('article.show', ['model' => $model]);
        }  else {
            abort(404,'1');
        }
	}
}
