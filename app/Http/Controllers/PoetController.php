<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Poet as Model;

class PoetController extends Controller
{
    public function index() {
        $poets = Model::all();
        return view('poet.index')->with('poets', $poets);
    }

    public function show($id) {

		view()->share('footer_js', <<<JS
		var audioPlayer = new Audioplayer({
			playerName: 'Poet',
			playlist: playlist,
			notification: false,
    		genHTML: false
		});
		audioPlayer.init();
JS
		);

        $poet = Model::find($id);
        return view('poet.show')->with('poet', $poet);
    }
}
