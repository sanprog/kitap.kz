<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Audiobook as Model;
use App\Model\Book;
use App\Model\Category;
use App\Model\Banner;
use App\Model\Dictor;
use App\Model\Library;
use Auth;

class AudiobookController extends Controller
{
	/**
	 * audiobook list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if(request('g')){
			return redirect()->route('audiobooks.list', ['g' => request('g')]);
		}
		$newestAudiobooks = Model::with('book', 'dictor')->orderByDesc('created_at')->offset(4)->limit(4)->get();
		$popularAudiobooks = Model::with('book', 'dictor')->orderByDesc('hits')->limit(4)->get();
		$dictors = Dictor::limit(5)->get();

		$banner = Banner::where('type', 'audio')->first();

		return view('audiobook.index', [
			'newestAudiobooks' => $newestAudiobooks,
			'popularAudiobooks' => $popularAudiobooks,
			'dictors' => $dictors,
			'banner' => $banner
		]);
	}

	/**
	 * audiobook list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list($category = Category::CATEGORY_ROOT_PUBLIC)
	{
		$categories = Category::descendantsAndSelf($category)->pluck('id')->toArray();

		$books = Book::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book']);

		if(request('g')){
			$books->whereHas('genre', function ($query) {

				$query->whereIn('genres.id', explode(',', request('g')));

			});
		}
		$books = $books->with('authors')->pluck('id');

		$model = Model::whereIn('book_id', $books)->paginate(20);


		return view('audiobook.list', [
			'model' => $model,
		]);

	}

	/**
	 * Show one audiobook.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{

		$model = Book::with('audiobook')->where(['slug' => $slug])->first();

		if(!$model){
			abort(404,'1');
		}

		$model->hits += 1;
		$model->save();

		view()->share('footer_js', <<<JS
		var audioPlayer = new Audioplayer({
			playerName: 'Poet',
			playlist: playlist,
			notification: false,
    		genHTML: false
		});
		audioPlayer.init();
JS
		);
		$library_books = Library::where('user_id', Auth::id())->where('resource_type', 'audiobook')->where('resource_id', $model->audiobook->id)->first();

		return view('audiobook.show', ['model' => $model, 'library_books' => $library_books]);
	}

	public function setRank(Request $request) {

		$user = Auth::user();

		if(!$user){
			return redirect()->to(app('url')->previous().'?'. http_build_query(['auth'=>1]))
				->with('error', trans('book.rating.errauth'));
		}

		if(\willvincent\Rateable\Rating::where(['user_id' => $user, 'rateable_id']))

			$this->validate($request, ['rate' => 'required|between:1,5']);

		$post = Model::find($request->id);

		if(isset($post->userAverageRating) && $post->userAverageRating){
			return back()->with('error', trans('book.rating.err'));
		}

		$rating = new \willvincent\Rateable\Rating;

		$rating->rating = $request->rate;

		$rating->user_id = $user->id;


		$post->ratings()->save($rating);


		return back()->with('status', trans('book.rating.ok'));
	}
}
