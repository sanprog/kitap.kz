<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Album as Model;
use App\Model\Category;
use App\Model\Library;
use Auth;

class MusicController extends Controller
{
	/**
	 * music album list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$model       = Model::limit(4)->get();
		$shahnama    = Category::where('slug', 'sha_nama')->first();
		if($shahnama){
			$shahnama = $shahnama->album()->limit(4)->get();
		}
		else{
			$shahnama = [];
		}


		$traditional = Category::where('slug', 'dasturli_an_oneri')->first();

		if($traditional) {
			$traditional = $traditional->album()->limit(4)->get();
		}
		else {
			$traditional =[];
		}

		return view('music.index', ['model' => $model, 'shahnama' => $shahnama, 'traditional' => $traditional]);
	}

	/**
	 * music album list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function indexList($category)
	{
		$categories = Category::descendantsAndSelf($category);

		$categoriesId = $categories->pluck('id');
        if(count($categories) == 0){
            abort(404,'1');
        }
		$model = Model::whereHas('categories', function ($query) use ($categoriesId) {
			$query->whereIn('categories.id', $categoriesId);
		})->get();

		return view('music.index_list', [
			'model' => $model,
			'categories' => $categories,
		]);
	}

	/**
	 * Show one music album.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{

		$model = Model::where('slug', $slug)->first();
        if(count($model) == 0){
			abort(404,'1');
        }
		$model->hits += 1;
		$model->save();

		view()->share('footer_js', <<<JS
		var audioPlayer = new Audioplayer({
			playerName: 'Poet',
			playlist: playlist,
			notification: false,
    		genHTML: false
		});
		audioPlayer.init();
JS
		);
		$library_audios = Library::where('user_id', Auth::id())->where('resource_type', 'audio')->where('resource_id', $model->id)->first();

		return view('music.show', ['model' => $model, 'library_audios' => $library_audios]);
	}
}
