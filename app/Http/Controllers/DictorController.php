<?php

namespace App\Http\Controllers;


use App\Model\Dictor;
use Illuminate\Http\Request;


/**
 * Class DictorController
 *
 * @package App\Http\Controllers
 */
class DictorController extends Controller
{


	/**
	 * Show dictor page.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( Dictor $dictor )
	{

		$audiobooks = $dictor->audiobooks()->paginate(12);

		if(count($audiobooks) == 0){
		    abort(404,'1');
        }

		return view('dictor.show', ['model' => $dictor, 'audiobooks' => $audiobooks]);
	}
}
