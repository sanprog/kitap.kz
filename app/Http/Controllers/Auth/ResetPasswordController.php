<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {

        $user = User::where('recovery_hash',$token)->first();

        if (empty($user)) {
            abort(404,"1");
        }

        return view('auth.passwords.reset')->with(
            ['token' => $token]
        );
    }

    public function reset(Request $request)
    {
        $this->validator($request->all())->validate();

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $user = User::where(['recovery_hash' => $request['token']])->first();

        if($user){
            $password = $request['password'];
            $user->salt = Str::random(5);
            $user->password = BmgUserProvider::userPassword($password, $user->salt);
            $user->recovery_hash = null;
            $user->save();
            return redirect('/')->with('status', 'Құпиясөз өзгертілді');
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return redirect(route('password.reset', ['token' => $request['token']], true));
    }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            // 'name' => 'required|string|max:255',
            'token' => 'required|string|max:255',
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|string|min:6|max:255|same:password',
        ],[
            'password_confirmation.same' => trans('auth.register_pass_not_equal'),
        ],[
            'password' => trans('auth.auth_title_password'),
            'password_confirmation' => trans('auth.register_field_repass')
        ]);
    }
}
