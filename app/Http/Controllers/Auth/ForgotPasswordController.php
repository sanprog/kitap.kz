<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $user = User::where('email',$request['email'])->first();

        if (!$user) {
            return redirect("/")->with('status','Нету пользователя');
        }
        $user->recovery_hash = BmgUserProvider::userPassword(Str::random(7), Str::random(5));
        $user->save();

        $link = route('password.reset', ['token' => $user->recovery_hash], true);

        Mail::send('emails.recovery_password', ['login' => $user->login, 'link' => $link], function($message) use ($request) {
            $message->from('admin@kitap.kz', 'Admin');
            $message->to($request['email'])->subject('Kitap.kz сайтының құпиясөзін');
        });

        return redirect("/")->with('status','Сіздің поштаға хат жіберілді');
    }
}
