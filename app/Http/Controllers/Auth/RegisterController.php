<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        \Validator::extend('checkUnique', function ($attribute, $value, $parameters) {
            return User::checkUnique($value);
        });
        return Validator::make($data, [
           // 'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|checkUnique',
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|string|min:6|max:255|same:password',
        ],[
            'password_confirmation.same' => trans('auth.register_pass_not_equal'),
            'check_unique' => trans('auth.register_err_unique_email')
        ],[
            'email' => 'Email',
            'password' => trans('auth.auth_title_password'),
            'password_confirmation' => trans('auth.register_field_repass')
        ]);
    }

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->all())));

		$this->guard()->login($user);

		Mail::send('emails.register_activation', ['login' => $user->login], function($message) use ($request) {
			$message->from('admin@kitap.kz', 'Admin');
			$message->to($request['email'])->subject(trans('mail.registration.title'));
		});

		return $this->registered($request, $user)
			?: redirect($this->redirectPath());
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		$salt = Str::random(5);
		$hash = md5(rand(0, 99999) . Str::random(16) . time());
		return User::create([
			'group_id' => 3,
			'status' => 0,
			'login' => $data['email'],
			'email' => $data['email'],
			'recovery_hash' => $hash,
			'salt' => $salt,
			'password' => BmgUserProvider::userPassword($data['password'], $salt)
		]);
    }

}
