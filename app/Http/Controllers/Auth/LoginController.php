<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'login';
    }

    public function login(Request $request)
    {
    	$this->redirectTo = $request->get('b');
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $joomla_login = DB::connection('joomla')->table('kt_users')->where(['email' => $request['login']])->first();
        $user_rms = User::where('email','=',$request['login'])->orWhere('email',$request['login'])->first();

        if ($joomla_login && !$user_rms){
            $salt = Str::random(5);
            $password = Str::random(8);
            $hash = md5(rand(0, 99999) . Str::random(16) . time());
            $user = User::create([
                    'group_id'      => 3,
                    'status'        => 0,
			        'login'         => $request['login'],
			        'email'         => $request['login'],
			        'recovery_hash' => $hash,
			        'salt'          => $salt,
			        'password'      => BmgUserProvider::userPassword($password, $salt)
            ]);
            $text = "Kitap.kz жаңа сайтына қош келдіңіз!\nТөменде сіздің жаңа профиль ақпараты бар. \nЛогин: ".$request['login']."\nҚұпиясөз: ".$password."\nСайтқа сiлтеме: kitap.kz";
            Mail::raw($text, function($message) use ($request) {
                $message->from('admin@kitap.kz', 'Admin');
                $message->to($request['login'])->subject('Kitap.kz Сайтына кіру ақпараты');
            });
            $request['password'] = $password;
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
	/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  mixed $user
	 * @return mixed
	 */
	protected function authenticated(Request $request, $user)
	{
		if ($request->ajax()) {
			\Cache::forget('users:' . $user->user_id);
			return response()->json([
				'success' => true,
				'redirect' => $this->redirectPath()
			]);
		}
	}
	protected function redirectTo(){

		if($this->redirectTo){
			return $this->redirectTo;
		}

		if (\url()->previous() == route('login', [], true)) {
			return route('index');
		}
		return \url()->previous();
	}
}
