<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Services\SocialAccountService;
use Socialite;
use Auth;
use Session;

class SocialController extends Controller
{
	public function login($provider)
	{
		return Socialite::with($provider)->redirect();
	}

	public function callback(SocialAccountService $service, $provider)
	{
		$driver   = Socialite::driver($provider);
		$user = $service->createOrGetUser($driver, $provider);
		if ($user == false) {
			Session::flash('error', 'Кешіріңіз, бірақ әлеуметтік желі арқылы кіру үшін, сіздің әлеуметтік желі аккаунтыңызда электрондық пошта (E-mail) болу керек.');
			return redirect('/');
		}
		Auth::login($user, true);
		return redirect()->intended('/');

	}
}
