<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Video as Model;
use App\Model\Category;
use App\Model\Library;
use Auth;

class VideoController extends Controller
{
	/**
	 * video list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::with(['videos'])->descendantsOf(Category::CATEGORY_ROOT_VIDEO);
        if(count($categories) == 0){
            abort(404,'1');
        }
		return view('video.index', ['categories' => $categories]);
	}

	/**
	 * video list.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list($category = Category::CATEGORY_ROOT_PUBLIC)
	{
		$categories = Category::with(['videos'])->where(['id' => $category])->get();
        if(count($categories) == 0){
            abort(404,'1');
        }
		$categoriesId = $categories->pluck('id');

		$model = Model::whereHas('categories', function ($query) use ($categoriesId) {
			$query->whereIn('categories.id', $categoriesId);
		})->paginate(12);

		return view('video.list', ['model' => $model, 'categoryName' => $categories[0]->name]);
	}

	/**
	 * Show one video.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{
		$model = Model::where('slug', $slug)->first();
        if(!$model){
            abort(404,'1');

        }
		$model->hits += 1;
		$model->save();
		$library_videos = Library::where('user_id', Auth::id())->where('resource_type', 'video')->where('resource_id', $model->id)->first();

		return view('video.show', ['model' => $model, 'library_videos' => $library_videos]);
	}
}
