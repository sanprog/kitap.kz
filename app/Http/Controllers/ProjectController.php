<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/15/18
 * Time: 3:44 PM
 */

namespace App\Http\Controllers;

use App\Model\Fairytale as Model;
use App\Model\Specauthor;
use App\Model\Category;

class ProjectController extends Controller
{
    public function fairytale()
    {
        return view('fairytale.fairytale',[
            'items' => Model::get()
        ]);
    }

    public function specauthor()
    {
        return view('specauthor.index',[
            'items' => Specauthor::with('author')->get()
        ]);
    }

	public function specBook()
	{
		$items = Category::where('old_id', 113)->first()->books()->paginate(9);

		if (\Request::ajax()) {
			return view('specbook.page', ['items' => $items])->render();
		}

		return view('specbook.index',[
			'items' => $items
		]);
	}


    public function showSpecauthor($id)
    {
        view()->share('footer_js', <<<JS
		var audioPlayer = new Audioplayer({
			playerName: 'Test',
			playlist: playlist,
			notification: false,
    		genHTML: false
		});
		audioPlayer.init();
JS
        );
        return view('specauthor.show',[
            'item' => Specauthor::with('author')->with('specauthorAudio')->where('id',$id)->first()
        ]);
    }
}