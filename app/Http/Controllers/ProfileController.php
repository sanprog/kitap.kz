<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\Audiobook;
use App\Model\Video;
use App\Model\Album;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Auth;
use Bmg\Core\Auth\BmgUserProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Model\Library;
use App\Model\Fragment;
use App\Model\UserSocialAccount;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('unauth');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show()
	{
		$user = Auth::user();
		$social_user = (!empty(UserSocialAccount::where('user_id', $user->user_id)) && empty($user->salt));

		return view('profile.show', ['user' => $user, 'is_social' => $social_user]);
	}

	public function update(UserRequest $request)
	{
		$user = Auth::user();
		$social_user = (!empty(UserSocialAccount::where('user_id', $user->user_id)) && empty($user->salt));

		$curPassword = $request->input('cur_password');

		if (BmgUserProvider::userPasswordCheck($user->password, $user->salt, $curPassword) && !$social_user) {
			$user->password = BmgUserProvider::userPassword($request->new_password, $user->salt);
			$user->save();
		} else if ($social_user) {
			$salt = Str::random(5);
			$hash = md5(rand(0, 99999) . Str::random(16) . time());

			$user->recovery_hash = $hash;
			$user->salt = $salt;
			$user->password = BmgUserProvider::userPassword($request->new_password, $salt);
			$user->save();
		}


		if ($request->hasFile('avatar')) {
			Storage::disk('public')->delete($user->img_path);

			$avatar         = $request->file('avatar');
			$filename       = $user->id . time() . '.' . $avatar->getClientOriginalExtension();
			$filepath       = $avatar->storeAs('avatar', $filename, 'public');
			$user->avatar_url = '/storage/'.$filepath;
		}

		$user->fill($request->toArray());


		$user->save();

		return redirect('profile')->with('status', 'Сіздің жеке ақпарат <br> жаңартылды.');
	}

	public function saveQuote(Request $request)
	{
		$user  = Auth::user();

		$quote = Fragment::firstOrNew([
			'user_id'  => $user->id,
			'book_id'  => $request->book_id,
			'fragment' => $request->quote
		]);

		$quote->save();

		return redirect()->back()->with('success', ['OK']);
	}

	public function deleteQuote($id)
	{
		$user  = Auth::user();
		$model = Fragment::where(['user_id' => $user->id, 'id' => $id])->first();
		$model->delete();
	}

	public function myQuote()
	{
		$user  = Auth::user();

		$quotes = Fragment::where(['user_id' => $user->id])->get();

		return view('profile.myquote', [
			'quotes'      => $quotes,
		]);
	}


	/**
	 * save to my library
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function saveToLib($id, $type)
	{
		$user = Auth::user();

		$save = Library::firstOrNew(['resource_id' => $id, 'resource_type' => $type]);
		$user->library()->save($save);

		switch ($type) {
			case Library::TYPE_BOOK:
				return redirect()->to(route('books.show', ['slug' => Book::find($id)->slug]))->with('status', 'Кітап сіздің <br> кітапханаңызға қосылды');
				break;
			case Library::TYPE_AUDIOBOOK:
				return redirect()->to(route('audiobooks.show', ['slug' => Audiobook::find($id)->book->slug]))->with('status', 'Аудиокітап сіздің <br> кітапханаңызға қосылды');
				break;
			case Library::TYPE_VIDEO:
				return redirect()->to(route('video.show', ['slug' => Video::find($id)->slug]))->with('status', 'Бейне сіздің <br> кітапханаңызға қосылды');
				break;
			case Library::TYPE_MUSIC:
				return redirect()->to(route('music.show', ['slug' => Album::find($id)->slug]))->with('status', 'Альбом сіздің <br> кітапханаңызға қосылды');
				break;
		}


	}

	/**
	 * Read the book.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function mylibrary()
	{
		$user = Auth::user();

		$books      = Library::where(['resource_type' => Library::TYPE_BOOK, 'user_id' => $user->id])->get()->pluck('resource_id');
		$audiobooks = Library::where(['resource_type' => Library::TYPE_AUDIOBOOK, 'user_id' => $user->id])->get()->pluck('resource_id');
		$video      = Library::where(['resource_type' => Library::TYPE_VIDEO, 'user_id' => $user->id])->get()->pluck('resource_id');
		$music      = Library::where(['resource_type' => Library::TYPE_MUSIC, 'user_id' => $user->id])->get()->pluck('resource_id');

		$books      = Book::whereIn('id', $books)->get();
		$audiobooks = Audiobook::whereIn('id', $audiobooks)->get();
		$video      = Video::whereIn('id', $video)->get();
		$music      = Album::whereIn('id', $music)->get();

		return view('profile.library', [
			'books'      => $books,
			'audiobooks' => $audiobooks,
			'video'      => $video,
			'music'      => $music,
		]);
	}

	public function deleteItem($type,$id)
    {
        $user = Auth::user();
        $model = Library::where(['resource_type' => $type, 'user_id' => $user->id, 'resource_id' => $id])->first();
        $model->delete();
    }

	public function deleteItemLibrary($type,$id)
    {
        $user = Auth::user();
        $model = Library::where(['resource_type' => $type, 'user_id' => $user->id, 'resource_id' => $id])->first();
        $model->delete();
		if($type == 'book') {
			$book = Book::find($id);
			return redirect()->route('books.show', ['slug' => $book->slug]);
		}
		else if ($type == 'audiobook') {
			$audiobook = Audiobook::find($id);
			return redirect()->route('audiobooks.show', ['slug' => $audiobook->book->slug]);
		}
		else if ($type == 'audio') {
			$audio = Album::find($id);
			return redirect()->route('music.show', ['slug' => $audio->slug]);
		}
		else if ($type == 'video') {
			$video = Video::find($id);
			return redirect()->route('video.show', ['slug' => $video->slug]);
		}
    }
}
