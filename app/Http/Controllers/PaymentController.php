<?php

namespace App\Http\Controllers;

use App\Model\Epay;


class PaymentController extends Controller
{

    public function donate()
    {
        $provider    = request()->input('provider');
        $donate      = intval(request()->input('donate'));
        $otherDonate = intval(request()->input('other-donate'));
        if ($otherDonate) {
            $donate = $otherDonate;
        }
        $destination = request()->input('destination');
        switch ($provider) {
            default:
            case 'epay':
                $epay = new Epay();
                $epay->donate(true)->setAmount($donate);
                $epay->destination = $destination;
                $order_request     = $epay->makeRequest();
                view()->share('footer_js', <<<JS
$('#epay_form').submit();
JS
                );
                break;
        }
        return view('payment.redirect', compact('order_request'));
    }


    public function status()
    {
        $order_id = request()->get('order_id');
        if (request()->get('success', false) !== false) {
            $payment_orders = [];
            if (session()->has('payment')) {
                $payment_orders = session('payment', []);
            }
            if (isset($payment_orders[$order_id])) {
                unset($payment_orders[$order_id]);
            }
            session()->put('payment', $payment_orders);
            return view('payment.status', [
                'error' => false
            ]);
        } elseif (request()->get('error', false) !== false) {
            $error = trans('subscribe.subscribe_payment_error');
            return view('payment.status', [
                'error' => $error
            ]);
        } else {
            return redirect(route('index'));
        }
    }
    public function receive()
    {
        $response = stripSlashes(@$_POST['response']);
        \Epay::log($response);
        $epay     = new Epay();
        $order    = $epay->processResponse($response);


        exit(0);
    }
}
