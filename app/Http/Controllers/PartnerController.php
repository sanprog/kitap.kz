<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Partner as Model;

class PartnerController extends Controller
{
    public function index() {
        $partners = Model::all();
        return view('partner.index')->with('partners', $partners);
    }

    public function show($id) {
        $partner = Model::find($id);
        $books = $partner->books()->paginate(20);
        return view('partner.show')->with('partner', $partner)->with('books', $books);
    }
}
