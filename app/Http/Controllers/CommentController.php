<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\Comment;
use App\Http\Controllers\Controller;
use App\Model\CommentUser;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class CommentController extends Controller
{
    /**
     * undocumented function
     *
     * @param Request $request
     * @return void
     * @author
     */
    public function add(Request $request)
    {

        $this->validate($request, [
            'comment'   => 'required|string|max:1500',
            'parent'    => 'required|integer',
            'id'        => 'required|integer',
            'type'      => 'required|string'
        ]);

        $userId = Auth::user()->user_id;

        if ($userId) {

            $parent = $request->parent;
            $commentBody = $request->comment;

            $comment = new Comment;

            $comment->user_id = $userId;
            $comment->parent_id = $parent;
            $comment->comment = $commentBody;

            if ($request->type == 'book') {
                $book = Book::find($request->id);
                $comment = $book->comments()->save($comment);
                return redirect(route("books.show",['slug' => $book->slug]));
            }




        }else {
            abort(404,"1");
        }
// dd($comment);

// dd($comment);
    }

    public function like($comment_id)
    {
        $user_id = Auth::user()->user_id;
        $check = CommentUser::where([
            'comment_id' => $comment_id,
            'user_id' => $user_id
        ])->first();
        if (isset($check)) {
            $check->delete();
            return count(CommentUser::where('comment_id', $comment_id)->get());
        } else {
            $like = new CommentUser;
            $like->comment_id = $comment_id;
            $like->user_id = $user_id;
            $like->save();
            return count(CommentUser::where('comment_id', $comment_id)->get());
        }
    }
}