<?php

namespace App\Http\Controllers;


use App\Model\Audiobook;
use App\Model\Book;
use App\Model\Album;
use App\Model\EmailSubscribe;
use App\Model\Fragment;
use App\Model\Video;
use App\Model\Author;
use App\Model\Dictor;
use App\Model\Slider;
use App\Model\Actual;
use App\Model\WordSearch;
use Illuminate\Http\Request;
use Mail;
use sngrl\SphinxSearch\SphinxSearch;
use Validator;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{

    /**
     * Show index site page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider            = Slider::all();
        $popularBooks      = Book::with('authors')->popular(6)->get();
        $popularAudiobooks = Audiobook::popular(6)->with('book.authors')->get();
        $actual            = Actual::all();

        //TODO доделать выборку цитат из БД (пока стоит заглушка)
        $quotes             = Fragment::limit(4)->get();


        return view('main.main', [
            'popularBooks'      => $popularBooks,
            'popularAudiobooks' => $popularAudiobooks,
            'slider'            => $slider,
            'actual'            => $actual,
            'quotes'            => $quotes
        ]);
    }

    /**
     * Search site page.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $word = $request->q;
        /*
        $books = Book::whereRaw('match(name,description) against(\''.$word.'\')')
            ->with('authors');
        $books = $books->paginate(20,['*'],'book');
        $books->setPageName('book');

        $musics = Album::whereRaw('match(name,description) against(\''.$word.'\')')
            ->with('authors');

        $musics = $musics->paginate(20,['*'],'music');
        $musics -> setPageName('music');

        $videos = Video::whereRaw('match(name,description) against(\''.$word.'\')');

        $videos = $videos->paginate(20,['*'],'video');
        $videos -> setPageName('video');

        $authors = Author::whereRaw('match(name,description) against(\''.$word.'\')');

        $authors = $authors->paginate(20,['*'],'authors');
        $authors -> setPageName('authors');

        $audiobooks = Audiobook::whereHas('book', function ($query) use ($word) {
            $query->whereRaw('match(name,description) against(\''.$word.'\')');
        })->with('book.authors');

        $audiobooks = $audiobooks->paginate(20,['*'],'audio');
        $audiobooks -> setPageName('audio');

        $dictors = Dictor::whereRaw('match(dictor_name,description) against(\''.$word.'\')');

        $dictors = $dictors->paginate(20,['*'],'dict');
        $dictors -> setPageName('dict');

        if(true || count($books) == 0 || count($audiobooks) == 0 || count($videos) == 0 || count($musics) == 0 || count($authors) == 0) {
        */
        $len = mb_strlen($word);

        $s = "";

        for ($i = 0; $i < mb_strlen($word); $i++) {
            if ($i == 0) {
                $s .= "__" . mb_substr($word, $i, 1) . " ";
            } elseif ($i == 1) {
                $s .= "_" . mb_substr($word, $i - 1, 2) . " ";
            } else {
                $s .= mb_substr($word, $i - 2, 3) . " ";
            }
        }

        if (strlen($word) > 1) {
            $s .= mb_substr($word, mb_strlen($word) - 2, 2) . "_ ";
        } else {
            $s .= "_" . mb_substr($word, mb_strlen($word) - 1, 1) . "_ ";
        }

        $s .= mb_substr($word, mb_strlen($word) - 1, 1) . "__";
        $sphinx = new SphinxSearch();
        $result = $sphinx->search($s,'searcher')
            ->range('len',$len-2,$len+2)
            ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_ANY)
            ->setRankingMode(\Sphinx\SphinxClient::SPH_RANK_WORDCOUNT)
            ->setSelect("*, @weight+2-abs(len-" . $len . ")+ln(frequency)/ln(1000) as myrank")
            ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "myrank desc, frequency desc")
            ->limit(5)
            ->get();
        $ids = [];
        // TODO без if проверки
        if ($result != false) {
            foreach ($result['matches'] as $key => $value) {
                if( $value['attrs']['myrank'] < 10 * $len) break;
                $ids[] = $key;
            }
        }

        $finds = WordSearch::select('word')->whereIn('id', $ids)->get();
        $book_ids = [];
        $album_ids = [];
        $video_ids = [];
        $author_ids = [];
        $dictor_ids = [];

        foreach ($finds as $find) {
            $sphinx = new SphinxSearch();
            $result = $sphinx->search($find->word, 'book') ->setFieldWeights(
                array(
                    'author_name'  => 10,
                    'book_name'    => 8
                )
            )->limit(10000)->get();
            if ($result != false) {
                foreach ($result['matches'] as $key => $value) {
                    $book_ids[] = $key;
                }
            }


            $sphinx = new SphinxSearch();
            $result = $sphinx->search($find->word, 'video')->limit(10000)->get();
            if ($result != false) {
                foreach ($result['matches'] as $key => $value) {
                    $video_ids[] = $key;
                }
            }

            $sphinx = new SphinxSearch();
            $result = $sphinx->search($find->word, 'album')->setFieldWeights(
                array(
                    'author_name'  => 10,
                    'book_name'    => 8
                )
            )->limit(10000)->get();
            if ($result != false) {
                foreach ($result['matches'] as $key => $value) {
                    $album_ids[] = $key;
                }
            }

            $sphinx = new SphinxSearch();
            $result = $sphinx->search($find->word, 'author')->limit(10000)->get();
            if ($result != false) {
                foreach ($result['matches'] as $key => $value) {
                    $author_ids[] = $key;
                }
            }

            $sphinx = new SphinxSearch();
            $result = $sphinx->search($find->word, 'dictor')->limit(10000)->get();
            if ($result != false) {
                foreach ($result['matches'] as $key => $value) {
                    $dictor_ids[] = $key;
                }
            }
        }
        if(true || count($books) == 0) {
            $books = Book::whereIn('id', $book_ids)
                ->with('authors')->orderByRaw("FIELD(id, ".implode(",",$book_ids).")");
            $books = $books->paginate(20, ['*'], 'book');
            $books->setPageName('book');

        }
        if(true || count($musics) == 0) {
            $musics = Album::whereIn('id', $album_ids)
                ->with('authors')->orderByRaw("FIELD(id, ".implode(",",$album_ids).")");

            $musics = $musics->distinct()->paginate(20, ['*'], 'music');
            $musics->setPageName('music');
        }
        if(true || count($videos) == 0) {
            $videos = Video::whereIn('id', $video_ids)->orderByRaw("FIELD(id, ".implode(",",$video_ids).")");

            $videos = $videos->paginate(20, ['*'], 'video');
            $videos->setPageName('video');
        }
        if(true || count($authors) == 0) {
            $authors = Author::whereIn('id', $author_ids)->orderByRaw("FIELD(id, ".implode(",",$author_ids).")");

            $authors = $authors->paginate(20, ['*'], 'authors');
            $authors->setPageName('authors');
        }
        if(true || count($audiobooks) == 0) {
            $audiobooks = Audiobook::whereHas('book', function ($query) use ($book_ids) {
                $query->whereIn('id', $book_ids);
            })->with('book.authors')->orderByRaw("FIELD(book_id, ".implode(",",$book_ids).")");

            $audiobooks = $audiobooks->paginate(20, ['*'], 'audio');
            $audiobooks->setPageName('audio');
        }
        //   }

        if(true || count($dictors) == 0) {
            $dictors = Dictor::whereIn('id',$dictor_ids)->orderByRaw("FIELD(id, ".implode(",",$dictor_ids).")");

            $dictors = $dictors->paginate(20, ['*'], 'dict');
            $dictors->setPageName('dict');
        }
		if(empty($request->curr)) {
			if (count($books) > 0) {
				$request->request->add(['curr' => 'book']);
			} else if (count($audiobooks) > 0) {
				$request->request->add(['curr' => 'audiobook']);
			} else if (count($musics) > 0) {
				$request->request->add(['curr' => 'music']);
			} else if (count($videos) > 0) {
				$request->request->add(['curr' => 'video']);
			} else if (count($authors) > 0) {
				$request->request->add(['curr' => 'author']);
			} else if (count($dictors) > 0) {
				$request->request->add(['curr' => 'dictor']);
			}
		}

		return view('main.search', [
            'books'      => $books,
            'musics'     => $musics,
            'videos'     => $videos,
            'authors'    => $authors,
            'audiobooks' => $audiobooks,
            'dictors'    => $dictors,
        ]);
    }


    public function mail(Request $request) {
        $data = $request->toArray();
        Mail::raw('Текст письма', function($message)
        {
            $message->from('us@example.com', 'Laravel');
            $message->to('foo@example.com')->cc('bar@example.com');
        });

        return redirect()->to(route('contacts'));
    }

    public function autocomplete(Request $request) {

        $dbConnect = DB::connection('dictionary');
        $dictionary = $dbConnect->select('select Word from dictionaryword where word like ? limit 0, 10', ['%'.$request->term.'%']);

        $data = [];
        foreach ($dictionary as $item){
            $data[] = $item->Word;
        }

        return response()->json($data, 200);
    }

    public function dictionary(Request $request) {

        $dbConnect = DB::connection('dictionary');
        $dictionary = $dbConnect->select("SELECT d.Id, d.Word, d.Definition, d.GrammerTypeName Grammer, gt.LongName GrammerLong, dc.Language, dc.Translation, dwr.Relation, dwr.Word RelationWord, de.Example 
										  FROM dictionarydefinition d 
										  INNER JOIN grammertype gt ON d.GrammerTypeName = gt.Name 
										  LEFT JOIN definitionwordrelation dwr ON d.Id = dwr.DefinitionId 
										  LEFT JOIN dictionary dc ON d.Id = dc.DefinitionId 
										  LEFT JOIN definitionexample de ON d.Id = de.DefinitionId 
										  WHERE d.word = ? ORDER BY dc.Language, dwr.Relation",
            [$request->word]);


        return response()->json($dictionary, 200);
    }

    public function saveEmail(Request $request){
        Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:email_subscribe',
        ])->validate();

        $email = new EmailSubscribe();
        $email->email = $request['email'];
        $email->save();

        return redirect("/")->with('status','Сіз пошта арқылы жазылдыңыз '.$request['email']);
    }
    public function error(){
        return view('errors.404');
    }

}
