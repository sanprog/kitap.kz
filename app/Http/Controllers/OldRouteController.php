<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\Video;
use App\Model\Album;
use Illuminate\Http\Request;
use App\Model\Audiobook;
use App\Model\Author;
use Illuminate\Support\Facades\Log;

class OldRouteController extends Controller
{
    public function book($id, $slug){
		$book = Book::where('slug', $slug)->first();
		if ($book == null){
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::info("[OldRouteController@book] url=".url()->current());
		}
		return redirect(route('books.show', ['slug' => $slug]),301);

	}

	public function audiobook($id, $slug){

    	$model = Audiobook::where(['old_id' => $id])->first();
    	if ($model == null){
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::error("[OldRouteController@audiobook] url=" . url()->current());
		}

		return redirect(route('audiobooks.show', ['slug' => $model->book->slug]), 301);

	}

	public function author($id, $slug){

		$model = Author::where(['old_id' => $id])->first();
		if($model == null){
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::error("[OldRouteController@author] url=" . url()->current());
		}

		return redirect(route('author.show', ['slug' => $model->id]), 301);

	}

	public function audio($id, $slug){
		$model = Album::where('slug', $slug)->first();
		if($model==null) {
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::error("[OldRouteController@audio] url=" . url()->current());
		}
		return redirect(route('music.show', ['slug' => $slug]), 301);
	}

	public function video($id, $slug){
		$model = Video::where('slug', $slug)->first();
		if($model == null) {
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::error("[OldRouteController@video] url=" . url()->current());
		}
		return redirect(route('video.show', ['slug' => $slug]), 301);

	}

	public function reader($slug){
		$book = Book::where('slug', $slug)->first();
		if ($book == null){
			Log::useFiles(base_path() . "/storage/logs/old_url.log", 'info');
			Log::error("[OldRouteController@book] url=".url()->current());
		}
		return redirect(route('book.read', ['slug' => $slug]), 301);

	}

}
