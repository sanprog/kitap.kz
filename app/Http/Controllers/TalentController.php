<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Talent as Model;
use App\Model\Talentitem;

class TalentController extends Controller
{
    public function index() {
        $model = Model::all();
        return view('talent.index')->with('model', $model);
    }

    public function show($id) {
        $model = Model::where(['id' => $id])->with('items')->first();
		if(count($model) == 0) {
			abort(404, '1');
		}
			$v = "";
        foreach($model->items as $item) {
            $v = $v . "
    		var audioPlayer_{$item->id} = new Audioplayer({
    			playerName: 'player_{$item->id}',
    			playlist: playlist_{$item->id},
    			notification: false,
        		genHTML: false
    		});
    		audioPlayer_{$item->id}.init();
            ";
        }

        view()->share('footer_js', <<<JS
		var audioPlayer = new Audioplayer({
			playerName: 'Poet',
			playlist: playlist,
			notification: false,
    		genHTML: false
		});
		audioPlayer.init();

        {$v}
JS
		);

		/*if(count($model) == 0){
		    abort(404,'1');
        }*/
        return view('talent.show')->with('model', $model);
    }

	public function read($talent)
	{

		$book = Talentitem::where('id', $talent)->first();
		if(!$book) {
			abort(404,'1');
		}
		//ToDo сделать полиморф Savedprogress
		/*
		$user_id = -1;
		if (!Auth::guest()) {
			$user_id = Auth::user()->id;
		}
		$progressfind = $request->get('cfi');

		if( empty($request->get('cfi')) &&  $user_id != -1) {
			$progressfind = Savedprogress::where('user_id', $user_id)->where('book_id', $book->id)->first();

			if (!empty($progressfind)) {
				$progressfind = $progressfind->progress;
			}
		}
		*/
		return view('reader.talent', ['book' => $book]);
	}
}
