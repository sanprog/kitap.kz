<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Author;
use Illuminate\View\View;

class AuthorController extends Controller
{
	/**
	 * Show one music album.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($author) : View
	{

		$model = Author::with('books')->find($author);
        if (count($model) == 0){
            abort(404,'1');
        }
		$books = $model->books()->paginate(12);

		$model->hits += 1;
		$model->save();
		return view('author.show', ['model' => $model, 'books' => $books]);
	}
}
