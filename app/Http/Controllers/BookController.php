<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Book as Model;
use App\Model\Error;
use App\Model\Genre;
use App\Model\Category;
use App\Model\Banner;
use App\Model\Library;
use App\Model\Savedprogress;
use Auth;

class BookController extends Controller
{
	/**
	 * book list. not use
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$books  = Model::with('authors')->get();
		$genres = Genre::all();

		$categoryTree = Category::descendantsOf(Category::CATEGORY_ROOT_SINCE)->toTree();


		return view('book.index', [
			'books'        => $books,
			'genres'       => $genres,
			'categoryTree' => $categoryTree,
		]);
	}

	/**
	 * book list from category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function booksPublic()
	{
		if (request('g')) {
			return $this->booksPublicList(Category::CATEGORY_ROOT_PUBLIC);
		}

		$categories = Category::descendantsAndSelf(Category::CATEGORY_ROOT_PUBLIC)->pluck('id')->toArray();

		$newestBooks = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])
			->with('authors')
			->newest(4)
			->get();

		$popularBooks = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])
			->with('authors')
			->popular(4)
			->get();

		$banner = Banner::where('type', 'public')->first();

		return view('book.public', [
			'newestBooks'  => $newestBooks,
			'popularBooks' => $popularBooks,
			'banner'       => $banner,
		]);
	}

	/**
	 * book list from category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function booksScience()
	{

		$categories = Category::descendantsAndSelf(Category::CATEGORY_ROOT_SINCE)->pluck('id')->toArray();

		$newestBooks = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])->with('authors')->newest(4)->get();

		$popularBooks = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])->with('authors')->popular(4)->get();

		$banner = Banner::where('type', 'science')->first();

		return view('book.science', [
			'newestBooks'  => $newestBooks,
			'popularBooks' => $popularBooks,
			'banner'       => $banner,
		]);
	}

	/**
	 * book list from category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function booksPublicList($category = Category::CATEGORY_ROOT_PUBLIC)
	{

		$categories = Category::descendantsAndSelf($category)->pluck('id')->toArray();

		$books = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book']);

		if(request('g')){
			$books->whereHas('genre', function ($query) {

					$query->whereIn('genres.id', explode(',', request('g')));

			});
		}
		$books = $books->with('authors')->paginate(20);

		return view('book.public_list', [
			'books' => $books,
		]);
	}

	/**
	 * book list from category.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function booksScienceList($category = Category::CATEGORY_ROOT_SINCE)
	{

		$categories = Category::descendantsAndSelf($category)->pluck('id')->toArray();

		$books = Model::whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])
			->with('authors')->paginate(20);

		return view('book.science_list', [
			'books' => $books,
		]);
	}

	/**
	 * Show one book.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{

		$book = Model::where('slug', $slug)->with('authors')->first();

		if(!$book){
			abort(404,'1');
		}
		$library_books = Library::where('user_id', Auth::id())->where('resource_type', 'book')->where('resource_id', $book->id)->first();

		return view('book.show', ['book' => $book, 'library_books' => $library_books]);
	}

	/**
	 * Read the book.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function read($slug, Request $request)
	{

		$book = Model::where('slug', $slug)->with('authors')->first();
		if(!$book) {
		    abort(404,'1');
        }
		$book->hits += 1;
		$book->save();

		$user_id = -1;
		if (!Auth::guest()) {
			$user_id = Auth::user()->id;
		}
		$progressfind = $request->get('cfi');

		if( empty($request->get('cfi')) &&  $user_id != -1) {
			$progressfind = Savedprogress::where('user_id', $user_id)->where('book_id', $book->id)->first();

			if (!empty($progressfind)) {
				$progressfind = $progressfind->progress;
			}
		}

		return view('reader.reader', ['book' => $book, 'progressfind' => $progressfind]);
	}

	public function readSpec($slug, Request $request)
	{
		$book = Model::where('slug', $slug)->first();
		if(!$book) abort(404,'1');
		if(!is_dir(storage_path('app/public/specbook/' . $book->id . '/'))){
			abort(404);
		}
		$files = scandir(storage_path('app/public/specbook/' . $book->id . '/pages/'));
		$countPages = count($files)/3;

		return view('reader.spec', ['book' => $book, 'countPages' => $countPages]);
	}

	public function readSpecGetPage( $slug, Request $request)
	{
		if (request()->ajax()) {
			$book = Model::where('slug', $slug)->first();
			$page = str_pad($request->input('page'), 4, 0, STR_PAD_LEFT);
			return '<img src="/storage/specbook/' . $book->id . '/pages/page'. $page .'.jpg">';
		}
		abort(404);
	}

    public function saveError(Request $request)
    {
        $this->validate($request, [
            'id'      => 'required|integer',
            'quote'   => 'required|string',
            'url'     => 'required|string'
        ]);
        $id = $request->id;
        $quote = $request->quote;
        $url = $request->url;

        $error = new Error();
        $error->book_id = $id;
        $error->selected_text = $quote;
        $error->url = $url;
        $error->save();

        return response()->json(['success' => 1]);
    }

    public function setRank(Request $request) {

		$user = Auth::user();

		if(!$user){
			return redirect()->to(app('url')->previous().'?'. http_build_query(['auth'=>1]))
				->with('error', trans('book.rating.errauth'));
		}

		if(\willvincent\Rateable\Rating::where(['user_id' => $user, 'rateable_id']))

		$this->validate($request, ['rate' => 'required|between:1,5']);

		$post = Model::find($request->id);

		if($post->userAverageRating){
			return back()->with('error', trans('book.rating.err'));
		}

		$rating = new \willvincent\Rateable\Rating;

		$rating->rating = $request->rate;

		$rating->user_id = $user->id;


		$post->ratings()->save($rating);


		return back()->with('status', trans('book.rating.ok'));
	}

    public function savePage(Request $request)
	{
		if($request->slug && $request->cfi && !Auth::guest()) {
			$userid = Auth::user()->id;
			$bookid = Model::where('slug', $request->slug)->first()->id;

			$progressfind = Savedprogress::where('user_id', $userid)->where('book_id', $bookid)->first();

			if(is_null($progressfind)){
				$savedprogress = new Savedprogress();

				$savedprogress->user_id = $userid;
				$savedprogress->book_id = $bookid;
				$savedprogress->progress = $request->cfi;

				$savedprogress->save();
			} else {
				if($progressfind->progress != $request->cfi) {
					$progressfind->progress = $request->cfi;
					$progressfind->update();
				}
			}
		}
	}
}
