<?php

namespace App\Services;

use App\Model\UserSocialAccount;
use App\User;

class SocialAccountService
{
	public function createOrGetUser($providerObj, $providerName)
	{

		$providerUser = $providerObj->user();
		$providerEmail = !empty($providerUser->getEmail()) ? $providerUser->getEmail() :
							(!empty($providerUser->accessTokenResponseBody['email']) ? $providerUser->accessTokenResponseBody['email'] : '');

		if (empty($providerEmail)) {
			return false;
		}

		$account = UserSocialAccount::whereProvider($providerName)
			->whereProviderUserId($providerUser->getId())
			->first();

		if ($account) {
			return $account->user;
		} else {
			$account = new UserSocialAccount([
				'provider_user_id' => $providerUser->getId(),
				'provider' => $providerName]);

			$user = User::whereEmail($providerEmail)->first();

			if (!$user) {
				$user = User::createBySocialProvider($providerUser);
			}

			$account->user()->associate($user);
			$account->save();

			return $user;

		}

	}
}