<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\{
	Category, Book, Author, Track, Video, Audiobook, Album, Audiofile, Article
};
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

use Symfony\Component\DomCrawler\Crawler;

class Parser extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'db:parse {type=all}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Parse from old joomla site kitap.kz';

	protected $joomla;
	protected $source;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->joomla = DB::connection('joomla');

		if (app()->environment() !== 'production') {
			$this->source = '/Volumes/kitap.kz';
		} else {
			$this->source = resource_path();
		}
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->output->getFormatter()->setStyle('error', new OutputFormatterStyle('red', 'black'));
		$this->output->getFormatter()->setStyle('blue', new OutputFormatterStyle('blue'));
		//$this->line("This text is Red"  , 'blue');


		$type = $this->argument('type');
		if(is_callable(array($this, $type))) {
			$this->$type();
		}
		elseif($type == 'all'){
			$this->parseCategory();
			$this->parseAutors();
			$this->parseBooks();
			$this->parseVideo();
			$this->parseAudio();
			$this->parseAudiobook();
			$this->parseArticle();
		}
		else{
			$this->error("Доступны методы: ". implode(', ', array_filter(get_class_methods($this), function ($item) {
					return strstr($item, 'parse');
				})));
		}
	}

	public function uploadEpub($oldId, $newId)
	{

		$item = $this->joomla->table(DB::raw('`kt_k2_items` as `item`, `kt_k2_items_content` as `content`'))->where([
				'item.category_type' => 'book',
				'content.type'       => 'folder',
				'item.id'            => $oldId,
			])->whereColumn('item.id', '=', 'content.item_id')->first();

		if (! is_dir("{$this->source}{$item->src}")) {
			$this->error("нет ресурса: {$this->source}{$item->src}");

			return false;
		}


		$file_path = 'uploads/books/' . $newId . '/';
		$file_name = "File_book" . ".epub";

		exec("
			cp -R {$this->source}{$item->src} " . base_path() . "/public/uploads/tmp &&
			cd " . base_path() . "/public/uploads/tmp &&
			
			zip -0Xq my-book.epub mimetype &&
			zip -Xr9Dq my-book.epub * &&
			mv " . base_path() . "/public/uploads/tmp/my-book.epub " . public_path($file_path) . $file_name . " &&" . "rm -rf " . base_path() . "/public/uploads/tmp/");

		/*exec("
			cd ".base_path()."/resources{$item->src} &&
			zip -0Xq ../my-book.epub mimetype &&
			zip -Xr9Dq ../my-book.epub * &&
			mv ".base_path()."/resources{$item->src}../my-book.epub ".public_path($file_path).$file_name
		);*/

		$this->info("Загружена книга - " . public_path($file_path) . $file_name);

		return $file_path . $file_name;
	}

	public function parseCategory()
	{
		$rawCategories = $this->joomla->table('kt_k2_categories as a')
			->select('a.*', 'b.id as bid', 'c.id as cid', 'd.id as did')
			->leftJoin('kt_k2_categories as b', 'a.parent', '=', 'b.id')
			->leftJoin('kt_k2_categories as c', 'b.parent', '=', 'c.id')
			->leftJoin('kt_k2_categories as d', 'c.parent', '=', 'd.id')
			->get();


		foreach ($rawCategories as $item) {

			//книги
			if ($item->bid == 1 || $item->cid == 1 || $item->did == 1) {
				$category = Category::where(['name' => $item->name])->first();
				if ($category !== null) {
					$category->slug   = $item->alias;
					$category->old_id = $item->id;

					$category->save();
				} else {
					Category::firstOrCreate([
						'name'          => $item->name,
						'slug'          => $item->alias,
						'parent_id'     => Category::CATEGORY_ROOT_PUBLIC,
						'old_id'        => $item->id,
						'old_parent_id' => $item->parent,
					]);
				}
			} //видео
			elseif ($item->bid == 4 || $item->cid == 4 || $item->did == 4) {
				$category = Category::firstOrCreate([
					'name'      => $item->name,
					'slug'      => $item->alias,
					'parent_id' => Category::CATEGORY_ROOT_VIDEO,
					'old_id'    => $item->id,
				]);
			} //музыка
			elseif ($item->bid == 3 || $item->cid == 3 || $item->did == 3) {
				$category = Category::firstOrCreate([
					'name'      => $item->name,
					'slug'      => $item->alias,
					'parent_id' => Category::CATEGORY_ROOT_MUSIC,
					'old_id'    => $item->id,
				]);
			}
		}

		//$affected = DB::update('update categories as a LEFT JOIN categories as b ON a.old_parent_id = b.old_id SET a.parent_id = b.id where a.old_parent_id is not null AND a.created_at is not null' );

		Category::fixTree();

		$this->info("Категории спарсены; ");
	}

	public function parseBooks()
	{

		$rawBooks   = $this->joomla->table('kt_k2_items')->where(['category_type' => 'book', 'catid' => 113])->get();
		$categories = Category::all()->keyBy('old_id');

		$errArr = [];
		foreach ($rawBooks as $item) {
			try {

				$book = Book::where(['old_id' => $item->id])->first();
				if ($book !== null) {
					$this->info("ID:{$book->id} - '{$item->title}' Уже есть в базе");

					if (! is_dir(public_path('uploads/books/' . $book->id . '/'))) {
						exec("mkdir " . public_path('uploads/books/' . $book->id . '/'));
					}
					$files = scandir(public_path('uploads/books/' . $book->id . '/'));
					$epubFiles = preg_grep('+.epub+', $files);

					if($epubFiles){
						foreach ($epubFiles as $epubOne){
							unlink(public_path('uploads/books/' . $book->id . '/'.$epubOne));
							$this->line(public_path('uploads/books/' . $book->id . '/'.$epubOne).': Удален', 'blue');
						}
					}

					$file_path = $this->uploadEpub($item->id, $book->id);

					if ($file_path) {
						$this->info("ID:{$book->id} - '.epub загружен");
						$book->file_path = '/' . $file_path;
						$book->slug = $item->alias;
						$book->save();

					}

					//upload image
					$kitapUrl = 'http://95.56.249.247/media/k2/items/cache/';

					$imgFile  = @fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');

					if ($imgFile) {
						$save_path = 'books/' . $book->id . '/';
						$file_name = 'Img_book.jpg';
						Storage::disk('uploads')->put($save_path . $file_name, $imgFile);
						$book->img_path = '/uploads/' . $save_path . $file_name;
						$book->save();
					} else {
						$this->error("'{$item->title}' нет изображения");
					}

					continue;
				}

				$newBook = Book::firstOrCreate([
					'name'   => $item->title,
					'slug'   => $item->alias,
					'old_id' => $item->id,
				]);

				$newBook->description = $item->introtext;
				$newBook->hits = $item->hits;
				$newBook->save();

				//upload image
				$kitapUrl = 'http://95.56.249.247/media/k2/items/cache/';
				$imgFile  = @fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');
				if ($imgFile) {
					$save_path = 'books/' . $newBook->id . '/';
					$file_name = 'Img_book' . '_' . time() . '.jpg';
					Storage::disk('uploads')->put($save_path . $file_name, $imgFile);
					$newBook->img_path = '/uploads/' . $save_path . $file_name;
					$newBook->save();
				} else {
					$this->error("'{$item->title}' нет изображения");
				}


				$file_path = $this->uploadEpub($item->id, $newBook->id);

				if ($file_path) {
					$newBook->file_path = '/' . $file_path;
					$newBook->save();
				}


				$rawAutors = $this->joomla->table('kt_k2_items_author')
					->where(['item_id' => $item->id])
					->get()
					->pluck('author_id');

				if ($rawAutors) {
					$newBook->authors()->sync(Author::whereIn('old_id', $rawAutors)->pluck('id'));
				}

				if ($item->catid && isset($categories[$item->catid])) {
					$newBook->categories()->sync($categories[$item->catid]->id);
				} else {
					if (! in_array($item->catid, $errArr)) {
						$this->info("'{$item->catid}' не нашел категорию");
						array_push($errArr, $item->catid);
					}
				}
			} catch (\Exception $exception) {
				$this->error("'{$item->title}' не добавлено");
				$this->error($exception->getMessage());
			}

			$this->info("'{$item->title}' ID:{$item->id} - книга добавлена");
		}
	}

	public function parseAutors()
	{
		//TODO: сделать привязку картинок
		$rawAutors = $this->joomla->table('kt_k2_items')->where(['category_type' => 'author'])->get();

		foreach ($rawAutors as $item) {
			try {
				$newAutor = Author::firstOrCreate([
					'name'        => $item->title,
					'old_id'      => $item->id,
				]);

				$newAutor->description = $item->introtext;
				$newAutor->hits        = $item->hits;
				$newAutor->save();

				$kitapUrl = 'http://95.56.249.247/media/k2/items/cache/'; ///uploads/authors/1/avtor-1.jpeg
				$imgFile  = @fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');
				if ($imgFile) {
					$file_name = 'avatar.jpg';
					Storage::disk('public')->put('authors/' . $newAutor->id . "/" . $file_name, $imgFile);
					$newAutor->img_path = 'authors/' . $newAutor->id . "/" . $file_name;
					$newAutor->save();
				}
				$this->line("Автор добавлен: " . $newAutor->name, 'blue');

			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}
		$this->line("Авторы импортированы: " . count($rawAutors), 'blue');
	}

	public function parseVideo()
	{
		Category::where('id', 4)->update(['old_id' => 4]);

		$categories = Category::all()->keyBy('old_id');

		$rawVideos = $this->joomla->table('kt_k2_items as i')
			->select(DB::raw('i.*, c.src as video_path, prev.src as preview_path'))
			->leftJoin('kt_k2_items_content as c', 'i.id', '=', 'c.item_id')
			->leftJoin('kt_k2_items_content as prev', 'i.id', '=', 'prev.item_id')
			->where([
				'i.category_type' => 'video',
				'c.type'          => 'video',
				'prev.type'       => 'preview',
			])
			->get();

		foreach ($rawVideos as $item) {
			try {
				$newVideo = Video::firstOrCreate([
					'name'        => $item->title,
					'slug'        => $item->alias,
					'description' => $item->introtext,
					'old_id'      => $item->id,
				]);
				$newVideo->categories()->sync($categories[$item->catid]->id);

				//загрузка видеофайла и превьюшки
				$file_path    = storage_path('app/public/video/' . $newVideo->id);
				$file_name    = "File_video" . ".mp4";
				$preview_name = "preview" . ".jpg";

				if (! is_file("{$this->source}{$item->video_path}")) {
					throw new \Exception('Не найден файл на удаленном сервере. ' . $item->video_path);
				}

				if (is_file($file_path . "/" . $file_name)) {
					$this->info("'{$item->id} - {$item->title}' Уже есть в базе");
					continue;
				}

				if (! is_dir($file_path)) {
					exec("mkdir $file_path");
				}

				$files = scandir($file_path . '/');
				if (preg_grep('+.mp4+', $files)) {
					$this->info('видео уже загружено ID:' . $newVideo->id);
					continue;
				}

				exec("cp -R {$this->source}{$item->preview_path} $file_path/$preview_name &&
								cp -R {$this->source}{$item->video_path} $file_path/$file_name");

				$newVideo->file_path = 'video/' . $newVideo->id . '/' . $file_name;
				$newVideo->img_path  = 'video/' . $newVideo->id . '/' . $preview_name;
				$newVideo->hits      = $item->hits;
				$newVideo->save();


				$this->info("Добавлено видео: " . $newVideo->name);
			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}

		$this->line("Видео импортировано: " . count($rawVideos), 'blue');
		//dd($rawVideos);

	}

	public function parseAudio()
	{
		$rawAudio   = $this->joomla->table('kt_k2_items')->where(['category_type' => 'audio'])->get();
		$categories = Category::all()->keyBy('old_id');

		foreach ($rawAudio as $item) {
			try {
				$newAlbum = Album::firstOrCreate([
					'name'        => $item->title,
					'slug'        => $item->alias,
					'description' => $item->introtext,
					'old_id'      => $item->id,
				]);

				$newAlbum->categories()->sync($categories[$item->catid]->id);

				//обложка альбома
				$kitapUrl  = 'http://95.56.249.247/media/k2/items/cache/';
				$imgFile   = fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');
				$file_name = 'Img_album' . '_' . time() . '.jpg';
				$file_path = storage_path('app/public/album/' . $newAlbum->id);
				Storage::disk('public')->put('album/' . $newAlbum->id . "/" . $file_name, $imgFile);
				$newAlbum->img_path = 'album/' . $newAlbum->id . "/" . $file_name;

				$newAlbum->hits = $item->hits;
				$newAlbum->save();

				if (! is_dir($file_path)) {
					exec("mkdir $file_path");
				}

				$files = scandir($file_path . '/');
				if (preg_grep('+.mp3+', $files)) {
					$this->info('айдиофайлы уже добавлены ID:' . $newAlbum->id);
					continue;
				}

				$rawTrack = $this->joomla->table('kt_k2_items_content')->where([
						'item_id' => $item->id,
						'type'    => 'audio'
					])->get();

				if ($rawTrack->isEmpty()) {
					$this->info('нет песен в альбоме ID:' . $item->id . ' - ' . $item->title);
					continue;
				}

				foreach ($rawTrack as $key => $track) {
					$newTrack = new Track(['name' => $track->name]);
					exec("cp -R {$this->source}{$track->src} $file_path/track_{$key}.mp3");
					$newTrack->file_path = 'album/' . $newAlbum->id . "/track_{$key}.mp3";
					$newAlbum->track()->save($newTrack);
					$this->info('Добавлена песня в альбом ID:' . $item->id . ' - ' . $track->name);
				}

				$this->info('Альбом добавлен ID:' . $item->id . ' - ' . $item->title);
			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}
	}

	public function parseAudiobook()
	{
		$rawAudiobook = $this->joomla->table('kt_k2_items')->where(['category_type' => 'audiobook'])->get();

		/*$deleted = Book::where('old_id', null)->get();

		dd($deleted);*/

		$aliases = [];

		foreach ($rawAudiobook as $item) {
			try {

				if(in_array($item->alias, $aliases)){
					$itemAlias = $item->alias.$item->id;
					$this->error($itemAlias);
					$newBook = new Book([
						'name' => $item->title,
					]);
					$this->info('добавлена: '.$itemAlias);
				}
				else
				{
					$aliases[] = $item->alias;
					$itemAlias = $item->alias;
					$newBook = Book::firstOrNew([
						'name' => $item->title,
					]);

				}




				if($newBook->old_id === null){
					$newBook->slug = $itemAlias;
					$newBook->description = $item->introtext;
					$newBook->save();
				}
				if($newBook->img_path == null){
					$kitapUrl  = 'http://95.56.249.247/media/k2/items/cache/';
					$imgFile   = @fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');

					if ($imgFile) {
						$save_path = 'books/' . $newBook->id . '/';
						$file_name = 'Img_book.jpg';
						Storage::disk('uploads')->put($save_path . $file_name, $imgFile);
						$newBook->img_path = '/uploads/' . $save_path . $file_name;
						$newBook->save();
						$this->info('Обложка аудиокниги добавлена: '.$newBook->img_path);
					} else {
						$this->error("'{$item->title}' нет изображения");
					}
				}



				$newAudiobook = Audiobook::firstOrCreate([
					'book_id'   => $newBook->id,
					'old_id'    => $item->id,
				]);

				$newAudiobook->hits = $item->hits;

				$newAudiobook->save();

				$file_path = storage_path('app/public/audiobook/' . $newAudiobook->id);
				if (! is_dir($file_path)) {
					exec("mkdir $file_path");
				}

				/*$files = scandir($file_path . '/');
				if (preg_grep('+.mp3+', $files)) {
					$this->info('айдиофайлы уже добавлены ID:' . $newAudiobook->id);
					continue;
				}*/

				$rawTrack = $this->joomla->table('kt_k2_items_content')->where([
						'item_id' => $item->id,
						'type'    => 'audiobook'
					])->get();

				if ($rawTrack->isEmpty()) {
					$this->info('нет аудиофайлов ID:' . $item->id . ' - ' . $item->title);
					continue;
				}

				$newAudiobook->audiofile()->delete();
				foreach ($rawTrack as $key => $track) {
					$newTrack = new Audiofile(['name' => $track->name]);
					exec("cp -R {$this->source}{$track->src} $file_path/track_{$key}.mp3", $output, $return_var);

					//проверяем успешно ли отработало cp
					if ($return_var === 0) {
						$newTrack->file_path = 'audiobook/' . $newAudiobook->id . "/track_{$key}.mp3";
						$newAudiobook->audiofile()->save($newTrack);
					}else{
						//$this->info('не удалось скопировать аудиофайл ID:' . $newAudiobook->id . ' - ' . $item->title);
					}
				}

				$this->info('Аудиокнига добавлена ID:' . $newAudiobook->id . ' - ' . $item->title);
			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}
	}

	public function parseArticle()
	{
		$rawArticle = $this->joomla->table('kt_easyblog_post')->get();


		foreach ($rawArticle as $item) {
			try {
				$newArticle = Article::firstOrCreate([
					'name'   => $item->title,
					'slug'   => $item->permalink,
					'old_id' => $item->id,
				]);

				$newArticle->title = strip_tags($item->intro);
				$newArticle->text  = $item->content;
				$newArticle->published  = $item->published;
				$newArticle->created_at  = $item->publish_up;
				$newArticle->hits  = $item->hits;

				//картинка статьи
				$jsonImage = json_decode($item->image);
				if($jsonImage){
					$imgFile   = @fopen($jsonImage->url, 'r');
					$file_name = 'img_article.jpg';
					Storage::disk('public')->put('article/' . $newArticle->id . "/" . $file_name, $imgFile);
					$newArticle->img_path = 'article/' . $newArticle->id . "/" . $file_name;
				}

				$newArticle->save();

				// Create new instance for parser.
				$crawler = new Crawler();
				$crawler->addHtmlContent($item->content, 'UTF-8');

				// Get images from body.
				$crawler->filter('img')->each(function (Crawler $node, $i) use ($newArticle) {
					$file = $node->getNode(0);

					$imageSrc = $file->getAttribute('src');
					$imgFile   = @fopen('http://95.56.249.247/'.$imageSrc, 'r');
					$file_name = 'article_'.$i.'.jpg';
					Storage::disk('public')->put('article/' . $newArticle->id . "/" . $file_name, $imgFile);

					$file->setAttribute('src', '/storage/article/' . $newArticle->id . "/" . $file_name);

				});
				$newArticle->text = $crawler->html();
				$newArticle->save();

				$this->info('Статья добавлена: '.$item->title.' | '.$newArticle->slug);

			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
				$this->error($item->title);
			}
		}
	}


	public function parseTed()
	{
		Category::where('id', 4)->update(['old_id' => 4]);

		$rawVideos = $this->joomla->table('kt_k2_items as i')
			->select(DB::raw('i.*, c.src as video_path'))
			->leftJoin('kt_k2_items_content as c', 'i.id', '=', 'c.item_id')
			->where([
				'i.category_type' => 'ted',
				'c.type'          => 'ted',
			])
			->get();

		foreach ($rawVideos as $item) {
			try {
				$newVideo = Video::firstOrCreate([
					'name'        => $item->title,
					'slug'        => $item->alias,
					'description' => $item->introtext,
					'old_id'      => $item->id,
				]);
				$newVideo->categories()->sync(Category::CATEGORY_ROOT_TED);

				//загрузка видеофайла и превьюшки
				$file_path    = storage_path('app/public/video/' . $newVideo->id);
				$file_name    = "File_video" . ".mp4";
				$preview_name = "preview" . ".jpg";


				if (is_file($file_path . "/" . $file_name)) {
					$this->info("'{$item->id} - {$item->title}' Уже есть в базе");
					continue;
				}

				if (! is_dir($file_path)) {
					exec("mkdir $file_path");
				}

				//upload image
				$kitapUrl = 'http://95.56.249.247/media/k2/items/cache/';
				$imgFile  = @fopen($kitapUrl . md5('Image' . $item->id) . '_M.jpg', 'r');
				if ($imgFile) {
					$save_path = 'video/' . $newVideo->id . '/';
					Storage::disk('public')->put($save_path . $preview_name, $imgFile);
					$newVideo->img_path = 'video/' . $newVideo->id . '/' . $preview_name;
					$newVideo->save();
				} else {
					$this->error("'{$item->title}' нет изображения");
				}


				if (! is_file("{$this->source}{$item->video_path}")) {
					throw new \Exception('Не найден файл на удаленном сервере. ' . $item->video_path);
				}

				exec("cp -R {$this->source}{$item->video_path} $file_path/$file_name");

				$newVideo->file_path = 'video/' . $newVideo->id . '/' . $file_name;
				$newVideo->img_path  = 'video/' . $newVideo->id . '/' . $preview_name;
				$newVideo->hits      = $item->hits;
				$newVideo->save();


				$this->info("Добавлено видео: " . $newVideo->name);
			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}

		$this->line("Видео импортировано: " . count($rawVideos), 'blue');

	}

	public function parseSpec()
	{
		//catid:113 - kus kanaty category
		$rawBooks   = $this->joomla->table('kt_k2_items')->where(['category_type' => 'book', 'catid' => 113, 'published' => 1])->get();

		foreach ($rawBooks as $item){
			try {
				$book = Book::where(['old_id' => $item->id])->first();

				$path = $this->source.'/reader/murazhai/'.$item->alias.'/files/assets/pages/';
				if (! is_dir($path)) {
					$this->error("нет ресурса: ".$path);
					throw new \Exception();
				}

				if(!is_dir(storage_path('app/public/specbook'))){
					mkdir(storage_path('app/public/specbook'), 0775);
				}

				$newPath = storage_path('app/public/specbook/' . $book->id . '/');
				if (! is_dir($newPath)) {
					exec("mkdir $newPath");
				}
				exec("cp -R $path $newPath");

				if($book) $this->line("есть: " . $item->id . ' - ' . $book->slug, 'blue');
				else $this->line("нет: " . $item->id, 'error');

			} catch (\Exception $exception) {
				$this->error($exception->getMessage());
			}
		}

		$this->line("Всего: " . $rawBooks->count(), 'blue');

	}
}
