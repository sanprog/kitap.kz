<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MySqlDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dump {--i} {file_path?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the mysqldump utility using info from .env';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $ds = DIRECTORY_SEPARATOR;


		// TODO: закончить импорт базы.
		if($this->option('i') && $this->argument('file_path')){
			dd(base_path().$ds.$this->argument('file_path'));
		}

        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $database = env('DB_DATABASE');
        
        $ts = time();

        $path = database_path() . $ds . 'backups' . $ds . date('Y', $ts) . $ds . date('m', $ts) . $ds . date('d', $ts) . $ds;
        $file = date('Y-m-d-His', $ts) . '-dump-' . $database . '.sql';
        $command = sprintf('mysqldump -h %s -u %s -p %s > %s', $host, $username, $database, $path . $file);

		$this->info('WILL EXEC: '.$command);

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        exec($command);

		$this->info('finished');

    }
}
