<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Model\Category;
use Illuminate\Support\Facades\Route;

class CategoryWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'type' => 1
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

		$routeName = Route::currentRouteName();
		$uriPrefix = '/';

		switch($routeName) {
			case 'books.science' :
			case 'books.science.category' :
				$rootCategory = Category::CATEGORY_ROOT_SINCE;
				$uriPrefix    = route('books.science');
				break;
			case 'audiobooks' :
			case 'audiobooks.list' :
			case 'audiobooks.list.category' :
				$rootCategory = Category::CATEGORY_ROOT_PUBLIC;
				$uriPrefix    = route('audiobooks.list');
				break;
			case 'books.public' :
			case 'books.public.category' :
				$rootCategory = Category::CATEGORY_ROOT_PUBLIC;
				$uriPrefix    = route('books.public');
				break;
			case 'music' :
			case 'music.list' :
				$rootCategory = Category::CATEGORY_ROOT_MUSIC;
				$uriPrefix    = route('music.list');
				break;
		}

		$activeCategory = request()->route('category');
		$parentLine = [];
		if($activeCategory) {
			$parentLine = Category::ancestorsOf($activeCategory)->pluck('id')->toArray();
		}
		//dump($parentLine);

		$categoryTree     = Category::descendantsOf($rootCategory)->toTree();
        $viewName = 'widgets.category_widget';
		if($this->config['type'] == 2) {
            $viewName = 'widgets.mob_category_widget';
        }
        return view($viewName, [
            'config' => $this->config,
            'categoryTree' => $categoryTree,
            'routeName' => $routeName,
            'uriPrefix' => $uriPrefix,
            'parentLine' => $parentLine,
			'activeCategory' => $activeCategory
        ]);

    }
}
