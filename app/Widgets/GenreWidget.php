<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Model\Genre;

class GenreWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'type' => 1
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$genres = Genre::all();

		$g = request('g');
		$g = explode(',', $g);
		$g = array_unique($g);

		$gLine = implode(',', $g);
		$viewName = 'widgets.genre_widget';
        if($this->config['type'] == 2) {
            $viewName = 'widgets.mob_genre_widget';
        }
        return view($viewName, [
            'config' => $this->config,
            'genres' => $genres,
            'gLine' => $gLine,
            'g' => $g
        ]);
    }
}
