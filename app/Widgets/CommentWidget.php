<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 3/6/18
 * Time: 3:54 PM
 */

namespace App\Widgets;


use App\Model\Book;
use App\Model\Comment;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentWidget extends AbstractWidget
{

    protected $config = [
        'id'    =>  null,
        'type'  =>  ''
    ];

    public function run()
    {
        switch ($this->config['type']) {
            case 'book':
                $object = Book::with([ 'comments' => function($query){

                	$query->withCount('likes');

				}])->find($this->config['id']);
                break;
        }
        $comments = null;
        if(isset($object->comments)) {
            $comments = $object->comments;
        }

        foreach ($comments as $comment){
            $userId = $comment->user_id;
            $user = \App\User::getAuthor($userId);
            $comment->name = $user['name'];
            $comment->avatar = $user['avatar'];
        }

        $children = [];
        foreach ($comments as $comment){
            $children[$comment->parent_id][] = $comment;
        }

        $user['user_id'] = -1;
        $user['name'] = 'Гость';
        $user['avatar'] = '../images/setting.png';
        return view('comment.comment',[
            'config'    => $this->config,
            'comments'  => $comments,
            'children'  => $children,
            'user'      => Auth::user() != null ? \App\User::getAuthor(Auth::user()->user_id) : $user
        ]);
    }

}