<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Model\Book;
use Cache;

class ChoiceWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if( !Cache::has('top_book') ) {
            Cache::put('top_book', 0, 360);
        }
        $temp = Cache::get('top_book');
        $top_books = Book::orderBy('hits', 'DESC')->limit(20)->get();
        $book = $top_books[$temp];
        if($temp >= count($top_books) - 1) {
            $temp = -1;
        }
        Cache::put('top_book', $temp + 1, 360);

        return view('widgets.choice_widget', [
            'config' => $this->config,
            'book' => $book,
        ]);
    }
}
