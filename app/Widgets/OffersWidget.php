<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Route;
use App\Model\{Category, Book, Audiobook};

class OffersWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$offers = Book::where(['offer' => true])->with('authors', 'audiobook')->limit(15)->get();

        $routeName = Route::currentRouteName();

        switch ($routeName){
            case 'books.science' :
                $categories = Category::descendantsAndSelf(Category::CATEGORY_ROOT_PUBLIC)->pluck('id')->toArray();
                $model = Book::where(['offer' => true])->whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])
                    ->with('authors')
                    ->get();
                break;
            case 'books.public' :
                $categories = Category::descendantsAndSelf(Category::CATEGORY_ROOT_SINCE)->pluck('id')->toArray();
                $model = Book::where(['offer' => true])->whereRaw("id in (select categoryable_id from categoryables where category_id in (" . implode(',', $categories) . ") and categoryable_type = ?)", ['App\Model\Book'])
                    ->with('authors')
                    ->get();
                break;
            case 'audiobooks' :
                $books      = Audiobook::pluck('book_id');
                $model      = Book::where(['offer' => true])->whereIn('id', $books)->get();
                break;
        }

        return view('widgets.offers_widget', [
            'config' => $this->config,
            'offers' => $model,
        ]);
    }
}
