<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Model\Book;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class StatisticWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $bookCount = Book::count();

        $onlineUsers = Analytics::getAnalyticsService()
                                    ->data_realtime
                                    ->get('ga:' . env('ANALYTICS_VIEW_ID'), 'rt:activeVisitors')
                                    ->totalsForAllResults['rt:activeVisitors'];

        $startDate = Carbon::now()->subMinute();
        $endDate = Carbon::now();
        $per = Period::create($startDate, $endDate);
        $todayBooks = Analytics::performQuery($per, 'ga:sessions', ['filters' => 'ga:pagePath%3D@books']);

        return view('widgets.statistic_widget', [
            'config'    => $this->config,
			'onlineUsers' => $onlineUsers,
            'todayBooks' => $todayBooks->rows[0][0]
        ]);
    }
}
