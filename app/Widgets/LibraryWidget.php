<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Auth;
use Illuminate\Support\Facades\Route;
use App\Model\{Library, Book, Audiobook};

class LibraryWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		if (!Auth::check()) {
			return '';
		}

		$user = Auth::user();
		$routeName = Route::currentRouteName();

		switch($routeName) {
			case 'books.science' :
			case 'books.public' :
				$books      = Library::where(['resource_type' => Library::TYPE_BOOK, 'user_id' => $user->id])
					->get()
					->pluck('resource_id');
				$model      = Book::whereIn('id', $books)->get();
				break;
			case 'audiobooks' :
				$audiobooks = Library::where(['resource_type' => Library::TYPE_AUDIOBOOK, 'user_id' => $user->id])
					->get()
					->pluck('resource_id');
				$books      = Audiobook::whereIn('id', $audiobooks)->pluck('book_id');
				$model      = Book::whereIn('id', $books)->get();
				break;
		}

        return view('widgets.library_widget', [
			'config' => $this->config,
			'model'  => $model,
			'user'   => $user
        ]);
    }
}
