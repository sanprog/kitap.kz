<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $fillable = [
		'id',
		'type',
		'img_path',
		'uri',
		'title',
	];
}
