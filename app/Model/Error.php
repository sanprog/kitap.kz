<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/1/18
 * Time: 4:47 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    protected $table = 'book_errors';
    protected $fillable = [
            'book_id',
            'selected_text',
            'url'
    ];
}