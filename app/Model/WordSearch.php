<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 3/2/18
 * Time: 10:37 AM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class WordSearch extends Model
{
    protected $table = 'word_search';
    protected $fillable = [
        'word',
        'trigram',
        'frequency'
    ];
}