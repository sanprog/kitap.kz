<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/19/18
 * Time: 4:53 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Specauthor extends Model
{
    protected $table = 'specauthor';
    protected $fillable = [
        'author_id',
        'desc'
    ];
    public $timestamps = false;

    public function author() {
        return $this->belongsTo('App\Model\Author');
    }

    public function specauthorAudio() {
        return $this->hasMany('App\Model\SpecauthorAudio');
    }
}