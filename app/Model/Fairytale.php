<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/15/18
 * Time: 2:09 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Fairytale extends Model
{
    protected $table = 'fairytale';
    protected $fillable = ['book_id'];
    public $timestamps = false;

    public function book()
    {
        return $this->belongsTo('App\Model\Book');
    }
}