<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = [
        'id',
        'name',
    ];
    public function books(){
        return $this->belongsToMany('App\Model\Book');
    }

    public static function all($columns = ['*'])
	{
		return (new static)->newQuery()->with('books')->get(
			is_array($columns) ? $columns : func_get_args()
		);
	}
}