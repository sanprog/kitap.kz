<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Dictor extends Model
{
	protected $fillable = [
		'id',
		'dictor_name',
		'img_src',
		'description',
	];

	public function audiobooks() {
		return $this->hasMany('App\Model\Audiobook');
	}

	protected function setImgSrcAttribute($file) {
		if (! empty($file)) {
			$file_name = str_slug($this->dictor_name) . '.' . $file->guessExtension();
			$path      = Storage::disk('uploads')->putFileAs('dictors', $file, $file_name);
			$old_path  = str_replace('/uploads', '', $this->img_src);
			if ($this->exists && Storage::disk('uploads')
					->exists($old_path) && $this->img_src != '/uploads/authors/default/avatar.png'
			) {
				Storage::disk('uploads')->delete($old_path);
			}
			\Event::listen('eloquent.saved: ' . static::class, function ($model) use ($file_name) {
				/** @var self $model */
				if ($model->isDirty('img_src')) {
					$save_path = public_path('uploads/dictors/' . $model->id . '/');
					if (! \File::isDirectory($save_path)) {
						\File::makeDirectory($save_path, 493, true);
					}
					$old_path = public_path(ltrim($model->img_src, '/'));
					if (\File::isFile($old_path)) {
						\File::move(public_path(ltrim($model->img_src, '/')), $save_path . $file_name);
					}
					self::where('id', '=', $model->id)
						->update(['img_src' => ('/uploads/dictors/' . $model->id . '/' . $file_name)]);
				}
			});
			$this->attributes['img_src'] = 'uploads/' . $path;

			return $this;
		}
	}
}
