<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Talentitem extends Model
{
	protected $table = 'talent_items';
	protected $fillable = [
		'id',
		'talent_id',
		'img_path',
		'book_path',
		'audio_path',
	];

	public function talent()
	{
		return $this->belongsTo('App\Model\Talent');
	}
}
