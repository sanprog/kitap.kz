<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Track extends Model {

	protected $fillable = [
		'id',
		'album_id',
		'name',
		'file_path',
	];

	public function album() {
		return $this->belongsTo('App\Model\Album');
	}
}
