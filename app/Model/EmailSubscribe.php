<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/12/18
 * Time: 12:26 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class EmailSubscribe extends Model
{
    protected $table = 'email_subscribe';
    protected $fillable = ['email'];
    public $timestamps = false;
}