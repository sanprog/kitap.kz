<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Audiofile extends Model {

	protected $fillable = [
		'id',
		'audiobook_id',
		'name',
		'file_path',
	];

	public function audiobook() {
		return $this->hasMany('App\Model\Audiobook');
	}

}
