<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';
    /**
     * Fillable array
     */
    protected $fillable = ['user_id', 'comment', 'parent_id', 'slug'];

    public function model()
    {
        return $this->morphTo();
    }

    public function likes()
    {
        return $this->hasMany('App\Model\CommentUser','comment_id');
    }
}