<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use willvincent\Rateable\Rateable;

class Book extends Model
{
	use Rateable;
    //
    protected $table = 'books';

    protected $fillable = [
        'id',
        'name',
        'type',
        'slug',
        'description',
        'img_path',
        'file_path',
        'offer',
        'old_id',
        'hits',
    ];

	public function categories()
	{
		return $this->morphToMany('App\Model\Category', 'categoryable');
	}

	public function authors()
	{
		return $this->belongsToMany('App\Model\Author')->withTimestamps();
	}

	public function genre()
	{
		return $this->belongsToMany('App\Model\Genre');
	}

	public function partner()
	{
		return $this->belongsToMany('App\Model\Partner');
	}

	public function audiobook()
	{
		return $this->hasOne('App\Model\Audiobook');
	}

	public function fairytale()
    {
        return $this->hasOne('App\Model\Fairytale');
    }

	public function savedprogresses() {
		return $this->hasMany('App\Model\Savedprogress');
	}

    public function comments()
    {
        return $this->morphMany('App\Model\Comment', 'model');
    }

    protected function setImgPathAttribute($file)
    {
        if(!empty($file) && $file instanceof \Illuminate\Http\UploadedFile) {

            $file_name = 'Img_book'. '_' . time() . '.' . $file->guessExtension();
            $path = Storage::disk('uploads')->putFileAs('books', $file, $file_name);
            $old_path = str_replace('/uploads/', '', $this->img_path);
            if ($this->exists && Storage::disk('uploads')->exists($old_path)) {
                Storage::disk('uploads')->delete($old_path);
            }
            \Event::listen('eloquent.saved: ' . static::class, function ($model) use ($file_name) {
                /** @var self $model */
                if ($model->isDirty('img_path')) {
                    $save_path = public_path('uploads/books/' . $model->id . '/');
                    if (!\File::isDirectory($save_path)) {
                        \File::makeDirectory($save_path, 493, true);
                    }
                    $old_path = public_path(ltrim($model->img_path, '/'));
                    if (\File::isFile($old_path)) {
                        \File::move(public_path(ltrim($model->img_path, '/')), $save_path . $file_name);
                    }
                    self::where('id', '=', $model->id)->update(['img_path' => ('/uploads/books/' . $model->id . '/' . $file_name)]);
                }
            });
            $this->attributes['img_path'] = 'uploads/' . $path;
            return $this;
        }
        else {
			$this->attributes['img_path'] = $file;
			return $this;
		}
    }

    protected function setFilePathAttribute($file)
    {
        if (!empty($file) && ($this->type==0 || $this->type==1) && $file instanceof \Illuminate\Http\UploadedFile) {
            $file_name = 'File_book' . '_' . time() . '.' . $file->guessExtension();
            $path = Storage::disk('uploads')->putFileAs('books', $file, $file_name);
            $old_path = str_replace('/uploads/', '', $this->file_path);
            if ($this->exists && Storage::disk('uploads')->exists($old_path)) {
                Storage::disk('uploads')->delete($old_path);
            }
            \Event::listen('eloquent.saved: ' . static::class, function ($model) use ($file_name) {
                /** @var self $model */
                if ($model->isDirty('file_path')) {
                    $save_path = public_path('uploads/books/' . $model->id . '/');
                    if (!\File::isDirectory($save_path)) {
                        \File::makeDirectory($save_path, 493, true);
                    }
                    $old_path = public_path(ltrim($model->file_path, '/'));
                    if (\File::isFile($old_path)) {
                        \File::move(public_path(ltrim($model->file_path, '/')), $save_path . $file_name);
                    }
                    self::where('id', '=', $model->id)->update(['file_path' => ('/uploads/books/' . $model->id . '/' . $file_name)]);
                }
            });
            $this->attributes['file_path'] = '/uploads/' . $path;
            return $this;
        }
		else {
			$this->attributes['file_path'] = $file;
			return $this;
		}
    }

	/**
	 * Scope a query to get popular books.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param integer $limit limit results
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopePopular($query, $limit)
	{
		return $query->orderByDesc('hits')->limit($limit);
	}

	/**
	 * Scope a query to get newest books.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param integer $limit limit results
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeNewest($query, $limit)
	{
		return $query->orderByDesc('created_at')->limit($limit);
	}
}
