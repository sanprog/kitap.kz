<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Image;
class Article extends Model
{
	protected $fillable = [
		'id',
		'name',
		'slug',
		'title',
		'text',
		'img_path',
		'old_id',
		'hits',
	];

	public $thumb_nail;

    /**
     * @return mixed
     */

	public function tags()
    {
		return $this->belongsToMany('App\Model\Tag');
	}

	public static function createNewImage(Article $model){

    }
}
