<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/19/18
 * Time: 4:55 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class SpecauthorAudio extends Model
{
    protected $table = 'specauthor_audio';
    protected $fillable = [
        'specauthor_id',
        'file_path',
        'name'
    ];
    public $timestamps = false;

    public function specauthor() {
        return $this->belongsToMany('App\Model\Specauthor');
    }
}