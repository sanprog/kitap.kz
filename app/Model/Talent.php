<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Talent extends Model
{
	protected $fillable = [
		'id',
		'name',
		'slug',
		'description',
		'img_path',
	];

	public function items() {
		return $this->hasMany('App\Model\Talentitem');
	}
}
