<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	protected $fillable = [
		'id',
		'img_path',
		'title',
	];

	public function books() {
		return $this->belongsToMany('App\Model\Book');
	}
}
