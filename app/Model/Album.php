<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Album
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Author[] $authors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Track[] $track
 * @mixin \Eloquent
 */
class Album extends Model {

	/**
	 * @var array
	 */
	protected $fillable = [
		'id',
		'name',
		'slug',
		'description',
		'img_path',
		'old_id',
		'hits',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function track()
	{
		return $this->hasMany('App\Model\Track');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function categories()
	{
		return $this->morphToMany('App\Model\Category', 'categoryable');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function authors()
	{
		return $this->belongsToMany('App\Model\Author')->withTimestamps();
	}
}
