<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
	protected $fillable = [
		'resource_id', 'resource_type'
	];

	const TYPE_BOOK      = 'book';
	const TYPE_AUDIOBOOK = 'audiobook';
	const TYPE_VIDEO     = 'video';
	const TYPE_MUSIC     = 'audio';

	public function user() {
		return $this->belongsTo('App\User', 'user_id');
	}
}
