<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fragment extends Model {

	protected $fillable = [
		'id',
		'user_id',
		'book_id',
		'fragment',
	];

	public function user() {
		return $this->belongsTo('App\User', 'user_id');
	}

	public function book() {
		return $this->belongsTo('App\Model\Book');
	}
}
