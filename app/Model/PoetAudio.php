<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PoetAudio extends Model
{
    protected $table = 'poet_audios';
    protected $fillable = [
        'poet_id',
        'name',
        'file_path'
    ];
    public $timestamps = false;

    public function poet() {
        return $this->belongsTo('App\Model\Poet');
    }
}
