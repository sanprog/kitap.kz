<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Author extends Model {
    protected $table = 'authors';
	protected $fillable = [
		'id',
		'name',
		'img_path',
		'description',
		'old_id',
		'hits',
	];

	public function specauthor() {
	    return $this->hasOne('App\Model\Specauthor');
    }

	public function books() {
		return $this->belongsToMany('App\Model\Book');
	}
	public function music() {
		return $this->belongsToMany('App\Model\Album');
	}

	protected function setImgPathAttribute($file)
	{
		if(!empty($file) && $file instanceof \Illuminate\Http\UploadedFile) {
			$file_name = str_slug($this->name) . '.' . $file->guessExtension();
			$path      = Storage::disk('uploads')->putFileAs('authors', $file, $file_name);
			$old_path  = str_replace('/uploads', '', $this->img_path);
			if ($this->exists && Storage::disk('uploads')
					->exists($old_path) && $this->img_path != '/uploads/authors/default/avatar.png'
			) {
				Storage::disk('uploads')->delete($old_path);
			}
			\Event::listen('eloquent.saved: ' . static::class, function ($model) use ($file_name) {
				/** @var self $model */
				if ($model->isDirty('img_path')) {
					$save_path = public_path('uploads/authors/' . $model->id . '/');
					if (! \File::isDirectory($save_path)) {
						\File::makeDirectory($save_path, 493, true);
					}
					$old_path = public_path(ltrim($model->img_path, '/'));
					if (\File::isFile($old_path)) {
						\File::move(public_path(ltrim($model->img_path, '/')), $save_path . $file_name);
					}
					self::where('id', '=', $model->id)
						->update(['img_path' => ('/uploads/authors/' . $model->id . '/' . $file_name)]);
				}
			});
			$this->attributes['img_path'] = 'uploads/' . $path;

			return $this;
		}
		else {
			$this->attributes['img_path'] = $file;
			return $this;
		}
	}
}
