<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 3/12/18
 * Time: 11:37 AM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class CommentUser extends Model
{
    protected $table = 'comment_user';
    protected $fillable = [
        'user_id',
        'comment_id'
    ];

    public function comment() {
        return $this->belongsTo('App\Model\Comment');
    }
}