<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

	protected $fillable = [
		'id',
		'name',
		'slug',
		'file_path',
		'img_path',
		'description',
		'count_views',
		'old_id',
		'hits'
	];

	public function categories()
	{
		return $this->morphToMany('App\Model\Category', 'categoryable');
	}
}
