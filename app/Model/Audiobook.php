<?php

namespace App\Model;

use Event;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use willvincent\Rateable\Rateable;

class Audiobook extends Model {

	use Rateable;

	protected $fillable = [
		'id',
		'book_id',
		'dictor_id',
		'timeline',
		'old_id',
		'hits',
	];

	public function book() {
		return $this->belongsTo('App\Model\Book');
	}

	public function dictor() {
		return $this->belongsTo('App\Model\Dictor');
	}

	public function audiofile() {
		return $this->hasMany('App\Model\Audiofile');
	}


	/**
	 * Scope a query to get popular books.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param integer $limit limit results
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopePopular($query, $limit)
	{
		return $query->orderByDesc('hits')->limit($limit);
	}

}
