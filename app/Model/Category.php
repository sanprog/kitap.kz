<?php

namespace App\Model;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	use NodeTrait;

	protected $fillable = [
		'id',
		'name',
		'slug',
		'parent_id',
		'old_id',
		'old_parent_id'
	];

	const CATEGORY_ROOT_SINCE  = 1;
	const CATEGORY_ROOT_PUBLIC = 2;
	const CATEGORY_ROOT_MUSIC  = 3;
	const CATEGORY_ROOT_VIDEO  = 4;
	const CATEGORY_ROOT_PROFI  = 5;
	const CATEGORY_ROOT_TED    = 6;

	public function books()
	{
		return $this->morphedByMany('App\Model\Book', 'categoryable');
	}

	public function album()
	{
		return $this->morphedByMany('App\Model\Album', 'categoryable');
	}

	public function videos()
	{
		return $this->morphedByMany('App\Model\Video', 'categoryable');
	}

}
