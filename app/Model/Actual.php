<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Actual extends Model
{
	protected $fillable = [
		'title',
		'img_path',
		'uri',
	];
}
