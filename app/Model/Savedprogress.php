<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Savedprogress extends Model
{
	protected $table = 'savedprogress';

	protected $fillable = [
		'user_id',
		'book_id',
		'progress'
	];

	public $timestamps = false;

	public function book() {
		return $this->belongsTo('App\Model\Book');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}
}
