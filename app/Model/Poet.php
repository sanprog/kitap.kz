<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Poet extends Model
{
    protected $table = 'poets';
    protected $fillable = [
        'name',
        'img_path',
        'description'
    ];
    public $timestamps = false;

    public function poetAudios() {
        return $this->hasMany('App\Model\PoetAudio');
    }
}
