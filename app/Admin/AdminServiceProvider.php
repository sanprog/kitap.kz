<?php

namespace App\Admin;

use App\Admin\Facades\AdminMenu;
use App\Admin\Http\Middleware\AdminMiddleware;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use App\Admin\Libraries\Menu;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $defer = false;


    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('admin_menu', function ($app) {
            return new Menu();
        });
        AliasLoader::getInstance()->alias('AdminMenu', AdminMenu::class);
        $this->app[\Illuminate\Routing\Router::class]->aliasMiddleware('admin', AdminMiddleware::class);
    }
    public function provides()
    {
        return ['admin_menu'];
    }
}
