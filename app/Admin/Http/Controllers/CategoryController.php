<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category as Model;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{

	public function index($id)
	{
		//needed in first start after seeding DB
		//Model::fixTree();

		$rootCategories = Model::whereIsRoot()->get();
		$categories     = Model::descendantsOf($id)->toTree();

		return view('admin.category.index')->with([
			'rootCategories' => $rootCategories,
			'categories'     => $categories,
		]);
	}

	public function getContent($id)
	{
		// ветка предков до корневой
		$ancestors = Model::ancestorsAndSelf($id);

		//корневая категория
		$rootCategory = $ancestors->first();

		//запрашиваемая категория
		$category = $ancestors->last();

		if ($rootCategory->id == Model::CATEGORY_ROOT_SINCE
			|| $rootCategory->id == Model::CATEGORY_ROOT_PUBLIC
			|| $rootCategory->id == Model::CATEGORY_ROOT_PROFI) {
			return view('admin.category.content_book_table', [
				'tableData'  => $category->books,
				'categoryId' => $id
			]);
		}

		if ($rootCategory->id == Model::CATEGORY_ROOT_MUSIC) {
			return view('admin.category.content_album_table', [
				'tableData'  => $category->album,
				'categoryId' => $id
			]);
		}
		if ($rootCategory->id == Model::CATEGORY_ROOT_VIDEO) {
			return view('admin.category.content_video_table', [
				'tableData'  => $category->videos,
				'categoryId' => $id
			]);
		}
	}

	public function postIndex(Request $request, $razdel = 1)
	{
		DB::beginTransaction();

		switch ($request->action) {
			case "renameCategory":
				$rename       = Model::find($request->id);
				$rename->name = $request->name;
				$status       = $rename->update();
				break;

			case "addCategory":
				$sourceCategory       = new Model();
				$sourceCategory->name = $request->name;

				$sourceCategory->save();

				$targetCategory = Model::root()->where('id', $razdel)->first();

				// append category to root
				if ($status = $sourceCategory->appendToNode($targetCategory)->save()) {
					DB::commit();

					return ["id" => $sourceCategory->id, "parent_id" => $sourceCategory->parent_id];
				}
				break;
			case "deleteCategory":
				try {
					$category = Model::find($request->id);
					$status   = $category->delete();
				} catch (\Exception $e) {
					$status = false;
				}
				break;
			case "moveCategory":
				// get source/target categories from DB
				$sourceCategory = Model::find($request->id);
				$targetCategory = Model::find($request->to);

				// check for data consistency (can also do a try&catch instead)
				if ($sourceCategory && $targetCategory && ($sourceCategory->parent_id == $request->parent_id)) {
					switch ($request->direction) {
						case "inside" :
							$status = $sourceCategory->prependToNode($targetCategory)->save();
							break;
						case "before" :
							$status = $sourceCategory->beforeNode($targetCategory)->save();
							break;
						case "after" :
							$status = $sourceCategory->afterNode($targetCategory)->save();
							break;
					}
				}
				break;
		}
		if (! isset($status) || $status == null) {
			DB::rollback();
			abort(400);
		}
		DB::commit();
	}
}
