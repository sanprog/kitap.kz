<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 3/7/18
 * Time: 1:46 PM
 */

namespace App\Admin\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Model\Comment as Model;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function index()
    {


        if ($q = \request('q',false)){
            $model = Model::where('comment','like','%'.$q.'%')->with('model')->paginate(15);
        } else {
            $model = Model::with('model')->paginate(15);
        }
        return view('admin.'.$this->controllerName().'.index',[
            'comments'      =>  $model
        ]);
    }

    public function edit($id)
    {
        $comment = Model::findOrFail($id);

        return view('admin.' . $this->controllerName() . '.edit', ['model' => $comment]);

    }

    public function update(Request $request,$id)
    {
        $model = Model::find($id);
        $model->comment = $request['comment'];
        $model->update();
        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    public function destroy($id)
    {
        $model     = Model::find($id);

        if ($model->delete()){
            return response()->json(['success' => 'OK']);
        }
    }

}