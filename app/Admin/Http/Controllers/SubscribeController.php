<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/12/18
 * Time: 1:02 PM
 */

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\EmailSubscribe as Model;
use Illuminate\Http\Request;
use Validator;

class SubscribeController extends Controller
{
    public function index()
    {
        if ($q = \request('q',false)){
            $items = Model::where('email','like','%'.$q.'%')->get();
        } else {
            $items = Model::get();
        }
        return view('admin.' . $this->controllerName() . '.index', [
            'items' => $items
        ]);
    }

    public function edit($id)
    {

        $model = Model::where('id',$id)->first();

        return view('admin.' . $this->controllerName() . '.edit', ['item' => $model]);
    }

    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:email_subscribe',
        ])->validate();

        $model = Model::where('id',$id)->first();

        $model->email = $request['email'];

        $model->update();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    public function destroy(Model $subscribe)
    {
        $subscribe->delete();
        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(lang_route('admin.' . $this->controllerName() . '.index'));
        }
    }
}