<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Talent as Model;
use App\Model\Talentitem;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class TalentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.'. $this->controllerName() .'.index',[
            'dates' => Model::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Model();

        return view('admin.'. $this->controllerName() .'.create',['model'=>$model]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {

		$data = $request->toArray();


        $model = new Model();
        $model->fill($data);
		if ($request->file('img_path')) {
			$model->img_path = $request->file('img_path')->store('talent', 'public');
		}
        $model->save();

		if(isset($data['items'])){
			$this->addTalentItems($model, $data['items']);
		}

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    protected function addTalentItems(Model $model, $data){

		foreach ($data as $key => $item) {
			$itemImg   = isset($item['img_path'])
				? $item['img_path']->storePubliclyAs('talent/' . $model->id, $key . 'img_file.' . $item['img_path']->extension(), 'public')
				: null;
			$itemBook  = isset($item['book_path'])
				? $item['book_path']->storePubliclyAs('talent/' . $model->id, $key . 'book_file.' . $item['book_path']->extension(), 'public')
				: null;
			$itemAudio = isset($item['audio_path'])
				? $item['audio_path']->storePubliclyAs('talent/' . $model->id, $key . 'audio_file.' . $item['audio_path']->extension(), 'public')
				: null;

			$model->items()->save(new Talentitem([
				'img_path'   => $itemImg,
				'book_path'  => $itemBook,
				'audio_path' => $itemAudio,
			]));
		}
	}

	protected function dropTalentItem(Talentitem $talentitem){
		Storage::disk('public')->delete($talentitem->img_path);
		Storage::disk('public')->delete($talentitem->book_path);
		Storage::disk('public')->delete($talentitem->audio_path);

		if ($talentitem->delete()){
			return response()->json(['success' => 'OK']);
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Model::with('items')->findOrFail($id);

        return view('admin.'. $this->controllerName() .'.edit',[
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model=Model::find($id);
		$data = $request->toArray();
        $model->fill($data);
		if($request->file('img_path')){
			Storage::disk('public')->delete($model->img_path);
			$model->img_path = $request->file('img_path')->store('talent', 'public');
		}
        $model->update();

		if(isset($data['items'])){
			$this->addTalentItems($model, $data['items']);
		}

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model=Model::find($id);
		Storage::disk('public')->delete($model->img_path);
		$model->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        }
    }
}
