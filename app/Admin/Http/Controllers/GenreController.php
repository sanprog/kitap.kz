<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Admin\Http\Requests\GenreRequest;
use App\Model\Genre as Model;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.'. $this->controllerName() .'.index',[
            'dates' => Model::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $model = new Model();

        return view('admin.'. $this->controllerName() .'.create',['model'=>$model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenreRequest $request)
    {
        //
        $model = new Model();
        $model->fill($request->toArray());
        $model->save();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Model::findOrFail($id);

        return view('admin.'. $this->controllerName() .'.edit',[
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GenreRequest $request, $id)
    {
        //
        $model=Model::find($id);
        $model->fill($request->toArray());
        $model->update();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Model::destroy($id);

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        }
    }
}
