<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/19/18
 * Time: 2:35 PM
 */

namespace App\Admin\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Model\Author;
use App\Model\Specauthor;
use App\Model\SpecauthorAudio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class SpecauthorController extends Controller
{
    public function index()
    {
        return view('admin.' . $this->controllerName() . '.index', [
            'dates' => Specauthor::with('author')->get()
        ]);
    }

    public function create()
    {
        $model = new Author();
        $items = [];
        if (($owner = \request('owner')) && is_numeric($owner)) {
            /** @var Model[] $relations */
            $relations = Model::query()->orderBy('created_at', 'desc')->where('mass_media_id', $owner)->get();
            foreach ($relations as $relation) {
                $items[$relation->id] = [
                    'id'    => $relation->id,
                    'url'   => $relation->url,
                ];
            }
        }
        return view('admin.'. $this->controllerName() .'.create',[
            'model' => $model,
            'items' => $items
        ]);
    }

    public function store(Request $request)
    {
        $specauthor = new Specauthor();
        $specauthor->author_id = $request['author_id'];
        $specauthor->desc = $request['desc'];
        $specauthor->save();

        if ($request['tracks']) {
            foreach ($request['tracks'] as $track) {
                $filename = $track->store('specauthor/' . $specauthor->id, 'public');
                SpecauthorAudio::create([
                    'specauthor_id'  => $specauthor->id,
                    'name'      => $track->getClientOriginalName(),
                    'file_path' => $filename
                ]);
            }
        }
        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $specauthor));
    }

    public function edit($id)
    {

        $specauthor = Specauthor::with('specauthorAudio')->with('author')->where('id',$id)->first();

        return view('admin.' . $this->controllerName() . '.edit', ['model' => $specauthor]);
    }

    public function update(Request $request, $id)
    {

        $model = Specauthor::findOrFail($id);
        $model->author_id = $request['author_id'];
        $model->desc = $request['desc'];

        $model->update();


        if($request['tracks']) {
            foreach ($request['tracks'] as $track) {
                $filename = $track->store('specauthor/' . $model->id, 'public');
                Track::create([
                    'album_id'  => $model->id,
                    'name'      => $track->getClientOriginalName(),
                    'file_path' => $filename
                ]);
            }
        }

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    public function destroy($id)
    {
        $model = Specauthor::find($id);

        $save_path = 'specauthor/' . $model->id;
        Storage::disk('public')->deleteDirectory($save_path);

        if ($model->delete()){

            return response()->json(['success' => 'OK']);
        }
    }

    public function destroyOneTrack($id)
    {
        $model = SpecauthorAudio::find($id);

        Storage::disk('public')->delete($model->file_path);

        if ($model->delete()){
            return response()->json(['success' => 'OK']);
        }
    }
}