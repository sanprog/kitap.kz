<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Book as Model;
use App\Model\Category;
use Illuminate\Support\Facades\Storage;
use App\Admin\Http\Requests\BookRequest;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    if ($q = \request('q',false)){
	        $model = Model::where('name','like','%'.$q.'%')->paginate(15);
        } else {
            $model = Model::paginate(15);
        }
		return view('admin.' . $this->controllerName() . '.index', [
			'dates' => $model,
            'q' => $q
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$model      = new Model();
		if($ci = Category::find(request('ci'))) {
			$model->categories[0] = $ci;
		}

		return view('admin.' . $this->controllerName() . '.create', ['model' => $model]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(BookRequest $request)
	{
		$model = new Model();
		$model->fill($request->toArray());
		$model->slug = str_slug($model->name, '-');
		$model->save();


		if ($cats = $request->input('category_id')) {
			$model->categories()->attach($cats);
		}

		if ($author = $request->input('author_id')) {
			$model->authors()->attach($author);
		}
		if ($genre = $request->input('genre_id')) {
			$model->genre()->attach($genre);
		}

		if ($dictor = $request->input('dictor_id')) {
			$model->dictors()->attach($dictor);
		}



		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{

		$album = Model::findOrFail($id);

		return view('admin.' . $this->controllerName() . '.edit', ['model' => $album]);
	}

	public function update(BookRequest $request, $id)
	{
		$model = Model::find($id);

		$model->name        = $request->name;
		$model->offer        = $request->has('offer');
		$model->description = $request->description;


		if ($request->file_path !== null) {
			$model->file_path = $request->file_path;
		}
		if ($request->img_path !== null) {
			$model->img_path = $request->img_path;
		}

		$model->update();

		$model->categories()->sync($request->input('category_id', []));
		$model->authors()->sync($request->input('author_id', []));
		$model->genre()->sync($request->input('genre_id', []));

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$model     = Model::find($id);
		$save_path = 'books/' . $model->id;
		Storage::disk('uploads')->deleteDirectory($save_path);

		if ($model->delete()) {
			DB::table('categoryables')
				->where('categoryable_id', $id)
				->where('categoryable_type', Model::class)
				->delete();
		}

		if (\request()->ajax()) {
			return response()->json(['success' => 'OK']);
		}
	}
}
