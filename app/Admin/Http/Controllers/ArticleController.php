<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Model\Article;
use App\Model\Tag;
use App\Admin\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\DB;
use Image;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
        if ($q = \request('q',false)){
            $model = Article::where('name','like','%'.$q.'%')->paginate(15);
        } else {
            $model = Article::paginate(15);
        }
		return view('admin.' . $this->controllerName() . '.index', [
			'dates' => $model,
            'q' => $q
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$model = new Article();

		return view('admin.' . $this->controllerName() . '.create', ['model' => $model]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Admin\Http\Requests\ArticleRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(ArticleRequest $request)
	{

		$article = new Article();
		$article->fill($request->toArray());
		$article->slug = str_slug($article->name, '-');

		$article->save();
		if ($request->file('img_path')) {
            $article->img_path = $request->file('img_path')->store('article/'.$article->id, 'public');

            $name = explode("/", $article->img_path);
            $name = 'thumb_'.$name[sizeof($name) - 1];

            $path = 'article/'.$article->id.'/'.$name;

            Image::make($request->file('img_path')->getRealPath())->resize(314,232,function ($constraint){
                $constraint->aspectRatio();
            })->save('storage/'.$path);

			$article->thumb_nail = $path;
			$article->save();
		}


		if($request->tags){
			$tags = array_map(function($tag) {
				$tag = Tag::firstOrCreate(['name' => $tag]);
				return $tag->id;
			}, $request->tags);
			$article->tags()->sync($tags);
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $article));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{

		$article = Article::findOrFail($id);

		return view('admin.' . $this->controllerName() . '.edit', ['model' => $article]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Admin\Http\Requests\ArticleRequest $request
	 * @param  int                                     $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(ArticleRequest $request, $id)
	{
		$model = Article::findOrFail($id);
		$model->fill($request->toArray());
		if($request->file('img_path')){
            Storage::disk('public')->delete($model->img_path);
            Storage::disk('public')->delete($model->thumb_nail);

            $model->img_path = $request->file('img_path')->store('article/'.$model->id, 'public');

            $name = explode("/", $model->img_path);
            $name = 'thumb_'.$name[sizeof($name) - 1];

            $path = 'article/'.$model->id.'/'.$name;

            Image::make($request->file('img_path')->getRealPath())->resize(314,232,function ($constraint){
                $constraint->aspectRatio();
            })->save('storage/'.$path);

            $model->thumb_nail = $path;
		}
		$model->update();

		if($request->tags){
			$tags = array_map(function($tag) {
				$tag = Tag::firstOrCreate(['name' => $tag]);
				return $tag->id;
			}, $request->tags);
			$model->tags()->sync($tags);
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$model = Article::find($id);

		$save_path = 'article/' . $model->id;
		Storage::disk('public')->deleteDirectory($save_path);

		if ($model->delete()) {
			DB::table('categoryables')
				->where('categoryable_id', $id)
				->where('categoryable_type', Article::class)
				->delete();

			return response()->json(['success' => 'OK']);
		}
	}
}
