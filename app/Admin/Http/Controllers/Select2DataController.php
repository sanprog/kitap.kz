<?php

namespace App\Admin\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Select2DataController extends Controller
{

    public function dataCategory(Request $request,$id)
    {

        $targetCategory=Category::find($id);
        $rgt=$targetCategory->_rgt;
        $lft=$targetCategory->_lft;

        $q = DB::table("categories")
                ->select("id","name")->where('_rgt','<',$rgt)->where('_lft','>',$lft);

        if($search = rawurldecode($request->q)){
            $q->where('name','LIKE',"%$search%");
        }
        $data=$q->get();
        return response()->json($data);
    }

	public function dataCategoryBook(Request $request)
	{

		$targetCategory=DB::table("categories")->whereIn('id', [1, 2])->get();



		$rgt=$targetCategory->last()->_rgt;
		$lft=$targetCategory->first()->_lft;

		//dd($lft);

		$q = DB::table("categories")
			->select("id","name")->where('_rgt','<',$rgt)->where('_lft','>',$lft);

		if($search = rawurldecode($request->q)){
			$q->where('name','LIKE',"%$search%");
		}
		$data=$q->get();
		return response()->json($data);
	}

	//not use
	public function dataCategoryBookToTree(Request $request)
	{
		$categorySince      = Category::descendantsOf(Category::CATEGORY_ROOT_SINCE)->toTree();
		$categoryPublic     = Category::descendantsOf(Category::CATEGORY_ROOT_PUBLIC)->toTree();
		$data = [];

		$traverse = function ($categories, $prefix = '') use (&$traverse, &$data) {
			foreach ($categories as $category) {
				$data[] = ['name' => $prefix.$category->name, 'id' => $category->id];
				$traverse($category->children, $prefix.'-- ');
			}
		};
		$traverse($categorySince);
		$traverse($categoryPublic);

		return response()->json($data);
	}

    public function dataAuthors(Request $request)
    {
        $q=DB::table("authors")
            ->select("id","name");
        if($search = rawurldecode($request->q)){
            $q->where('name','LIKE',"%$search%");
        }
        $data=$q->get();

        return response()->json($data);
    }

	public function dataBooks(Request $request)
	{
		$q=DB::table("books")
			->select("id","name");
		if($search = rawurldecode($request->q)){
			$q->where('name','LIKE',"%$search%");
		}
		$data=$q->get();

		return response()->json($data);
	}

    public function dataGenres(Request $request)
    {
        $q = DB::table("genres")
            ->select("id","name");

        if($search = rawurldecode($request->q)){
            $q->where('name','LIKE',"%$search%");
        }
        $data=$q->get();

        return response()->json($data);
    }

    public function dataDictors(Request $request)
    {
        $q=DB::table("dictors")
            ->select("id","dictor_name");
        if($search = rawurldecode($request->q)){
            $q->where('dictor_name','LIKE',"%$search%");
        }
        $data=$q->get();

        return response()->json($data);
    }

	public function dataTags(Request $request)
	{
		$q=DB::table("tags")
			->select("id","name");
		if($search = rawurldecode($request->q)){
			$q->where('name','LIKE',"%$search%");
		}
		$data=$q->get();

		return response()->json($data);
	}

}
