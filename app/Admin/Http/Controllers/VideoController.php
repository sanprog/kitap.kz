<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Model\Video;
use App\Model\Category;
use Illuminate\Support\Facades\DB;
use App\Admin\Http\Requests\VideoRequest;

class VideoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
        if ($q = \request('q',false)){
            $model = Video::where('name','like','%'.$q.'%')->get();
        } else {
            $model = Video::get();
        }
		return view('admin.' . $this->controllerName() . '.index', [
			'dates' => $model,
            'q' => $q
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$model = new Video();

		if($ci = Category::find(request('ci'))) {
			$model->categories[0] = $ci;
		}

		return view('admin.'. $this->controllerName() .'.create',['model'=>$model]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(VideoRequest $request)
	{
		$model = new Video();

		$model->fill($request->toArray());
		$model->slug        = str_slug($model->name, '-');

		$model->save();

		if ($request->file('file_path')) {
			$model->file_path = $request->file('file_path')->store('video/'.$model->id, 'public');
		}
		if ($request->file('img_path')) {
			$model->img_path = $request->file('img_path')->store('video/'.$model->id, 'public');
		}

		$model->save();


		if ($category = $request->input('category_id')) {
			$model->categories()->attach($category);
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$video = Video::findOrFail($id);

		return view('admin.' . $this->controllerName() . '.edit', ['model' => $video]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(VideoRequest $request, $id) {

		$model=Video::findOrFail($id);
		$model->fill($request->toArray());
		if($request->file('file_path')){
			Storage::disk('public')->delete($model->file_path);
			$model->file_path = $request->file('file_path')->store('video/'.$model->id, 'public');
		}
		if($request->file('img_path')){
			Storage::disk('public')->delete($model->img_path);
			$model->img_path = $request->file('img_path')->store('video/'.$model->id, 'public');
		}
		$model->update();

		$model->categories()->sync($request->input('category_id', []));

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$model = Video::find($id);

		Storage::disk('public')->delete($model->file_path);
		Storage::disk('public')->delete($model->img_path);

		if ($model->delete()){
			DB::table('categoryables')
				->where('categoryable_id', $id)
				->where('categoryable_type', Video::class)
				->delete();

			return response()->json(['success' => 'OK']);
		}
	}
}
