<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Admin\Http\Requests\Translation as Request;
use Waavi\Translation\Models\Translation as Model;

class TranslateController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        \Route::bind('translate', function ($value) {
            if (is_numeric($value)) {
                return Model::query()->where('id', $value)->firstOrFail();
            } else {
                return Model::searchToCode($value);
            }
        });
    }
    /**
     * @return View
     */
    public function index(): View
    {
        $q = Model::orderBy('namespace', 'asc')
            ->orderBy('group', 'asc');
        $appends = [];
        if ($search = \request('search', false)) {
            //TODO для данного функционала должно быть выключено
            $search = e($search);
            $q->distinct();
            $q->select([
                'namespace',
                'group',
                'item',
                'id'
            ]);
            $q->where('text', 'LIKE', '%' . $search . '%')
                ->where('item', 'LIKE', '%' . $search . '%','or');
            $appends['search'] = $search;
        } else {
            $q->where('locale', 'ru');
        }
        return view('admin.' . $this->controllerName() . '.index', [
            'locales' => \Waavi\Translation\Models\Language::get(),
            'items'   => $q->paginate(20)->appends($appends),
            'search'  => $search
        ]);
    }
    /**
     * @return View
     */
    public function create(): View
    {
        \Assets::addJs('admin/plugins/ckeditor/ckeditor.js');
        $all   = old($this->controllerName());
        $model = new Model();
        return view('admin.' . $this->controllerName() . '.create', [
            'locales' => \Waavi\Translation\Models\Language::get(),
            'model'   => $model
        ]);
    }
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $course = Model::create($request->mutatorData());
        if ($request->input('commit') == 1) {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $course));
        }
    }

    /**
     * @param Model $translate
     * @return View
     */
    public function edit(Model $translate): View
    {
        \Assets::addJs('admin/plugins/ckeditor/ckeditor.js');
        $translates = Model::where('locale', '<>', $translate->locale)
            ->where([
                'namespace' => $translate->namespace,
                'group'     => $translate->group,
                'item'      => $translate->item,
            ])->get();
        $translates = $translates->add($translate)->keyBy('locale');
        return view('admin.' . $this->controllerName() . '.edit', [
            'locales'    => \Waavi\Translation\Models\Language::get(),
            'model'      => $translate,
            'translates' => $translates
        ]);
    }

    /**
     * @param Request $request
     * @param Model $translate
     * @return RedirectResponse
     */
    public function update(Request $request, Model $translate): RedirectResponse
    {
        $all = $request->mutatorData();
        $translates = Model::where('locale', '<>', $translate->locale)
            ->where([
                'namespace' => $translate->namespace,
                'group' => $translate->group,
                'item' => $translate->item,
            ])->get();
        $translates->add($translate);
        $translates = $translates->keyBy('locale');
        $locales = \Waavi\Translation\Models\Language::get();
        foreach ($locales as $locale) {
            $translate_update = array_get($translates, $locale->locale);
            if (!$translate_update) {
                $translate_update = new Model();
                $translate_update->namespace = $translate->namespace;
                $translate_update->group = $translate->group;
                $translate_update->item = $translate->item;
                $translate_update->locale = $locale->locale;
            }
            if ($text = array_get($all, $translate_update->locale . '_text')) {
                $translate_update->text = $text;
            } else {
                $translate_update->text = '';
            }
            $translate_update->save();
            \TranslationCache::flush($translate_update->locale, $translate_update->group, $translate_update->namespace);
        }
        if ($request->input('commit') == 1) {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $translate));
        }
    }

    /**
     * @param Model $translate
     * @return RedirectResponse | \Response
     * @throws \Exception
     */
    public function destroy(Model $translate)
    {
        $translate->delete();
        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        }
    }
}
