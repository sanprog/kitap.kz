<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Model\Album;
use App\Model\Track;
use App\Model\Category;
use App\Admin\Http\Requests\MusicRequest;
use Illuminate\Support\Facades\DB;

class MusicController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

        if ($q = \request('q',false)){
            $model = Album::where('name','like','%'.$q.'%')->get();
        } else {
            $model = Album::get();
        }
        return view('admin.' . $this->controllerName() . '.index', [
            'dates' => $model,
            'q' => $q
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$model = new Album();

		if($ci = Category::find(request('ci'))) {
			$model->categories[0] = $ci;
		}

		return view('admin.'. $this->controllerName() .'.create',['model'=>$model]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(MusicRequest $request) {

		$newAlbum         = $request->all();
		$newAlbum['slug'] = str_slug($newAlbum['name'], '-');

		$album = Album::create($newAlbum);

		if ($author = $request->input('author_id')) {
			$album->authors()->attach($author);
		}
		if ($category = $request->input('category_id')) {
			$album->categories()->attach($category);
		}

		if ($request->file('img_path')) {
			$album->img_path = $request->file('img_path')->store('album/' . $album->id, 'public');
			$album->save();
		}

		if($request->tracks){
			foreach ($request->tracks as $track) {
				$filename = $track->store('album/' . $album->id, 'public');
				Track::create([
					'album_id'  => $album->id,
					'name'      => $track->getClientOriginalName(),
					'file_path' => $filename
				]);
			}
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $album));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$album = Album::with('track')->findOrFail($id);

		return view('admin.' . $this->controllerName() . '.edit', ['model' => $album]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(MusicRequest $request, $id) {

		$model = Album::findOrFail($id);
		$model->fill($request->toArray());

		if($request->file('img_path')){
			Storage::disk('public')->delete($model->img_path);
			$model->img_path = $request->file('img_path')->store('album/' . $model->id, 'public');
		}

		$model->update();

		$model->authors()->sync($request->input('author_id', []));
		$model->categories()->sync($request->input('category_id', []));

		if($request->tracks) {
			foreach ($request->tracks as $track) {
				$filename = $track->store('album/' . $model->id, 'public');
				Track::create([
					'album_id'  => $model->id,
					'name'      => $track->getClientOriginalName(),
					'file_path' => $filename
				]);
			}
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$model = Album::find($id);

		$save_path = 'album/' . $model->id;
		Storage::disk('public')->deleteDirectory($save_path);

		if ($model->delete()){
			DB::table('categoryables')
				->where('categoryable_id', $id)
				->where('categoryable_type', Album::class)
				->delete();

			return response()->json(['success' => 'OK']);
		}
	}

	/**
	 * Remove one file and record in track model
	 *
	 * @param  int $id track id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroyOneTrack($id) {
		$model = Track::find($id);

		Storage::disk('public')->delete($model->file_path);

		if ($model->delete()){
			return response()->json(['success' => 'OK']);
		}
	}
}
