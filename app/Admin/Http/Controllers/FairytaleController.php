<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/15/18
 * Time: 12:18 PM
 */

namespace App\Admin\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Model\Book;
use App\Model\Fairytale as Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FairytaleController extends Controller
{
    public function index()
    {
        return view('admin.'. $this->controllerName() .'.index',[
            'dates' => Model::with('book')->get()
        ]);
    }
    public function create()
    {
        $model = new Model();
        return view('admin.'. $this->controllerName() .'.create',['model' => $model]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $model = new Model();
        $model->book_id = $request['book_id'];

        $model->save();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Model::with('book')->where('id',$id)->first();

        return view('admin.'. $this->controllerName() .'.edit',[
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Model::find($id);

        $model->book_id = $request['book_id'];
        $model->update();


        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model=Model::find($id);
        $model->delete();

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        }
    }
}