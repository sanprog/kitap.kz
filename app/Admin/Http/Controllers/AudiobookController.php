<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Model\Audiobook;
use App\Model\Book;
use App\Model\Audiofile;
use App\Admin\Http\Requests\AudiobookRequest;

class AudiobookController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
        if ($q = \request('q',false)){

			$model = Audiobook::whereHas('book', function ($query) use ($q) {
				$query->where('name','like','%'.$q.'%');
			})->paginate(15);
        } else {
            $model = Audiobook::paginate(15);
        }
		return view('admin.' . $this->controllerName() . '.index', [
			'dates' => $model,
            'q' => $q
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$model = new Audiobook();

		if(request('b')){
			$model->book = Book::find(request('b'));
		}

		return view('admin.'. $this->controllerName() .'.create',['model'=>$model]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(AudiobookRequest $request) {

		$newAudiobook         = $request->all();
		$newAudiobook['timeline'] = 1;

		$model = Audiobook::create($newAudiobook);
		if($request->audiofiles){
			foreach ($request->audiofiles as $audiofile) {
				$filename = $audiofile->store('audiobook/' . $model->id, 'public');
				Audiofile::create([
					'audiobook_id'  => $model->id,
					'name'      => $audiofile->getClientOriginalName(),
					'file_path' => $filename
				]);
			}
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', ['id' => $id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$album = Audiobook::with('audiofile')->findOrFail($id);

		return view('admin.' . $this->controllerName() . '.edit', ['model' => $album]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(AudiobookRequest $request, $id) {

		$model=Audiobook::findOrFail($id);
		$model->fill($request->toArray());
		$model->update();

		if($request->audiofiles) {
			foreach ($request->audiofiles as $audiofile) {
				$filename = $audiofile->store('audiobook/' . $model->id, 'public');
				Audiofile::create([
					'audiobook_id' => $model->id,
					'name'         => $audiofile->getClientOriginalName(),
					'file_path'    => $filename
				]);
			}
		}

		return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$model = Audiobook::find($id);

		$save_path = 'audiobook/' . $model->id;
		Storage::disk('public')->deleteDirectory($save_path);

		if ($model->delete()){
			return response()->json(['success' => 'OK']);
		}
	}

	/**
	 * Remove one file and record in audiofile model
	 *
	 * @param  int $id audiofile id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroyOneAudiofile($id) {
		$model = Audiofile::find($id);
		Storage::disk('public')->delete($model->file_path);
		if ($model->delete()){
			return response()->json(['success' => 'OK']);
		}
	}
}
