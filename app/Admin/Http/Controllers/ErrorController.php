<?php
/**
 * Created by PhpStorm.
 * User: anatoliy
 * Date: 2/1/18
 * Time: 4:50 PM
 */

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Error as Model;

class ErrorController extends Controller
{
    public function index()
    {
        $items = Model::query()->paginate(20);
        return view('admin.' . $this->controllerName() . '.index', [
           'items' => $items
        ]);
    }
    public function destroy(Model $book)
    {
        $book->delete();
        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(lang_route('admin.' . $this->controllerName() . '.index'));
        }
    }
}