<?php

namespace App\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Poet as Model;
use App\Model\PoetAudio;
use Illuminate\Support\Facades\Storage;

class PoetController extends Controller
{
    public function index() {
        if ($q = \request('q',false)){
            $model = Model::where('name','like','%'.$q.'%')->paginate(15);
        } else {
            $model = Model::paginate(15);
        }
        return view('admin.' . $this->controllerName() . '.index', ['dates' => $model, 'q' => $q]);
    }

    public function create() {
        $model = new Model();
        return view('admin.' . $this->controllerName() . '.create', ['model' => $model]);
    }

    public function store(Request $request) {
        //validate data
        $this->validate($request, array(
            'name'        => 'required',
            'description' => 'required',
            'img_path'    => 'image'
        ));

        $model = new Model();
        $model->name = $request->name;
        $model->description = $request->description;

        $model->save();

        if ($request->img_path !== null) {
            $filename = $request->img_path->store('poets/' . $model->id, 'public');
            $model->img_path = $filename;
        }

        if($request->audiofiles){
			foreach ($request->audiofiles as $audiofile) {
				$filename = $audiofile->store('poets/' . $model->id, 'public');
				PoetAudio::create([
					'poet_id'   => $model->id,
					'name'      => $audiofile->getClientOriginalName(),
					'file_path' => $filename
				]);
			}
		}

        $model->save();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    public function edit($id) {
        $model = Model::find($id);
        return view('admin.' . $this->controllerName() . '.edit', ['model' => $model]);
    }

    public function update(Request $request, $id) {
        //validate data
        $this->validate($request, array(
            'name'        => 'required',
            'description' => 'required',
            'img_path'    => 'image'
        ));

        $model = Model::find($id);
		$model->name = $request->name;
        $model->description = $request->description;
        if ($request->img_path !== null) {
            $filename = $request->img_path->store('poets/' . $model->id, 'public');
            $model->img_path = $filename;
        }

        if($request->audiofiles){
			foreach ($request->audiofiles as $audiofile) {
				$filename = $audiofile->store('poets/' . $model->id, 'public');
				PoetAudio::create([
					'poet_id'   => $model->id,
					'name'      => $audiofile->getClientOriginalName(),
					'file_path' => $filename
				]);
			}
		}

		$model->update();

        return redirect()->to(route('admin.' . $this->controllerName() . '.edit', $model));
    }

    public function destroy($id) {
        $model = Model::find($id);

        if($model->img_path !== null) {
            $save_path = 'poets/' . $model->id;
            Storage::disk('public')->deleteDirectory($save_path);
        }

        $poetaudio = PoetAudio::where('poet_id', $model->id)->delete();     //delete from poet_audio table
        Model::destroy($id);

        if (\request()->ajax()) {
            return response()->json(['success' => 'OK']);
        } else {
            return redirect()->to(route('admin.' . $this->controllerName() . '.index'));
        }
    }

    public function destroyOneTrack($id) {
        $model = PoetAudio::find($id);
		Storage::disk('public')->delete($model->file_path);
		if ($model->delete()){
			return response()->json(['success' => 'OK']);
		}
    }
}
