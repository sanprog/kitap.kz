<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'        => 'required',
			'description' => 'required',
			'file_path'   => 'file',
			'img_path'    => 'file'
		];
	}

	/**
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes(): array
	{
		return [
			'name'        => 'Название',
			'slug'        => 'Чпу',
			'description' => 'Описание'
		];
	}

	/**
	 * Get the validation massages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'file_path.required' => 'Необходимо загрузить видеофайл.'
		];
	}
}
