<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AudiobookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'book_id'      => 'sometimes|required|unique:audiobooks,book_id,|exists:books,id',
			'dictor_id'    => 'required|exists:dictors,id',
			'audiofiles'   => 'array',
			'audiofiles.*' => 'mimes:mpga',
		];
	}

	/**
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes(): array
	{
		return [
			'book_id' => 'Книга',
			'dictor_id' => 'Диктор',
			'audiofiles' => 'Аудиофайлы',
		];
	}

	/**
	 * Get the validation massages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'audiofiles.*'        => 'Один из загружаемых объектов не вляется аудиофайлом.',
			'book_id.required'    => 'Необходимо выбрать связаную книгу.',
			'book_id.unique'      => 'Для этой книги уже есть добавленная аудиокнига.'
		];
	}
}
