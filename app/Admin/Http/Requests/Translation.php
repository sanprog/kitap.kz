<?php

namespace App\Admin\Http\Requests;

use Bmg\Core\Models\Pages as Model;

class Translation extends Request
{
    public $name_array = 'translate';
    /**
     * @param Model $model
     * @return array
     */
    public function rules(Model $model): array
    {
        $result = [];
        $locales=\Waavi\Translation\Models\Language::get();
        foreach ($locales as $locale) {
            $result[$locale->locale.'_text']='string|nullable';
        }
        return $model->mutatorRules($result);
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        $result = [];
        $locales=\Waavi\Translation\Models\Language::get();
        foreach ($locales as $locale) {
            $result[$locale->locale.'_text']='Текст';
        }
        return $result;
    }
}
