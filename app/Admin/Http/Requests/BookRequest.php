<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'        => 'required',
			'description' => 'required',
			'file_path'   => 'file',
			'img_path'    => 'image',
			'category_id' => 'exists:categories,id',
			'author_id'   => 'exists:authors,id',
			'genre_id'    => 'exists:genres,id',
		];
	}

	/**
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes(): array
	{
		return [
			'name' => 'Назавание книги',
		];
	}

	/**
	 * Get the validation massages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [];
	}
}
