<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DictorRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'dictor_name' => 'required',
			'img_src'     => 'image',
			'description' => 'required',
		];
	}

	/**
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes(): array
	{
		return [
			'dictor_name' => 'Диктор',
			'img_src'     => 'Изображение',
			'description' => 'Описание',
		];
	}

	/**
	 * Get the validation massages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [];
	}
}
