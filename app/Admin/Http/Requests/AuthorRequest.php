<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'        => 'required',
			'img_path'    => 'image',
			'description' => 'required',
		];
	}

	/**
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes(): array
	{
		return [
			'name'        => 'Автор',
			'description' => 'Описание',
			'img_path'    => 'Изображение',
		];
	}

	/**
	 * Get the validation massages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [];
	}
}
