<?php

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ғылыми әдебиет',
                'slug' => '',
                '_lft' => 1,
                '_rgt' => 28,
                'parent_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Көркем әдебиет',
                'slug' => '',
                '_lft' => 29,
                '_rgt' => 58,
                'parent_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Музыка',
                'slug' => '',
                '_lft' => 59,
                '_rgt' => 60,
                'parent_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Видео',
                'slug' => '',
                '_lft' => 61,
                '_rgt' => 68,
                'parent_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Проф литература',
                'slug' => '',
                '_lft' => 69,
                '_rgt' => 70,
                'parent_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'TED',
                'slug' => '',
                '_lft' => 62,
                '_rgt' => 63,
                'parent_id' => 4,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Театры',
                'slug' => '',
                '_lft' => 64,
                '_rgt' => 65,
                'parent_id' => 4,
            ),
            7 => 
            array (
                'id' => 35,
                'name' => 'Философия',
                'slug' => '',
                '_lft' => 2,
                '_rgt' => 3,
                'parent_id' => 1,
            ),
            8 => 
            array (
                'id' => 36,
                'name' => 'Филология',
                'slug' => '',
                '_lft' => 4,
                '_rgt' => 9,
                'parent_id' => 1,
            ),
            9 => 
            array (
                'id' => 37,
                'name' => 'Әдебиеттану',
                'slug' => '',
                '_lft' => 5,
                '_rgt' => 6,
                'parent_id' => 36,
            ),
            10 => 
            array (
                'id' => 38,
                'name' => 'Лингвистика',
                'slug' => '',
                '_lft' => 7,
                '_rgt' => 8,
                'parent_id' => 36,
            ),
            11 => 
            array (
                'id' => 39,
                'name' => 'Шығыстану',
                'slug' => '',
                '_lft' => 10,
                '_rgt' => 11,
                'parent_id' => 1,
            ),
            12 => 
            array (
                'id' => 40,
                'name' => 'Түркітану',
                'slug' => '',
                '_lft' => 12,
                '_rgt' => 13,
                'parent_id' => 1,
            ),
            13 => 
            array (
                'id' => 41,
                'name' => 'Тарих',
                'slug' => '',
                '_lft' => 14,
                '_rgt' => 15,
                'parent_id' => 1,
            ),
            14 => 
            array (
                'id' => 42,
                'name' => 'Құқықтану',
                'slug' => '',
                '_lft' => 16,
                '_rgt' => 17,
                'parent_id' => 1,
            ),
            15 => 
            array (
                'id' => 43,
                'name' => 'Педагогика',
                'slug' => '',
                '_lft' => 18,
                '_rgt' => 19,
                'parent_id' => 1,
            ),
            16 => 
            array (
                'id' => 44,
                'name' => 'Мәдениеттану',
                'slug' => '',
                '_lft' => 20,
                '_rgt' => 21,
                'parent_id' => 1,
            ),
            17 => 
            array (
                'id' => 45,
                'name' => 'Саясаттану',
                'slug' => '',
                '_lft' => 22,
                '_rgt' => 23,
                'parent_id' => 1,
            ),
            18 => 
            array (
                'id' => 46,
                'name' => 'Жаратылыстану',
                'slug' => '',
                '_lft' => 24,
                '_rgt' => 25,
                'parent_id' => 1,
            ),
            19 => 
            array (
                'id' => 47,
                'name' => 'Бизнес',
                'slug' => '',
                '_lft' => 26,
                '_rgt' => 27,
                'parent_id' => 1,
            ),
            20 => 
            array (
                'id' => 48,
                'name' => 'Халық ауыз әдебиеті',
                'slug' => '',
                '_lft' => 30,
                '_rgt' => 37,
                'parent_id' => 2,
            ),
            21 => 
            array (
                'id' => 49,
                'name' => 'Ертегі',
                'slug' => '',
                '_lft' => 31,
                '_rgt' => 32,
                'parent_id' => 48,
            ),
            22 => 
            array (
                'id' => 50,
                'name' => 'Батырлар жыры',
                'slug' => '',
                '_lft' => 33,
                '_rgt' => 34,
                'parent_id' => 48,
            ),
            23 => 
            array (
                'id' => 51,
                'name' => 'Лиро-эпостық жырлар',
                'slug' => '',
                '_lft' => 35,
                '_rgt' => 36,
                'parent_id' => 48,
            ),
            24 => 
            array (
                'id' => 52,
                'name' => 'Ежелгі дәуір әдебиеті',
                'slug' => '',
                '_lft' => 38,
                '_rgt' => 39,
                'parent_id' => 2,
            ),
            25 => 
            array (
                'id' => 53,
                'name' => 'Жыраулық  поэзия',
                'slug' => '',
                '_lft' => 40,
                '_rgt' => 41,
                'parent_id' => 2,
            ),
            26 => 
            array (
                'id' => 54,
                'name' => 'XIX ғасыр әдебиеті',
                'slug' => '',
                '_lft' => 42,
                '_rgt' => 47,
                'parent_id' => 2,
            ),
            27 => 
            array (
                'id' => 55,
                'name' => 'I жартысындағы әдебиет',
                'slug' => '',
                '_lft' => 43,
                '_rgt' => 44,
                'parent_id' => 54,
            ),
            28 => 
            array (
                'id' => 56,
                'name' => 'II жартысындағы әдебиет',
                'slug' => '',
                '_lft' => 45,
                '_rgt' => 46,
                'parent_id' => 54,
            ),
            29 => 
            array (
                'id' => 57,
                'name' => 'XX ғасыр әдебиеті',
                'slug' => '',
                '_lft' => 48,
                '_rgt' => 55,
                'parent_id' => 2,
            ),
            30 => 
            array (
                'id' => 58,
                'name' => '1940 жылға дейінгі әдебиет',
                'slug' => '',
                '_lft' => 49,
                '_rgt' => 50,
                'parent_id' => 57,
            ),
            31 => 
            array (
                'id' => 59,
                'name' => '1940-1950 жылдардағы әдебиет',
                'slug' => '',
                '_lft' => 51,
                '_rgt' => 52,
                'parent_id' => 57,
            ),
            32 => 
            array (
                'id' => 60,
                'name' => '1960-1990 жылдардағы әдебиет',
                'slug' => '',
                '_lft' => 53,
                '_rgt' => 54,
                'parent_id' => 57,
            ),
            33 => 
            array (
                'id' => 61,
                'name' => 'Жаңа ғасыр әдебиеті',
                'slug' => '',
                '_lft' => 56,
                '_rgt' => 57,
                'parent_id' => 2,
            ),
            34 => 
            array (
                'id' => 62,
                'name' => 'Фильмы',
                'slug' => '',
                '_lft' => 66,
                '_rgt' => 67,
                'parent_id' => 4,
            ),
        ));

		//needed in first start after seeding DB
		Category::fixTree();
        
        
    }
}