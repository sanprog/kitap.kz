<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('genres')->delete();
        
        \DB::table('genres')->insert([
			[ 'name' => 'Проза'],
			[ 'name' => 'әлеуметтік-cаяси'],
			[ 'name' => 'тарихи'],
			[ 'name' => 'мемуарлық'],
			[ 'name' => 'романтикалық'],
			[ 'name' => 'психологиялық'],
			[ 'name' => 'сатира'],
			[ 'name' => 'философиялық'],
			[ 'name' => 'фантастика'],
			[ 'name' => 'публицистикалық'],
			[ 'name' => 'этнографиялық'],
			[ 'name' => 'діни'],
			[ 'name' => 'ғұмырнамалық'],
			[ 'name' => 'Поэзия'],
			[ 'name' => 'өлеңдер'],
			[ 'name' => 'поэмалар'],
			[ 'name' => 'тарихи жырлар'],
			[ 'name' => 'лиро эпостық жырлар'],
			[ 'name' => 'Драма'],
			[ 'name' => 'мелодрама'],
			[ 'name' => 'трагедия'],
			[ 'name' => 'комедия'],
			[ 'name' => 'Балалар әдебиеті'],
			[ 'name' => 'Әлем әдебиеті'],
			[ 'name' => 'Ертегілер'],
		]);
        
        
    }
}