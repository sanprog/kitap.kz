<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('articles')->delete();
        
        \DB::table('articles')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Статья 1',
                'slug' => 'statya-1',
                'title' => NULL,
                'text' => '<p>описание 1</p>',
                'created_at' => '2017-11-15 11:56:14',
                'updated_at' => '2017-11-15 11:56:14',
            ),
        ));
        
        
    }
}