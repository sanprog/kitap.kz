<?php

use Illuminate\Database\Seeder;

class TranslateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('translator_languages')->insert(array(
            0 => array(
                'id'        => 5,
                'locale'    => 'ru',
                'name'      => 'Казахский'
            ),
        ));
    }
}
