<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('authors')->delete();
        
        \DB::table('authors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Автор 1',
                'img_path' => '/uploads/authors/1/avtor-1.jpeg',
                'description' => '<p>Описание 1</p>',
                'created_at' => '2017-11-14 09:29:03',
                'updated_at' => '2017-11-14 09:29:03',
            ),
        ));
        
        
    }
}