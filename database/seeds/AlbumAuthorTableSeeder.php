<?php

use Illuminate\Database\Seeder;

class AlbumAuthorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('album_author')->delete();
        
        \DB::table('album_author')->insert(array (
            0 => 
            array (
                'album_id' => 1,
                'author_id' => 1,
                'created_at' => '2017-11-14 09:29:49',
                'updated_at' => '2017-11-14 09:29:49',
            ),
        ));
        
        
    }
}