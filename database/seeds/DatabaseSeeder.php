<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

	public function run()
	{
		$this->call('DefaultAdminUserSeeder');
	    $this->call(VideosTableSeeder::class);
        $this->call(AlbumsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(AudiobooksTableSeeder::class);
        $this->call(AudiofilesTableSeeder::class);
        $this->call(AuthorsTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        $this->call(DictorsTableSeeder::class);
        $this->call(GenresTableSeeder::class);
		$this->call(AlbumAuthorTableSeeder::class);
		$this->call(AuthorBookTableSeeder::class);
		$this->call(BookGenreTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(TranslateSeed::class);
    }

}
