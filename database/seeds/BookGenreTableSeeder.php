<?php

use Illuminate\Database\Seeder;

class BookGenreTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('book_genre')->delete();
        
        \DB::table('book_genre')->insert(array (
            0 => 
            array (
                'id' => 2,
                'book_id' => 2,
                'genre_id' => 1,
            ),
        ));
        
        
    }
}