<?php

use Illuminate\Database\Seeder;

class AuthorBookTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('author_book')->delete();
        
        \DB::table('author_book')->insert(array (
            0 => 
            array (
                'id' => 2,
                'author_id' => 1,
                'book_id' => 2,
                'created_at' => '2017-11-15 07:55:40',
                'updated_at' => '2017-11-15 07:55:40',
            ),
        ));
        
        
    }
}