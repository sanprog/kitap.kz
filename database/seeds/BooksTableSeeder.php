<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('books')->delete();
        
        \DB::table('books')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Книга 1',
                'slug' => 'book1',
                'img_path' => NULL,
                'file_path' => NULL,
                'description' => '<p>описание 1</p>',
                'created_at' => '2017-11-15 07:55:40',
                'updated_at' => '2017-11-15 07:55:40',
            ),
        ));
        
        
    }
}