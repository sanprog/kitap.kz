<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('videos')->delete();
        
        \DB::table('videos')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Видео 1',
                'slug' => 'video-1',
                'description' => '<p>описание 1</p>',
                'img_path' => '1',
                'file_path' => '1',
                'created_at' => '2017-11-15 11:46:52',
                'updated_at' => '2017-11-15 11:46:52',
            ),
        ));
        
        
    }
}