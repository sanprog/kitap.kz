<?php

use Illuminate\Database\Seeder;

class DictorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('dictors')->delete();
        
        \DB::table('dictors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'dictor_name' => 'Диктор 1',
                'img_src' => '/uploads/dictors/1/diktor-1.jpeg',
                'description' => '<p>описание 1</p>',
                'created_at' => '2017-11-14 09:28:31',
                'updated_at' => '2017-11-14 09:28:31',
            ),
        ));
        
        
    }
}