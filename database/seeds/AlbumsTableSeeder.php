<?php

use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('albums')->delete();
        
        \DB::table('albums')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Альбом 1',
                'slug' => 'albom-1',
                'img_path' => 'def.jpg',
                'description' => '<p>описание 1</p>',
                'created_at' => '2017-11-14 09:29:49',
                'updated_at' => '2017-11-14 09:29:49',
            ),
        ));
        
        
    }
}