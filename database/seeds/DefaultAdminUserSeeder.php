<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultAdminUserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('access_user')->delete();
		DB::table('users')->delete();
		DB::table('access_group')->delete();


		DB::table('access_group')->insert([
			['id' => 1, 'group_id' => 1, 'name' => 'Гость', 'description' => 'Guest',],
			['id' => 2, 'group_id' => 2, 'name' => 'Администратор', 'description' => 'Admin',]
		]);

		DB::table('users')->insert([
			'id'       => 1,
			'name'     => 'test',
			'email'    => 'test@test.test',
			'password' => '$2y$10$AWKUsOLDiHECRQnv.ATORu27PKTe.82T.KRUZMRiIcNuV1qqMy6Ei' //123456
		]);

		DB::table('access_user')->insert(['user_id' => 1, 'group_id' => 2]);

		$this->command->info('тестовый пользовательс с правами админина создан.');
	}
}
