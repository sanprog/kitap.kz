<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableForFindBugInText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_errors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id')->comment('книга');
            $table->text('selected_text')->comment('выделенный текст в книге');
            $table->string('url')->comment('url страницы');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('book_errors');
    }
}
