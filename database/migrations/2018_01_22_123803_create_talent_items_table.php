<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_items', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('talent_id', false, true)->comment('Связь с таблицой talents');
			$table->string('img_path', 150)->nullable();
			$table->string('book_path', 150)->nullable();
			$table->string('audio_path', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent_items');
    }
}
