<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoetAudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poet_audios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poet_id')->comment('Связь с таблицей Poets');
            $table->string('name', 255)->comment('Имя аудио');;
            $table->string('file_path', 255, true)->comment('ссылка на файл');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poet_audios');
    }
}
