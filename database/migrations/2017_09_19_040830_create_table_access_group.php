<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccessGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('access_group', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('group_id', false, true)->comment('Связь с таблицой access_group');
            $table->string('name', 150)->comment('Название группы');
            $table->string('description')->comment('Описания группы');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_group');
    }
}
