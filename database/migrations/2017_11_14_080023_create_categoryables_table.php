<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoryables', function (Blueprint $table) {
			$table->integer('category_id')->unsigned()->comment('связь с таблицей categories');
			$table->integer('categoryable_id')->unsigned()->comment('полиморфная связь');
			$table->string('categoryable_type')->comment('тип связываемой модели');
            $table->timestamps();

			$table->foreign('category_id')
				->references('id')
				->on('categories')
				->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoryables');
    }
}
