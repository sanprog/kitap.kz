<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDictors extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('dictors', function (Blueprint $table) {
			$table->increments('id');
			$table->string('dictor_name', false, true)->comment('Имя автора');
			$table->string('img_src', false, true)->comment('Фото автора');
			$table->longText('description', false, true)->comment('О авторе');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('dictors');
	}
}
