<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForAudioWithSpecauthors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specauthor_audio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specauthor_id')->comment('связь с таблицой specauthor');
            $table->string('file_path')->comment('путь к файлу');
            $table->string('name')->comment('Название');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specauthor_audio');
    }
}
