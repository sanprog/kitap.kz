<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiobooksTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('audiobooks', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('book_id', false, true)->comment('Связь с таблицой book');
			$table->integer('dictor_id', false, true)->comment('Связь с таблицой dictor');
			$table->integer('timeline')->comment('длина аудиокниги');
			$table->timestamps();
			//$table->integer('sort', false)->comment('Сортировка аудиофайлов');
			//$table->string('file_src', false, true)->comment('Путь к аудиофайлу');

			$table->foreign('book_id')->references('id')->on('books')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('dictor_id')->references('id')->on('dictors')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('audiobooks');
	}
}
