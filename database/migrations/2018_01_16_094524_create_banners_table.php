<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('banners', function (Blueprint $table) {
			$table->increments('id');
			$table->string('type', 100)->default('public');
			$table->string('img_path', 150)->nullable()->comment('Путь изображения');
			$table->string('uri', 150)->nullable()->comment('ссылка на ресурс');
			$table->string('title', 150)->nullable();
			$table->boolean('active')->default(false);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('banners');
    }
}
