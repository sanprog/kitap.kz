<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesForPerser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('categories', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('joomla category id');
			$table->integer('old_parent_id')->nullable()->comment('joomla parent category id');
		});

		Schema::table('authors', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
		});

		Schema::table('books', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
		});

		Schema::table('videos', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
		});

		Schema::table('audiobooks', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
		});

		Schema::table('albums', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('categories', function (Blueprint $table) {
			$table->dropColumn('old_id');
			$table->dropColumn('old_parent_id');
		});

		Schema::table('authors', function (Blueprint $table) {
			$table->dropColumn('old_id');
		});

		Schema::table('books', function (Blueprint $table) {
			$table->dropColumn('old_id');
		});

		Schema::table('videos', function (Blueprint $table) {
			$table->dropColumn('old_id');
		});

		Schema::table('audiobooks', function (Blueprint $table) {
			$table->dropColumn('old_id');
		});

		Schema::table('albums', function (Blueprint $table) {
			$table->dropColumn('old_id');
		});
    }
}
