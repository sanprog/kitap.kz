<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumAuthorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('album_author', function (Blueprint $table) {
			$table->integer('album_id')->unsigned()->comment('связь с таблицей albums');
			$table->integer('author_id')->unsigned()->comment('связь с таблицей авторы');
			$table->timestamps();

			$table->foreign('album_id')
				->references('id')
				->on('albums')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('author_id')
				->references('id')
				->on('authors')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('album_author');
    }
}
