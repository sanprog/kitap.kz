<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForSpecauthors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specauthor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->comment('связь с таблицой authors');
            $table->text('desc')->comment('краткое описание');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specauthor');
    }
}
