<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('articles', function (Blueprint $table) {
			$table->integer('old_id')->nullable()->comment('old Id from joomla');
			$table->integer('published')->nullable();
		});
		DB::statement('ALTER TABLE articles MODIFY text LONGTEXT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('articles', function (Blueprint $table) {
			$table->dropColumn('old_id');
			$table->dropColumn('published');
		});
    }
}
