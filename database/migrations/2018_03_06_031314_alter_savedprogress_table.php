<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSavedprogressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('savedprogress', function (Blueprint $table) {
			DB::statement("alter table savedprogress drop foreign key savedprogress_user_id_foreign");
			DB::statement("alter table savedprogress drop foreign key savedprogress_book_id_foreign");
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
