<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesForStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('books', function (Blueprint $table) {
			$table->integer('hits')->default(0);
		});
		Schema::table('audiobooks', function (Blueprint $table) {
			$table->integer('hits')->default(0);
		});
		Schema::table('albums', function (Blueprint $table) {
			$table->integer('hits')->default(0);
		});
		Schema::table('videos', function (Blueprint $table) {
			$table->integer('hits')->default(0);
			$table->dropColumn('count_views');
		});
		Schema::table('authors', function (Blueprint $table) {
			$table->integer('hits')->default(0);
		});
		Schema::table('articles', function (Blueprint $table) {
			$table->integer('hits')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('books', function (Blueprint $table) {
			$table->dropColumn('hits');
		});
		Schema::table('audiobooks', function (Blueprint $table) {
			$table->dropColumn('hits');
		});
		Schema::table('albums', function (Blueprint $table) {
			$table->dropColumn('hits');
		});
		Schema::table('videos', function (Blueprint $table) {
			$table->dropColumn('hits');
			$table->integer('count_views')->default(0);
		});
		Schema::table('authors', function (Blueprint $table) {
			$table->dropColumn('hits');
		});
		Schema::table('articles', function (Blueprint $table) {
			$table->dropColumn('hits');
		});
    }
}
