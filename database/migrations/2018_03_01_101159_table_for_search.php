<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableForSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_search', function (Blueprint $table) {
            $table->increments('id');
            $table->string('word')->unique()->comment('слово');
            $table->string('trigram', 255)->comment('Триграмма слова');
            $table->integer('frequency')->comment('частота');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("word_search");
    }
}
