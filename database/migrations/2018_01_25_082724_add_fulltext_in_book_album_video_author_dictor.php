<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFulltextInBookAlbumVideoAuthorDictor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE books ADD FULLTEXT(name,description)");

        DB::statement("ALTER TABLE albums ADD FULLTEXT(name,description)");

        DB::statement("ALTER TABLE videos ADD FULLTEXT(name,description)");

        DB::statement("ALTER TABLE authors ADD FULLTEXT(name,description)");

        DB::statement("ALTER TABLE dictors ADD FULLTEXT(dictor_name,description)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
