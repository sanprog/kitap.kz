<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CresteTableAuthorsBook extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('author_book', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('author_id', false, true)->comment('Связь с таблицой author');
			$table->integer('book_id', false, true)->comment('Связь с таблицой book');

			$table->timestamps();

			$table->foreign('book_id')->references('id')->on('books')->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('author_book');
	}
}
