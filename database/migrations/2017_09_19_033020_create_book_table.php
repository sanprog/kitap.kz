<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150)->comment('Название книги');
            $table->string('slug', 150)->comment('Алиас (ЧПУ) книги');
            $table->string('img_path', 150)->nullable()->comment('Путь изображения книги');
            $table->string('file_path', 150)->nullable()->comment('Путь файла книги');
            $table->text('description')->nullable()->comment('Описние книги');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
