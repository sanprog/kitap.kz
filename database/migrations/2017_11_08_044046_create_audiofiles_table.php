<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiofilesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('audiofiles', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('file_path', 255, true)->comment('ссылка на файл');
			$table->integer('audiobook_id', false, true)->comment('Связь с таблицой audiobooks');
			$table->timestamps();

			$table->foreign('audiobook_id')
				->references('id')
				->on('audiobooks')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('audiofiles');
	}
}
