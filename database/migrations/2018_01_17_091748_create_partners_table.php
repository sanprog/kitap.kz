<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title', 255)->nullable();
			$table->string('img_path', 150)->nullable()->comment('Путь изображения');
            $table->timestamps();
        });

		Schema::create('book_partner', function (Blueprint $table) {
			$table->integer('book_id')->unsigned()->comment('связь с таблицей books');
			$table->integer('partner_id')->unsigned()->comment('связь с таблицей partners');

			$table->foreign('book_id')
				->references('id')
				->on('books')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('partner_id')
				->references('id')
				->on('partners')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
		Schema::dropIfExists('book_partner');
    }
}
