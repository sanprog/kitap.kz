function open_submenu() {
    var data_submenu_open = $('[data-submenu-open]');
    var data_submenu = $('[data-submenu]');
    if ($(data_submenu_open).length === 0) {
        return 0;
    }
    var doc_click_close = function(e) {
        if ($(e.target).closest(data_submenu).length === 0 && $(e.target).closest(data_submenu_open).length === 0) {
            $('body').removeClass('block-scroll');
            $(data_submenu_open).parent().removeClass('is-active');
            $(data_submenu_open).removeClass('is-active');
            $(data_submenu).fadeOut(200);
            $(document).off('click', doc_click_close);
        }
    };
    $(data_submenu_open).on('click', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('is-active')) {
            if ($(this).data('scroll-lock') === 'lock') {
                $('body').addClass('block-scroll');
            }
            // $(data_submenu_open).parent().removeClass('is-active');
            $(data_submenu_open).removeClass('is-active');
            $(data_submenu).fadeOut(200);
            $(this).parent().addClass('is-active');
            $(this).addClass('is-active');
            $('[data-submenu="' + $(this).data('submenu-open') + '"]').slideDown(200);
            if ($(this).data('submenu-doc') !== 'off') {
                $(document).off('click', doc_click_close);
                $(document).on('click', doc_click_close);
            }
        } else {
            if ($(this).data('submenu-doc') !== 'off') {
                $(document).off('click', doc_click_close);
            }
            if ($(this).data('scroll-lock') === 'lock') {
                $('body').removeClass('block-scroll');
            }
            $(this).parent().removeClass('is-active');
            $(this).removeClass('is-active');
            $('[data-submenu="' + $(this).data('submenu-open') + '"]').fadeOut(200);
        }
    });
}
open_submenu();
