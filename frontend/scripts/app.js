$(window).scroll(function() {
    if($(this).scrollTop() > 2000) {
        $('#tp').css('display','flex');
    } else {
        $('#tp').css('display','none');
    }
});
$(document).ready(function() {
	if($(this).scrollTop() > 2000) {
		$('#tp').css('display','flex');
	}
  var owl = $('.offers-carousel');
  owl.trigger('refresh.owl.carousel');
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);

    });
  $('.js-enter').click(function() {
    // $('.js-reg').addClass('_active');
    $('.js-reg').fadeIn(500);
    $(document.body).addClass('_active');
  });
  $(document).mousedown(function(e) {
       var regContainer = $(".js-reg-inner");
       if (regContainer.length && regContainer.has(e.target).length === 0) {
         $('.js-reg').fadeOut(300);
         $(document.body).removeClass('_active');
       }

        var imgContainer = $(".js-imgmodal-inner");
       if (imgContainer.length && imgContainer.has(e.target).length === 0) {

         $('.js-imgmodal').fadeOut(300);
         $(document.body).removeClass('_active');
       }
  });

  $('.js-ok').click(function() {
    $('.js-reg').fadeOut(300);
    $(document.body).removeClass('_active');
  });

  $('.js-burger').click(function() {
    $(this).toggleClass('__active');
    $('.header-nav').toggleClass('__active');
  });
  $('.js-mylibrary-close').click(function() {
    $(this).parent().find('.js-mylibrary-alert').fadeIn(500);
  });
  $('.js-mylibrary-alert-close').click(function() {
    $(this).closest('.js-mylibrary-alert').fadeOut(300);
  });
  $('.js-myquote-close').click(function() {
    $(this).parent().find('.js-myquote-alert').fadeIn(500);
  });
  $('.js-myquote-alert-close').click(function() {
    $(this).closest('.js-myquote-alert').fadeOut(300);
  });
  $('.js-addquote-close').click(function() {
    $('.js-addquote').fadeOut(300);
    $(document.body).removeClass('_active');
  });

  $('.js-article-img').click(function() {
    $('.js-imgmodal').fadeIn(500);
    $(document.body).addClass('_active');
  });

  $('.js-imgmodal-close').click(function() {
    $('.js-imgmodal').fadeOut(300);
    $(document.body).removeClass('_active');
  });

  $('ul.tabs li').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

	$('.tab-link').click(function() {
		var tab_id = $(this).attr('data-tab');

		$('.tab-content').removeClass('current');

		$('#' + tab_id).addClass('current');
	});

  if ($(window).width() < 770) {
   function mobileTabs() {

    var value = $( ".searchresult-select" ).val();
    $('.searchresul-content').removeClass('current');
    $('#' + value).addClass('current');
  };
    $( ".searchresult-select" ).change( mobileTabs );
    mobileTabs();
}

  $('.js-filter').click(function(){
    $('.js-filter-block').toggleClass('_active');
    window.setTimeout(function(){
      $('.filter-control__ico').toggleClass('__active')
    }, 500);
  });
  // $('.catalog-item').click(function(){
  //   $(this).toggleClass('__open');
  // });
  $('.accordion').accordion({
      "transitionSpeed": 400,
        singleOpen: false
    });

    $('.accordion .catalog-item__icon').trigger('accordion.refresh');
  $('.library-carousel').owlCarousel({
    items: 2,
    loop: true,
    margin: 60,
    nav: true,
    navContainerClass: 'library-carousel__controls',
    navText: [],
    smartSpeed: 500,
    responsive: {
      0: {
        items: 1,
        stagePadding: 20
      },
      600: {
        items: 2
      }
    }
  });
  $('.speaker-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 60,
    nav: true,
    navContainerClass: 'speaker-carousel__controls',
    navText: [],
    smartSpeed: 500,
    responsive: {
      0: {
        items: 1,
        stagePadding: 20
      },
      600: {
        items: 1
      }
    }
  });
  $('.js-popular-slider').owlCarousel({
      stagePadding: 70,
       loop:true,
       margin:35,
       nav:false,
       responsive:{
           0:{
               items:1
           },
           500:{
               items:3
           },
           1000:{
               items:5
           }
       }
  });
  $('.js-popaudio-slider').owlCarousel({
      stagePadding: 70,
       loop:true,
       margin:35,
       nav:false,
       responsive:{
           0:{
               items:1
           },
           500:{
               items:3
           },
           1000:{
               items:5
           }
       }
  });
  $('.js-studies-slider').owlCarousel({
      stagePadding: 70,
       loop:true,
       margin:35,
       nav:false,
       responsive:{
           0:{
               items:1
           },
           600:{
               items:3
           },
           1000:{
               items:5
           }
       }
  });
  $('.js-new-slider').owlCarousel({
      stagePadding: 70,
       loop:true,
       margin:35,
       nav:false,
       responsive:{
           0:{
               items:1
           },
           600:{
               items:3
           },
           1000:{
               items:5
           }
       }
  });
  $('.offers-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: true,
    navContainerClass: 'offers-carousel__controls',
    navText: [],
    smartSpeed: 500,
    autoHeight: true,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0
      },
      600: {
        items: 1
      }
    }
  });
  // $('.js-ted-desc').click(function(){
  //   $('.ted-desc').addClass('_active');
  //   $('.js-ted-desc').hide();
  // });
  $('.js-phone').mask('+7(000) 000-00-00');
  $('.setting-select-item').selectpicker({
    iconBase:'fa fa-check',
    template: {
      caret: '<span class="fa fa-angle-down"></span>'
    }
  });
  $('.allbooks-select').selectpicker({
    theme:'allbooks-theme',
    template: {
      caret: '<span class="fa fa-chevron-down"></span>'
    }
  });
  $('.donate-select').selectpicker({
    theme:'donate-theme',
    template: {
      caret: '<span class="fa fa-angle-down"></span>'
    }
  });
  $('.personal-selec').selectpicker({
    theme:'personal-selec',
    template: {
      caret: '<span class="fa fa-angle-down"></span>'
    }
  });
  $('#userbirthdate').datepicker([]);

  $('.infoSliderContainer').bxSlider({
    mode:'fade',
    pager:false,
    nextSelector: '#infoSlider-icon__next',
    prevSelector: '#infoSlider-icon__prev',
    nextText: ' ',
    prevText: ' ',
    auto:true,
    pause:15000
  });  
  $('.quote-slider').bxSlider({
    mode:'fade',
    pager:false,
    nextSelector: '.icons-quote__left',
    prevSelector: '.icons-quote__right',
    nextText: ' ',
    prevText: ' ',
    auto:true,
    pause:20000
  });

  $('.js-categories-slider').owlCarousel({
  	loop: true,
  	margin: 0,
  	nav: true,
  	navText: ' ',
  	navContainerClass: 'categories-nav',
  	dotsClass: 'categories-dot',
  	responsive: {
		0: {
			items: 1
		},
		600: {
			items: 1
		},
		1000: {
			items: 1
		}
	}
});

// slice function

$(function () {
    $(".autor-info-item").slice(0, 1).show();
    $(".js-author-desc__btn").on('click', function (e) {
        e.preventDefault();
        $(".autor-info-item:hidden").slice(0, 5).slideDown();
        if ($("autor-info-item:hidden").length == 0) {
          $('.js-author-desc__btn').fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        $('.js-author-desc__btn').fadeOut('slow');
    });
});

$(function () {
    $(".talents-desc__item").slice(0, 2).show();
    $(".js-talents-desc__btn").on('click', function (e) {
        e.preventDefault();
        $(".talents-desc__item:hidden").slice(0, 5).slideDown();
        if ($("talents-desc__item:hidden").length == 0) {
          $('.js-talents-desc__btn').fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        $('.js-talents-desc__btn').fadeOut('slow');
    });
});
$(function () {
    $(".talentsinner-desc__item").slice(0, 1).show();
    $(".js-talentsinner-desc__btn").on('click', function (e) {
        e.preventDefault();
        $(".talentsinner-desc__item:hidden").slice(0, 2).slideDown();
        if ($("talentsinner-desc__item:hidden").length == 0) {
            $('.js-talentsinner-desc__btn').fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        $('.js-talentsinner-desc__btn').fadeOut('slow');
    });
});

$(function () {
    $(".ted-desc__item").slice(0, 2).show();
    $(".js-ted-desc").on('click', function (e) {
        e.preventDefault();
        $(".ted-desc__item:hidden").slice(0, 2).slideDown();
        if ($("ted-desc__item:hidden").length == 0) {
            $('.js-ted-desc').fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        $('.js-ted-desc').fadeOut('slow');
    });
});

// slice function end

// fileinput
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#settingPhotoImg').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#settingPhotoImgInput").change(function(){
    readURL(this);
});

jQuery('.translatemodal-inner').scrollbar();

$('.article-slider-top').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   asNavFor: '.article-slider-nav'
});
$('.article-slider-nav').slick({
   slidesToShow: 5,
   slidesToScroll: 1,
   asNavFor: '.article-slider-top',
   dots: false,
   centerMode: true,
   focusOnSelect: true,
   appendArrows:$('.article-slider-controls'),
   responsive: [{

       breakpoint: 1024,
       settings: {
         slidesToShow: 4,
         infinite: true
       }

     }, {

       breakpoint: 600,
       settings: {
         slidesToShow: 2,
         dots: true
       }

     }, {

       breakpoint: 400,
       settings: {
         slidesToShow: 2
       }
     }]
});

$('.tooltip').tooltipster();
$('.modal-row.__error').tooltipster({
  theme: ['tooltipster-light', 'tooltipster-light-customized'],
  maxWidth: 260,
  speed: 300,
  animation: 'grow',
  side: ['top'],
    functionPosition: function(instance, helper, position){
      if ($(window).width() > 400) {
      position.coord.left += 140;
      return position;
    }
}
});
	// $('.faq-item').click(function(){
	//   $('.faq-item').removeClass('__active');
	//   $('.faq-item__text').slideIn(200);
	//   $(this).toggleClass('__active');
	//   $(this).find('.faq-item__text').slideDown(200);
	// });
	if ($('.faq').length !== 0) {
		$('.faq-item').click(function() {
			var item = $(this);
			if ($(item).hasClass('__active')) {
				$(item).removeClass('__active');
				$(item).children('.faq-item__text').slideUp(350);
			}
			else {
				$('.faq-item').removeClass('__active');
				$('.faq-item__text').slideUp(200);
				$(item).toggleClass('__active');
				$(item).children('.faq-item__text').slideToggle(350);
			}
		});
	}
	window.setTimeout(function() {
		$('.push-item._active').fadeOut(500);
	}, 10000)
	$('.push-item._active').click(function() {
		$(this).fadeOut(500);
	});

// translate
$.ajaxSetup({
  headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

  $('form.transtate-form').on('submit', function(e) {
      e.preventDefault();
  $('.js-translatemodal').fadeIn(500, function() {
    $('.js-translatemodal').on('click', modalClickClose);
    $(document).on('keyup', modalButtonClose);


  });
  $('.translatemodal-input').val($('.transtate-search__input').val());
  sendDictionaryRequest($('.transtate-search__input').val());
  });

$('form.translatemodal-form').on('submit', function(e) {
  e.preventDefault();
  sendDictionaryRequest($(this).children('input').val());
});


  function modalClickClose(e) {
      if ($(e.target).hasClass('js-translatemodal')) {
          $('.js-translatemodal').fadeOut(500, function() {
              $('.js-translatemodal').off('click', modalClickClose);
          });
      }
  }
  function modalButtonClose(e) {
      if (e.keyCode === 27) {
          $('.js-translatemodal').fadeOut(500, function() {
              $('.js-translatemodal').off('click', modalButtonClose);
          });
      }
  }

$( ".translatemodal-input" ).autocomplete({
  source: "dictionary/autocomplete",
});

  function sendDictionaryRequest(word) {
  $.ajax({
    type: "POST",
    url: "dictionary",
    data:{'word':word},
    beforeSend: function(){
    },
    success: function(data){
      if(data.length === 0){
        $('.translatemodal-subtitle').html('');
        $('.translatemodal-section').html('Такого слова не найдено');
        $('.translatemodal-item').remove();
      }
      $('.translatemodal-subtitle').html(data[0].Word);
      $('.translatemodal-section').html(data[0].GrammerLong);
      $('.translatemodal-item').remove();
      data.forEach(function(item, i) {
        $('.translatemodal-info').append("" +
          "<div class='translatemodal-item'>"+
          "<span class='translatemodal-item__title'>" + ++i + ". " + item.Definition +".</span>"+
          "<span class='translatemodal-item__text'>"+ item.Example + "</span>"+
          "<span class='translatemodal-item__important'>"+ item.Translation +
          "<span class='translatemodal-item__lang'>"+ item.Language +"</span>"+
          "</span>"+
          "</div>");
      })
    }
  });
}

	var talentsinner_realHeight = $('.talentsinner-desc').height();
	var talentsinner_beffHeight = 8 * 16;
	$('.talentsinner-desc').css('height', talentsinner_beffHeight);

	$('.js-talentsinner-desc__btn_2').click(function(e) {
		e.stopPropagation();
		if ($('.talentsinner-desc').height() == talentsinner_beffHeight) {
			$('.js-talentsinner-desc__btn_2').css({'transform': 'rotate(180deg)'});
			$('.talentsinner-desc').animate({"height" : talentsinner_realHeight}, 1000);
		}
		if ($('.talentsinner-desc').height() == talentsinner_realHeight) {
			$('.js-talentsinner-desc__btn_2').css({'transform': 'rotate(0deg)'});
			$('.talentsinner-desc').animate({"height" : talentsinner_beffHeight}, 1000);
		}
	});
});

function runTolltip() {
	$('.modal-row.__error').tooltipster({
		theme: ['tooltipster-light', 'tooltipster-light-customized'],
		maxWidth: 260,
		speed: 300,
		animation: 'grow',
		side: ['top'],
        trigger: 'custom',
        timer: 30000,
        triggerOpen: {
            click: true
        },
        triggerClose: {
            click: true
        },
		functionPosition: function(instance, helper, position){
			if ($(window).width() > 400) {
				position.coord.left += 140;
				return position;
			}
		}
	}).tooltipster('show');
}

//login and rigister form

$('body').on('click', '[data-action="auth"]', function(e) {
	e.preventDefault();
	var form = $(this).closest('form');

	$.ajax({
		url: $(form).attr('action'),
		method: 'POST',
		dataType: 'JSON',
		data: $(form).serialize(),
		success: function(response) {
			if (response.success) {
				window.location = response.redirect;
			}
		},
		error: function(response) {
			if (response.status === 422 || response.status === 423) {
                $(".tooltipstered").tooltipster('destroy');

				if(response.responseJSON.login){
					$('#login_email').parent().attr('title', response.responseJSON.login).addClass('__error');
				} else {
                    $('#login_email').parent().removeClass('__error');
                }
				if(response.responseJSON.password){
					$('#login_password').parent().attr('title', response.responseJSON.password).addClass('__error');
				} else {
                    $('#login_password').parent().removeClass('__error');
                }
			} else if (response.status === 200) {
				window.location.href(response.redirect);
			} else {
				// show_notice(window.L.error_on_server, 'error');
			}
			runTolltip();
		},
		beforeSend: function() {
		},
		complete: function() {
		}
	});

}).on('click', '[data-action="register"]', function(e) {
	e.preventDefault();

	var form = $(this).closest('form');

	$.ajax({
		url: $(form).attr('action'),
		method: 'POST',
		dataType: 'JSON',
		data: $(form).serialize(),
		success: function(response) {
            if (response.success) {
                window.location = response.redirect;
            }
		},
		error: function(response) {
			if (response.status === 422 || response.status === 423) {
				if(response.responseJSON.email){
					$('#register_email').parent().attr('title', response.responseJSON.email).addClass('__error');

				}
				if(response.responseJSON.password){
					$('#register_password').parent().attr('title', response.responseJSON.password).addClass('__error');
				}
			} else if (response.status === 200) {
                window.location.reload();
			} else {
				// show_notice(window.L.error_on_server, 'error');
			}
			runTolltip();
		},
		beforeSend: function() {
		},
		complete: function() {
		}
	});

});




//catalog menu wrapper
$(function() {
    if ($('.catalog-menu-wrapper').length !== 0) {
        $('.catalog-menu-wrapper .catalog-menu__dropmenu a').on('click', 'span', function(e) {
            e.preventDefault();
            var parent_local = $(this).closest('.catalog-menu__dropmenu');
            if (!$(parent_local).hasClass('is-active')) {
                $(parent_local).addClass('is-active');
                $(parent_local).children('.catalog-menu__submenu').slideDown(200);
            } else {
                $(parent_local).removeClass('is-active');
                $(parent_local).children('.catalog-menu__submenu').slideUp(200);
            }
        });
    }
});

//mobile
$(function() {
    if ($('.catalogmobile-wrapper').length !== 0) {
        $('.catalogmobile-wrapper .catalogmobile__dropmenu a').on('click', 'span', function(e) {
            e.preventDefault();
            var parent_local = $(this).closest('.catalogmobile__dropmenu');
            if (!$(parent_local).hasClass('is-active')) {
                $(parent_local).addClass('is-active');
                $(parent_local).children('.catalogmobile__submenu').slideDown(200);
            } else {
                $(parent_local).removeClass('is-active');
                $(parent_local).children('.catalogmobile__submenu').slideUp(200);
            }
        });
    }
});

function mylibdelete(id,type,name) {

    $.ajax({
        url: "deleteitem/"+type+"/"+id,
        method: "GET",
        success: function () {
            if(type == "video") location.reload();
            $("#"+name).remove();
            var x = document.getElementById(type).innerHTML;
            $("#"+type).text(x-1);
        }
    });
}

function myQuoteDelete(id,type,name) {

	$.ajax({
		url: "deletequote/"+id,
		method: "GET",
		success: function () {
			$("#"+name).remove();
			location.reload();
		}
	});
}

//choosing other donation option
function other_donate(nameSelect){
    if(nameSelect){
        if(nameSelect.value == "other"){
            $('#other-donate').fadeIn();
            $('#other-donate').animate({'margin-top' : '1.2rem'}, { duration: 200, queue: false });
        }
        else{
            $('#other-donate').animate({'margin-top' : '-2.1rem'}, { duration: 200, queue: false });
            $('#other-donate').fadeOut();
        }
    }
}
$('.comment-actions__item-text').click(function () {
    var toggle = $(this).data('toggle');
    if ( toggle.indexOf('comment-add') == -1 ) {
        var i = toggle.length - 1, id = 0, base = 1;

        while( toggle[i] >= '0' && toggle[i] <= '9' ) {
            id += parseInt(toggle[i]) * base;
            base *= 10;
            i--;
        }
        $.ajax({
            url: "/comments/comment/like/"+id,
            method: "GET",
            success: function (response) {
                $('#like_count-'+id).html(response);
            },
            error: function (response) {
                if (response.status == 422){
                    $('.js-reg').fadeIn(500);
                    $(document.body).addClass('_active');
                }
            }
        });
    }
    else {
        $('#' + toggle).fadeToggle('normal');
    }
});
