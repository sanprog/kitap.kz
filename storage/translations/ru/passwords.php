<?php return [
  'password' => 'Құпиясөздердің ұзындығы кемінде алты таңба болуы және ол растауға сәйкес болуы керек.',
  'reset' => 'Құпия сөзіңіз қайта орнатылды!',
  'sent' => 'Сізге құпия сөзді қайта орнату сілтемесін жібердік!',
  'token' => 'Осы құпиясөзді қайта орнату таңбалауышы жарамсыз.',
  'user' => 'Бұл электрондық поштамен ешбір пайдаланушыны таба алмадық.',
];