<?php return [
  'failed' => 'Тіркелу деректері жазбаларға сәйкес келмейді.',
  'throttle' => 'Кіру әрекеті тым көп болды. :seconds секундтан соң қайталап көріңіз.',
  'register_field_email' => 'Email немесе Логин',
  'auth_title_password' => 'Құпиясөз',
  'register_pass_not_equal' => 'Енгізілген құпиясөздер бір-біріне сәйкес емес',
  'register_err_unique_email' => 'Бұндай Email-ді басқа пайдаланушы қолдануда',
  'register_field_repass' => 'Құпиясөзді қайталаңыз',
  'recovery_submit' => 'Құпиясөзді қалпына келтіру',
  'recovery_success_send' => 'Поштаңызға құпиясөзді қалпына келтіру үшін хат жолданды',
];