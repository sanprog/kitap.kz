<?php return [
  'translate' => [
    'title' => 'Әмбебап сөздік',
    'desc' => 'Қазақ тілінің әмбебап сөздігінде
                            <span class="transtate-text-block">
                                <span class="transtate-text__ico-left ico"></span>
                                <span class="transtate-text__center">
                                    150 156
                                </span>
                                <span class="transtate-text__ico-right ico"></span>
                            </span>
                            лексикалық бірлік қамтылған',
    'translate' => [
      'title' => 'Қазақ тілінің әмбебап сөздігі',
      'button' => 'Сөзді аудару',
    ],
    'dictionary' => 'Қазақ тілінің әмбебап сөздігі',
    'dict' => 'Қазақ тілінің әмбебап сөздігі',
  ],
  'popular' => 'Көп оқылған кітаптар',
  'quote' => 'Ойып алынған ой',
];