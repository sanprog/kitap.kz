<?php
// Home
Breadcrumbs::register('main', function ($breadcrumbs) {
	$breadcrumbs->push('Басты', route('main'));
});

Breadcrumbs::register('contacts', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Байланыс', route('contacts'));
});

Breadcrumbs::register('about', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Жоба туралы', route('about'));
});

Breadcrumbs::register('partners', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Серіктестер', route('partners'));
});

Breadcrumbs::register('partner', function ($breadcrumbs, $partner) {
	$breadcrumbs->parent('partners');
	$breadcrumbs->push($partner->title, route('partners.show', $partner->id));
});

Breadcrumbs::register('faq', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Сұрақ-жауап', route('faq'));
});

//special
Breadcrumbs::register('special', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Арнайы жобалар', route('special'));
});
Breadcrumbs::register('special.specbook', function ($breadcrumbs) {
	$breadcrumbs->parent('special');
	$breadcrumbs->push('Құнды кітап', route('specbook'));
});
Breadcrumbs::register('special.fairytale', function ($breadcrumbs) {
	$breadcrumbs->parent('special');
	$breadcrumbs->push('Әдеби кітаптар', route('fairytale'));
});

Breadcrumbs::register('special.specauthor', function ($breadcrumbs) {
	$breadcrumbs->parent('special');
	$breadcrumbs->push('Ұлылардың үні', route('specauthor'));
});

Breadcrumbs::register('special.talents', function ($breadcrumbs) {
	$breadcrumbs->parent('special');
	$breadcrumbs->push('Құс қанаты', route('talents'));
});

Breadcrumbs::register('special.specpoets', function ($breadcrumbs) {
	$breadcrumbs->parent('special');
	$breadcrumbs->push('Қазақтың ақындары', route('poets'));
});


Breadcrumbs::register('toauthor', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Авторларға ақпарат', route('toauthor'));
});

Breadcrumbs::register('dictionary', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Қазақ тілінің әмбебап сөздігі', route('dictionary'));
});

// Home > About
Breadcrumbs::register('books.public', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Көркем әдебиет', route('books.public'));
});

Breadcrumbs::register('books.public.list', function ($breadcrumbs) {
	$breadcrumbs->parent('books.public');
	$activeCategory = request()->route('category');
	$parentLine     = [];
	if ($activeCategory) {
		$parentLine = App\Model\Category::ancestorsAndSelf($activeCategory);
	}
	foreach ($parentLine as $ancestor) {
		if ($ancestor->isRoot()) continue;
		$breadcrumbs->push($ancestor->name, route('books.public.category', $ancestor->id));
	}
});

// Home > Blog
Breadcrumbs::register('books.science', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Ғылыми әдебиет', route('books.science'));
});

Breadcrumbs::register('books.science.list', function ($breadcrumbs) {
	$breadcrumbs->parent('books.science');
	$activeCategory = request()->route('category');
	$parentLine     = [];
	if ($activeCategory) {
		$parentLine = App\Model\Category::ancestorsAndSelf($activeCategory);
	}
	foreach ($parentLine as $ancestor) {
		if ($ancestor->isRoot()) continue;
		$breadcrumbs->push($ancestor->name, route('books.science.category', $ancestor->id));
	}
});

Breadcrumbs::register('books.show', function ($breadcrumbs, $book) {
	$breadcrumbs->parent('main');
	$activeCategory = null;
	if(count($book->categories) > 0) {
		$activeCategory = $book->categories[0]->id;
	}
	$parentLine     = [];
	if ($activeCategory) {
		$parentLine = App\Model\Category::ancestorsAndSelf($activeCategory);
	}
	foreach ($parentLine as $ancestor) {
		$breadcrumbs->push($ancestor->name, route('books.science.category', $ancestor->id));
	}
	$breadcrumbs->push($book->name, route('books.show', $book->slug));
});

Breadcrumbs::register('audiobooks', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Аудиокітап', route('audiobooks'));
});

Breadcrumbs::register('audiobooks.show', function ($breadcrumbs, $book) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Аудиокітап', route('audiobooks'));
	$activeCategory = null;
	if(count($book->categories) > 0) {
		$activeCategory = $book->categories[0]->id;
	}
	$parentLine     = [];
	if ($activeCategory) {
		$parentLine = App\Model\Category::ancestorsAndSelf($activeCategory);
	}
	foreach ($parentLine as $ancestor) {
		$breadcrumbs->push($ancestor->name, route('books.science.category', $ancestor->id));
	}
	$breadcrumbs->push($book->name, route('audiobooks.show', $book->slug));
});

Breadcrumbs::register('dictor', function ($breadcrumbs, $id) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Диктор', route('dictor.show', ['dictor' => $id]));
});

Breadcrumbs::register('article', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Мақалалар', route('article'));
});

Breadcrumbs::register('article.show', function ($breadcrumbs, $name) {
	$breadcrumbs->parent('article');
	$breadcrumbs->push($name, route('article.show', ''));
});

Breadcrumbs::register('music', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Музыка', route('music'));
});
Breadcrumbs::register('music.list', function ($breadcrumbs) {
	$breadcrumbs->parent('music');

	$activeCategory = request()->route('category');
	$parentLine     = [];
	if ($activeCategory) {
		$parentLine = App\Model\Category::ancestorsAndSelf($activeCategory);
	}
	foreach ($parentLine as $ancestor) {
		if ($ancestor->isRoot()) continue;
		$breadcrumbs->push($ancestor->name, route('music.list', $ancestor->id));
	}
});
Breadcrumbs::register('music.show', function ($breadcrumbs, $name) {
	$breadcrumbs->parent('music.list');
	$breadcrumbs->push($name, route('music.show', ''));
});

Breadcrumbs::register('video', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Видео', route('video'));
});

Breadcrumbs::register('video.list', function ($breadcrumbs, $categoryName) {
	$breadcrumbs->parent('video');
	$breadcrumbs->push($categoryName, route('video.list.category', ['category' => '']));
});

Breadcrumbs::register('video.show', function ($breadcrumbs, $video) {
	$breadcrumbs->parent('video');
	$breadcrumbs->push($video->name, route('video.show', ['slug' => $video->slug]));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Page]
Breadcrumbs::register('page', function ($breadcrumbs, $page) {
	$breadcrumbs->parent('category', $page->category);
	$breadcrumbs->push($page->title, route('page', $page->id));
});

Breadcrumbs::register('author.show', function ($breadcrumbs, $name) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push($name, route('author.show', ['author' => '']));
});
