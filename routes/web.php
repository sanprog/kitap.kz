<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/reader/{slug}', 'OldRouteController@reader');

Route::get('/catalog/book/{id}-{slug}', 'OldRouteController@book');
Route::get('/index.php/catalog/book/{id}-{slug}', 'OldRouteController@book');

Route::get('/catalog/audiobook/{id}-{slug}', 'OldRouteController@audiobook');
Route::get('/index.php/catalog/audiobook/{id}-{slug}', 'OldRouteController@audiobook');

Route::get('/catalog/author/{id}-{slug}', 'OldRouteController@author');
Route::get('/index.php/catalog/author/{id}-{slug}', 'OldRouteController@author');

Route::get('/catalog/audio/{id}-{slug}', 'OldRouteController@audio');
Route::get('/index.php/catalog/audio/{id}-{slug}', 'OldRouteController@audio');

Route::get('/catalog/video/{id}-{slug}', 'OldRouteController@video');
Route::get('/index.php/catalog/video/{id}-{slug}', 'OldRouteController@video');





Route::get('/profile', 'ProfileController@show')->name('profile');
Route::post('/profile', 'ProfileController@update')->name('profile.update');
Route::get('/profile/mylibrary', 'ProfileController@mylibrary')->name('profile.mylibrary');
Route::get('/profile/myquote', 'ProfileController@myQuote')->name('profile.myquote');
Route::get('/profile/deleteitem/{type}/{id}', 'ProfileController@deleteItem');
Route::get('/profile/deleteitemlib/{type}/{id}', 'ProfileController@deleteItemLibrary')->name('profile.deleteitem');
Route::get('/profile/deletequote/{id}', 'ProfileController@deleteQuote');

Route::post('/savequote', 'ProfileController@saveQuote')->name('profile.savequote');

Route::get('/donate', function (){ return view('pages.donate'); })->name('donate');
Route::post('/send-donate', 'PaymentController@donate')->name('send.donate');
Route::post('/payment/receive', 'PaymentController@receive')->name('payment.receive');
Route::get('/payment/status', 'PaymentController@status')->name('payment.status');

Route::get('/', 'MainController@index')->name('main');
Route::get('/search', 'MainController@search')->name('search');
Route::get('/contacts', function (){ return view('pages.contacts'); })->name('contacts');
Route::post('/contacts', 'MainController@mail')->name('contacts.send');
Route::get('/about', function (){ return view('pages.about'); })->name('about');
Route::get('/partners', 'PartnerController@index')->name('partners');
Route::get('/partners/{partner}', 'PartnerController@show')->name('partners.show');
Route::get('/faq', function (){ return view('pages.faq'); })->name('faq');
Route::get('/toauthor', function (){ return view('pages.toauthor'); })->name('toauthor');
Route::get('/dictionary', function (){ return view('pages.dictionary'); })->name('dictionary');

Route::get('/dictionary/autocomplete', 'MainController@autocomplete')->name('dictionary.autocomplete');
Route::post('/dictionary/autocomplete', 'MainController@autocomplete')->name('dictionary.autocomplete');
Route::post('/dictionary', 'MainController@dictionary')->name('dictionary');

Route::get('/special', function (){ return view('pages.special'); })->name('special');
Route::get('/special/specbook', 'ProjectController@specBook')->name('specbook');
Route::get('/special/fairytale', function (){ return view('pages.specprojects.fairytale'); })->name('fairytale');
Route::get('/special/fairytale_back', 'ProjectController@fairytale')->name('fairytale');
Route::get('/special/specauthor', 'ProjectController@specauthor')->name('specauthor');
Route::get('/special/specauthor/{id}', 'ProjectController@showSpecauthor')->name('specauthor.show');

Route::get('/special/talents', 'TalentController@index')->name('talents');
Route::get('/special/talents/{talent}', 'TalentController@show')->name('talents.show');
Route::get('/special/talents/{talent}/read', 'TalentController@read')->name('talents.read');


Route::get('/special/poets', 'PoetController@index')->name('poets');
Route::get('/special/poets/{poet}', 'PoetController@show')->name('poets.show');

Route::get('/books', function (){ return redirect()->route('books.public'); })->name('books');

Route::get('/books/public', 'BookController@booksPublic')->name('books.public');
Route::get('/books/public/{category}', 'BookController@booksPublicList')->name('books.public.category');

Route::get('/books/science', 'BookController@booksScience')->name('books.science');
Route::get('/books/science/{category}', 'BookController@booksScienceList')->name('books.science.category');


Route::get('/books/savepage', 'BookController@savePage')->name('book.savepage');
Route::get('/books/{slug}', 'BookController@show')->name('books.show');
Route::get('/books/{slug}/read', 'BookController@read')->name('book.read');
Route::get('/books/{slug}/readspec', 'BookController@readSpec')->name('book.readspec');
Route::post('/books/{slug}/specpage', 'BookController@readSpecGetPage')->name('book.readspec.page');
Route::post('/books/{slug}/setrank', 'BookController@setRank')->name('book.set.rank');

Route::get('/save/{id}/{type}', 'ProfileController@saveToLib')->name('book.savetolib');

Route::get('/audiobooks', 'AudiobookController@index')->name('audiobooks');
Route::get('/audiobooks/list/', 'AudiobookController@list')->name('audiobooks.list');
Route::get('/audiobooks/list/{category}', 'AudiobookController@list')->name('audiobooks.list.category');
Route::get('/audiobooks/{slug}', 'AudiobookController@show')->name('audiobooks.show');
Route::post('/audiobooks/{slug}/setrank', 'AudiobookController@setRank')->name('audiobook.set.rank');

Route::get('/dictor/{dictor}', 'DictorController@show')->name('dictor.show');

Route::get('/article', 'ArticleController@index')->name('article');
Route::get('/article/{slug}', 'ArticleController@show')->name('article.show');

Route::get('/video', 'VideoController@index')->name('video');
Route::get('/video/{slug}', 'VideoController@show')->name('video.show');
Route::get('/video/list/{category}', 'VideoController@list')->name('video.list.category');

Route::get('/music', 'MusicController@index')->name('music');
Route::get('/music/category/{category?}', 'MusicController@indexList')->name('music.list');
Route::get('/music/{slug}', 'MusicController@show')->name('music.show');

Route::get('/author/{author}', 'AuthorController@show')->name('author.show');

Route::get('/social_login/{provider}', 'SocialController@login')->name('login.social');
Route::get('/social_login/callback/{provider}', 'SocialController@callback');

Route::post('/save-error', 'BookController@saveError');

Route::post('/email/save','MainController@saveEmail')->name('email.save');

Route::group([ 'prefix'=>'comments', 'middleware' => 'web'], function (){
    Route::group(['middleware' => 'unauth'], function (){
        Route::post('/comment/add', 'CommentController@add')->name('comment.book');
        Route::get('/comment/like/{comment_id}', 'CommentController@like')->name('like.book');
    });
});

Route::get('/404error','MainController@error')->name('terror');


