<?php
/**
 * Created by PhpStorm.
 * User: imac-manas
 * Date: 9/14/17
 * Time: 11:22 AM
 */

Route::group(['prefix' => 'inside', 'as' => 'admin.', 'middleware' => 'admin'], function () {

	Route::get('/', function () {
		return redirect(route('admin.category.view', ['id' => 1]));
	})->name('index');

	Route::get('category', function () {
		return redirect(route('admin.category.view', ['id' => 1]));
	})->name('category.index');

    Route::get('category/{id}','CategoryController@index')->name('category.view');
    Route::get('category/{id}/getcontent','CategoryController@getContent')->name('category.getcontent');


    Route::post('category/{razdel?}','CategoryController@postIndex')->name('category.post');

    Route::get('selectCategory/{id}','Select2DataController@dataCategory');
    Route::get('selectCategoryBook','Select2DataController@dataCategoryBook');
    Route::get('selectAuthor','Select2DataController@dataAuthors');
    Route::get('selectGenre','Select2DataController@dataGenres');
    Route::get('selectDictor','Select2DataController@dataDictors');
    Route::get('selectBook','Select2DataController@dataBooks');
    Route::get('selectTags','Select2DataController@dataTags');


    Route::resource('author','AuthorController');
    Route::resource('book','BookController');
    Route::resource('genre','GenreController');
    Route::resource('dictor','DictorController');
	Route::resource('music', 'MusicController');
	Route::resource('video', 'VideoController');
	Route::resource('article', 'ArticleController');
	Route::resource('audiobook', 'AudiobookController');
	Route::resource('slider', 'SliderController');
	Route::resource('actual', 'ActualController');
	Route::resource('banner', 'BannerController');
	Route::resource('partner', 'PartnerController');
	Route::resource('talent', 'TalentController');
    Route::resource('error', 'ErrorController');
    Route::resource('subscribe', 'SubscribeController');
    Route::resource('fairytale','FairytaleController');
    Route::resource('translate','TranslateController');
    Route::resource('specauthor','SpecauthorController');
	Route::resource('poet','PoetController');
    Route::resource('comment','CommentController');

    Route::delete('specauthor/destroytrack/{id}', 'SpecauthorController@destroyOneTrack')->name('specauthor.destroytrack');
    Route::delete('music/destroytrack/{id}', 'MusicController@destroyOneTrack')->name('music.destroytrack');
	Route::delete('audiobook/destroytrack/{id}', 'AudiobookController@destroyOneAudiofile')->name('audiobook.destroytrack');
	Route::delete('talent/destroyitem/{talentitem}', 'TalentController@dropTalentItem')->name('talent.destroyitem');

	Route::delete('poet/destroytrack/{id}', 'PoetController@destroyOneTrack')->name('poet.destroytrack');
});
/*
Route::any('{catchall}', function($page) {
    abort(404, "1");
} )->where('catchall', '(.*)');
*/

