<?php
return [
    'box_version' => env('BOX_VERSION', false),
    'from_email' => 'noreplay@itest.kz',
    'lang_id' => 1,
    'langs' => [
        1 => [
            'code' => 'ru',
            'url' => 'rus'
        ],
        2 => [
            'code' => 'kz',
            'url' => 'kaz'
        ]
    ],
    'active_session' => [
        'mobile' => 1,
        'android' => 1,
        'ios' => 1,
        'desktop' => 1
    ],
    'node' => [
        'main_subject' => [
            'math',
            'history_kz',
            'kaz',
            'rus'
        ],
        'question_limit' => [
            'subject' => 25,
            'course' => 25,
            'lecture' => 10
        ],
        'timer_limit' => 180,
    ],
    'cron' => [
        'node_limit' => 3, # число хранимых тестов для одной ноды
        'non_finish_expired_time' => 60 * 60 * 5, # время жизни незавершённого теста 5 часов
        'test_expired_day' => 30, # срок жизни теста
    ],
    'project_id' => 3,
    'password_encrypt_key' => 'CORE',
    'project_tag' => 'itest',
    'user' => [
        'menus' => [
            'profile' => [
                'title' => 'Личная страница',
                'route' => 'profile',
            ],
            'filling' => [
                'title' => 'Заполнить анкету',
                'route' => 'profile.filling',
            ],
            'purchases' => [
                'title' => 'Покупки',
                'route' => 'profile.purchases',
            ],
            'balance' => [
                'title' => 'Баланс',
                'route' => 'profile.balance',
            ],
        ]
    ]
];
