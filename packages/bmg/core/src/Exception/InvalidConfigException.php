<?php

namespace Bmg\Core\Exception;

/**
 * Class InvalidConfigException
 * @package Bmg\Core\Exception
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class InvalidConfigException extends \Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Invalid Configuration';
    }
}
