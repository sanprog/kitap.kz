<?php

namespace Bmg\Core\Exception;

use Exception;

/**
 * Class ErrorBmgException
 *
 * @package Bmg\Core\Exception
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class ErrorBmgException extends Exception
{
    /**
     * The path the client should be redirected to.
     *
     * @var string
     */
    public $redirectTo;
    /**
     * @var array
     */
    private $errors = [];
    /**
     * The status code to use for the response.
     *
     * @var int
     */
    public $status = 422;
    /**
     * ErrorBmgException constructor.
     *
     * @param string $application
     * @param string $message
     */
    public function __construct($application, $message)
    {
        parent::__construct($application . ': ' . $message);
        $this->errors[$application] = $message;
    }
    /**
     * Set the URL to redirect to on a validation error.
     *
     * @param  string $url
     *
     * @return $this
     */
    public function redirectTo($url)
    {
        $this->redirectTo = $url;
        return $this;
    }
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function render($request)
    {
        return $request->expectsJson()
            ? $this->invalidJson($request)
            : $this->invalid($request);
    }
    /**
     * Convert a validation exception into a response.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function invalid($request)
    {
        $url = $this->redirectTo ?? url()->previous();
        return redirect($url)
            ->withErrors(
                $this->errors
            );
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request)
    {
        return response()->json([
            'message' => $this->getMessage(),
            'errors'  => $this->errors,
        ], $this->status);
    }
}
