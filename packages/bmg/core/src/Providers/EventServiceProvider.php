<?php

namespace Bmg\Core\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'Bmg\Core\Listeners\UserEvent@onUserLogin',
        ],
        'Illuminate\Auth\Events\Authenticated' => [
            'Bmg\Core\Listeners\UserEvent@onUserAuthenticated',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'Bmg\Core\Listeners\UserEvent@onUserLogout',
        ],
        'Illuminate\Foundation\Events\LocaleUpdated' => [
            'Bmg\Core\Listeners\LocaleEvent@onChangeLocale',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
