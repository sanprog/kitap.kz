<?php

namespace Bmg\Core\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * The SEO facade.
 *
 * @package Bmg\Core\Facades
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class SEO extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'seo';
    }
}
