<?php

namespace Bmg\Core\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * The SendMail facade.
 *
 * @package Bmg\Core\Facades
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class SendMail extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sendmail';
    }
}
