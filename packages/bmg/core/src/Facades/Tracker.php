<?php

namespace Bmg\Core\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * The Tracker facade.
 *
 * @package Bmg\Core\Facades
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class Tracker extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'tracker';
    }
}
