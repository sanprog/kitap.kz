<?php

namespace Bmg\Core;

use Bmg\Core\Models\SSendEmails;

/**
 * The SendMail facade.
 *
 * @package Bmg\Core
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class SendMail
{
    public function addMessage($template, array $to_emails, $subject, array $data = [])
    {
        //\SendMail::addMessage('emails.welcome',['test@mail.ru'],'Проверка',[]);
        $message = view($template, $data)->render();
        SSendEmails::insert([
            'from_name'  => config('bmg.from_name'),
            'from_email' => config('bmg.from_email'),
            'to_emails'  => json_encode($to_emails),
            'subject'    => $subject,
            'body'       => $message,
            'status'     => 0,
            'locale'     => \App::getLocale(),
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }
    public function sendMessage()
    {
        SSendEmails::where('status', 0)
            ->orderBy('created_at', 'asc')
            ->chunk(50, function ($send_emails) {
                $ids_success = [];
                $ids_fail    = [];
                /** @var $send_emails SSendEmails[] | \Illuminate\Database\Eloquent\Collection */
                foreach ($send_emails as $send_email) {
                    \App::setLocale($send_email->locale);
                    $error = null;
                    try {
                        \Mail::send('emails._layout',
                            [
                                'body'    => $send_email->body,
                                'subject' => $send_email->subject
                            ],
                            function ($message) use ($send_email) {
                                /** @var \Illuminate\Mail\Message $message */
                                $emails = json_decode($send_email->to_emails);
                                $message->from($send_email->from_email, $send_email->from_name)->to($emails)->subject($send_email->subject);
                            }
                        );
                    } catch (\Throwable $e) {
                        $error = $e->getMessage();
                    }
                    if (count(\Mail::failures()) > 0 || $error) {
                        $ids_fail[] = $send_email->id;
                    } else {
                        $ids_success[] = $send_email->id;
                    }
                }
                if ($ids_success) {
                    SSendEmails::whereIn('id', $ids_success)->update(['status' => 1, 'updated_at' => time()]);
                }
                if ($ids_fail) {
                    SSendEmails::whereIn('id', $ids_fail)->update(['status' => 2, 'updated_at' => time()]);
                }
            });
    }
}
