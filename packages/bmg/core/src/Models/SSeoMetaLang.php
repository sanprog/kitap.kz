<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SSeoMetaLang
 *
 * @property integer $id
 * @property integer $locale
 * @property integer $owner_id
 * @property string  $title
 * @property string  $keywords
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @mixin \Eloquent
 */
class SSeoMetaLang extends Model
{
    protected $table = 's_seo_meta_lang';
    protected static $unguarded = true;
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }

}
