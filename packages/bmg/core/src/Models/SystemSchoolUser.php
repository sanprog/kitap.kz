<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscribeUser
 * @package Bmg\Core\Models
 *
 * @property integer $user_id
 * @property integer $school_id
 * @property integer $packet_id
 * @property string $password
 *
 * @mixin \Eloquent
 */
class SystemSchoolUser extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'system_school_user';
    protected $primaryKey = 'user_id';
}
