<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SSeoRedirects
 *
 * @property integer $id
 * @property string $old_url
 * @property string $new_url
 * @property string $type
 * @property bool $isRegex
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @mixin \Eloquent
 */
class SSeoRedirects extends Model
{
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }
}
