<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemSession
 *
 * @package Bmg\Core\Models
 * @property integer $session_id
 * @property string $session_hash
 * @property integer $session_start
 * @property integer $user_id
 * @mixin \Eloquent
 */
class SystemSession extends Model
{
    public $timestamps = false;
    public $primaryKey = 'session_id';
    protected $table = 'system_session';
    //
}
