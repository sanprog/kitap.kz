<?php

namespace Bmg\Core\Models;

use Bmg\Core\Services\Database\Eloquent\InsertOnDuplicateKey;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SCron
 *
 * @property string  $command
 * @property integer $last_start
 *
 * @mixin \Eloquent
 */
class SCron extends Model
{
    use InsertOnDuplicateKey;
    public $timestamps = false;
    protected $table = 's_cron';
    protected $primaryKey = 'command';
    protected $keyType = 'string';
    protected $fillable = [
        'command',
        'last_start',
    ];
    public static function start($command, $param)
    {
        $time   = time();
        $result = 0;
        /** @var SCron $record */
        $record = self::firstOrNew(['command' => $command . ' ' . $param]);
        if ($record->exists) {
            $result = $record->last_start;
            $record->update(['last_start' => $time]);
        } else {
            $record->last_start = $time;
            $record->save();
        }
        return $result;
    }
}
