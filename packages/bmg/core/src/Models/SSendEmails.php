<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SSendEmails
 *
 * @property integer $id
 * @property string  $from_name
 * @property string  $from_email
 * @property string  $to_emails
 * @property string  $subject
 * @property string  $body
 * @property string  $locale
 * @property integer $status
 * @property string  $error
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @mixin \Eloquent
 */
class SSendEmails extends Model
{
    protected $table = 's_send_emails';
    protected $fillable = [
        'from_name',
        'from_email',
        'to_emails',
        'subject',
        'body',
        'status',
        'error',
        'locale'
    ];
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }
}
