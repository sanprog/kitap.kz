<?php

namespace Bmg\Core\Models;

use Bmg\Core\Services\Database\Eloquent\InsertOnDuplicateKey;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemSessionStatus
 *
 * @package Bmg\Core\Models
 * @property integer $session_id
 * @property integer $user_id
 * @property integer $status
 * @property string $client_ip
 * @property string $client_browser
 * @property integer $last_activity
 * @property string $device
 * @mixin \Eloquent
 */
class SystemSessionStatus extends Model
{
    public $timestamps = false;
    public $primaryKey = 'session_id';
    public $keyType = 'string';
    public $incrementing = false;
    protected $table = 'system_session_status';
    use InsertOnDuplicateKey;
    //
}
