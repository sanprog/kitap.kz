<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Content
 *
 * @property integer $post_id
 * @property integer $post_type_id
 * @property integer $post_category_id
 * @property string $post_alias
 * @property string $post_title
 * @property string $post_image
 * @property string $post_short_text
 * @property string $post_json
 * @property string $post_full_text
 * @property integer $lang_id
 * @property string $status
 * @property integer $visit_count
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $modify_time
 * @property integer $modify_user_id
 * @property string $publish_time
 *
 * @property $category ContentCategory
 *
 * @mixin \Eloquent
 */
class Content extends Model
{
    public $timestamps = false;
    protected $connection = 'bilimland';
    protected $table = 'post_item';
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'visit_count',
    ];
    public static function list($type, $size_page = 10)
    {
        $type = self::type($type);
        $result = [];
        if ($type) {
            $result = self::where('post_item.status', 'published')
                ->where('post_item.lang_id', config('bmg.lang_id'))
                ->where('post_item.post_type_id', $type->post_type_id)
                ->orderBy('post_item.publish_time', 'desc')
                ->with([
                    'category' => function ($query) {
                        $query->where('post_category.lang_id', '=', config('bmg.lang_id'));
                    }
                ])
                ->paginate($size_page);
        }
        return $result;
    }
    public static function one($type, $pk)
    {
        $type = self::type($type);
        $result = false;
        if ($type) {
            $result = self::where('post_item.status', '=', 'published')
                ->where('post_id', '=', $pk)
                ->where('post_item.lang_id', config('bmg.lang_id'))
                ->where('post_item.post_type_id', $type->post_type_id)
                ->with([
                    'category' => function ($query) {
                        $query->where('post_category.lang_id', '=', config('bmg.lang_id'));
                    }
                ])
                ->first();
        }
        return $result;
    }
    public static function type($type)
    {
        $key = __METHOD__;
        $result = \Cache::get($key, false);
        $types = [];
        if ($result === false) {
            $types = \DB::connection('bilimland')
                ->table('post_type')
                ->select(['post_type_id', 'post_type_alias'])
                ->get()
                ->keyBy('post_type_alias');
            \Cache::put($key, $result, 60);
        }
        return array_get($types, $type, 0);
    }

    public function date($format = 'd F Y')
    {
        //TODO не поддерживает когда в $format прописаны html tags по хорошему надо свой класс написать
        return \Date::make(strtotime($this->publish_time))->format($format);
    }
    public function img()
    {
        return 'http://bilimland.kz/' . $this->post_image;
    }
    public function addVisit()
    {
        self::where(['post_id' => $this->post_id])->update(['visit_count' => \DB::raw('`visit_count`+1')]);
        //TODO тут должна ещё заполняться таблица post_visit если зашёл авторизованный пользователь
    }
    public function category()
    {
        return $this->belongsTo(ContentCategory::class, 'post_category_id');
    }
    public function formatDescription()
    {
        return str_replace(['/images/','/upload/'], ['http://bilimland.kz/images/','http://bilimland.kz//upload/'], $this->post_full_text);
    }
    private $isWide = null;
    public function isWide()
    {
        if ($this->isWide === null) {
            if ($this->post_json) {
                $data = json_decode($this->post_json, true);
                $this->isWide = array_get($data, 'wide', 0);
            } else {
                $this->isWide = false;
            }
        }
        return $this->isWide;
    }
}
