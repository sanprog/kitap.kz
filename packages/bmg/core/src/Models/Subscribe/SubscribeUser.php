<?php

namespace Bmg\Core\Models\Subscribe;

use Bmg\Core\Models\Payment\Order;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscribeUser
 * @package Bmg\Core\Models\Subscribe
 *
 * @property integer $record_id
 * @property integer $user_id
 * @property integer $project_id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $code_id
 * @property integer $balance_id
 * @property integer $bonus_id
 * @property integer $school_subscribe_id
 * @property integer $create_user_id
 * @property integer $close_user_id
 * @property integer $create_time
 * @property integer $start_time
 * @property integer $expire_time
 * @property integer $close_time
 * @property integer $status
 *
 * @mixin \Eloquent
 */
class SubscribeUser extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'subscribe_user';
    protected $primaryKey = 'record_id';
    /**
     * @param null $user_id
     * @param null $project_id
     * @return self
     */
    public static function activeSubscribe($user_id = null, $project_id = null)
    {
        if ($user_id === null) {
            $user_id = \Auth::id();
        }
        if (!$project_id) {
            $project_id = config('bmg.project_id');
        }
        return SubscribeUser::where('user_id', $user_id)
            ->where('status', 1)
            ->where('project_id', $project_id)
            ->where('expire_time', '>=', time())
            ->orderBy('expire_time', 'asc')
            ->first();
    }

	/**
	 * @param integer $product_id
	 * @param integer $user_id
	 * @param integer $project_id
	 *
	 * @return boolean
	 */
	public static function productPaid($product_id, $user_id = null, $project_id = null)
	{
		if ($user_id === null) {
			$user_id = \Auth::id();
		}
		if (!$project_id) {
			$project_id = config('bmg.project_id');
		}
		return SubscribeUser::where('user_id', $user_id)
			->where('status', 1)
			->where('product_id', $product_id)
			->where('project_id', $project_id)
			->exists();
	}

	/**
	 * @param integer $user_id
	 * @param integer $project_id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public static function productPaidList($user_id = null, $project_id = null)
	{
		if ($user_id === null) {
			$user_id = \Auth::id();
		}
		if (!$project_id) {
			$project_id = config('bmg.project_id');
		}
		return SubscribeUser::where('user_id', $user_id)
			->where('status', 1)
			->where('project_id', $project_id)
			->get();
	}

    /**
     * @param null $user_id
     * @param null $project_id
     * @return boolean|self[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function listSubscribe($user_id = null, $project_id = null)
    {
        if (!$user_id) {
            $user_id = \Auth::id();
        }
        if (!$project_id) {
            $project_id = config('bmg.project_id');
        }
        if (!$user_id || !$project_id || !count($product_list = Product::productsStatus([1, 2]))) {
            return false;
        }
        $history = SubscribeUser::where('user_id', $user_id)
            ->where('project_id', $project_id)
            ->orderBy(\DB::raw('IF( close_time IS NOT NULL, close_time, expire_time )'), 'desc')->get();
        if ($history) {
            $product_list = $product_list->keyBy('product_id');
            $orders = Order::userOrders()->keyBy('order_id');
            foreach ($history as $item) {
                if ($item->product_id && ($product = $product_list->get($item->product_id))) {
                    $item->d_name_service = trans('subscribe.subscribe_product_' . $product->product_type);
                }
                //TODO на старом нету проверок на code_id
                if ($item->order_id && ($order = $orders->get($item->order_id))) {
                    $item->d_trans_id = $order->order_id;
                } elseif ($item->balance_id) {
                    $item->d_trans_id = trans('cabinet.subscribe_source_balance');
                } elseif ($item->bonus_id) {
                    $bonus = SubscribeBonus::find($item->bonus_id);
                    $item->d_name_service = trans('cabinet.subscribe_bonus_' . strtolower($bonus->get('bonus_name')));
                }
            }
        }
        return $history;
    }
    public static function addSubscribe($data, $project_id = null, &$error = null)
    {
        if (!$user_id = intval(array_get($data, 'user_id'))) {
            if (\Auth::guest()) {
                $error = 'invalid_user';
                return false;
            } else {
                $user_id = \Auth::id();
            }
        }
        if (!$project_id) {
            $project_id = config('bmg.project_id');
        }
        $product = null;
        $set = [];
        if ($product_id = intval(@$data['product_id'])) {
            $product = Product::activeProduct($product_id, $project_id);
            if ($school_subscribe_id = intval(@$data['school_subscribe_id'])) {
                $set['school_subscribe_id'] = $school_subscribe_id;
            }
        } elseif ($product_type = @$data['product_type']) {
            $product = Product::productByType($product_type);
        }
        if (!$product) {
            $error = 'invalid_product';
            return false;
        }
        if (@$data['start_time']) {
            if (!$set['start_time'] = strtotime($data['start_time'])) {
                $error = 'invalid_start_time';
                return false;
            }
        } elseif (@$data['expire_time']) {
            if (!$set['expire_time'] = strtotime($data['expire_time'])) {
                $error = 'invalid_expire_time';
                return false;
            }
            $set['start_time'] = time();
        } else {
            $subscribe_user = self::where('user_id', '=', $user_id)
                ->where('project_id', '=', $project_id)
                ->where('status', '=', 1)
                ->orderBy('expire_time', 'desc')
                ->first();
            if ($subscribe_user && $subscribe_user->expire_time) {
                $expire_time = $subscribe_user->expire_time;
            } else {
                $expire_time = 0;
            }
            $set['start_time'] = max(time(), $expire_time);
        }
        if ($product->product_period) {
            $set['expire_time'] = $set['start_time'] + ($product->product_period * 86400);
        }
        $additional_keys = array('order_id', 'balance_id', 'code_id', 'bonus_id', 'school_subscribe_id', 'create_user_id');
        foreach ($additional_keys as $akey) {
            if (!isset($data[$akey])) {
                continue;
            }
            $set[$akey] = intval($data[$akey]);
        }
        $set['status'] = '1';
        $set['user_id'] = $user_id;
        $set['project_id'] = $product->project_id;
        $set['create_time'] = time();
        $set['product_id'] = $product->product_id;
        self::insert($set);
        return true;
    }
    public static function shareSubscribe($data, &$error = null)
    {
        $subscribe = array();
        if (!$user_id = intval(@$data['user_id'])) {
            $error = 'invalid_user_id';
            return false;
        }
        $subscribe['user_id'] = $user_id;
        if (!$product_id = intval(@$data['product_id'])) {
            $error = 'invalid_product_id';
            return false;
        }
        if ($order_id = intval(@$data['order_id'])) {
            $subscribe['order_id'] = $order_id;
        } elseif ($balance_id = intval(@$data['balance_id'])) {
            $subscribe['balance_id'] = $balance_id;
        } else {
            $product = Product::activeProduct($product_id);
            if (!$product) {
                $error = 'undefined_product';
                return false;
            } elseif ($product->product_type != 'admin') {
                $error = 'invalid_payment_source';
                return false;
            }
        }
        $result = \DB::connection('rms')->select('SELECT
				sp.project_id,
				sp.product_id AS `id`
			FROM
				subscribe_product AS `sp`,
				subscribe_product AS `sp2`
			WHERE
				sp2.product_id = :product_id
				AND sp.product_type = sp2.product_type
				AND sp.project_id != sp2.project_id
				AND sp.status = sp2.status;', ['product_id' => $product_id]);
        if (!$result) {
            $error = 'product_for_share_not_found';
            return false;
        }
        foreach ($result as $product) {
            $subscribe['product_id'] = $product->id;
            $subscribe['project_id'] = $product->project_id;
            SubscribeUser::addSubscribe($subscribe, $product->project_id);
        }
        return true;
    }
}
