<?php

namespace Bmg\Core\Models\Subscribe;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscribeBonus
 * @package Bmg\Core\Models\Subscribe
 *
 * @property integer $bonus_id
 * @property string $bonus_name
 * @property string $bonus_code
 * @property integer $bonus_period
 * @property integer $status
 * @property integer $start_time
 * @property integer $expire_time
 *
 * @mixin \Eloquent
 */
class SubscribeBonus extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'subscribe_bonus';
    protected $primaryKey = 'bonus_id';
    /**
     * @param $code
     * @return self
     */
    public static function activeBonus($code)
    {
        $result = self::where('bonus_code', '=', $code)
            ->where('status','=',1)
            ->where('start_time', '>', time())
            ->where('expire_time', '<', time())
            ->first();
        return $result;
    }
}
