<?php

namespace Bmg\Core\Models\Subscribe;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package Bmg\Core\Models\Subscribe
 *
 * @property integer $product_id
 * @property integer $project_id
 * @property string  $product_type
 * @property integer $product_period
 * @property integer $product_price
 * @property integer $product_discount
 * @property integer $position
 * @property integer $status
 *
 * @mixin \Eloquent
 */
class Product extends Model
{
    public    $timestamps = false;
    protected $connection = 'rms';
    protected $table      = 'subscribe_product';
    protected $primaryKey = 'product_id';
    protected $fillable   = [
        'product_type',
        'project_id'
    ];
    /**
     * @param $project_id integer|null
     *
     * @return \Illuminate\Database\Eloquent\Collection|self[]
     */
    public static function activeProducts($project_id = null)
    {
        if ($project_id == null) {
            $project_id = config('bmg.project_id');
        }
        return self::where('status', '=', 1)
            ->where('project_id', '=', $project_id)
            ->orderBy('position', 'asc')
            ->get();
    }
    /**
     * @param $pk         integer
     * @param $project_id integer|null
     *
     * @return self
     */
    public static function activeProduct($pk, $project_id = null)
    {
        if ($project_id == null) {
            $project_id = config('bmg.project_id');
        }
        return self::where('status', '=', 1)
            ->where('project_id', '=', $project_id)
            ->where('product_id', '=', $pk)
            ->first();
    }
    /**
     * @param $product_type string
     * @param $status       array
     * @param $project_id   integer|null
     *
     * @return self
     */
    public static function productByType($product_type, $status = [], $project_id = null)
    {
        if ($project_id == null) {
            $project_id = config('bmg.project_id');
        }
        if (!$status) {
            $status[] = 2;
        }
        return self::where('project_id', '=', $project_id)
            ->whereIn('status', $status)
            ->where('product_type', '=', $product_type)
            ->first();
    }
    /**
     * @param $status     array|integer
     * @param $project_id integer|null
     *
     * @return \Illuminate\Database\Eloquent\Collection|self[]
     */
    public static function productsStatus($status, $project_id = null)
    {
        if ($project_id == null) {
            $project_id = config('bmg.project_id');
        }
        $q = self::where('project_id', '=', $project_id)
            ->orderBy('position', 'asc');
        if (is_array($status)) {
            $q->whereIn('status', $status);
        } else {
            $q->where('status', '=', $status);
        }
        return $q->get();
    }
    public function real_price()
    {
        $price = $this->product_price;
        return ($price - ceil($price * (intval($this->product_discount) / 100)));
    }
}
