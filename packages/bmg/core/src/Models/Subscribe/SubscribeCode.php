<?php

namespace Bmg\Core\Models\Subscribe;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscribeCode
 * @package Bmg\Core\Models\Subscribe
 *
 * @property integer $code_id
 * @property string $code
 * @property string $product_type
 * @property integer $create_user_id
 * @property integer $create_time
 * @property string $create_note
 * @property integer $activate_user_id
 * @property integer $activate_time
 * @property integer $status
 *
 * @mixin \Eloquent
 */
class SubscribeCode extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'subscribe_code';
    protected $primaryKey = 'code_id';
    protected $fillable = [
        'status',
        'activate_user_id',
        'activate_time',
    ];
    /**
     * @param $code
     * @return SubscribeCode|bool
     */
    public static function checkCode($code)
    {
        //TODO api.class.php функция attemptSet для блокировки подбора кода
        $code = preg_replace('#[^\d]+#u', '', $code);
        /** @var self $item */
        $item = self::where('code', '=', $code)->first();
        if (!$item || ($item && $item->status == 0)) {
            return false;
        } else {
            return $item;
        }
    }
}
