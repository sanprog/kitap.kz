<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Content
 *
 * @property integer $post_category_id
 * @property string $post_category_title
 * @property string $post_category_alias
 * @property integer $post_type_id
 * @property integer $lang_id
 * @property string $status
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $modify_time
 * @property integer $modify_user_id
 *
 * @mixin \Eloquent
 */
class ContentCategory extends Model
{
    public $timestamps = false;
    protected $connection = 'bilimland';
    protected $table = 'post_category';
    protected $primaryKey = 'post_category_id';
}
