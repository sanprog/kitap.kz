<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Waavi\Translation\Traits\Translatable;

/**
 * Class Pages
 *
 * @property integer  $id
 * @property integer  $locale
 * @property string   $name
 * @property string   $body
 * @property string   $slug
 * @property string   $module
 * @property integer  $sort
 * @property integer  $created_at
 * @property integer  $updated_at
 *
 * @property SSeoMeta $seo
 * @mixin \Eloquent
 */
class Pages extends Model
{
    protected $table = 'pages';
    protected $fillable = [
        'locale',
        'name',
        'body',
        'slug',
        'module',
        'sort',
        'translate'//Это надо для переводов
    ];
    use Translatable;
    public $translatedAttributes = ['name', 'body'];
    public $translationModel = PagesLang::class;
    public $translationForeignKey = 'owner_id';
    public static $modules = [
        'page'      => 'Без модуля',
        'about'    => 'О нас',
        'support'  => 'Поддержка',
        'policies' => 'Условия использования'
    ];
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        \Event::listen('eloquent.saved: ' . static::class, function ($obj) {
            \Cache::tags([static::class])->flush();
        });
    }
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }
    public function seo()
    {
        return $this->morphOne(SSeoMeta::class, 'module');
    }
    public function url($absolute = false)
    {
        return ($absolute ? request()->root() : '') . url_add_lang($this->module . '/' . $this->slug);
    }
    public static function searchByPath($slug, $module)
    {
        $page = Pages::query()->where('slug', $slug)->where('module', $module)->first();
        if ($page) {
            return $page;
        }
        return false;
    }
    public function isActive()
    {
        if (\Route::currentRouteName() == $this->module && \Request::route()->parameter('slug') == $this->slug) {
            return true;
        }
        return false;
    }
    public static function listPages($module)
    {
        return \Cache::tags([self::class])->remember('list_pages_' . $module, 43200, function () use ($module) {
            return Pages::query()->where('module', $module)->orderBy('sort', 'asc')->get();
        });
    }
}
