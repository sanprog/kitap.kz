<?php

namespace Bmg\Core\Models\Payment;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 *
 * @package Bmg\Core\Models\Payment
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $project_id
 * @property string  $order_key
 * @property string  $order_source
 * @property integer $create_time
 *
 * @mixin \Eloquent
 */
class Order extends Model
{
    public    $timestamps = false;
    protected $connection = 'rms';
    protected $table      = 'payment_order';
    protected $primaryKey = 'order_id';

    public static $sources = [
        'qiwi'          => '',
        'qiwi_terminal' => '',
        'processing'    => '',
        'epay'          => '',
        'kaspi'         => '',
        'kassa24'       => ''
    ];
    /**
     * @param null $user_id
     *
     * @return self[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function userOrders($user_id = null)
    {
        if (!$user_id) {
            $user_id = \Auth::id();
        }
        return self::where('user_id', '=', $user_id)->orderBy('create_time', 'desc')->get();
    }
    public static function addOrder($data, &$error = [])
    {
        $source = array_get($data, 'source');
        if (!$source || !isset(self::$sources[$source])) {
            $error[] = 'Undefined order source';
            return false;
        }
        $key = array_get($data, 'key');
        if (!@$key || !is_numeric($key)) {
            $error[] = 'Order key not numeric';
            return false;
        }
        $user_id = array_get($data, 'user_id');
        if (!$user_id = intval($user_id)) {
            $error[] = 'Require user id';
            return false;
        }
        $project_id = array_get($data, 'project_id');
        if (!$project_id = intval($project_id)) {
            $project_id = config('bmg.project_id');
        }
        return self::insertGetId(
            [
                'user_id'      => $user_id,
                'project_id'   => $project_id,
                'order_key'    => $key,
                'order_source' => $source,
                'create_time'  => time(),
            ],
            'order_id'
        );
    }
}
