<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemUserClient
 * @package Bmg\Core\Models
 *
 * @property integer $client_id
 * @property string $client_value
 * @property integer $create_time
 * @property integer $request_time
 * @property integer $request_count
 *
 * @mixin \Eloquent
 */
class SystemUserClient extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'system_user_client';
    protected $primaryKey = 'client_id';
}
