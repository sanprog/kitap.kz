<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemUserIp
 * @package Bmg\Core\Models
 *
 * @property integer $ip_id
 * @property string $ip_value
 * @property string $ip_json
 * @property string $ip_country
 * @property string $ip_city
 * @property string $ip_geo
 * @property integer $request_count
 * @property integer $create_time
 * @property integer $request_time
 *
 * @mixin \Eloquent
 */
class SystemUserIp extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'system_user_ip';
    protected $primaryKey = 'ip_id';
}
