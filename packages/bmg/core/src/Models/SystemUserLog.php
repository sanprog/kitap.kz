<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemUserLog
 *
 * @package Bmg\Core\Models
 * @property integer $log_id
 * @property integer $user_id
 * @property integer $project_id
 * @property string $log_type
 * @property string $log_data
 * @property integer $log_time
 * @property string $platform
 * @property integer $ip_id
 * @property integer $client_id
 * @property integer $moderator_id
 * @mixin \Eloquent
 */
class SystemUserLog extends Model
{
    public $timestamps = false;
    protected $connection = 'rms';
    protected $table = 'system_user_log';
    protected $primaryKey = 'log_id';
}
