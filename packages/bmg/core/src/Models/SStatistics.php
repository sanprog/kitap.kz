<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SStatistics
 *
 * @property integer $id
 * @property string  $locale
 * @property integer $owner_id
 * @property string  $module
 * @property string  $action
 * @property integer $user_id
 * @property string  $uuid
 *
 * @mixin \Eloquent
 */
class SStatistics extends Model
{
    protected $table = 's_statistics';
    protected $fillable = [
        'locale',
        'owner_id',
        'module',
        'action',
        'user_id',
        'uuid',
    ];

}
