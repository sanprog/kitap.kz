<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PagesLang
 *
 * @property integer $id
 * @property integer $locale
 * @property integer $owner_id
 * @property string $name
 * @property string $body
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @mixin \Eloquent
 */
class PagesLang extends Model
{
    protected $table = 'pages_lang';
    protected static $unguarded = true;
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }

}
