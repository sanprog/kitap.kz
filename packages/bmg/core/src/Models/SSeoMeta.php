<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Waavi\Translation\Traits\Translatable;

/**
 * Class SSeoMeta
 *
 * @property integer $id
 * @property integer $locale
 * @property string  $module
 * @property integer $module_id
 * @property string  $title
 * @property string  $keywords
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @mixin \Eloquent
 */
class SSeoMeta extends Model
{
    protected $table = 's_seo_meta';
    protected $fillable = [
        'locale',
        'module',
        'module_id',
        'title',
        'keywords',
        'description',
        'translate'//Это надо для переводов
    ];
    use Translatable;
    public $translatedAttributes = ['title', 'keywords', 'description'];
    public $translationModel = SSeoMetaLang::class;
    public $translationForeignKey = 'owner_id';
    /**
     * Convert a DateTime to a storable string.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }
    public function seo()
    {
        //TODO надо поектсперементировать и разобрать как точно работает
        return $this->morphTo();
    }
}
