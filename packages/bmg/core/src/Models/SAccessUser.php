<?php

namespace Bmg\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SAccessUser
 * @package App\Models
 *
 * @property integer $user_id
 * @property integer $group_id
 *
 * @mixin \Eloquent
 */
class SAccessUser extends Model
{
    protected $table = 'access_user';
    protected $fillable = [
        'user_id',
        'group_id',
    ];
}
