<?php

namespace Bmg\Core;

use App\Admin\Libraries\Uuid;
use Auth;
use Bmg\Core\Models\SStatistics;
use Bmg\Core\Models\SystemSessionStatus;
use Bmg\Core\Models\SystemUserClient;
use Bmg\Core\Models\SystemUserIp;
use Bmg\Core\Models\SystemUserLog;
use Illuminate\Support\Str;

/**
 * The Tracker facade.
 *
 * @package Bmg\Core
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class Tracker
{
    /**
     * @param integer            $user_id
     * @param string             $log_type
     * @param null|array|integer $data
     *
     * @return bool
     */
    public static function userLog($user_id, $log_type, $data = null)
    {
        $log_time   = time();
        $log_data   = null;
        $project_id = config('bmg.project_id');
        if (is_int($data)) {
            $log_time = $data;
        } elseif (is_array($data)) {
            if (array_get($data, 'log_time')) {
                $log_time = intval(array_get($data, 'log_time'));
            }
            if (array_get($data, 'log_data')) {
                $log_data = array_get($data, 'log_data');
            }
            if (array_get($data, 'project_id')) {
                $project_id = intval(array_get($data, 'project_id'));
            }
        }
        if (!$user_id = intval($user_id)) {
            return false;
        }
        $set = array
        (
            'user_id'    => $user_id,
            'project_id' => $project_id,
            'log_type'   => $log_type,
            'log_time'   => $log_time,
        );
        if ($log_data) {
            $set['log_data'] = $log_data;
        }
        if ($ip_id = self::ipId()) {
            $set['ip_id'] = $ip_id;
        }
        if ($client_id = self::clientId()) {
            $set['client_id'] = $client_id;
        }
        if (Auth::check() && ($user_id != Auth::id())) {
            $set['moderator_id'] = Auth::id();
        }
        $set['platform'] = self::checkDevice();
        SystemUserLog::query()->insert($set);
        return true;
    }
    public static function ipId($ip = null)
    {
        if (!$ip && !($ip = \Request::ip())) {
            return null;
        }
        /** @var SystemUserIp $ip_model */
        $ip_model = SystemUserIp::select('ip_id')->where('ip_value', $ip)->first();
        if ($ip_model) {
            SystemUserIp::where('ip_id', $ip_model->ip_id)->update([
                'request_count' => \DB::raw('request_count +1'),
                'request_time'  => time()
            ]);
            return $ip_model->ip_id;
        }
        return SystemUserIp::insertGetId(
            [
                'ip_value'    => $ip,
                'create_time' => time()
            ],
            'ip_id'
        );
    }
    public static function clientId($client = null)
    {
        if (!$client && (!$client = self::clientGet())) {
            return null;
        }
        /** @var SystemUserClient $client_model */
        $client_model = SystemUserClient::select('client_id')->where('client_value', $client)->first();
        if ($client_model) {
            SystemUserClient::where('client_id', $client_model->client_id)->update([
                'request_count' => \DB::raw('request_count +1'),
                'request_time'  => time()
            ]);
            return $client_model->client_id;
        }
        return SystemUserClient::insertGetId(
            [
                'client_value' => $client,
                'create_time'  => time()
            ],
            'client_id'
        );
    }

    public static function clientGet($length = null)
    {
        if (!$agent = \Request::header('User-Agent')) {
            return null;
        }
        if ($length && is_int($length)) {
            $agent = str_limit($agent, $length, '');
        }
        return $agent;
    }
    public static function checkDevice()
    {
        if (\Agent::is('iOS') || \Agent::is('AndroidOS')) {
            return 'mobile';
        } elseif (preg_match('/okhttp\/\d+(\.\d+)*/', \Request::header('User-Agent'))) {
            return 'android';
        } elseif (preg_match('/iTest\/\d+\s*CFNetwork\/\d+(\.\d+)*\s*Darwin\/\d+(\.\d+)*/',
            \Request::header('User-Agent'))) {
            return 'ios';
        } else {
            return 'desktop';
        }
    }

    protected static $checkSingle = false;

    public static function checkSingleAccess($user_id)
    {
        if (self::$checkSingle || !$user_id || config('bmg.box_version')) {//защита от повторного запуска в рамке одного запроса
            return true;
        }
        self::$checkSingle = true;
        //Из-за того что id сесии может быть изменён мы создаём свой уникальный ключ
        $session_id = \Session::get('active_session', false);
        if (!$session_id) {
            $session_id = Str::random(64);
            \Session::put('active_session', $session_id);
        }
        $session_end_lifetime  = time() - (intval(config('session.lifetime')) * 60);
        $disable_other_session = [];
        $deleted_session       = [];
        $logout                = false;
        $device                = self::checkDevice();
        $max_session           = config('bmg.active_session.' . $device, 1);
        $count_session         = 1;
        /** @var SystemSessionStatus[] | \Illuminate\Database\Eloquent\Collection $sessions */
        $sessions = SystemSessionStatus::where('device', $device)
            ->where('user_id', $user_id)
            ->where('last_activity', '>', $session_end_lifetime)
            ->orderBy('last_activity', 'desc')
            ->get()->keyBy('session_id');
        if (isset($sessions[$session_id])) {//Проверяем текущию сессию
            if ($sessions[$session_id]->status == 0) {//Текущая сессия должна закрыться
                $logout = true;
                $count_session--;
                $deleted_session[] = $session_id;
                unset($sessions[$session_id]);
            }
        }
        foreach ($sessions as $session) {
            if ($session->status == 1) {// Активная сессия
                if ($count_session > $max_session) {//Привышен лимит макс. сессий на устройство
                    $disable_other_session[] = $session->session_id;
                } else {
                    $count_session++;
                }
            }
        }
        if ($disable_other_session) {
            SystemSessionStatus::whereIn('session_id', $disable_other_session)->update(['status' => 0]);
        }
        if ($logout) {
            $redirect = \Redirect::to(lang_route('index'));
            Auth::logout();
            \Request::session()->invalidate();
            add_notice(trans('notice.logout_by_single_access'));
            return $redirect;
        } else {
            SystemSessionStatus::insertOnDuplicateKey(
                [
                    'session_id'     => $session_id,
                    'user_id'        => $user_id,
                    'device'         => $device,
                    'status'         => 1,
                    'last_activity'  => time(),
                    'client_browser' => self::clientGet(),
                    'client_ip'      => \Request::ip()
                ],
                [
                    'device',
                    'status',
                    'last_activity',
                    'client_browser',
                    'client_ip'
                ]
            );
            return true;
        }
    }

    public static function deletedSingleAccess($user_id)
    {
        if ($user_id || !config('bmg.box_version')) {
            $session_id = \Session::get('active_session', false);
            if ($session_id) {
                \Session::forget('active_session');
                SystemSessionStatus::where('session_id', $session_id)->where('user_id', $user_id)->delete();
            }
        }
    }
    /**
     * @param $owner_id int Запись
     * @param $module   string Модуль
     * @param $action   string Действие
     */
    public static function addStatistics($owner_id, $module, $action)
    {
        $uuid = request()->cookie('user_uuid');
        if (!$uuid) {
            $uuid = Uuid::uuid4();
            \Cookie::queue(\Cookie::make('user_uuid', $uuid, 60 * 24 * 365));
        }
        $insert_data = [
            'locale'     => \App::getLocale(),
            'owner_id'   => $owner_id,
            'module'     => $module,
            'action'     => $action,
            'uuid'       => $uuid,
            'ip'         => \Request::ip(),
            'user_agent' => \Request::header('User-Agent'),
            'created_at' => time()
        ];
        if (!\Auth::guest() && \Auth::id()) {
            $insert_data['user_id'] = \Auth::id();
        }
        \DB::table('s_statistics')->insert($insert_data);
    }
    public static function getStatistics($module, $action, $start_time = 0, $unique = false, $return_q = false)
    {
        $q = SStatistics::query()->where([
            'module' => $module,
            'action' => $action
        ])->where('created_at', '>=', $start_time);
        if ($unique) {
            $q->groupBy([
                'owner_id',
            ]);
            $q->select([
                \DB::raw('COUNT( DISTINCT owner_id,`uuid`) AS `count`'),
                'owner_id',
            ]);
        }
        if ($return_q) {
            return $q;
        } else {
            return $q->get()->toArray();
        }
    }
}
