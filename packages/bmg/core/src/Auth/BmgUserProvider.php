<?php

namespace Bmg\Core\Auth;

use Bmg\Core\Models\SystemSchoolUser;
use Bmg\Core\Models\SystemSession;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class BmgUserProvider implements UserProvider
{
    /**
     * The Eloquent user model.
     *
     * @var string
     */
    protected $model;

    /**
     * Create a new database user provider.
     *
     * @param  string $model
     *
     * @return void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        //После первого запроса сохраняем пользователя в кеш
        $user = \Cache::remember('users:' . $identifier, 60, function () use ($identifier) {
            return $this->createModel()->newQuery()->find($identifier);
        });
        return $user;
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $model = $this->createModel();
        /** @var SystemSession $db_session */
        $db_session = SystemSession::query()
            ->where('user_id', $identifier)
            ->where('session_hash', $token)
            ->first();
        $user = null;
        if ($db_session) {
            /** @var User|null $user */
            $user = $model->newQuery()
                ->where($model->getAuthIdentifierName(), $db_session->user_id)
                ->first();
            if($user){
                \Cache::forget('users:' . $user->user_id);
            }
        }
        return $user;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     *
     * @return void
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        /** @var SystemSession $db_session */
        /*
        $db_session = SystemSession::query()->where('user_id', $user->getAuthIdentifier())->first();
        if (!$db_session) {
            $db_session = new SystemSession();
            $db_session->user_id = $user->getAuthIdentifier();
        }
        $db_session->session_hash = $token;
        $db_session->session_start = time();
        $db_session->save();
        */
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }
        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $query = $this->createModel()->newQuery();
        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                if (Str::contains($key, 'login')) {
                    $query->whereRaw('login = :login OR email = :email', ['login' => $value, 'email' => $value]);
                } else {
                    $query->where($key, $value);
                }
            }
        }
        return $query->first();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     *
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        /** @var \App\Models\User $user */
        $result = self::userPasswordCheck($user->password, $user->salt, $credentials['password']);
        if (!$result) {
            if (!$user->school_id) {
                return $result;
            }
            /** @var SystemSchoolUser $user_school */
            $user_school = SystemSchoolUser::where('user_id', $user->user_id)->first();
            if ($user_school && $user_school->password == $credentials['password']) {
                return true;
            }
        }
        return $result;
    }
    public static function userPassword($password, $salt)
    {
        if (!$password || !$salt) {
            return false;
        }
        $password = md5($password);
        return md5
        (
            md5(strlen(config('bmg.password_encrypt_key'))) .
            substr($password, 0, 12) .
            md5(substr($salt, 2, 3)) .
            substr($password, 10, 14) .
            md5(substr($salt, 0, 3)) .
            substr($password, 20, 12) .
            md5(config('bmg.password_encrypt_key'))
        );
    }

    public static function userPasswordCheck($hash, $salt, $password)
    {
        return (self::userPassword($password, $salt) == $hash);
    }
    /**
     * Create a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createModel()
    {
        $class = '\\' . ltrim($this->model, '\\');
        return new $class;
    }

    /**
     * Gets the name of the Eloquent user model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Sets the name of the Eloquent user model.
     *
     * @param  string $model
     *
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
}
