<?php

namespace Bmg\Core;

use Bmg\Core\Models\SSeoMeta;

/**
 * The SEO facade.
 *
 * @package Bmg\Core
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class SEO
{
    /**
     * @param SSeoMeta|string $seo
     * @param null            $default
     */
    public function init($seo, $default = null)
    {
        if (is_string($seo)) {
            $seo = SSeoMeta::where('module_type', $seo)->first();
        }
        if ($seo) {
            \SEOMeta::setTitle($seo->title, false);
            \SEOMeta::setDescription($seo->description);
            \SEOMeta::setKeywords($seo->keywords);
        }
        if ($default) {
            if (is_array($default)) {
                foreach ($default as $key => $value) {
                    if ($key == 'title') {
                        \SEOMeta::setTitle($value, false);
                    } elseif ($key == 'description') {
                        \SEOMeta::setDescription($value);
                    } elseif ($key == 'keywords') {
                        \SEOMeta::setKeywords($value);
                    }
                }
            } elseif (is_string($default)) {
                \SEOMeta::setTitle($default, false);
            }
        }
    }
}
