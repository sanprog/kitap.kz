<?php

namespace Bmg\Core\Libraries;

class Json
{
    /**
     * Краткий UTF8-json без перевода не английских символов в коды :) Без него
     * получается слииишком длинный json
     *
     * @param array $value
     *
     * @return string
     */
    public static function encode($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        array_walk_recursive($value, function (&$item, $key) {
            if (is_string($item)) {
                $item = mb_encode_numericentity($item, [0x80, 0xffff, 0, 0xffff], 'UTF-8');
            }
        });
        return mb_decode_numericentity(json_encode($value), [0x80, 0xffff, 0, 0xffff], 'UTF-8');
    }
}
