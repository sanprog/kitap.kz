<?php

namespace Bmg\Core;

use Bmg\Core\Exception\ErrorBmgException;
use Bmg\Core\Exception\InvalidConfigException;
use Bmg\Core\Models\Subscribe\Product;

/**
 * Class Epay
 *
 * @package Bmg\Core
 * @property integer $order_id
 * @property integer $amount
 * @property Product $product
 * @property string  $destination
 */
class Epay
{
    private $order_id;
    private $amount;
    private $product;
    private $donate = false;
    public  $destination;
    public function __construct($order_id = null)
    {
        if (!$order_id) {
            $order_id = rand(10000, 99999) . time();
        }
        $this->setOrderId($order_id);
        $this->product = new Product();
    }
    public function setOrderId($value)
    {
        if ($value = trim(preg_replace('#([^0-9]+)#', '', $value))) {
            $this->order_id = $value;
        } else {
            throw new ErrorBmgException('epay', 'no_valid_order_id');
        }
    }
    public function productById(int $value)
    {
        if (!$this->product = Product::activeProduct($value)) {
            throw new ErrorBmgException('epay', trans('payment.undefined_product_id'));
        }
        $this->amount = $this->product->real_price();
    }
    public function setAmount(int $value)
    {
        $this->amount = max(100, min($value, 25000));
    }
    public function donate($boolean)
    {
        $this->donate = $boolean;
        return $this;
    }
    public function makeRequest($terminal_id = null)
    {
        if (!$this->amount && !$this->product) {
            throw new ErrorBmgException('epay', trans('payment.not_product_and_amount'));
        }
        $lang = $this->aliasLocale(\App::getLocale());
        \DB::connection('rms')->table('payment_epay_order')->insert([
            'order_id'       => $this->order_id,
            'product_id'     => intval($this->product->product_id),
            'is_donate'      => intval($this->donate),
            'user_id'        => \Auth::id(),
            'status'         => 1,
            'amount'         => $this->amount,
            'lang'           => $lang,
            'destination'    => $this->destination,
            'merchant_id'    => $this->config('merchant_id'),
            'client_ip'      => \Request::ip(),
            'client_browser' => \Request::header('User-Agent'),
            'create_time'    => time()
        ]);
        if ($this->product->exists) {
            $product_name = trans('subscribe.subscribe_product_' . strtolower($this->product->product_type) . '_fullname');
        } else {
            if ($this->donate) {
                $product_name = trans('payment.donate');
            } else {
                $product_name = trans('subscribe.subscribe_product_balance_fullname');
            }
        }
        $xml            = '<merchant cert_id="' . $this->config('merchant_certificate_id') . '" name="' . $this->config('merchant_name') . '"><order order_id="' . $this->order_id . '" amount="' . $this->amount . '" currency="398"><department merchant_id="' . $this->config('merchant_id') . '" amount="' . $this->amount . '" abonent_id="" ' . ($terminal_id ? 'terminal="' . $terminal_id . '"' : '') . '/></order></merchant>';
        $xml            = '<document>' . $xml . '<merchant_sign type="RSA">' . $this->getSignature($xml) . '</merchant_sign></document>';
        $appendix       = '
			<document>
				<item number="1" name="' . $product_name . '" quantity="1" amount="' . $this->amount . '"/>
			</document>';
        $return         = [
            'order_id'          => $this->order_id,
            'order'             => base64_encode($xml),
            'appendix'          => base64_encode($appendix),
            'lang'              => $lang,
            'process_link'      => $this->config('process_link'),
            'back_link'         => lang_route('payment.status', ['success' => true, 'order_id' => $this->order_id], true),
            'failure_back_link' => lang_route('payment.status', ['error' => true, 'order_id' => $this->order_id], true),
            'post_link'         => lang_route('payment.receive', [], true),
            'failure_post_link' => lang_route('payment.receive', [], true),
        ];
        $payment_orders = [];
        if (session()->has('payment')) {
            $payment_orders = session('payment', []);
        }
        $payment_orders[$this->order_id] = 1;
        session()->put('payment', $payment_orders);
        return $return;
    }
    private function parseXML($xml)
    {
        if (!$xml) {
            return false;
        }
        $xml   = simplexml_load_string($xml);
        $array = array();
        $this->parseXMLFn($xml, $array);
        return $array;
    }

    private function parseXMLFn($xml, &$array)
    {
        foreach ($xml as $key => $node) {
            if ($value = trim($node->__toString())) {
                $array[$key]['value'] = $value;
            }
            foreach ($node->attributes() as $k => $v) {
                $array[$key][$k] = $v->__toString();
            }
            if (!$node->count()) {
                continue;
            }
            $this->parseXMLFn($node->children(), $array[$key]);
        }
    }
    public function processResponse($xml)
    {
        try {
            //TODO не корректно обрабоатывает приходящий ответ ошибки https://testpay.kkb.kz/doc/htm/failurePostLinkXML_description.html
            $data     = $this->parseXML($xml);
            $customer = array_get($data, 'bank.customer');
            $info     = array_get($data, 'bank.results.payment');
            $order    = array_get($customer, 'merchant.order');
            $this->setOrderId(array_get($order, 'order_id'));
            if (!$record = $this->orderById($this->order_id)) {
                throw new ErrorBmgException('epay', 'order_not_found');
            }
            if (array_get($data, 'error')) {
                $error_message = preg_replace('#([\s]+)#', '_', $data['error']['value']);
                $error_message = strToLower($error_message);
                $error_message = $data['error']['type'] . '_' . $error_message;
                \DB::connection('rms')
                    ->table('payment_epay_order')
                    ->where('order_id', '=', $record->order_id)
                    ->update([
                        'status'               => 0,
                        'transaction_response' => array_get($data, 'error.code'),
                        'transaction_error'    => $error_message,
                    ]);
                throw new ErrorBmgException('epay', $error_message);
            }
            if (!$signature = array_get($data, 'bank_sign.value')) {
                throw new ErrorBmgException('epay', 'no_signature');
            }
            $clear_xml = $this->splitSignature($xml);
            if (!$check = $this->checkSignature($clear_xml, $signature)) {
                throw new ErrorBmgException('epay', 'no_check_signature');
            }
            $set =
                [
                    'status'               => 2,
                    'amount'               => array_get($order, 'amount'),
                    'currency'             => array_get($order, 'currency'),
                    'merchant_id'          => array_get($order, 'department.merchant_id'),
                    'return_time'          => time(),
                    'pay_time'             => strtotime(array_get($data, 'bank.results.timestamp')),
                    'transaction_id'       => array_get($info, 'reference'),
                    'transaction_approve'  => array_get($info, 'approval_code'),
                    'transaction_response' => array_get($info, 'response_code'),
                    'transaction_error'    => '',
                    'purchaser_card'       => $this->cardFormat(array_get($info, 'card')),
                    'purchaser_country'    => array_get($info, 'card_bin'),
                    'purchaser_name'       => array_get($customer, 'name'),
                    'purchaser_email'      => array_get($customer, 'mail'),
                    'purchaser_phone'      => array_get($customer, 'phone'),
                ];
            if ($record->status > 1) {
                throw new ErrorBmgException('epay', 'order_already_completed');
            }
            if ($record->amount != $set['amount']) {
                $set['status']            = 0;
                $set['transaction_error'] = 'different_order_amount: ' . $set['amount'] . '-' . $set['currency'];
                \DB::connection('rms')
                    ->table('payment_epay_order')
                    ->where('order_id', '=', $record->order_id)
                    ->update($set);
                throw new ErrorBmgException('epay', 'different_order_amount: ' . $set['amount'] . '-' . $set['currency']);
            }
            \DB::connection('rms')
                ->table('payment_epay_order')
                ->where('order_id', '=', $record->order_id)
                ->update($set);
            exec($command = 'php ' . base_path('artisan') . ' bmg-core:epay-send order_complete ' . $this->order_id . ' > /dev/null &');
            $this->log($command);
        } catch (\Exception $e) {
            $this->writeError($e->getMessage());
            \Log::critical('Epay: ' . $e->getFile() . ':' . $e->getLine());
            exit(0);
        }
        return $record;
    }
    public function complete()
    {
        if (!$record = $this->orderById($this->order_id)) {
            throw new ErrorBmgException('epay', 'order_not_found');
        }
        if ($record->status == 3) {
            return true;
        }
        $xml = '<merchant id="' . $record->merchant_id . '"><command type="complete" /><payment reference="' . $record->transaction_id . '" approval_code="' . $record->transaction_approve . '" orderid="' . $record->order_id . '" amount="' . $record->amount . '" currency_code="398" /></merchant>';
        $xml = $this->trimXML($xml);
        //<command type="reverse"/>
        //<reason>Only for reverse</reason>
        $xml  = '<document>' . $xml . '<merchant_sign type="RSA">' . $this->getSignature($xml) . '</merchant_sign></document>';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->config('complete_link') . '?' . urlencode($xml));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSLVERSION, 6);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        $response = curl_exec($curl);
        $data     = trim(str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $response));
        curl_close($curl);
        $set = ['complete_time' => time()];
        if ($data = $this->parseXML($data)) {
            $data = $data['bank']['response'];
            if ($data['code'] == '00') {
                $set['status']       = 3;
                $set['complete_msg'] = $data['message'];
            } else {
                $set['complete_msg'] = $data['code'] . ': ' . $data['message'];
            }
        } else {
            $this->log($response);
            throw new ErrorBmgException('epay', 'invalid_xml');
        }
        \DB::connection('rms')
            ->table('payment_epay_order')
            ->where('order_id', '=', $record->order_id)
            ->update($set);
        return true;
    }
    public function log($data)
    {
        if ($this->config('log', true)) {
            $log_dir = storage_path('logs/epay/');
            if (!is_dir($log_dir)) {
                mkdir($log_dir, 0777, true);
            }
            if (is_array($data)) {
                $data = implode("\n", $data);
            }
            file_put_contents($log_dir . '.' . date('Y.m.d-G.i') . '_(' . rand(100, 999) . ').log', $data);
        }
    }
    public function testResponse()
    {
        $xml = '<bank name="Kazkommertsbank JSC"><customer name="Ucaf Test Maest" mail="viktor.v@bilimmail.kz" phone=""><merchant cert_id="' . $this->config('merchant_certificate_id') . '" name="' . $this->config('merchant_name') . '"><order order_id="181331511497394" amount="2000" currency="398"><department merchant_id="' . $this->config('merchant_id') . '" amount="2000"/></order></merchant><merchant_sign type="RSA"/></customer><customer_sign type="RSA"/><results timestamp="2006-07-06 17:21:50"><payment merchant_id="' . $this->config('merchant_id') . '" amount="2000" reference="618704198173" approval_code="447753" response_code="00"/></results></bank>';
        $xml = '<document>' . $xml . '<bank_sign cert_id="' . $this->config('merchant_certificate_id') . '" type="SHA/RSA">' . $this->getSignature($xml) . '</bank_sign></document>';
        return $xml;
    }
    private function writeError($error)
    {
        if (!$this->order_id) {
            return false;
        }
        if (is_array($error)) {
            $error = implode("\n", $error);
        }
        \DB::connection('rms')
            ->table('payment_epay_order')
            ->where('order_id', '=', $this->order_id)
            ->update([
                'transaction_error' => $error,
            ]);
    }
    /**
     * Подготовка XML к отправке в ККБ
     *
     * @param $xml string
     *
     * @return string
     */
    private static function trimXML($xml)
    {
        /*
         * Удаляем первую строку сгенерированного файла
         * (<?xml version="1.0" encoding="UTF-8"?>)
         */
        //TODO немного не корректно удалять первую строку, необходимо именно удалять <?xml, надо переписать и проверить
//        $xml = preg_replace('/^.+\n/', '', $xml);
        /*
         * Удаляем пробелы, табы, символ обрыва строки
         */
        return preg_replace('/^\s+|\n|\r|\s+$/m', '', $xml);
    }
    /**
     * @param $order_id
     *
     * @return bool|\stdClass
     */
    private function orderById($order_id)
    {
        return \DB::connection('rms')->table('payment_epay_order')
            ->where('order_id', '=', $order_id)
            ->first();
    }
    private function cardFormat($string)
    {
        if (!$string) {
            return '';
        }
        $string = str_replace('-', '', $string);
        $string = implode('-', str_split($string, 4));
        return $string;
    }
    private function loadPrivateKey()
    {
        if (!file_exists($private_key = $this->config('private_key'))) {
            throw new InvalidConfigException("Epay Private certificate file 'private_key' does not exists!");
        }
        $private_key = file_get_contents($private_key);
        if (!$private_key = openssl_get_privatekey($private_key, $this->config('private_key_pass'))) {
            $error = openssl_error_string();
            if ($error) {
                throw new InvalidConfigException('Epay Private certificate: ' . $error);
            }
        }
        if (is_resource($private_key)) {
            return $private_key;
        }
        throw new InvalidConfigException('Epay Can`t create signature from private key');
    }
    private function getSignature($data)
    {
        if (!$private_key = $this->loadPrivateKey()) {
            return false;
        }
        $signature = '';
        openssl_sign($data, $signature, $private_key);
        $signature = base64_encode(strrev($signature));
        openssl_free_key($private_key);
        return $signature;
    }
    private function splitSignature($xml)
    {
        $array    = array();
        $letterst = stristr($xml, '<BANK');
        $signst   = stristr($xml, '<BANK_SIGN');
        $signed   = stristr($xml, '</BANK_SIGN');
        $doced    = stristr($signed, '>');
        return subStr($letterst, 0, -strLen($signst));
    }
    /**
     * @param $xml
     * @param $signature
     *
     * @return bool
     * @throws ErrorBmgException
     */
    private function checkSignature($xml, $signature)
    {
        if (!$signature = strrev(base64_decode($signature))) {
            throw new ErrorBmgException('epay', 'invalid_signature');
        }
        if (config('bmg.box_version')) {
            return true;
        }
        if (!file_exists($public_key = $this->config('bank_certificate'))) {
            throw new ErrorBmgException('epay', 'public_key_file_not_exists');
        }
        $public_key = file_get_contents($public_key);
        if (!$public_key = openssl_get_publicKey($public_key)) {
            throw new ErrorBmgException('epay', openssl_error_string());
        }
        $verify = @openssl_verify($xml, $signature, $public_key);
        openssl_free_key($public_key);
        switch ($verify) {
            case 1:
                return true;
                break;
            case 0:
                throw new ErrorBmgException('epay', 'incorrect_signature');
                break;
            case -1:
                throw new ErrorBmgException('epay', 'verify_signature_error: ' . openssl_error_string());
                break;
            default:
                throw new ErrorBmgException('epay', 'undefined_error');
                break;
        }
    }
    private function config($name, $global = false)
    {
        if ($global) {
            return config('payment.epay.' . $name);
        } else {
            return config('payment.epay.' . config('payment.epay.status') . '.' . $name);
        }
    }
    private function aliasLocale($locale)
    {
        return config('payment.epay.alias_locale.' . $locale);
    }
}
