<?php

namespace Bmg\Core\Middleware;

use \Illuminate\Contracts\Auth\Factory as Auth;

/**
 * Class SingleAccess
 * @package Bmg\Core\Middleware
 * Данный Middleware должен быть привязан именно к web так как только там есть авторизация иначе
 * $this->auth->guest() всегда будет true
 */
class SingleAccess
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
//        if (!$this->auth->guest() && ($result = \Tracker::checkSingleAccess($this->auth->user()->getAuthIdentifier()))) {
//            if ($result instanceof \Illuminate\Http\RedirectResponse) {
//                return $result;
//            }
//        }
        return $next($request);
    }
}
