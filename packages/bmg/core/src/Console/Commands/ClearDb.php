<?php

namespace Bmg\Core\Console\Commands;

use Bmg\Core\Models\SSendEmails;
use Bmg\Core\Models\SystemSessionStatus;
use Illuminate\Console\Command;

class ClearDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bmg-core:clear-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистка базы от лишних записей';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Удаление просроченных сессий
        $session_end_lifetime = time() - (intval(config('session.lifetime')) * 60);
        SystemSessionStatus::where('last_activity', '<', $session_end_lifetime)
            ->delete();
        //Удаление писем старше 1 месяцев
        SSendEmails::where('updated_at', '<', time()-(2629743))->where('status','=',1)->delete();
    }
}
