<?php
namespace Bmg\Core\Console\Commands;

use Illuminate\Console\Command;

class SendMailConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bmg-core:send-mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Рассылка писем';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \SendMail::sendMessage();
    }
}
