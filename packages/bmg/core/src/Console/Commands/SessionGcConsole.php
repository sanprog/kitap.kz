<?php

namespace Bmg\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class SessionGcConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bmg-core:session:gc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистка старых сессий';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $lifetime = Arr::get(\Session::getSessionConfig(), 'lifetime') * 60;
        \Session::getHandler()->gc($lifetime);
    }
}
