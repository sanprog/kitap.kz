<?php

namespace Bmg\Core\Console\Commands;

use Illuminate\Console\Command;

class EpayConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bmg-core:epay-send {action} {order_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка запроса в rms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'order_complete': {
                if (config('payment.epay.status') == 'dev') {
                    exit();
                }
                $host = 'rms.100kitap.kz';
                if (config('app.debug')) {
                    $host = 'rms.wikibilim.local';
                }
                $order_id = (int)$this->argument('order_id');
                $curl = curl_init();
                curl_setOpt($curl, CURLOPT_URL, 'http://' . $host . '/process/payment/epay_complete');
                curl_setOpt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setOpt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, 'order_id=' . $order_id);
                $response = curl_exec($curl);
                curl_close($curl);
                exit($response);
            }
        }
    }
}
