<?php

namespace Bmg\Core;

use Bmg\Core\Auth\BmgUserProvider;
use Bmg\Core\Console\Commands\ClearDb;
use Bmg\Core\Console\Commands\EpayConsole;
use Bmg\Core\Console\Commands\SendMailConsole;
use Bmg\Core\Console\Commands\SessionGcConsole;
use Illuminate\Support\ServiceProvider;

/**
 * CoreServiceProvider
 *
 * @package Bmg\Core
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $router->pushMiddlewareToGroup('web', Middleware\SingleAccess::class);
        $packageConfigPath = __DIR__ . '/../config/bmg.php';
        $packageMigrationsPath = __DIR__ . '/../database/migrations';
        /**
         * Loading and publishing package's config
         */
        $config = config_path('bmg.php');
        if (file_exists($config)) {
            $this->mergeConfigFrom($packageConfigPath, 'core');
        }
        $this->publishes([
            $packageConfigPath => $config,
        ], 'config');
        if ($this->app->runningInConsole()) {
            $this->commands([
                ClearDb::class,
                EpayConsole::class,
                SendMailConsole::class,
                SessionGcConsole::class,
            ]);
        }
        /**
         * Loading and publishing package's migrations
         */
        $this->loadMigrationsFrom($packageMigrationsPath);
        $this->publishes([
            $packageMigrationsPath => database_path('/migrations')
        ], 'migrations');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations/');
        $this->app->register(\Bmg\Core\Providers\EventServiceProvider::class);
        $this->app->register(\Jenssegers\Agent\AgentServiceProvider::class);
        $this->app->register(\Bmg\Core\Providers\DateServiceProvider::class);
        \Auth::provider('bmg_auth', function ($app, array $config) {
            return new BmgUserProvider($config['model']);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('core', function ($app) {
            return new Core();
        });
        $this->app->singleton('tracker', function ($app) {
            return new Tracker();
        });
        $this->app->singleton('subscribe', function ($app) {
            return new Subscribe();
        });
        $this->app->singleton('sendmail', function ($app) {
            return new SendMail();
        });
        $this->app->singleton('epay', function ($app) {
            return new Epay();
        });
        $this->app->singleton('seo', function ($app) {
            return new SEO();
        });
        $this->app->booting(function () {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Epay', \Bmg\Core\Facades\Epay::class);
            $loader->alias('Tracker', \Bmg\Core\Facades\Tracker::class);
            $loader->alias('Subscribe', \Bmg\Core\Facades\Subscribe::class);
            $loader->alias('SendMail', \Bmg\Core\Facades\SendMail::class);
            $loader->alias('Agent', \Jenssegers\Agent\Facades\Agent::class);
            $loader->alias('Date', \Bmg\Core\Date::class);
            $loader->alias('SEO', \Bmg\Core\Facades\SEO::class);
        });
        foreach (glob(__DIR__ . '/Helpers/*.php') as $filename) {
            require_once($filename);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['core', 'tracker', 'subscribe', 'sendmail', 'epay', 'seo'];
    }

}
