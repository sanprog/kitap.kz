<?php

namespace Bmg\Core;

use Bmg\Core\Models\Subscribe\SubscribeUser;

/**
 * The Subscribe facade.
 *
 * @package Bmg\Core
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class Subscribe
{
    /**
     * Check availability subscribe
     *
     * @return bool
     */
    public function checkServiceAccess()
    {
        if (config('bmg.box_version', false)) {
            return true;
        } elseif (!\Auth::guest()) {
            /** @var \App\Models\User $user */
            $user = \Auth::user();
            if ($user->isAdmin()) {
                return true;
            }
            if (!isset($user->subscribe)) {
                /** @var SubscribeUser $subscribe */
                $user->subscribe = SubscribeUser::activeSubscribe();
            }
            if ($user->subscribe && $user->subscribe->expire_time && time() < $user->subscribe->expire_time) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Check availability product paid
	 * @param integer $product_id
	 *
	 * @return bool
	 */
	public function checkProductAccess($product_id)
	{
		if (config('bmg.box_version', false)) {
			return true;
		} elseif (!\Auth::guest()) {
			/** @var \App\Models\User $user */
			$user = \Auth::user();
			if ($user->isAdmin()) {
				return true;
			}
			return SubscribeUser::productPaid($product_id, $user->user_id);
		}
		return false;
	}
}
