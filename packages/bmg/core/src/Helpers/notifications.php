<?php
function add_notice($message, $type = 'message')
{
    $notifications = [];
    if (session()->has('notifications')) {
        $notifications = session('notifications');
        session()->forget('notifications');
    }
    if (is_string($message)) {
        $notifications[$type][] = $message;
    } else {
        $notifications[$type] = array_merge(array_get($notifications,$type,[]), $message);
    }
    session()->flash('notifications', $notifications);
}

function get_notice($type = 'message')
{
    return array_get(session('notifications', []), $type);
}
