<?php

namespace Bmg\Core\Listeners;

use Illuminate\Auth\Events\Authenticated;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;

class UserEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * User login the event.
     *
     * @param Login $event
     * @return void
     */
    public function onUserLogin(Login $event)
    {
        \Cache::forget('users:' . $event->user->getAuthIdentifier());
        \Tracker::userLog($event->user->getAuthIdentifier(), 'auth');
    }

    /**
     * User authenticated the event.
     *
     * @param  Authenticated $event
     * @return void
     */
    public function onUserAuthenticated(Authenticated $event)
    {
        if ($event->user) {
            if ($event->user->info_status == 0) {
                config(['bmg.user.menus.info.title' => trans('cabinet.menu_register')]);
            }
            if ($event->user->isAdmin()) {
                config([
                    'bmg.user.menus.admin' => [
                        'title' => 'Панель управления',
                        'route' => 'admin.login.form',
                        'icon' => 'anketa'
                    ]
                ]);
            }
        }
    }
    /**
     * User authenticated the event.
     *
     * @param  Logout $event
     * @return void
     */
    public function onUserLogout(Logout $event)
    {
        if ($event->user) {
         //   \Tracker::deletedSingleAccess($event->user->getAuthIdentifier());
        }
    }
}
