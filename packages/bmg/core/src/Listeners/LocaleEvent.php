<?php

namespace Bmg\Core\Listeners;

use Illuminate\Foundation\Events\LocaleUpdated;
use Waavi\Translation\Repositories\LanguageRepository;

class LocaleEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * Change locale the event.
     *
     * @param LocaleUpdated $event
     * @return void
     */
    public function onChangeLocale(LocaleUpdated $event)
    {
        $currentLanguage = $this->languageRepository->findByLocale($event->locale);
        if ($currentLanguage) {
            app('config')->set('bmg.lang_id', $currentLanguage->id);
        }
    }
}
