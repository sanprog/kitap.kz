<?php

namespace Bmg\Core;

/**
 * The Core facade.
 *
 * @package Bmg\Core
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class Core
{
    public function welcome()
    {
        return 'Welcome to Bmg\Core package';
    }
}
