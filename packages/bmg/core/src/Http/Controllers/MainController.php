<?php

namespace Bmg\Core\Http\Controllers;

use App\Http\Controllers\Controller;
use Bmg\Core\Facades\Core;

/**
 *
 * @package Bmg\Core\Http\Controllers
 * @author  Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class MainController extends Controller
{

    public function index()
    {
        return Core::welcome();
    }

}
