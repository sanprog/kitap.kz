<?php

namespace Bmg\Core\Http\Middleware;

use Bmg\Core\Models\SSeoRedirects;
use Closure;
use Illuminate\Http\Request;

/**
 * RedirectsPages
 *
 * @package Bmg\Core\Http\Middleware
 * @author Viktor Vassilyev <viktor.v@bilimmail.kz>
 */
class RedirectsPages
{
    public function handle(Request $request, Closure $next)
    {
        //TODO вариант с вырезкой языка из ссылки, в данное время такой вариант делает вечный редирект
//        $uri = trim(\UriLocalizer::cleanUrl($request->getUri()), '/');
        //простой вариант без вырезания языка
        $uri = trim($request->getRequestUri(), '/');
        //TODO по хорошему тут лучше использовать кеш вместо прямого запроса в базе
        /** @var SSeoRedirects $redirect */
        if ($uri && ($redirect = SSeoRedirects::where('old_url', $uri)->first())) {
            return redirect()->to('/' . trim($redirect->new_url, '/'), $redirect->type);
        }
        return $next($request);
    }
}
