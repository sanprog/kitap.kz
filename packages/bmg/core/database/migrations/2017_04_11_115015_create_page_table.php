<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('pages', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('locale', 10)->comment('Основной язык');
//            $table->string('name', 255)->comment('Название');
//            $table->text('body')->comment('Текст');
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//            $table->index(['locale']);
//        });
//        Schema::create('pages_lang', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('locale', 10)->comment('Язык записи');
//            $table->integer('owner_id')->unsigned()->comment('Основная запись');
//            $table->string('name', 255)->comment('Название')->nullable();
//            $table->text('body')->comment('Текст')->nullable();
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//            $table->unique(['locale', 'owner_id']);
//            $table->foreign('owner_id')
//                ->references('id')->on('pages')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_lang');
        Schema::dropIfExists('pages');
    }
}
