<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('pages', function (Blueprint $table) {
//            $table->string('slug', 255)->comment('ЧПУ')->after('body');
//            $table->string('module', 255)->comment('Модуль')->after('slug');
//            $table->integer('sort')->comment('Порядок')->after('module');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn([
                'slug',
                'module',
                'sort'
            ]);
        });
    }
}
