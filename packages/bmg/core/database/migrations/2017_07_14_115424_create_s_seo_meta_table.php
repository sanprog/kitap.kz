<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSSeoMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('s_seo_meta', function (Blueprint $table) {
//            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';
//            $table->increments('id');
//            $table->string('locale', 10)->comment('Основной язык');
//            $table->string('module_type', 255)->comment('Модуль');
//            $table->integer('module_id')->unsigned()->nullable()->comment('Запись модуля');
//            $table->string('title', 500)->nullable();
//            $table->string('keywords', 500)->nullable();
//            $table->string('description', 500)->nullable();
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//            $table->index(['locale']);
//            $table->index(['module_id', 'module_type']);
//        });
//        Schema::create('s_seo_meta_lang', function (Blueprint $table) {
//            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';
//            $table->increments('id');
//            $table->string('locale', 10)->comment('Язык записи');
//            $table->integer('owner_id')->unsigned()->comment('Основная запись');
//            $table->string('title', 500)->nullable();
//            $table->string('keywords', 500)->nullable();
//            $table->string('description', 500)->nullable();
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//            $table->unique(['locale', 'owner_id']);
//            $table->foreign('owner_id')
//                ->references('id')->on('s_seo_meta')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_seo_meta_lang');
        Schema::dropIfExists('s_seo_meta');
    }
}
