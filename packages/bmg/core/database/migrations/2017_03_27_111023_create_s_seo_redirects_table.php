<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSSeoRedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('s_seo_redirects', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('old_url', 255)->unique();
//            $table->string('new_url', 255);
//            $table->string('type', 3)->default('301');
//            $table->boolean('isRegex')->default(0);
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('s_seo_redirects');
    }
}
