<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('s_statistics', function (Blueprint $table) {
//            $table->bigIncrements('id');
//            $table->string('locale', 10)->comment('Язык')->nullable();
//            $table->integer('owner_id')->comment('Запись в модуле');
//            $table->string('module')->comment('Ключ модуля');
//            $table->string('action')->comment('Действие');
//            $table->integer('user_id', false, true)->nullable();
//            $table->string('ip')->nullable()->comment('IP Address');
//            $table->string('user_agent')->nullable()->comment('User-Agent');
//            $table->char('uuid', 36)->comment('Уникальный ключ (ставиться в куки)');
//            $table->integer('created_at');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_statistics');
    }
}
