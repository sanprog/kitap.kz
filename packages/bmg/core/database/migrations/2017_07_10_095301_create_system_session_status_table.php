<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSessionStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_session_status', function (Blueprint $table) {
            $table->string('session_id', 64);
            $table->integer('user_id');
            $table->boolean('status');
            $table->string('client_ip', 64);
            $table->string('client_browser', 255);
            $table->integer('last_activity');
            $table->enum('device', [
                'browser',
                'ios',
                'android',
                'mobile',
                'desktop',
            ]);
            $table->primary([
                'session_id',
                'user_id'
            ]);
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_session_status');
    }
}
