<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSSendEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('s_send_emails', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('from_name', 255)->comment('Имя кого отправлять');
//            $table->string('from_email', 255)->comment('Почта кого отправлять');
//            $table->text('to_emails')->comment('Кому отправлять JSON');
//            $table->string('subject', 500)->comment('Заголовок сообщения');
//            $table->text('body')->comment('Сообщение');
//            $table->tinyInteger('status')->default(0)->comment('Статус');
//            $table->text('error')->comment('Ошибка')->nullable();
//            $table->char('locale', 2)->default('ru')->comment('Язык');
//            $table->integer('created_at')->nullable();
//            $table->integer('updated_at')->nullable();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_send_emails');
    }
}
