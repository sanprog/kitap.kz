<?php
function lang_route($name, $parameters = [], $absolute = false)
{
    $url = \UriLocalizer::localize(route($name, $parameters, $absolute), App::getLocale());
    if ($absolute) {
        return request()->root() . $url;
    } else {
        return $url;
    }
}

function url_add_lang($url, $locale = null)
{
    if ($locale == null) {
        $locale = App::getLocale();
    }
    return \UriLocalizer::localize($url, $locale);
}
