<?php

namespace Waavi\Translation\Traits;

use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Str;

trait Translatable
{
    protected $defaultLocale;
    public function mutatorRules($rules)
    {
        foreach ($this->translatedAttributes as $value) {
            if ($rules_attr = array_get($rules, $value)) {
                $rules_attr = explode('|', $rules_attr);
                foreach ($rules_attr as $key => $rule) {
                    if (strpos($rule, 'required') !== false) {
                        unset($rules_attr[$key]);
                    }
                }
                if ($rules_attr) {
                    $rules_attr[]                   = 'nullable';
                    $rules['translate.*.' . $value] = trim(implode('|', $rules_attr), '|');
                }
            }
        }
        return $rules;
    }
    /**
     * Alias for getTranslation().
     *
     * @param string|null $locale
     * @param bool        $withFallback
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function translate($locale = null, $withFallback = false)
    {
        return $this->getTranslation($locale, $withFallback);
    }

    /**
     * Alias for getTranslation().
     *
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function translateOrDefault($locale)
    {
        return $this->getTranslation($locale, true);
    }

    /**
     * Alias for getTranslationOrNew().
     *
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function translateOrNew($locale)
    {
        return $this->getTranslationOrNew($locale);
    }

    /**
     * @param string|null $locale
     * @param bool        $withFallback
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getTranslation($locale = null, $withFallback = null)
    {
        $configFallbackLocale = $this->getFallbackLocale();
        $locale               = $locale ?: $this->currentLocale();
        $withFallback         = $withFallback === null ? $this->useFallback() : $withFallback;
        $fallbackLocale       = $this->getFallbackLocale($locale);
        if ($translation = $this->getTranslationByLocaleKey($locale)) {
            return $translation;
        }
        if ($withFallback && $fallbackLocale) {
            if ($translation = $this->getTranslationByLocaleKey($fallbackLocale)) {
                return $translation;
            }
            if ($translation = $this->getTranslationByLocaleKey($configFallbackLocale)) {
                return $translation;
            }
        }
        return null;
    }

    /**
     * @param string|null $locale
     *
     * @return bool
     */
    public function hasTranslation($locale = null)
    {
        $locale = $locale ?: $this->currentLocale();
        foreach ($this->translations as $translation) {
            if ($translation->getAttribute($this->getLocaleKey()) == $locale) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getTranslationModelName()
    {
        return $this->translationModel ?: $this->getTranslationModelNameDefault();
    }

    /**
     * @return string
     */
    public function getTranslationModelNameDefault()
    {
        $config = app()->make('config');
        return get_class($this) . $config->get('translator.translation_suffix', 'Lang');
    }

    /**
     * @return string
     */
    public function getRelationKey()
    {
        if ($this->translationForeignKey) {
            $key = $this->translationForeignKey;
        } elseif ($this->primaryKey !== 'id') {
            $key = $this->primaryKey;
        } else {
            $key = $this->getForeignKey();
        }
        return $key;
    }

    /**
     * @return string
     */
    public function getLocaleKey()
    {
        $config = app()->make('config');
        return $this->localeKey ?: $config->get('translator.locale_key', 'locale');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany($this->getTranslationModelName(), $this->getRelationKey());
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        list($attribute, $locale) = $this->getAttributeAndLocale($key);
        if ($this->hasGetMutator($attribute)) {
            $value = $this->getAttributeFromArray($attribute);
            return $this->{'get' . Str::studly($attribute) . 'Attribute'}($value, $locale);
        }
        if ($this->isTranslationAttribute($attribute)) {
            if (array_get($this->getAttributes(), 'locale') == $locale) {
                return parent::getAttribute($attribute);
            }
            if (($translation = $this->getTranslation($locale)) === null) {
                if (strpos($key, ':') !== false) {
                    return null;
                } else {
                    return parent::getAttribute($attribute);
                }
            } else {
                if ($translation->{$attribute} != null) {
                    // If the given $attribute has a mutator, we push it to $attributes and then call getAttributeValue
                    // on it. This way, we can use Eloquent's checking for Mutation, type casting, and
                    // Date fields.
                    if ($this->hasGetMutator($attribute)) {
                        $this->attributes[$attribute] = $translation->{$attribute};
                        return $this->getAttributeValue($attribute);
                    }
                    return $translation->{$attribute};
                }
            }
        }
        return parent::getAttribute($key);
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        list($attribute, $locale) = $this->getAttributeAndLocale($key);
        if ($this->hasGetMutator($attribute)) {
            return $this->{'set' . Str::studly($attribute) . 'Attribute'}($value, $locale);
        }
        if ($this->isTranslationAttribute($attribute)) {
            if ($locale == $this->locale) {
                parent::setAttribute($attribute, $value);
            }
            $this->getTranslationOrNew($locale)->$attribute = $value;
        } else {
            return parent::setAttribute($key, $value);
        }
        return $this;
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        $saved = parent::save($options);
        if ($saved) {
            $saved = $this->saveTranslations();
        }
        return $saved;
    }

    /**
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function getTranslationOrNew($locale)
    {
        if (($translation = $this->getTranslation($locale, false)) === null) {
            $translation = $this->getNewTranslation($locale);
        }
        return $translation;
    }

    /**
     * @param array $attributes
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @return $this
     */
    public function fill(array $attributes)
    {
        $translate = array_get($attributes, 'translate', false);
        if ($translate !== false) {
            $main_locale = array_get($attributes, 'locale', $this->locale);
            if ($this->isKeyALocale($main_locale)) {
                $this->setDefaultLocale($main_locale);
                $this->locale = $main_locale;
            }
            foreach ($translate as $key => $values) {
                if ($this->isKeyALocale($key)) {
                    if ($main_locale != $key) {
//                        $this->getTranslationOrNew($key)->fill($values);
                        $totallyGuarded = $this->totallyGuarded();
                        foreach ($this->fillableFromArray($values) as $attribute_fill => $value) {
                            $attribute_fill = $this->removeTableFromKey($attribute_fill);
                            if ($this->isFillable($attribute_fill)) {
                                $this->setAttribute($attribute_fill . ':' . $key, $value);
                            } elseif ($totallyGuarded) {
                                throw new MassAssignmentException($attribute_fill);
                            }
                        }
                    } else {
                        $attributes = array_merge($attributes, $values);
                    }
                }
            }
            unset($attributes['translate']);
        }
        return parent::fill($attributes);
    }
    private $tmp_translations;
    /**
     * @param string $key
     */
    private function getTranslationByLocaleKey($key)
    {
        if ($this->exists) {
            foreach ($this->translations as $translation) {
                if ($translation->getAttribute($this->getLocaleKey()) == $key) {
                    return $translation;
                }
            }
        } else {
            if (!isset($this->tmp_translations[$key])) {
                $this->tmp_translations[$key] = $this->getNewTranslation($key);
            }
            return $this->tmp_translations[$key];
        }
        return null;
    }

    /**
     * @param null $locale
     *
     * @return string
     */
    private function getFallbackLocale($locale = null)
    {
        if ($locale && $this->isLocaleCountryBased($locale)) {
            if ($fallback = $this->getLanguageFromCountryBasedLocale($locale)) {
                return $fallback;
            }
        }
        return app()->make('config')->get('translator.fallback_locale');
    }

    /**
     * @param $locale
     *
     * @return bool
     */
    private function isLocaleCountryBased($locale)
    {
        return strpos($locale, $this->getLocaleSeparator()) !== false;
    }

    /**
     * @param $locale
     *
     * @return string
     */
    private function getLanguageFromCountryBasedLocale($locale)
    {
        $parts = explode($this->getLocaleSeparator(), $locale);
        return array_get($parts, 0);
    }

    /**
     * @return bool|null
     */
    private function useFallback()
    {
        if (isset($this->useTranslationFallback) && $this->useTranslationFallback !== null) {
            return $this->useTranslationFallback;
        }
        return app()->make('config')->get('translator.use_fallback');
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function isTranslationAttribute($key)
    {
        return in_array($key, $this->translatedAttributes);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    protected function isKeyALocale($key)
    {
        $locales = $this->getLocales();
        return in_array($key, $locales);
    }

    /**
     * @return array
     */
    protected function getLocales()
    {
        $localesConfig = (array)app()->make('config')->get('translator.locales');
        if (empty($localesConfig)) {
            throw new \ErrorException('Empty config translator.locales');
        }
        $locales = [];
        foreach ($localesConfig as $key => $locale) {
            if (is_array($locale)) {
                $locales[] = $key;
                foreach ($locale as $countryLocale) {
                    $locales[] = $key . $this->getLocaleSeparator() . $countryLocale;
                }
            } else {
                $locales[] = $locale;
            }
        }
        return $locales;
    }

    /**
     * @return string
     */
    protected function getLocaleSeparator()
    {
        return app()->make('config')->get('translator.locale_separator', '-');
    }

    /**
     * @return bool
     */
    protected function saveTranslations()
    {
        $saved = true;
        foreach ($this->translations as $translation) {
            if ($saved && $this->isTranslationDirty($translation)) {
                $translation->setAttribute($this->getRelationKey(), $this->getKey());
                $saved = $translation->save();
            }
        }
        return $saved;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $translation
     *
     * @return bool
     */
    protected function isTranslationDirty(Model $translation)
    {
        $dirtyAttributes = $translation->getDirty();
        unset($dirtyAttributes[$this->getLocaleKey()]);
        return count($dirtyAttributes) > 0;
    }

    /**
     * @param string $locale
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getNewTranslation($locale)
    {
        $modelName   = $this->getTranslationModelName();
        $translation = new $modelName();
        $translation->setAttribute($this->getLocaleKey(), $locale);
        $this->translations->add($translation);
        return $translation;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function __isset($key)
    {
        return $this->isTranslationAttribute($key) || parent::__isset($key);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeTranslatedIn(Builder $query, $locale = null)
    {
        $locale = $locale ?: $this->currentLocale();
        return $query->whereHas('translations', function (Builder $q) use ($locale) {
            $q->where($this->getLocaleKey(), '=', $locale);
        });
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeNotTranslatedIn(Builder $query, $locale = null)
    {
        $locale = $locale ?: $this->currentLocale();
        return $query->whereDoesntHave('translations', function (Builder $q) use ($locale) {
            $q->where($this->getLocaleKey(), '=', $locale);
        });
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeTranslated(Builder $query)
    {
        return $query->has('translations');
    }

    /**
     * Adds scope to get a list of translated attributes, using the current locale.
     * Example usage: Country::listsTranslations('name')->get()->toArray()
     * Will return an array with items:
     *  [
     *      'id' => '1',                // The id of country
     *      'name' => 'Griechenland'    // The translated name
     *  ].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $translationField
     */
    public function scopeListsTranslations(Builder $query, $translationField)
    {
        $withFallback     = $this->useFallback();
        $translationTable = $this->getTranslationsTable();
        $localeKey        = $this->getLocaleKey();
        $query
            ->select($this->getTable() . '.' . $this->getKeyName(), $translationTable . '.' . $translationField)
            ->leftJoin($translationTable, $translationTable . '.' . $this->getRelationKey(), '=', $this->getTable() . '.' . $this->getKeyName())
            ->where($translationTable . '.' . $localeKey, $this->currentLocale());
        if ($withFallback) {
            $query->orWhere(function (Builder $q) use ($translationTable, $localeKey) {
                $q->where($translationTable . '.' . $localeKey, $this->getFallbackLocale())
                    ->whereNotIn($translationTable . '.' . $this->getRelationKey(), function (QueryBuilder $q) use (
                        $translationTable,
                        $localeKey
                    ) {
                        $q->select($translationTable . '.' . $this->getRelationKey())
                            ->from($translationTable)
                            ->where($translationTable . '.' . $localeKey, $this->currentLocale());
                    });
            });
        }
    }

    /**
     * This scope eager loads the translations for the default and the fallback locale only.
     * We can use this as a shortcut to improve performance in our application.
     *
     * @param Builder $query
     */
    public function scopeWithTranslation(Builder $query)
    {
        $query->with([
            'translations' => function (Relation $query) {
                $query->where($this->getTranslationsTable() . '.' . $this->getLocaleKey(), $this->currentLocale());
                if ($this->useFallback()) {
                    return $query->orWhere($this->getTranslationsTable() . '.' . $this->getLocaleKey(), $this->getFallbackLocale());
                }
            },
        ]);
    }

    /**
     * This scope filters results by checking the translation fields.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $value
     * @param string                                $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeWhereTranslation(Builder $query, $key, $value, $locale = null)
    {
        return $query->whereHas('translations', function (Builder $query) use ($key, $value, $locale) {
            $query->where($this->getTranslationsTable() . '.' . $key, $value);
            if ($locale) {
                $query->where($this->getTranslationsTable() . '.' . $this->getLocaleKey(), $locale);
            }
        });
    }

    /**
     * This scope filters results by checking the translation fields.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $value
     * @param string                                $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeWhereTranslationLike(Builder $query, $key, $value, $locale = null)
    {
        return $query->whereHas('translations', function (Builder $query) use ($key, $value, $locale) {
            $query->where($this->getTranslationsTable() . '.' . $key, 'LIKE', $value);
            if ($locale) {
                $query->where($this->getTranslationsTable() . '.' . $this->getLocaleKey(), 'LIKE', $locale);
            }
        });
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        if ($this->relationLoaded('translations') || $this->toArrayAlwaysLoadsTranslations()) {
            // continue
        } else {
            return $attributes;
        }
        $hiddenAttributes = $this->getHidden();
        foreach ($this->translatedAttributes as $field) {
            if (in_array($field, $hiddenAttributes)) {
                continue;
            }
            if ($translations = $this->getTranslation()) {
                $attributes[$field] = $translations->$field;
            }
        }
        return $attributes;
    }

    /**
     * @return string
     */
    private function getTranslationsTable()
    {
        return app()->make($this->getTranslationModelName())->getTable();
    }

    /**
     * @return string
     */
    protected function currentLocale()
    {
        if ($this->defaultLocale) {
            return $this->defaultLocale;
        }
//        if (($locale = parent::getAttribute('locale')) && $this->isKeyALocale($locale)) {
//            return $locale;
//        }
        return app()->make('config')->get('translator.locale')
            ?: app()->make('translator')->getLocale();
    }

    /**
     * Set the default locale on the model.
     *
     * @param $locale
     *
     * @return $this
     */
    public function setDefaultLocale($locale)
    {
        $this->defaultLocale = $locale;
        return $this;
    }

    /**
     * Get the default locale on the model.
     *
     * @return mixed
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * Deletes all translations for this model.
     *
     * @param string|array|null $locales The locales to be deleted (array or single string)
     *                                   (e.g., ["en", "de"] would remove these translations).
     */
    public function deleteTranslations($locales = null)
    {
        if ($locales === null) {
            $this->translations()->delete();
        } else {
            $locales = (array)$locales;
            $this->translations()->whereIn($this->getLocaleKey(), $locales)->delete();
        }
        // we need to manually "reload" the collection built from the relationship
        // otherwise $this->translations()->get() would NOT be the same as $this->translations
        $this->load('translations');
    }

    /**
     * @param $key
     *
     * @return array
     */
    private function getAttributeAndLocale($key)
    {
        if (str_contains($key, ':')) {
            return explode(':', $key);
        }
        return [$key, $this->currentLocale()];
    }

    /**
     * @return bool
     */
    private function toArrayAlwaysLoadsTranslations()
    {
        return app()->make('config')->get('translator.to_array_always_loads_translations', true);
    }
}
