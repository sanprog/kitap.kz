<?php

namespace Waavi\Translation\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class ImportToFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translator:to_files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Переносит из базы в файлы переводы';
    /**
     * @var \Illuminate\Filesystem\FilesystemAdapter
     */
    protected $disk;
    /**
     * @var \Illuminate\Filesystem\FilesystemAdapter
     */
    protected $diskUploads;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //TODO перенос их базы в файл переводов
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->disk = \Storage::createLocalDriver([
            'root' => storage_path('translations'),
        ]);
        $result     = [];
        $items      = DB::table('translator_translations')->get();
        foreach ($items as $item) {
            array_set($result, $item->locale . '.' . $item->group . '.' . $item->item, $item->text);
        }
        foreach ($result as $locale => $files) {
            foreach ($files as $file => $values) {
                $this->disk->put($locale.'/' . $file . '.php', '<?php return ' . $this->var_export_short($values, true) . ';');
            }
        }
    }
    private function var_export_short($data, $return=true)
    {
        $dump = var_export($data, true);

        $dump = preg_replace('#(?:\A|\n)([ ]*)array \(#i', '[', $dump); // Starts
        $dump = preg_replace('#\n([ ]*)\),#', "\n$1],", $dump); // Ends
        $dump = preg_replace('#=> \[\n\s+\],\n#', "=> [],\n", $dump); // Empties

        if (gettype($data) == 'object') { // Deal with object states
            $dump = str_replace('__set_state(array(', '__set_state([', $dump);
            $dump = preg_replace('#\)\)$#', "])", $dump);
        } else {
            $dump = preg_replace('#\)$#', "]", $dump);
        }

        if ($return===true) {
            return $dump;
        } else {
            echo $dump;
        }
    }
}
