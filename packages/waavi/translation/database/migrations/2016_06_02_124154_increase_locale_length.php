<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class IncreaseLocaleLength extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('translator_languages')) {
            Schema::table('translator_languages', function (Blueprint $table) {
                $table->string('locale', 10)->change();
            });
            Schema::table('translator_translations', function (Blueprint $table) {
                $table->string('locale', 10)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
