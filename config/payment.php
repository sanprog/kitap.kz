<?php
return [
    'epay' => [
        'log'          => true,
        'status'       => env('EPAY_STATUS', 'local'),
        //4405-6450-0000-6150    09-2025     653
        //5483185000000293    09-2025     343
        //377514500009951     09-2025     3446
        'local'          => [
            'merchant_id'             => '92061101',
            'merchant_name'           => 'Test shop',
            'merchant_certificate_id' => '00C182B189',
            'private_key'             => __DIR__ . '/payment/test.private',
            'private_key_pass'        => 'nissan',
            'bank_certificate'        => __DIR__ . '/payment/kkbca.cert',
            'process_link'            => 'https://testpay.kkb.kz/jsp/process/logon.jsp',
            'complete_link'           => 'https://testpay.kkb.kz/jsp/remote/control.jsp',
        ],
        'dev'          => [
            'merchant_id'             => '92061103',
            'merchant_name'           => 'Demo Shop 3',
            'merchant_certificate_id' => '00c183d70b',
            'private_key'             => __DIR__ . '/payment/dev/test.prv',
            'private_key_pass'        => '1q2w3e4r',
            'bank_certificate'        => __DIR__ . '/payment/dev/kkbca_test.pub',
            'process_link'            => 'https://testpay.kkb.kz/jsp/process/logon.jsp',
            'complete_link'           => 'https://testpay.kkb.kz/jsp/remote/control.jsp',
        ],
        'prod'         => [

        ],
        'alias_locale' => [
            'en' => 'eng',
            'kz' => 'kaz',
            'ru' => 'rus',
        ]
    ],
];
