<?php
return [
    'lang_id'              => 1,
    'langs'                => [
        1 => [
            'code' => 'ru',
            'url'  => 'rus'
        ],
        2 => [
            'code' => 'kz',
            'url'  => 'kaz'
        ]
    ],
    'active_session'       => [
        'mobile'  => 1,
        'android' => 1,
        'ios'     => 1,
        'desktop' => 1
    ],
    'project_id'           => 3,
    'password_encrypt_key' => 'CORE',
    'project_tag'          => 'kitap',
    'rest'                 => [
        'rms_host' => env('BMG_REST_RMS_HOST', 'http://rms.local'),
    ],
    'upload-site'          => 'kitap.kz',
];
