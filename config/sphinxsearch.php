<?php
return array(
    'host'    => '127.0.0.1',
    'port'    => 9312,
    'timeout' => 30,
    'indexes' => array(
        'searcher' => false,
        'book'     => false,
        'video'    => false,
        'album'    => false,
        'author'   => false,
        'dictor'   => false
    )
);
