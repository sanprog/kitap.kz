const gulp = require('gulp');

gulp.task('music', function() {
  return gulp.src('./frontend/music/**/*')
    .pipe(gulp.dest('./build/music'))
});
