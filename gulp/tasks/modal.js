const gulp = require('gulp');
const sass = require('gulp-sass');
const combiner = require('stream-combiner2').obj; // вызывает потоки поочередно, имеет один общий обработчик ошибок для всех потоков
const debug = require('gulp-debug');
const notify = require('gulp-notify');
const gulpif = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const isDev = require('../utils/isDev');
const csso = require('gulp-csso');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');

gulp.task('modal', function() {
  return combiner(
        gulp.src('./frontend/styles/modal.scss'),
        gulpif(isDev, sourcemaps.init()),
        sass(),
        debug({ title: 'modal' }),
        postcss([autoprefixer()]),
        gulpif(isDev, sourcemaps.write()),
        gulpif(!isDev, csso()),
        gulp.dest('./build/styles/'),
        gulp.dest('./public/css/')
    ).on('error', notify.onError(
        notify.onError(function(err) {
          return {
            title: 'modal',
            message: err.message,
          };
        })
    ));
});
