const gulp = require('gulp');

gulp.task('video', function() {
  return gulp.src('./frontend/video/**/*')
    .pipe(gulp.dest('./build/video'))
});
