const gulp = require('gulp');
const debug = require('gulp-debug');
const gulpif = require('gulp-if');
const imagemin = require('gulp-imagemin');
const isDev = require('../utils/isDev');

gulp.task('email', function() {
  return gulp.src('./frontend/images/email/*.{png,jpg,svg,gif,ico}')
      .pipe(gulpif(!isDev(),imagemin({})))
      .pipe(debug({
        title: 'email:imagemin'
      }))
      .pipe(gulp.dest('./build/images/email'))
      .pipe(gulp.dest('./public/images/email'));
});
