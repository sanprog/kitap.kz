const gulp = require('gulp');

gulp.task('font', function() {
  return gulp.src('./frontend/fonts/**/*')
    .pipe(gulp.dest('./build/fonts'))
	.pipe(gulp.dest('./public/fonts'));
});
